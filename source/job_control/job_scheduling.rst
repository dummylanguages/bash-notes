Scheduling jobs
===============

The at command
--------------
On Unix-like operating systems, ``at`` is used to create what is known as an "at-job" which is carried out at a specified later date. The ``at`` command can read a series of commands from standard input and also can read a list of jobs from a computer file.

cron
----

`cron`_ and the `at command`_

.. _`job control`: https://en.wikipedia.org/wiki/Job_control_(Unix)
.. _`at command`: https://en.wikipedia.org/wiki/At_(command)
.. _`cron`: https://en.wikipedia.org/wiki/Cron