###########
Job control
###########
.. toctree::
	:maxdepth: 3

	some_definitions
	job_control
	job_scheduling

Job vs process
==============
Whenever we need some work to be done in the computer, it's useful to divide its execution in smaller units, each of one is known as a `job`_. Sometimes the job itself have to be done by several smaller units known as **tasks**. 

..note:: When a job has tasks that have to run **sequentially** it's known as a **job stream**

Sometimes when we execute a job, a **single process** is enough to get the work done. Other times, a job may be comprised of several smaller tasks, in that case we have a **parent process** (the one corresponding to the job) and several **child processes** for each of the smaller tasks.

To control a job and the process or group of processes implied in its execution there are tools that fall into the category of `job control(Unix)`_.
Check also `job control(computing)`_




.. _`job`: https://en.wikipedia.org/wiki/Job_(computing)
.. _`job control(Unix)`: https://en.wikipedia.org/wiki/Job_control_(Unix)
.. _`job control(computing)`: https://en.wikipedia.org/wiki/Job_control_(computing)
