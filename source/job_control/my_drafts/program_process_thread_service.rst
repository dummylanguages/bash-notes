

Here are the definitions and the explanations from Wikipedia.

Program:
    A computer program, or just a program, is a sequence of instructions, written to perform a specified task with a computer.

    http://en.wikipedia.org/wiki/Computer_program

Process:

    A process is an instance of a computer program that is being executed. It contains the program code and its current activity. Depending on the operating system (OS), a process may be made up of multiple threads of execution that execute instructions concurrently.

    A computer program is a passive collection of instructions; a process is the actual execution of those instructions. Several processes may be associated with the same program; for example, opening up several instances of the same program often means more than one process is being executed.

    http://en.wikipedia.org/wiki/Process_(computing)

    As you can see, you can consider a program is just a sequence of instruction stored in the secondary memory. It is not necessary to be loaded to RAM and not necessary to be executed.

    However, for a process, it is a piece of program that is loaded, being executing and running in the system. Every instructions would probably be executed and the values of the declared variables would be charged accordingly.

Thread:
    The implementation of threads and processes differs between operating systems, but in most cases a thread is a component of a process.

    http://en.wikipedia.org/wiki/Thread_(computing)

    Thus, you may find several threads in the same process. Each thread has its own stack. In a process, or a program, there is only 1 thread by default but one may normally create multiple threads by “pthread” library, #include in C/C++. 

Service:
A service is a program/process which **responds to requests from other programs** over some inter-process communication mechanism (usually over a network)
A service is what a server provides.
A service doesn't have to be a background process(daemon), but usually is. In Windows, daemons are called services.

.. note:: The word daemon comes from the Unix culture to denote processes that run in the background. In Windows, daemons are called services, because **usually** services run in the background, but it's not always the case.

For example, the **X Window service**, doesn't run in the background: it takes over your screen, keyboard and pointing device. It is a service because it responds to requests from other applications (to create and manipulate windows, etc), which can even be elsewhere on the network. The X service is **interactive**, it responds to your every keystroke and mouse movement. Daemons are not interactive.

.. note:: A daemon is a background, **non-interactive** running program. It is detached from the keyboard and display of any interactive user.

Also according to wikipedia a `daemon`_ is a computer program that runs as a **background process**, rather than being under the direct control of an interactive user. When running, daemons are easily recognizable because their names end with the letter **d**. Traditionally it has been like that to differentiate them from normal computer program.

.. _daemon: https://en.wikipedia.org/wiki/Daemon_(computing)
