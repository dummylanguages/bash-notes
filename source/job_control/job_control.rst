Job control
===========
When we log in in a terminal (or terminal emulator) a user will initially only have a single process running, their login shell. 

A job running in the **foreground** can be stopped by typing the suspend character (Ctrl-Z). This sends the "terminal stop" signal (SIGTSTP) to the process group. 

A **stopped job** can be resumed as a **background job** with the ``bg`` builtin, or as the **foreground job** with ``fg``. In either case, the shell redirects I/O appropriately, and sends the ``SIGCONT`` signal to the process, which causes the operating system to resume its execution.

A job running in the foreground can be interrupted by typing the interruption character (Ctrl-C). This sends the "interrupt" signal (SIGINT), which defaults to terminating the process, though it can be overridden. 

