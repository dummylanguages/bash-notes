.. _quoting-section:

*******
Quoting
*******

.. toctree::
	:maxdepth: 3
	:hidden:

	escape_character
	single_quotes
	double_quotes

There are three `quoting`_ mechanisms: 

	1. The `escape character`_
	2. `Single quotes`_, sometimes known as strong quoting.
	3. `Double quotes`_, sometimes known as weak quoting.

The three of them are used to escape the special meaning of some characters in Bash, so they are interpreted as literal characters, without any special meaning.

.. note:: In the book `Learning the Bash shell`_ the terms **weak quoting** and **strong quoting** are used to refer to the use of **double** and **single** quotes respectively.

.. _`quoting`: https://www.gnu.org/software/bash/manual/html_node/Quoting.html#Quoting
.. _`escape character`: https://www.gnu.org/software/bash/manual/html_node/Escape-Character.html#Escape-Character
.. _`Single quotes`: https://www.gnu.org/software/bash/manual/html_node/Single-Quotes.html
.. _`Double quotes`: https://www.gnu.org/software/bash/manual/html_node/Double-Quotes.html
.. _`Learning the Bash shell`: http://shop.oreilly.com/product/9780596009656.do
