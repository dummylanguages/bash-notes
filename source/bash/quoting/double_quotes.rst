Double quotes
=============
Enclosing characters in `double quotes`_ will also escape its special meaning. For example, in normal circumstances Bash performs `tilde expansion`_ when it founds the ``~`` character::

    $ echo ~
    /home/bob

But if we place it inside double quotes::

    $ echo "~"
    ~

It's treated like the literal ``~`` character, without expanding it to the name of the user's home directory.

Exceptions
----------
There are a few characters that keep its special meaning even when they are surrounded by double quotes:

* The ``$`` sign will keep performing `parameter expansion`_ and `command substitution`_.
* The backticks ````` used for `command substitution`_.
* The ``\`` will still work as the `escape character`_.
* The ``!`` character, only when history expansion is enabled.



.. note:: In the book `Learning the Bash shell`_ the term **weak quoting** is used to refer to the use of **double quotes** to escape the special meaning of characters.

.. _`escape character`: https://www.gnu.org/software/bash/manual/html_node/Escape-Character.html#Escape-Character
.. _`single quotes`: https://www.gnu.org/software/bash/manual/html_node/Single-Quotes.html#Single-Quotes
.. _`double quotes`: https://www.gnu.org/software/bash/manual/html_node/Single-Quotes.html#Double-Quotes

.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html#Shell-Parameter-Expansion
.. _`command substitution`: https://www.gnu.org/software/bash/manual/html_node/Command-Substitution.html#Command-Substitution
.. _`tilde expansion`: https://www.gnu.org/software/bash/manual/html_node/Tilde-Expansion.html#Tilde-Expansion
.. _`Learning the Bash shell`: http://shop.oreilly.com/product/9780596009656.do
