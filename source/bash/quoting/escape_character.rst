The escape character
====================
The **backslash character** (``\``) is known in Bash as the `escape character`_ and it's used for two things:

Line continuation
-----------------
As a **line continuation**. For example::

    $ echo "hello \
    > world"
    hello world

Here we started typing a string and use the backslash character ``\`` to continue writing on the next line. This use of the ``\`` is quite helpful when writing long lines of commands. It even works in the middle of a command, in this case ``pwd``::

    $ p\
    > wd
    /Users/bob

It prints the working directory as if we had typed ``pwd`` altogether.

What the ``\`` is doing here is escaping the `newline character`_ that we introduce each time we press the **Enter** key in our command line. Even though *visually* we'll move to a new line to continue typing, the newline character is escaped and ignored by Bash.

To escape especial characters
-----------------------------
It can be used to escape the meaning of **special characters**. For example::

    $ echo $BASH
    /usr/local/bin/bash

Here we use the ``$`` which is a special character to perform `parameter expansion`_ and get the value stored in the ``BASH`` environment variable.

But if what we want is to print the string ``$BASH`` we can escape the ``$`` preceding it with a ``\``::

    $ echo \$BASH
    $BASH

Using the ``\`` we took away the special meaning of ``$`` which is treated as any other character.

Another example::

    $ cd \~
    bash: cd: ~: No such file or directory

.. _`escape character`: https://www.gnu.org/software/bash/manual/html_node/Escape-Character.html#Escape-Character
.. _`newline character`: https://en.wikipedia.org/wiki/Newline
.. _`single quotes`: https://www.gnu.org/software/bash/manual/html_node/Single-Quotes.html#Single-Quotes
.. _`double quotes`: https://www.gnu.org/software/bash/manual/html_node/Single-Quotes.html#Double-Quotes

.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html#Shell-Parameter-Expansion
.. _`tilde expansion`: https://www.gnu.org/software/bash/manual/html_node/Tilde-Expansion.html#Tilde-Expansion
