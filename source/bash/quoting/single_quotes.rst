Single quotes
=============
Any special character placed between single quotes (``'``) is striped of its special meaning. For example::

    $ echo '$BASH'
    $BASH


.. note:: In the book `Learning the Bash shell`_ the term **strong quoting** is used to refer to the use of **single quotes** to escape the special meaning of characters.


.. _`escape character`: https://www.gnu.org/software/bash/manual/html_node/Escape-Character.html#Escape-Character
.. _`single quotes`: https://www.gnu.org/software/bash/manual/html_node/Single-Quotes.html#Single-Quotes
.. _`double quotes`: https://www.gnu.org/software/bash/manual/html_node/Single-Quotes.html#Double-Quotes

.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html#Shell-Parameter-Expansion
.. _`tilde expansion`: https://www.gnu.org/software/bash/manual/html_node/Tilde-Expansion.html#Tilde-Expansion
.. _`Learning the Bash shell`: http://shop.oreilly.com/product/9780596009656.do
