**************************
The Command line interface
**************************
Some people also refer to the shell as the `command line interface`_. This is not totally inacurate, but in truth, the command line interface shouldn't be reduced to just the `command line interpreter`_.

.. note:: For example, **Bash** is a **Command Line Interpreter** but it does not provide just by itself a **Command Line Interface**.

The Command line interface is made up of several different parts that work together to provide the user with a powerful text interface. Some of these parts are:

* The **Command line interpreter** is an essential component in charge of interpreting the commands. But it shouldn't be confused with the commands themselves. Apart from a few builtins that each shell includes, most of the commands we use are not part of the interpreter. For example, the `ls`_ command is not part of let's say `Bash`_, the latter one just interprets it.

* The **commands** themselves, which vary according to the operating system. They usually come grouped in packages such as:

    * The `GNU Core Utilities`_ (aka ``coreutils``), is a package of GNU software containing reimplementations for many of the basic tools, such as ``cat``, ``ls``, and ``rm``, which are used on Unix-like operating systems. 
    * `util-linux`_ is another standard package distributed by the Linux Kernel Organization as part of the Linux operating system.
    * `The Heirloom Toolchest`_ is a collection of standard Unix utilities which aims to provide versions of the utilities whose behavior mimics that of the classic versions.
    * `Busybox`_ a software suite that provides several Unix utilities in a **single executable file**.

* There are some **essential libraries** that are not part of the interpreter, but they are essential for a functional **Command Line Interface**. For example the `GNU Readline`_ library is the one that provides the editing features of the command line. It is not part of the interpreter, and without it, using the command line would be impossible.

To sum it up, the Command Line Interface is the system that provides a text interface to a computer, and it's made up of several parts:

* A Command Line Interpreter.
* The commands themselves.
* Other libraries.

Confusing the whole with one of the parts it's a mistake.

.. _`command line interface`: https://en.m.wikipedia.org/wiki/Command-line_interface
.. _`command line interpreter`: https://en.m.wikipedia.org/wiki/Command-line_interface#Command-line_interpreter
.. _`ls`: https://en.wikipedia.org/wiki/Ls
.. _`Bash`: https://www.gnu.org/software/bash/
.. _`GNU Core Utilities`: https://en.wikipedia.org/wiki/GNU_Core_Utilities
.. _`util-linux`: https://en.wikipedia.org/wiki/Util-linux
.. _`The Heirloom Toolchest`: http://heirloom.sourceforge.net/tools.html
.. _`Busybox`: https://www.busybox.net/
.. _`GNU Readline`: https://en.wikipedia.org/wiki/GNU_Readline
