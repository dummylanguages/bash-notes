############
Introduction
############

.. toctree::
	:maxdepth: 3
	:hidden:

	01_shell
	02_command_line_interface
	03_terminals_and_consoles
	04_history

People use interchangeably a series of words when they want to refer to that black screen in the computer where they type `commands`_ and shit happens:

* A shell.
* The terminal.
* The console.
* The command line interface
* Bash, Zsh...

Truth is, technically speaking almost all of these are really different things with something in common: they offer a `text based interface`_ to interact with computers.

.. figure:: /_images/bash-logo.png
   :scale: 35%
   :align: center

The name `Bash`_ stands for **Bourne Again Shell**, and as the name implies, it's a `shell`_ widely used in most Linux distributions and other Unix systems.

.. note:: A **shell** is just another name for a `command line interpreter`_.

Other well known UNIX shells are:

* The `Bourne shell`_
* The `Z shell`_
* The `C shell`_ or the improved version `Tenex C shell`_
* The `Korn shell`_
* The modern `Fish shell`_, non-compliant with POSIX.
* And many others.

Regarding Bash, these online **manuals** and **guides** are interesting sources of information:

* The `Bash Guide for Beginners`_
* The `Bash programming - Intro howto`_
* The `Advanced Bash-Scripting Guide`_
* The `Bash hackers wiki`_
* The `Bash prompt howto`_

Specially useful:

* The `official manual`_
* The `BashGuide`_

In this section we'll try to shed some light about the differences between all these terms.

.. _`text based interface`: https://en.wikipedia.org/wiki/Text-based_user_interface
.. _`commands`: https://en.m.wikipedia.org/wiki/Command_(computing)
.. _`Bash`: https://en.m.wikipedia.org/wiki/Bash_(Unix_shell)
.. _`shell`: https://en.m.wikipedia.org/wiki/Shell_(computing)
.. _`command line interpreter`: https://en.m.wikipedia.org/wiki/Command-line_interface#Command-line_interpreter
.. _`Bourne shell`: https://en.wikipedia.org/wiki/Bourne_shell
.. _`Z shell`: https://en.wikipedia.org/wiki/Z_shell
.. _`C shell`: https://en.wikipedia.org/wiki/C_shell
.. _`Tenex C shell`: https://en.wikipedia.org/wiki/Tcsh
.. _`Korn shell`: http://www.kornshell.org/
.. _`Fish shell`: http://fishshell.com/

.. _`Bash Guide for Beginners`: http://www.tldp.org/LDP/Bash-Beginners-Guide/html/index.html
.. _`Bash programming - Intro howto`: http://www.tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html
.. _`Advanced Bash-Scripting Guide`: http://www.tldp.org/LDP/abs/html/index.html
.. _`Bash hackers wiki`: https://wiki.bash-hackers.org/start
.. _`Bash prompt howto`: http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/index.html

.. _`official manual`: https://www.gnu.org/software/bash/manual/html_node/index.html
.. _`BashGuide`: http://mywiki.wooledge.org/BashGuide
