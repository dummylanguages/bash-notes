*******************************
The real terminals and consoles
*******************************
Before, we went over what is a terminal emulator or a virtual console. But why are they called terminal *emulators*, or why *virtual* consoles instead of just terminals, or consoles?

Well, there was a time when terminals were things, in the sense of real pieces of **hardware** which you could read from, type in, and not just a software emulation. Next we'll review a bit of the historic evolution of terminals.

A bit of history
================
Back in the day, during **the mainframe era**, (from the late 1950s to the mid 1990s, when PCs became popular) computers were room-sized monsters owned by corporations and universities. Sometimes, they were available to be used by a group of users according to time-sharing systems.

These machines used to by controlled by a `computer terminal`_, and in multiuser systems several of them. A **terminal** was just a hardware device used to **enter** and **display** data, into and from a computer. Let's talk a bit about their really interesting evolution.

Typewriters, teleprinters and consoles
--------------------------------------
At the beginning, people interacted with computers using **physical consoles** and **electric typewriters**. For example, in 1959 the `IBM 1620`_ was a small mainframe, marketed as an inexpensive "scientific computer". 

.. figure:: /_images/bash/IBM_1620.jpg
	:scale: 50%

	IBM 1620 - Scientific computer

As you can see, it was controlled by and upper and lower **physical console** and a modified `IBM Electric typewriter`_ model B1, which you can see in the image below.

.. figure:: /_images/bash/IBM_Model_B1.png
	:scale: 50%

	IBM Model B1 - Electric typewriter

Apart from electric typewriters, `teleprinters`_ were also a popular way of interacting with computers. These were machines initially used in *telegraphy*, which were adapted to provide a **user interface** to early mainframe computers and minicomputers, sending typed data to the computer and printing the response on paper (hardcopy).

.. figure:: /_images/bash/model-33-teletype.jpg
	:scale: 70%

	Teletype model 33 ASR

The **Model 33** was widely used with early `minicomputers`_. In the name of its manufacturer, the `Teletype Corporation`_, we find the origin of another word that is still in use today, at least in the `hacker slang`_: **TTY**, short for TeleTYpe, it's used to design any type of terminal.

.. note:: Both typewriters and teleprinters were the early versions of the computer terminal.

Glass TTYs and dumb terminals
-----------------------------
In the late 1960s, **Visual Display Units** were introduced. These were devices manufactured specifically to serve as a computer terminal. In 1969, the `Computer Terminal Corporation`_ introduced the `Datapoint 3300`_, one of the first computer terminals with a glass screen, hence they were popularly known as **glass TTYs**.

.. figure:: /_images/bash/datapoint_3300.jpg
	:scale: 70%

	DataPoint 3300 (a glass TTY)

This terminal derived its name from the existing competitor Teletype Model 33, and went beyond what a Teletype could achieve with its paper output thanks to the support of control codes for:

* Move the **cursor** up, down, left and right. To the top left of the screen. To the start of the bottom line.
* It could also **clear** to the end of the current line, or clear to the end of the screen.

Among the cons of this terminal:

* It did not support direct cursor positioning.
* It only had **25 rows** of **72 columns** of upper-case characters, instead of the **80 x 24** that would become more common in subsequent years.
* It did not use a microprocessor in its implementation, but instead used TTL transistors in small-scale integrated circuits.

Another terminal worth mentioning is the **ADM-3** video display terminal introduced in 1975 by `Lear Siegler`_, and literally advertised as the $995 **dumb terminal**!

.. figure:: /_images/bash/dumb-terminal.png
	:scale: 50%

	ADM-3, The $995 dumb terminal.

This terminal quickly became a commercial success among other things, because the wave of new minicomputer systems that was hitting the market from dozens of manufacturers. These minis paired with an inexpensive operator console was a match made in heaven. 

Some of the characteristics of this terminal:

* Only displayed **capital letters**.
* **12 rows** of **80 characters**.

In 1976 an update to this terminal was introduced with the `ADM-3A`_

.. figure:: /_images/bash/terminal_ADM-3A.jpg
	:figwidth: 70 %

	Lear Siegler ADM-3A

The new model supported:

* An option to display both lower and upper case characters.
* **24 rows** or **80 characters**.
* Added support for **control codes** to move the cursor around on the screen, and directly position the cursor at any point in the display. It did not, however, support **clear to end of line** or **clear to end of screen**, or other more advanced codes that appeared in later terminals.
* It was implemented using TTL transistors, not a microprocessor.

It's worth mentioning that the `vi`_ text editor was written by `Bill Joy`_ using a **Lear-Siegler ADM-3A**. Since this terminal did not have dedicated arrow keys on the keyboard, the movement of the cursor around the screen was achieved using the `HJKL`_ keys since they had printed arrow labels. Also, the control key was located above, not below, the shift key—in the same place where most modern PC keyboards put the Caps Lock key.

Even though the **ADM-3/3A** was the first **dumb terminal**, its nickname was used to designate a category of terminals, which had in common some characteristics:

* Adoption of the `ASCII`_ character encoding.
* Use of the `RS-232`_ serial port.
* **80x24** screens (24 lines of 80 characters).
* Implementation of at least 3 **control codes**: 
    
    1. Carriage Return (Ctrl-M)
    2. Line-Feed (Ctrl-J)
    3. Bell (Ctrl-G)

The great variations in the control codes between makers gave rise to software that identified and grouped terminal types so the system software would correctly display input forms using the appropriate control codes; In Unix-like systems the ``termcap`` or ``terminfo`` files, the ``stty`` utility, and the ``TERM`` **environment variable** would be used.

Intelligent terminals
---------------------
The thing that classed a terminal as **smart** was their ability of processing the user input within the terminal, and send a block of data at a time. For example, when the user has finished editing a whole field or form, the terminal sends only the completed form. This is known as a `block oriented terminal`_, because they communicate with its host (mainframe or minicomputer) in blocks of data. Block-oriented terminals typically incorporate a buffer which stores one screen or more of data, and also a microprocessor.

As opposed to this, **dumb terminals** would require a full screen-full of characters to be re-sent to the computer. For this reason dumb terminals are considered to be `character oriented`_, they communicate with the host one character at a time. 

There were lots of smart terminal models, maybe we can mention here the `DEC VT100`_ introduced in 1978, since it was one of the first terminals to support `ANSI escape codes`_ for cursor control and other tasks. This led to rapid uptake of the ANSI standard, becoming the de facto standard. The VT100 used the new `Intel 8080`_ as its internal processor.

.. figure:: /_images/bash/DEC_VT100.png
	:figwidth: 50 %

	DEC VT100

The introduction of microprocessors simplified and reduce the electronics required in a terminal. It also allowed terminal emulation, so a terminal model could emulate several other popular terminals. We are talking about **physical terminal emulators**, nowadays this is achieve with software.


.. _`computer terminal`: https://en.wikipedia.org/wiki/Computer_terminal

.. _`minicomputers`: https://en.wikipedia.org/wiki/Minicomputer
.. _`Teletype Corporation`: https://en.wikipedia.org/wiki/Teletype_Corporation
.. _`hacker slang`: http://catb.org/jargon/html/T/tty.html
.. _`IBM 1620`: https://en.wikipedia.org/wiki/IBM_1620
.. _`IBM electric typewriter`: https://en.wikipedia.org/wiki/IBM_Electric_typewriter
.. _`teleprinters`: https://en.wikipedia.org/wiki/Teleprinter
.. _`Computer Terminal Corporation`: https://en.wikipedia.org/wiki/Datapoint
.. _`Datapoint 3300`: https://en.wikipedia.org/wiki/Datapoint_3300
.. _`ADM-3A`: https://en.wikipedia.org
.. _`Lear Siegler`: https://en.wikipedia.org
.. _`vi`: https://en.wikipedia.org/wiki/Vi
.. _`Bill Joy`: https://en.wikipedia.org/wiki/Bill_Joy
.. _`HJKL`: https://en.wikipedia.org/wiki/Arrow_keys#HJKL_keys
.. _`ASCII`: https://en.wikipedia.org/wiki/ASCII
.. _`RS-232`: https://en.wikipedia.org/wiki/RS-232
.. _`block oriented terminal`: https://en.wikipedia.org/wiki/Computer_terminal#block-oriented_terminal
.. _`character oriented`: https://en.wikipedia.org/wiki/Computer_terminal#character-oriented_terminal
.. _`DEC VT100`: https://en.wikipedia.org/wiki/VT100
.. _`ANSI escape codes`: https://en.wikipedia.org/wiki/ANSI_escape_code
.. _`Intel 8080`: https://en.wikipedia.org/wiki/Intel_8080