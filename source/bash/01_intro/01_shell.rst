********************
What is a/the shell?
********************
In computing, the term `shell`_ may refer to:

* One of the layers of the operating system.
* A `command line interpreter`_.

The shell layer
===============
The diagram below represents a simplification of the layered structure of an operating system where:

.. image:: /_images/bash/shell-layer.png

Technically speaking, the **shell** is the name of one the layers of an operating system, but there are other ones:

1. The `kernel`_ is at the center, and provides the low level control of the computer's hardware: the CPU, RAM, and also drivers for external devices (graphics card, mouse, keyboard, etc). By the way, this is exactly what Linux is, just a kernel, not an OS.
2. The **shell** is a higher level layer, where resides the software dedicated for **user interaction**. In this layer we can find 2 types of shells:

* Graphical shells.
* Text-based shells.

Graphical shells
----------------
A `graphical shell`_ is software that allows the user to control the computer using a `Graphical User Interface`_. Graphical shells may be included with desktop environments or come separately, such as the `Gnome shell`_ or `Unity`_ which are examples of graphical shells for the Gnome desktop environment.

Graphical shells typically build on top of a `windowing system`_. In Unix and Unix-like operating systems, such as Linux, the two most popular windowing systems are the `X Window System`_ and `Wayland`_, both open source.

.. note:: In macOS there's `XQuartz`_, etc.

On top of the windowing system sits the `desktop environment`_. For **Linux** based operating systems there's a lot of desktop environments: `Gnome`_, `KDE`_, `Xfce`_, etc.

.. note:: For **macOS** it's named `Aqua`_, and for **Windows** it's called `Desktop Window Manager`_.

Lastly we have all the application software that provides a GUI; think of `LibreOffice`_, `Blender`_, etc.

Text-based shells
-----------------
Generically speaking, a text-based shell is all the software that comes together to provide the user with a `text based interface`_. This type of interface is implemented as what is known as a `command line interface`_, which is a text interface that presents the user with a `command prompt`_ where she can introduce commands to interact with the system.

A command line interface is supported by several programs and libraries:

* A terminal emulator or virtual console.
* Libraries to control the command line itself.
* The commands that the user invoke.
* A `Command line interpreter`_ to interpret these commands. Bash is a command line interpreter.

From all the parts that come together to offer the user with a command line interface, it's the **command interpreter** the one that has become known as the **shell**.

The shell as the command line interpreter
=========================================
Back in the day, GUIs didn't exist, the only way users could interact with computers was using software utilities that only offered a text interface.

.. figure:: /_images/bash/shell.jpg
	:figwidth: 50 %

As the diagram above shows, there are several programs that provide a text interface to a system. A shell is synonymous not with the whole set of them, but only with the most essential one, the `command line interpreter`_.

Today, GUIs are available, but some people (software developers, system administrators and advanced users) still rely heavily on the command-line interface to perform tasks more efficiently, configure their machines, or access programs and program features that are not available through a graphical interface.

.. _`shell`: https://en.m.wikipedia.org/wiki/Shell_(computing)
.. _`command line interpreter`: https://en.m.wikipedia.org/wiki/Command-line_interface#Command-line_interpreter
.. _`kernel`: https://en.wikipedia.org/wiki/Kernel_(operating_system)
.. _`Graphical shell`: https://en.wikipedia.org/wiki/Shell_(computing)#GUI
.. _`Gnome shell`: https://en.wikipedia.org/wiki/GNOME_Shell
.. _`Unity`: https://en.wikipedia.org/wiki/Unity_(user_interface)
.. _`Graphical User Interface`: https://en.wikipedia.org/wiki/Graphical_user_interface
.. _`windowing system`: https://en.wikipedia.org/wiki/Windowing_system
.. _`display server`: https://en.wikipedia.org/wiki/Display_server
.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
.. _`Wayland`: https://wayland.freedesktop.org/
.. _`XQuartz`: https://www.xquartz.org/
.. _`desktop environment`: https://en.wikipedia.org/wiki/Desktop_environment
.. _`GNOME`: https://en.wikipedia.org/wiki/GNOME
.. _`KDE`: https://en.wikipedia.org/wiki/KDE
.. _`Xfce`: https://en.wikipedia.org/wiki/Xfce
.. _`Aqua`: https://en.wikipedia.org/wiki/Aqua_(user_interface)
.. _`Desktop Window Manager`: https://en.wikipedia.org/wiki/Desktop_Window_Manager
.. _`LibreOffice`: https://en.wikipedia.org/wiki/LibreOffice
.. _`Blender`: https://www.blender.org/
.. _`command line interface`: https://en.m.wikipedia.org/wiki/Command-line_interface
.. _`command prompt`: https://en.wikipedia.org/wiki/Command-line_interface#Command_prompt
.. _`text based interface`: https://en.wikipedia.org/wiki/Text-based_user_interface