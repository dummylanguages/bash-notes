**********************
Terminals and consoles
**********************
There are other terms that are used interchangeably when talking about text interfaces:

* The `terminal emulator`_, or simply **terminal**.
* The `virtual console`_, aka **console** or **TTY**.

Truth is Bash (or whatever shell you use) is not the terminal, nor the console.

Nowadays, the terminal is just the software where the interpreter, and all the other pieces that support the Command Line Interface run. The terminal or console is the software that connects your monitor, keyboard and other input/output devices to your Command Line Interface.

.. note:: Back in the day, **terminals** were **physical devices** without computing power, just a screen and a keyboard. These devices used to be connected to a mainframe computer, allowing several users to access the system.

Terminal emulators
==================
A terminal emulator is a program that runs on the window system and provides a connection to the `command line interface`_. Terminal emulators can be configured to emulate the behaviour of a real and physical `computer terminal`_, such as the `VT100`_.

Some popular **terminal emulators** are:

* `Xterm`_, which is the standard terminal emulator for the X Window System. 
* macOS offers preinstalled the official `Terminal.app`_, but `iTerm2`_ is quite popular.
* `rxvt-unicode`_, which is a fork of the well known `rxvt`_.
* `guake`_ for the Gnome Desktop.
* Or the minimalistic `st - simple terminal`_.

Virtual consoles
================
Virtual consoles, aka **virtual terminals**, or **ttys** for short, are a feature of *some* Unix-like operating systems (not available on macOS) in which the `system console`_ can be used to switch between multiple virtual consoles. 

For example, the `Linux console`_ has available **7 virtual consoles**: the first six of them offer a login prompt into the shell; The last VT is for the window system. Each of the virtual consoles is represented by device special files ``/dev/tty1``, ``/dev/tty2``, etc. 

.. note:: **tty** is short for **TeleTYpe**, which was the name of the American hardware company, `Teletype Corporation`_, that created a computer terminal with a video screen.

Since ttys run independently from the graphical environment, we can't interact with them using the mouse, only the keyboard. For this reason, most of the times we'll want to use a terminal emulator. Still, there are situations where they are the only interface available, for example:

* Systems that don't have a window system installed.
* When accessing remote servers over SSH, telnet, etc.


.. _`terminal emulator`: https://en.wikipedia.org/wiki/Terminal_emulator
.. _`virtual console`: https://en.wikipedia.org/wiki/Virtual_console
.. _`system console`: https://en.wikipedia.org/wiki/System_console
.. _`Linux console`: https://en.wikipedia.org/wiki/Linux_console
.. _`Teletype Corporation`: https://en.wikipedia.org/wiki/Teletype_Corporation
.. _`command line interface`: https://en.wikipedia.org/wiki/Command-line_interface
.. _`computer terminal`: https://en.wikipedia.org/wiki/Computer_terminal
.. _`VT100`: https://en.wikipedia.org/wiki/VT100
.. _`Xterm`: https://en.wikipedia.org/wiki/Xterm
.. _`Terminal.app`: https://en.wikipedia.org/wiki/Terminal_(macOS)
.. _`iTerm2`: https://iterm2.com/
.. _`rxvt-unicode`: http://software.schmorp.de/pkg/rxvt-unicode.html
.. _`rxvt`: http://rxvt.sourceforge.net/
.. _`guake`: http://guake-project.org/
.. _`st - simple terminal`: https://st.suckless.org/