********************
The ``if`` statement
********************
The ``if`` statement is one of the Bash `conditional constructs`_. It can appear in several forms, being the most basic one:

.. code-block:: bash

  if test-commands
  then
    consequent-commands
  fi

Both ``then`` and ``fi`` are essential part of the syntax in the ``if`` statement. Forgetting to use any of them them will result in **syntax errors**. Both are in the list of Bash `reserved words`_.

Alternatively, it's possible to write the same statement in a slightly more compact form:

.. code-block:: bash

  if test-commands; then
    consequent-commands
  fi

The semicolon ``;`` is the **command separator**, which it's used when we want to write more than one command in the same line. Some people also use it at the end of *consequent-commands*, but that's unnecessary since the **new line character** works also a command separator.

A bit of terminology
====================
The `Bash official manual`_, use the words *test-commands* to refer to:

* The ``test`` `Bourne Shell Builtin`_ command.
* The ``[`` command, which is just an alternate form of ``test``.
* The ``[[`` compound command.

We go deeper into these commands in another section.

By *consequent-commands* they mean the commands in between ``then`` and ``fi`` keywords.

An example
==========
For example, check this script:

.. literalinclude:: /_code/conditionals_2-1.sh
    :language: bash

It prompts the user to type his age, and perform an age checkup. If the user is underage, when the ``test`` command evaluates the boolean expression returns an exit code ``0`` (true), and the *consequent-commands* will run. But if the user types an age over 18, ``test`` will return ``1``(false), and the commands between ``then`` and ``if`` will not run.

The test commands may return three types of **exit code**:

* ``0`` when the boolean expression is **true**
* ``1`` when the expression evaluates to **false**.
* ``>1`` when some **error** ocurs while evaluating the expression.

Nested ``if``
=============
It is possible to nest an ``if`` statement inside another one. For example, consider the following script:

.. literalinclude:: /_code/conditionals_2-2.sh
    :language: bash

Not using ``if``
================
A good understanding of **logical operators** may come in handy when using conditionals. For example, consider the following script:

.. code-block:: bash

    #!/bin/bash

    read -p 'Name the file you want to create: ' filename

    if [ -f "$filename.txt" ]
    then
        echo "$filename already exists!"
    else
        touch "$filename.txt"
        echo "$filename.txt" was created!
    fi

We could rewrite is as:

.. code-block:: bash

    #!/bin/bash

    read -p 'Name the file you want to create: ' filename

    ( [[ -f "$filename.txt" ]] && echo "$filename already exists!" )\
    || { touch "$filename.txt"; echo "$filename.txt" was created!; }

Quite shorter, although less readable. Let's explain what's going on in the last two lines of the script:

* The first line (expression between parentheses) contains and **AND** logical expression. If the file exists, the test evaluates to **true**, and the second part of the expression is checked; this second part runs ``echo`` warning that the file exists. In that case, the expression in the parens is **true** and the second line after the ``||`` doesn't even run. This is known as `short-circuit evaluation`_, only if the first part of the **logical OR** it's **false**, the second part runs to check if it's true.
* But if the file does not exists, the first part of the **logical AND** is false, so the second part does not run either and the whole expression between parens is **false**. The expression in the parens is the first part of the **logical OR**, and it's false, so it tries to check if the second part it's true; in this side we have put the other commands to create the file.

As a remainder:

* **logical &&** evaluates to **true** if both sides are true, but if the first side is **false** it stops there.
* **logical ||** evaluates to **true** if one side is true, but if the first side is **true**, it stops there.

.. note:: By the way, in the second part of the ``||`` operator we had to group the commands in between ``{}`` and separate them with ``;``, the command separator; read `command grouping`_ for more information.

This way of writing conditional constructs lacks readability and should be avoid in scripts, but in the command line comes in handy. Just use a single logical operator:

* **Logical OR** (``||``) when we want to run a command if something is **false**. For example:

.. code-block:: bash

    $ file='notes.txt'  # The file doesn't exist!
    $ [ -f "$file.*" ] && { touch "$file.*"; echo "$file has been created'; }

* **Logical AND** (``&&``) when we want to run a command if something is **true**:

.. code-block:: bash

    $ file='notes.txt'
    $ touch $file
    $ [ -f "$file.*" ] && { rm "$file.*"; echo "$file has been removed'; }

.. _`conditional constructs`: https://www.gnu.org/software/bash/manual/html_node/Conditional-Constructs.html#Conditional-Constructs
.. _`reserved words`: https://www.gnu.org/software/bash/manual/html_node/Reserved-Word-Index.html
.. _`Bash official manual`: https://www.gnu.org/software/bash/manual/
.. _`Bourne shell builtin`: https://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Builtins.html#Bourne-Shell-Builtins#
.. _`short-circuit evaluation`: https://en.wikipedia.org/wiki/Short-circuit_evaluation
.. _`command grouping`: https://www.gnu.org/software/bash/manual/html_node/Command-Grouping.html