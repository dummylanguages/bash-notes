.. _control statements:

******************
Control statements
******************
In this section we'll go over the statements that allow us to control the order in which the instructions in a program are executed, which is known as the **flow of a program**. Being able of controlling this order, is the main distinction between `imperative programming languages`_ and `declarative programming languages`_.

Sequential flow
===============
By **default**, the statements in a program are executed sequentially, in the same order they are written, which is known as **sequential flow** or **natural flow**. For example, consider a script containing the following three statements:

.. literalinclude:: /_code/conditionals_1.sh
    :language: bash

If we run it, we'll get the following output::

    one
    two
    three

The ability to alter the **natural flow** of a program is accomplished using `control flow`_ statements.

Control statements
==================
Control statements are the main distinctive feature of imperative programming languages. They allow us to create several `branches`_ of execution inside the flow of our program.

.. note:: **Control flow statements** are also known simply as **control statements**.

Control statements can be classified as:

* Selection statements, which we'll cover in this section.
* Iteration statements, aka loops.

In a way, the use of **functions** (aka routines, subroutines or procedures) also allow us to control the flow. But their main purpose is to help making a program more structured.

.. note:: In some programming languages, the flow can also be controlled using `labels`_ in combination with `goto`_. Bash doesn't support ``goto``.

Conditional statements
======================
Sometimes the execution of a branch depends on the evaluation of certain boolean condition. For example:

.. literalinclude:: /_code/conditionals_6.sh
    :language: bash

In the script above, we have two branches:

1. If the user introduces the right password, the 1st **conditional expression** (``"$PASSWORD" = "$INPUT"``) evaluates to ``0``, **true**, and the statements on *Branch A* are run.
2. If the password introduced is something other than ``1234``, the conditional evaluates to ``1`` (**false**) and *Branch B* will be the one executed.

This feature is present in most programming languages and is known generically as `conditional statements`_. Bash provides several `conditionals`_:

1. The ``if`` statement, which may include an ``else`` and ``elif`` clauses.
2. The ternary operator, which is a shortcut for **if-else** statements.
3. The ``case`` statement.
4. The ``select`` statement.

.. note:: Conditional statements are not the only ones whose execution is based on boolean conditions. Sometimes `loops`_ also depend on some condition evaluation, but they are not referred to as **conditionals**.

.. _`imperative programming languages`: https://en.wikipedia.org/wiki/Imperative_programming
.. _`declarative programming languages`: https://en.wikipedia.org/wiki/Declarative_programming
.. _`control flow`: https://en.wikipedia.org/wiki/Control_flow
.. _`conditionals`: https://en.wikipedia.org/wiki/Conditional_(computer_programming)
.. _`conditional statements`: http://tldp.org/LDP/Bash-Beginners-Guide/html/chap_07.html
.. _`branches`: https://en.wikipedia.org/wiki/Branch_(computer_science)
.. _`labels`: https://en.wikipedia.org/wiki/Label_(computer_science)
.. _`goto`: https://en.wikipedia.org/wiki/Goto
.. _`loops`: https://en.wikipedia.org/wiki/Control_flow#Loops
