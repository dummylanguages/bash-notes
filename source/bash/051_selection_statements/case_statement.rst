The ``case`` statement
======================
The ``case`` command is another `conditional construct`_ available in Bash. Its syntax looks like this::

  case word in
    pattern1)
      command-list
      ;;
    pattern2)
      command-list
      ;;
  esac
    
When branch execution depends on the different values a single variable may take, it's preferable to use a ``case`` statement over ``elif`` statements. For example, the following script the execution of each of the four branches depends on the value of variable ``OPTION``:

.. literalinclude:: /_code/conditionals_7.sh
    :language: bash

We can write the same script using the ``case`` statement:

.. literalinclude:: /_code/conditionals_8.sh
    :language: bash

Note that each ``case`` clause finish with one of the following operators:

* ``;;`` no subsequent matches are attempted after the first pattern match.
* ``;&`` causes execution to continue with the *command-list* associated with the next clause, if any. 
* ``;;&`` causes the shell to test the patterns in the next clause, if any, and execute any associated command-list on a successful match, continuing the case statement execution as if the pattern list had not matched. 

Let's see the ``;&`` operator in action:

.. literalinclude:: /_code/conditionals_9.sh
    :language: bash

Let's see the ``;;&`` operator in action:

.. literalinclude:: /_code/conditionals_10.sh
    :language: bash

.. warning:: WTF, can't figure out how these operators work!

.. _`conditional construct`: https://www.gnu.org/software/bash/manual/html_node/Conditional-Constructs.html#Conditional-Constructs
