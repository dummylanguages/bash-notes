The test commands
=================
In Bash, conditional statements need two components to function:

1. A test-command.
2. A conditional expression.

These two components work together, in the sense that the test-command will return an `exit status`_ based on a `conditional expression`_.

In Bash there are available two *test-commands*.

The condition evaluation utility
--------------------------------
The **condition evaluation utility**, commonly known as the ``test`` command has two forms:

* The ``test`` command.
* The ``[`` command, which is just an alternate form of test.

For example, you can also open the manual entry for ``test`` if you type in your terminal::

    $ man [

You'll get the same manual entry if you type::

    $ man test

The ``test`` utility is a `Bourne shell builtin`_, meaning a command inherited from the old Bourne shell, in order to comply with the list of mandatory utilities of the `POSIX`_ standard.

The ``test`` form
-----------------
Consider the following script:

.. literalinclude:: /_code/conditionals_3.sh
    :language: bash

The ``test`` command evaluates the ``5 -gt 3`` `conditional expression`_, and returns a **status code** ``0`` (*true* in Bash). Meaning that the statements following the ``then`` **reserved word** will run.

The ``[`` form
--------------
The script above can be written with the ``[`` command:

.. literalinclude:: /_code/conditionals_4.sh
    :language: bash

Since ``[`` is a command, we need a **space** to separate it from the ``5 -gt 3`` conditional expression, which becomes the **first argument** to this command. When we use this form, the **last argument** must always be ``]``.

.. note:: Don't forget to put spaces around the ``[`` command, and use another space after the **conditional expression** to separate it from the **last argument**, ``]``.

The extended test command
-------------------------
There is another construct that can be used to evaluate conditional expressions, that uses two square brackets instead of just one. Check the following example:

.. literalinclude:: /_code/conditionals_4-2.sh
    :language: bash

Notice that we can use operators such as ``||`` inside the brackets, which would result in an error using the ``[`` command.

This construct was adopted from the original `KornShell`_ (ks88)

.. note:: For an explanation above the differences between these commands, check GreyCat's `BashFAQ/031`_.


.. _`exit status`: https://www.gnu.org/software/bash/manual/html_node/Exit-Status.html
.. _`BashFAQ/031`: http://mywiki.wooledge.org/BashFAQ/031
.. _`Bourne shell builtin`: https://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Builtins.html#Bourne-Shell-Builtins#
.. _`POSIX`: http://get.posixcertified.ieee.org/
.. _`conditional expression`: https://www.gnu.org/software/bash/manual/html_node/Bash-Conditional-Expressions.html
.. _`KornShell`: https://en.wikipedia.org/wiki/KornShell