The ``else`` clause
===================
The ``else`` clause can only appear **once** in an given ``if`` statement::

    if test-commands; then
        consequent-commands
    else
        alternate-consequents
    fi

Notice that its *alternate-consequents* commands are **not** followed by a ``then``, if you use one with ``else`` you'll get a **syntax error**.

Let's see a simple example:

.. literalinclude:: /_code/conditionals_6.sh
    :language: bash

Here we also use the ``read`` `Bash builtin command`_ to receive input from the user that we store in the ``INPUT`` variable (the ``-p`` option allows us to specify a prompt). We then check the input with the hardcoded password stored in the variable ``PASSWORD``, and if it doesn't match, the ``else`` clause runs.

The ``else`` clause can also be used as a *catch-all* at the end of several ``elif`` clauses, like this:

.. literalinclude:: /_code/conditionals_7.sh
    :language: bash

In this case, using an ``else`` clause at the end is a good way to deal with non-valid user input.

.. _`Bash builtin command`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html#Bash-Builtins