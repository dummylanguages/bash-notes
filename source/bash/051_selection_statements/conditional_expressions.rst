***********************
Conditional expressions
***********************
All *test-commands* base their `exit status`_ on a **conditional expression**. But what is a conditional expression?

The Bash official documentation, in the section regarding `conditional expressions`_ makes a distinction between:

* **Unary expressions**, often used to examine the status of a file.
* **Binary expressions**.

They also use the term **primary** when talking about conditional expressions regarding files::

    If the file argument to one of the primaries is of the form...

Which seems to imply that a **primary** is an operator such as ``-a``, whereas the *file argument* can be considered an **operand**.

So we have that a **conditional expression** is made up of:

* An **operator** (known as a primary) 
* One or two **operands**, depending if the expression is unary or binary.

Primaries in POSIX
==================
The `POSIX.1-2017`_ specification, in the volume dedicated to `Shell & Utilities`_, contains a page dedicated to the the `test utility`_. There, the term **primary** is used quite ambiguously::

    The application shall ensure that all operators and elements of primaries
    are presented as separate arguments to the test utility.

Which seems to equate **primaries** to **expressions**, since they may be made up of elements(plural).

And a bit further, it clearly says::

    The primaries with two elements of the form:

        -primary_operator primary_operand

    are known as unary primaries. The primaries with three elements in either of the two forms:

        primary_operand -primary_operator primary_operand


        primary_operand primary_operator primary_operand

    are known as binary primaries.

So according to the specification, a **primary** can be one of three things:

1. An **expression** (unary primaries and binary primaries).
2. An **operator** (primary_operator).
3. An **operand** (primary_operand).

Using a word that can mean 3 things, and wait for the reader to extract its concrete meaning from the context is a bit ambiguous. Fuck it! We'll stick to the Bash manual and our common sense.

.. note::

    People waste time and energy trying to makes sense of cryptic specifications which not even Rick Sanchez could make sense of.

    Check this: https://unix.stackexchange.com/questions/254410/what-is-the-definition-of-a-primary-in-bash
    
    Or this: https://unix.stackexchange.com/questions/365941/what-are-command-primaries?noredirect=1&lq=1

File expressions
================
These are mostly **unary** expressions that deal with files. For example, consider the following script:

.. literalinclude:: /_code/conditionals_11.sh
    :language: bash

The conditional expression ``-f foo.txt`` is built using a single **unary expression**. The filename ``foo.txt`` is the **operand**, and ``-a`` is the **operator**.

There are plenty of operators than can be used with files. The following table contains operators that take a single file operand to build a **unary expression**:

+-------------+-----------------------------------------------------------------+
|  Operator   |            Evaluates to **True** (0) when the file:             |
+=============+=================================================================+
| ``-a`` file | exists.                                                         |
+-------------+-----------------------------------------------------------------+
| ``-b`` file | exists and is a **block** special file.                         |
+-------------+-----------------------------------------------------------------+
| ``-c`` file | exists and is a **character** special file.                     |
+-------------+-----------------------------------------------------------------+
| ``-d`` file | exists and is a **directory**.                                  |
+-------------+-----------------------------------------------------------------+
| ``-e`` file | exists. (same as ``-e``)                                        |
+-------------+-----------------------------------------------------------------+
| ``-f`` file | exists and is a **regular file**.                               |
+-------------+-----------------------------------------------------------------+
| ``-g`` file | exists and  and its **set-group-id bit** is set.                |
+-------------+-----------------------------------------------------------------+
| ``-h`` file | exists and is a **symbolic link**.                              |
+-------------+-----------------------------------------------------------------+
| ``-k`` file | exists and its **"sticky" bit** is set.                         |
+-------------+-----------------------------------------------------------------+
| ``-p`` file | exists and is a **named pipe** (FIFO).                          |
+-------------+-----------------------------------------------------------------+
| ``-r`` file | exists and is **readable**.                                     |
+-------------+-----------------------------------------------------------------+
| ``-s`` file | exists and has a **size greater than zero**.                    |
+-------------+-----------------------------------------------------------------+
| ``-t`` file | if file descriptor ``fd`` is open and refers to a **terminal**. |
+-------------+-----------------------------------------------------------------+
| ``-u`` file | exists and its **set-user-id bit** is set.                      |
+-------------+-----------------------------------------------------------------+
| ``-w`` file | exists and is **writable**.                                     |
+-------------+-----------------------------------------------------------------+
| ``-x`` file | exists and is **executable**.                                   |
+-------------+-----------------------------------------------------------------+
| ``-G`` file | exists and is **owned** by the **effective group id**.          |
+-------------+-----------------------------------------------------------------+
| ``-L`` file | exists and is a **symbolic link**.                              |
+-------------+-----------------------------------------------------------------+
| ``-G`` file | exists and is **owned** by the **effective group id**.          |
+-------------+-----------------------------------------------------------------+
| ``-N`` file | exists and has been **modified** since it was last read.        |
+-------------+-----------------------------------------------------------------+
| ``-O`` file | exists and is **owned** by the **effective user id**.           |
+-------------+-----------------------------------------------------------------+
| ``-S`` file | exists and is a **socket**.                                     |
+-------------+-----------------------------------------------------------------+

The following table contains operators that take two file operands, labeled here as f1 and f2:

+---------------+-------------------------------------------------------------------------------------------+
|   Operator    |                               Evaluates to **True** (0) if:                               |
+===============+===========================================================================================+
| f1 ``-ef`` f2 | f1 and f2 refer to the same device and inode numbers.                                     |
+---------------+-------------------------------------------------------------------------------------------+
| f1 ``-nt`` f2 | if f1 is newer (according to modification date) than f2, or if f1 exists and f2 does not. |
+---------------+-------------------------------------------------------------------------------------------+
| f1 ``-ot`` f2 | if f1 is older than f2, or if f2 exists and f1 does not.                                  |
+---------------+-------------------------------------------------------------------------------------------+

When using these three operators, we'll be building **binary expressions**.

Operators for checking options and variables
============================================
There are some operators that check if a shell **option name** or **variable name** have been set. This operators take a single operand, hence the resulting conditional expression is **unary**.

+----------------+--------------------------------------------------------------------+
|    Operator    |                   Evaluates to **True** (0) if:                    |
+================+====================================================================+
| ``-o`` optname | if the shell option optname is enabled.                            |
+----------------+--------------------------------------------------------------------+
| ``-v`` varname | if the shell variable varname is set (has been assigned a value).  |
+----------------+--------------------------------------------------------------------+
| ``-R`` varname | True if the shell variable varname is set and is a name reference. |
+----------------+--------------------------------------------------------------------+

Optname is short for option name. The list of **options** we can check for, appears in the description of the ``-o`` option to the `set builtin`_ command. 

Operators for checking strings
==============================
For checking strings we have unary operators, and binary operators.

+---------------+-----------------------------------+
|   Operator    |   Evaluates to **True** (0) if:   |
+===============+===================================+
| string        | the length of string is non-zero. |
+---------------+-----------------------------------+
| ``-n`` string | the length of string is non-zero. |
+---------------+-----------------------------------+
| ``-z`` string | the length of string is zero.     |
+---------------+-----------------------------------+

If we use the string itself, the expression will evaluate to true without the need to use an operator. In this case the **unary expression** is just the string itself which becomes operand, operator and expression (hence the POSIX definition of primary?) at the same time. Using the ``-n`` operator produces the same result.

We also have binary operators:

+-----------------------+-------------------------------------------------+
|       Operator        |          Evaluates to **True** (0) if:          |
+=======================+=================================================+
| string ``==`` string  | the strings are equal. (non POSIX compliant)    |
+-----------------------+-------------------------------------------------+
| string ``=`` string   | the strings are equal.                          |
+-----------------------+-------------------------------------------------+
| string ``!=`` string  | the strings are **not** equal.                  |
+-----------------------+-------------------------------------------------+
| string1 ``<`` string2 | string1 sorts before string2 lexicographically. |
+-----------------------+-------------------------------------------------+
| string1 ``>`` string2 | string1 sorts after string2 lexicographically.  |
+-----------------------+-------------------------------------------------+

Arithmetic operators
====================
When using the ``test`` command (or its ``[`` form) Arg1 and Arg2 can be only positive or negative **integers**.

+-------------------+---------------------------------------+
|     Operator      |     Evaluates to **True** (0) if:     |
+===================+=======================================+
| Arg1 ``-eq`` Arg2 | Arg1 is **equal** to Arg2.            |
+-------------------+---------------------------------------+
| Arg1 ``-ne`` Arg2 | Arg1 is **not equal** to Arg2.        |
+-------------------+---------------------------------------+
| Arg1 ``-lt`` Arg2 | Arg1 is **less than** Arg2.           |
+-------------------+---------------------------------------+
| Arg1 ``-le`` Arg2 | Arg1 is **less or equal** to Arg2.    |
+-------------------+---------------------------------------+
| Arg1 ``-gt`` Arg2 | Arg1 is **greater than** Arg2.        |
+-------------------+---------------------------------------+
| Arg1 ``-ge`` Arg2 | Arg1 is **greater or equal** to Arg2. |
+-------------------+---------------------------------------+

When used with the ``[[`` command, Arg1 and Arg2 can be **arithmetic expressions**.

.. _`exit status`: https://www.gnu.org/software/bash/manual/html_node/Exit-Status.html#Exit-Status
.. _`Bash official manual`: https://www.gnu.org/software/bash/manual/
.. _`conditional expressions`: https://www.gnu.org/software/bash/manual/html_node/Bash-Conditional-Expressions.html#Bash-Conditional-Expressions
.. _`POSIX.1-2017`: https://pubs.opengroup.org/onlinepubs/9699919799/mindex.html
.. _`Shell & Utilities`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/toc.html
.. _`test utility`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/test.html
.. _`set builtin`: https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html#The-Set-Builtin
