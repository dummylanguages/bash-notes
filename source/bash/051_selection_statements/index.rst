####################
Selection statements
####################

.. toctree::
    :maxdepth: 3
    :hidden:

    control_statements
    if_statement
    elif_clause
    else_clause
    case_statement
    test_commands
    conditional_expressions

By default, a series of statements in a program will be executed sequentially, in the same order they are written. This is also known as **natural flow**. The ability to alter the natural flow of a program is one of the most basic features in almost every programming language. That's accomplished using `control flow`_ statements, or simply **control statements**.

.. note:: Control statements allow us to create several `branches`_ of execution inside our program.

Most of the times, **control statements** depend on the evaluation of certain boolean condition, which is why they are also known as `conditional statements`_ (or simply **conditionals**).

Bash provides several `conditionals`_:

1. The ``if`` statement, which may include an ``else`` and ``elif`` clauses.
2. The ternary operator, which is a shortcut for **if-else** statements.
3. The ``case`` statement.
4. The ``select`` statement.

.. note:: Conditional statements are not the only ones whose execution is based on boolean conditions. **Iteration statements**, also known as `loops`_ also depend on the evaluation of some condition, but they are not commonly referred to as **conditionals**.

.. _`control flow`: https://en.wikipedia.org/wiki/Control_flow
.. _`conditionals`: https://en.wikipedia.org/wiki/Conditional_(computer_programming)
.. _`conditional statements`: http://tldp.org/LDP/Bash-Beginners-Guide/html/chap_07.html
.. _`branches`: https://en.wikipedia.org/wiki/Branch_(computer_science)
.. _`loops`: https://en.wikipedia.org/wiki/Control_flow#Loops
