The ``elif`` clause
===================
The ``if`` statement can include an ``elif`` clause, for example::

    if test-commands; then
      consequent-commands
    elif more-test-commands; then
      more-consequents
    fi

When the return status of the first *test-commands* is not zero, program control flows to the *more-test-commands* of the next ``elif`` clause. We can add as many ``elif`` clauses as we deem necessary, for example:

.. literalinclude:: /_code/conditionals_5.sh
    :language: bash

In the example above, only the conditional expression of the last ``elif`` returned a 0 status, hence the output::

    You chose option B...
