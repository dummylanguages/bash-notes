*******************
The ``let`` command
*******************
According to the GNU Bash manual, the ``let`` `builtin`_ allows arithmetic to be performed on shell **variables**. The general syntax looks like this::

    let expression [expression …]

Each expression is evaluated according to the rules described in `shell arithmetic`_.

Some examples
=============
For example, if we tried:

.. code-block:: bash

    $ var="2 + 2"
    $ echo $var
    2 + 2

Using ``let``:

.. code-block:: bash

    $ let var="2 + 2"
    $ echo $var
    4

A slightly more interesting example:

.. code-block:: bash

    $ let "var=2, var*=3"
    $ echo $var
    6

Assigning variables to variables
--------------------------------
If we want to assign the numberic value of a variable to another one, this obviously won't work:

.. code-block:: bash

    $ var=2
    $ another_var=var
    $ echo $another_var
    var

We have to use `parameter expansion`_:

.. code-block:: bash

    $ var=2
    $ another_var=$var
    $ echo $another_var
    var

Or the ``let`` command:

.. code-block:: bash

    $ var=5
    $ let another_var=var  # Without parameter expansion!
    $ echo $another_var
    5

Another more interesting example:

.. code-block:: bash

    $ let "var1 = 33, var2 = var1++"  # No parameter expansion!
    $ echo "var1: $var1, and var2: $var2"
    var1: 34, and var2: 33

If the output above has confused you, try the **pre-increment** operator (``++var1``) so that the value of ``var1`` is incremented **before** being assigned to ``var2``.

``let`` vs Arithmetic Evaluation and Arithmetic Expansion
=========================================================



Using ``declare``
=================
Another way of achieving the same is to declare ``var`` explicitely as an **integer**:

.. code-block:: bash

    $ var=33
    $ declare -i another_var=var # No parameter expansion either!
    $ echo $another_var
    33

.. _`builtin`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html
.. _`shell arithmetic`: https://www.gnu.org/software/bash/manual/html_node/Shell-Arithmetic.html
.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html

