******************
The ``bc`` command
******************

Arithmetic operators
====================
The following table summarizes the most basic operators:

+----------+----------------+
| Operator |                |
+==========+================+
| ``+``    | Addition       |
+----------+----------------+
| ``-``    | Substraction   |
+----------+----------------+
| ``*``    | Multiplication |
+----------+----------------+
| ``\``    | Division       |
+----------+----------------+
| ``**``   | Exponentiation |
+----------+----------------+
| ``%``    | Remainder      |
+----------+----------------+
| ``++``   | Increment      |
+----------+----------------+
| ``--``   | Decrement      |
+----------+----------------+

`Arithmetic expansion`_ allows to perform arithmetic computations with integer values and substitute in the result. The format for 
The arithmetic expression is evaluated according to the rules for used by the `((`_ compound command.

Relational operators
====================
+----------+-----------------------+
| Operator |                       |
+==========+=======================+
| ``>``    | Greater than          |
+----------+-----------------------+
| ``<``    | Less than             |
+----------+-----------------------+
| ``>=``   | Greater or equal to   |
+----------+-----------------------+
| ``<=``   | Less than or equal to |
+----------+-----------------------+
| ``==``   | Equal to              |
+----------+-----------------------+
| ``!=``   | Not equal to          |
+----------+-----------------------+

Logical (boolean) operators
===========================

+----------+-------------+
| Operator |             |
+==========+=============+
| ``&&``   | Logical AND |
+----------+-------------+
| ``||``   | Logical OR  |
+----------+-------------+
| ``!``    | Logical NOT |
+----------+-------------+

Bitwise operators
=================

Assignment operators
====================

Conditional operator
====================

.. _`Arithmetic expansion`: https://www.gnu.org/software/bash/manual/html_node/Arithmetic-Expansion.html
.. _`shell arithmetic`: https://www.gnu.org/software/bash/manual/html_node/Shell-Arithmetic.html
.. _`((`: https://www.gnu.org/software/bash/manual/html_node/Conditional-Constructs.html
.. _`$?`: https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html
.. _`conditional constructs`: https://www.gnu.org/software/bash/manual/html_node/Conditional-Constructs.html

