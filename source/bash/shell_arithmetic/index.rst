.. _shell_arithmetic_label:

################
Shell Arithmetic
################

.. toctree::
	:maxdepth: 3
	:hidden:

	operators
	let_command
	bc_command


The GNU Bash official manual contains a section named `shell arithmetic`_, where they describe all the **operators** that can be used:

* Inside the ``((`` compound command, and also `arithmetic expansion`_: ``$(( expression ))``.
* As arguments in expressions for the ``let`` builtin.
* After the ``-i`` option to the ``declare`` builtin.

Bash only has support for **integers**, so we'll have to resort to **external commands** in case we need to deal with **floating-point values**.

.. _`arithmetic expansion`: https://www.gnu.org/software/bash/manual/html_node/Arithmetic-Expansion.html#Arithmetic-Expansion
