##############
Bash Scripting
##############

.. toctree::
	:maxdepth: 3
	:hidden:

	intro/index
	functions

We have already seen how Bash can be used at the **command line**, where the user types a command and gets inmediate output or lack of it as a result. The interactive feedback allows the user to add small incremental modifications every time a line completes execution. This interactivity makes possible a process of fine tweaking the commands until optimal results are achieved.

But imagine the situation of the user that needs to run frequently the same list of already optimized commands. It's not very efficient having to type the same commands all over again, plus waiting for each line to complete execution before typing in the next one. In this scenario it's more practical to add the commands in a file, and run them all together as a batch.

A `shell script`_ is just a simple **text file** which contains a batch of shell commands. These files can be run as many times we want, without having to type the commands all over again. We may even schedule its execution at any given point in the future. This is specially useful for complex and repetitive tasks needed when maintaining systems.

.. note:: Some people may argue that **shell scripting** is the **non interactive** way of using Bash, but we'll see how scripts can also be as interactive as the user choses to. It all depends on how they are written.

Hence, Bash is not only the program in charge of interpreting the commands the user inputs at the command line. It also becomes the `scripting language`_ that we use for writing **shell scripts**.

.. _`scripting language`: https://en.wikipedia.org/wiki/Scripting_language
.. _`shell script`: https://en.m.wikipedia.org/wiki/Shell_script
