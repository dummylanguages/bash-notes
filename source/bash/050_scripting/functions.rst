*********
Functions
*********
`Shell functions`_ are a way to group commands for later execution using a single name for the group.

Declaration: General Syntax
===========================
Functions are declared using two different syntaxes::

  function_name ()
  {
    commands
  }

Or also using the ``function`` keyword, which is **optional**::

  function fname
  {
    commands
  }

If the ``function`` reserved word is supplied, the **parentheses** are **optional**. The **body of the function** is the `compound command`_, which is usually a list of commands enclosed between ``{`` and ``}``. Placing a list of commands between curly braces causes the list to be executed in the current shell context. No subshell is created. A **semicolon** or **newline** is required at the end of the list.

Invocation
==========
To call a function we just have to write its name, for example::

  fn_do_something

Note that we don't use the **parentheses** at all; in Bash they are used only when we define the function, and they remain empty all the time. Functions are executed just like **regular commands**. When the name of a shell function is used as a simple command name, the list of commands associated with that function name is executed.

Arguments
---------
We may send data to the function in a similar way to passing **command line arguments** to a script. We supply the arguments directly after the function name. For example::

  fn_do_something argv1 argv2

.. note:: In other programming languages it is common to pass arguments to the function by writing them inside the parentheses ``()``. In Bash parentheses are used only when we define the function; even in that case we never put anything inside them, they're just a syntax formality.

Within the function they are accessible as ``$1``, ``$2``, etc.

Local variables
===============
Variables local to the function may be declared with the `local builtin`_. These variables are visible only to the function and the commands it invokes. This is particularly important when a shell function calls other functions.

Local variables "shadow" variables with the same name declared at previous scopes. For instance, a local variable declared in a function hides a global variable of the same name: references and assignments refer to the local variable, leaving the global variable unmodified. When the function returns, the global variable is once again visible. 

Return status
=============
Most other programming languages have the concept of a return value for functions, a means for the function to send data back to the original calling location. Bash functions don't allow us to do this. They do however allow us to set a return status. Similar to how a program or command exits with an exit status which indicates whether it succeeded or not. We use the keyword ``return`` to indicate a **return status**.

Passing arrays to Functions
===========================
Let's say we want to pass an array declared in the global scope to a function. The :download:`following script </_code/bash/functions/fruity.sh>` is self explanatory:

.. literalinclude:: /_code/bash/functions/fruity.sh
  :language: bash
  :linenos:

Another option is to pass the array subscripted with ``*``, which will expand all array elements into the same string! For example, check this :download:`other version </_code/bash/functions/fruity2.sh>` :

.. literalinclude:: /_code/bash/functions/fruity2.sh
  :language: bash
  :linenos:

Note how we had to declare a local array inside the function, where we expand the function's argument into a proper array using ``my_array=($1)``.

.. _`compound command`: https://www.gnu.org/software/bash/manual/html_node/Command-Grouping.html
.. _`Shell functions`: https://www.gnu.org/software/bash/manual/html_node/Shell-Functions.html
.. _`local builtin`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html
