Shebang
=======
The **first line** in every shell script may look something like this::

    #!/bin/bash

This line is known as the `shebang`_, and it is used to control which interpreter will parse and interpret the commands contained in the file.

.. note:: Technically speaking a **shebang** is an `interpreter directive`_, which are not used only for Bash shell scripts, but for any other script that needs an interpreter to be run, such as Perl or Python scripts.

A shebang line has two parts:

* The **prefix** ``#!``.
* The path to the interpreter.

The prefix
----------
The **prefix** is the first two characters, ``#!`` known as the **shebang** hence the name of the directive itself. In Unix, Linux and other Unix-like operating systems, the first two bytes in a file can be the characters "#!", which constitute a magic number (hexadecimal 23 and 21, the ASCII values of "#" and "!") 

The path to the interpreter
---------------------------
There are a couple of ways to setup the path to the interpreter, both have pros and cons:

Absolute path
^^^^^^^^^^^^^
In most Unix-like operating systems, Bash is usually installed in ``/bin/bash``, so it is safe to use the absolute path to the **standard location**::

    #!/bin/bash

The main advantage of the line above is **portability**, meaning that our script can be used in those systems wich include Bash in the standard location.

A not so standard location
""""""""""""""""""""""""""
Sometimes, the system's Bash installed in the standard location may be a bit *outdated*, lacking some of the features of more up to date versions. Since it's usually a bad idea to mess with the system's Bash, users usually install more recent versions in other locations, being ``/usr/local/bin/bash`` one of the most popular ones. We can point our shebang to this not-so standard location::

    #!/usr/local/bin/bash

This way, our script can take advantage of the latest features included in newer Bash versions.

The problem with this approach is that if we try to run our script in other system that doesn't have Bash installed in that location, the system's `program loader`_ will be unable to find the interpreter and our script won't run.

Using the ``env`` command
^^^^^^^^^^^^^^^^^^^^^^^^^
It's also quite popular to use ``/bin/env bash`` which makes use of the ``env`` command to find the first Bash on the system's ``$PATH``. This is great for portability reasons but it has a couple of cons:

* What if ``env`` is not present in the system, or it's install on some place other than ``/bin/env``?
* Some older systems won't allow to pass more than one argument to ``env``.

Generally speaking is unlikely to find ourselves in any of the above mentioned situations, so if our goal is **portability** use the following line::

    #!/bin/env bash

Usually I find this line quite convenient after putting my favourite Bash upfront in the ``$PATH``. This way my script is quite portable and at the same time I can specify precisely with which version of Bash I want to run it.


.. _`shebang`: https://en.wikipedia.org/wiki/Shebang_(Unix)
.. _`Hello, World!`: https://en.wikipedia.org/wiki/%22Hello,_World!%22_program
.. _`interpreter directive`: https://en.wikipedia.org/wiki/Interpreter_directive
.. _`program loader`: https://en.wikipedia.org/wiki/Loader_(computing)
