Using a scripts folder
======================
It is quite useful to create a folder in our ``$HOME`` directory where we can place our scripts, instead of having them sprinkled all over our file system. We can choose any name for it, such as ``~/bin`` or ``~/scripts``, etc::

    $ touch ~/scripts

Adding the folder to our ``PATH``
---------------------------------
Usually to run a script, we have to invoke it either from the same folder where it's located, or using its **full path**. For example, imagine we are deep down the file system, and we want to run a script we wrote a while ago. We have two options here:

1. Change our working directory to the location where the script is, and then launch it.
2. Invoke it using its full path, for example::

    $ source ~/scripts/do_useful_task.sh

Keeping our scripts in a dedicated folder is not useful for organizational purposes. If we add it to our ``$PATH``::

    export PATH="${HOME}/scripts:${PATH}"

We'll be able to run our scripts regardless of our current working directory. No matter where we are, we can do::

    $ do_useful_task.sh

There's no need of using the ``source`` command, if the folder is on the ``PATH``, typing the name of the script is enough to run it.
