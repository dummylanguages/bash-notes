Hello world
===========
Let's write the classic `Hello, World!`_ program using Bash. We just have to create a new file::

    $ touch hello-world
    
And add the following two lines::

    #!/bin/env bash
    echo Hello, World!

1. The first line is known as the `shebang`_. This line specifies which program is going to interpret the script, in this case ``bash`` itself.
2. The second line uses the command ``echo`` followed by the string of text we want to display in the command line.

Making the file executable
--------------------------
Before being able to run this script we have to make it **executable**, which is easily acomplished with the command::

    $ chmod +x hello-world

.. note:: If the script is **not executable**, we still can run it with: ``bash hello-world``.

Running the script
------------------
Once the file is executable, we can run it with the command::

    $ source hello-world
    Hello, World!

The ``source`` command is used to execute/run any shell script. It is more **portable** to use a **dot**, then the path to the script, as in::

    $ . hello-world
    Hello, World!

In this case we were running the script from the same directory, so the path was just the name of the script.

About the file extension
------------------------
Some people add the ``.sh`` file extension to their shell scripts. That's completely **optional**.

.. _`Hello, World!`: https://en.wikipedia.org/wiki/%22Hello,_World!%22_program
.. _`shebang`: https://en.wikipedia.org/wiki/Shebang_(Unix)
