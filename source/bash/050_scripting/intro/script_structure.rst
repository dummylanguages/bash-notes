****************
Script structure
****************
We should keep in mind that we can only reference variables or invoke functions after they have been defined in our code. For example, this would not work:

.. code-block:: bash

    #!/bin/bash
    say_answer

    say_answer () { echo "The answer is $answer" }
    answer=42

Mistakes:

* We are invoking the ``say_answer`` function before it has been defined.
* In the function definition we are referencing the ``answer`` variable even though it's defined later.

Medium sized scripts
====================
It is quite useful to get used the following structure for **medium sized** scripts:

.. code-block:: bash

    #!/bin/bash
    function main () {}
    function fun1 () {}
    function fun2 () {}
    main "$@"

This way nothing runs until we get to the bottom of the script where ``main`` is called with all the arguments passed when invoking the script. The code inside the other function definitions should have been read, and the calls to them from ``main`` will find them.

Bigger sized scripts
====================
When your script grows past a certain size you may consider to split the code into several files. For example a file where you put your function definitions, let's call it **library.sh** (This file doesn't need a **shebang** because it's not executed):

.. code-block:: bash

    function foo () {}
    function bar () {}
    function baz () {}

And the **main.sh** file:

.. code-block:: bash

    #!/bin/bash

    # Sourcing the library file
    . library.sh

    # Using the functions defined in library.sh
    foo
    bar
    baz

.. note:: I'm using a dot ``.`` because it is POSIX compliant, instead of the ``source`` statement, defined by Bash as an **alias** to ``.``.

Super big scripts
=================
Don't use Bash and start thinking of other options such as **Perl**, **Ruby** or **Python**.