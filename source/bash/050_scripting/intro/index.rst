###############################
Introduction to Shell Scripting
###############################

.. toctree::
    :maxdepth: 3
    :hidden:

    hello_world
    shebang
    scripts_folder
    script_structure

In this section we are gonna introduce the essentials of **shell scripting**.

.. _`scripting language`: https://en.wikipedia.org/wiki/Scripting_language
.. _`shell scripts`: https://en.m.wikipedia.org/wiki/Shell_script
