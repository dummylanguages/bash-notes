#############
Startup files
#############

.. toctree::
    :maxdepth: 3
    :hidden:

    type_of_sessions
    file_precedence
    macos
    environment_variables
    aliases

The `startup files`_ are the ones that Bash reads every time it starts. In these file we can add specific configuration settings, to achieve useful things such as:

* Setting **environment variables** such as ``$PATH``, which contains the paths to executable files.
* Controlling the appearance of the **prompt**.
* Handy command **aliases**.

Depending on how Bash is started, it will read different files.


.. _`startup files`: https://www.gnu.org/software/bash/manual/html_node/Bash-Startup-Files.html#Bash-Startup-Files
