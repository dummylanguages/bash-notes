Aliases
=======
`Aliases`_ allow a string to be substituted for a word when it is used as the first word of a simple comma

.. _`Aliases`: https://www.gnu.org/software/bash/manual/html_node/Aliases.html#Aliases

