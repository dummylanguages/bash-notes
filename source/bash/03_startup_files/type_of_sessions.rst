Types of shell sessions
=======================
Shell sessions can be classified according to different criteria:

Interactive and non-interactive
-------------------------------
1. **Interactive sessions** are those in which the user, interacts with the command line. For example, when we launch our terminal emulator we are starting an **interactive shell**.
2. In a **non-interactive session**, the user doesn't interact with the command line, like for example a shell session running in the background or one launched by a shell script.

Login and non-login
-------------------
Non-login sessions
""""""""""""""""""
In modern systems, most of the times we'll be logging in using a graphical `login manager`_. Login managers are also known as **display managers** and there are plenty of them. Basically every `desktop environment`_ includes their own display manager, such as:

    * `XDM`_ (short for **X Display Manager**) is the **default** display manager for the `X Window System`_.
    * `GNOME Display Manager`_, short for Gnome Display Manager.
    * The login screen on macos.

Once we use one of these to log in, and we launch a **terminal emulator**, the shell session will be an **interactive non-login** one. In this case Bash reads and executes commands from ``~/.bashrc``, if that file exists. 

.. note:: The ``--rcfile foo`` option will force Bash to read and execute commands from ``foo`` instead of ``~/.bashrc``. 

Login sessions
""""""""""""""
There are some situations, when we have to log in into a system using a shell, for example:

1. We have to login using a `virtual console`_, also known as **virtual terminal** meaning those terminal emulators that do not run on the GUI. For example:

    * Unix/Linux systems have available multiple virtual consoles (usually 6 ttys), that we can open pressing ``Alt + F1/F6``).
    * We start the system in some sort of **recovery mode** to perform some maintenance tasks.
    * Or those cases where the system doesn't have any sort of graphical **desktop environment**.

3. We log into a **remote machine** using `SSH`_.
4. When Bash is invoked with the ``--login`` option.



.. _`login manager`: https://en.wikipedia.org/wiki/Login_manager
.. _`desktop environment`: https://en.wikipedia.org/wiki/Desktop_environment
.. _`XDM`: https://en.wikipedia.org/wiki/X_display_manager
.. _`GNOME Display Manager`: https://en.wikipedia.org/wiki/GNOME_Display_Manager
.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
.. _`SSH`: https://en.wikipedia.org/wiki/Secure_Shell
.. _`virtual console`: https://en.wikipedia.org/wiki/Virtual_console
