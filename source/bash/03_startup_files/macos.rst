The situation in macOS
======================
In macOS, and a bit counterintuitively, once we are already logged in and every time we launch a terminal emulator, we are starting a **login interactive shell**. This is something that can be tested running::

    $ echo $0
    -bash

The command ``echo $0`` should return the name of the running process, wich is ``bash`` itself. The preceding dash (note ``-bash``) means that Bash is running as a **login shell**.

If we launch another Bash session from the same terminal emulator and run the command above we would get::

    $ bash
    $ echo $0
    bash

This time, there's no dash in the output, ``bash``, which means that this time Bash is running as a **non-login** shell. We would have to run ``exit`` twice to exit:

    1. Once for the child **non-login** Bash process.
    2. And again for the parent Bash process launched by default by our terminal emulator.

Because of this peculiarity, macOS users tend to place all of their Bash configuration in ``~/.bash_profile``, since this is the file that Bash reads by default everytime a shell session is launched.


.. _`virtual console`: https://en.wikipedia.org/wiki/Virtual_console
