File precedence
===============
When we start Bash as an **interactive login shell**, the following files are read:

1. First of all a file name ``/etc/profile``, if it exists.
2. After reading that file, it looks for:

    * ``~/.bash_profile``
    * ``~/.bash_login``
    * ``~/.profile``
    
Bash will follow that exact order, until it finds one that exists and is readable. Meaning that if ``~/.bash_profile`` is found, ``~/.bash_login`` and ``~/.profile`` will be ignored.

Bourne shell compatibility: ``~/.profile``
------------------------------------------
This is the file used by ``/bin/sh``, the old **Bourne shell** for **login shells**. Since Bash, aka the **Bourne Again shell** tries to be backwards-compatible with ``/bin/sh``, it will also read ``.profile`` if one exists. The **Z shell** also reads this file for the same reasons, so if we are gonna be using this shell, we may move our configuration here.

Some people use ``~/.profile`` to set up all the **environment variables** that may be sourced by other programs such as **desktop environments**. If you do that, and want to keep a ``~/.bash_profile`` at the same time, you'll have to **source** ``~/.profile``::

    if [ -f ~/.profile ]; then
        source ~/.profile
    fi

Because remember, if Bash finds a ``~/.bash_profile`` or a ``~/.bash_login`` it will stop there, and won't even source ``~/.profile``.

Logging out
-----------
When an **interactive login shell** exits, Bash reads and executes commands from the file ``~/.bash_logout``, if it exists.

.. note:: When a **non-interactive login** shell executes the ``exit`` builtin command, ``~/.bash_logout`` will also be read.


Maintaining only one file
-------------------------
Keeping track of Bash configuration settings is easier if we use only one file. A possible way of doing that is adding the following into our ``~/.bash_profile``::

    if [ -f ~/.bashrc ]; then
        source ~/.bashrc;
    fi

The lines above allow us to keep all our configuration in ``~/.bashrc``, which will be sourced at **log in** from ``~/.bash_profile``. This is especially useful in **macos**, where all sessions are **login shells**, and ``~/.bash_profile`` is read everytime we open a new terminal.


.. _`login manager`: https://en.wikipedia.org/wiki/Login_manager
.. _`desktop environment`: https://en.wikipedia.org/wiki/Desktop_environment
.. _`XDM`: https://en.wikipedia.org/wiki/X_display_manager
.. _`GNOME Display Manager`: https://en.wikipedia.org/wiki/GNOME_Display_Manager
.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
.. _`SSH`: https://en.wikipedia.org/wiki/Secure_Shell
.. _`virtual console`: https://en.wikipedia.org/wiki/Virtual_console
