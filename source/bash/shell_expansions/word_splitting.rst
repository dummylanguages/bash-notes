**************
Word splitting
**************
`Word splitting`_ occurs after any of the following expansions has happened:

* `Parameter and variable expansion`_
* `Arithmetic expansion`_
* `Command substitution`_

Bash will scan the results of these expansions for the characters that mark that mark word boundaries, which by **default** are:

* The **space** character.
* The **tab** character.
* The **newline** character.

For example::

    $ title='I am Legend'
    $ for word in $title; do echo $word; done
    I
    am
    Legend

The ``title`` variable was created as a **single string**. This variable is expanded (``$title`` parameter expansion) in the for loop, and as you can see, split into **three words**: ``I``, ``am`` and ``Legend``.

No expansion, no word splitting
===============================
Note that if no expansion occurs, no splitting is performed. For example::

    $ for word in 'I am Legend'; do echo $word; done
    I am Legend

In this case we are not expanding any variable, but using a string directly.

Double quotes, no word splitting
================================
Let's go back to the first example, but this time let's enclose the expanded variable inside **double quotes**, like this ``"$title"``::

    $ for word in "$title"; do echo $word; done
    I am Legend

Internal Field Separator
========================
As long as `IFS`_ is not set, the word separators are **spaces**, **tabs** or **newline** characters. But if we set ``IFS`` to something other than that::

    $ IFS=-
    $ for word in $title; do echo $word; done
    I am Legend

The spaces are not considered as separators, hence no word splitting occurs. But if the string contains dashes::

    $ title='I- am-   legend'
    $ for word in $title; do echo $word; done
    I
     am
       legend

Note how the dashes are not even printed, they are interpreted as **field separators**. Also, the spaces are interpreted as part of the words, ``□am`` and ``□□□legend``. (The ``□`` character represents a space)

.. warning:: Don't forget to ``unset IFS`` to avoid unexpected behaviour.

.. _`Word splitting`: https://www.gnu.org/software/bash/manual/html_node/Word-Splitting.html#Word-Splitting
.. _`IFS`: https://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Variables.html#index-IFS
.. _`Parameter and variable expansion`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
.. _`Command substitution`: https://www.gnu.org/software/bash/manual/html_node/Command-Substitution.html#Command-Substitution
.. _`Arithmetic expansion`: https://www.gnu.org/software/bash/manual/html_node/Arithmetic-Expansion.html#Arithmetic-Expansion

