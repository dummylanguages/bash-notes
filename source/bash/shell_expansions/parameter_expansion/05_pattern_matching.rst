****************
Pattern matching
****************
There are several syntaxes where `pattern matching`_ is used to match sections of strings that we want to remove or replace.

Substring removal
=================
We can expand only a part of a string, according to pattern matching rules. There are 4 different syntaxes:

1. To remove from the **beginning of the string** we have two variations:

* This one, ``${parameter#pattern}``, will remove the **shortest match**, which usually is one character:

.. code-block:: bash

    $ var="The quick fox jumped over the brown dog"
    $ echo ${var#The quick}
    fox jumped over the brown 
    
Or if we wanted to remove the first four characters on a string, no matter what they are:

.. code-block:: bash

    $ var="The quick fox jumped over the brown dog"
    $ echo ${var#????}
    quick fox jumped over the brown

We can use **character classes**:

.. code-block:: bash

    $ v=1234abcd
    $ echo ${v#*[[:digit:]]}
    234abcd

* The other syntax, ``${parameter##pattern}``, will remove **longest match**:

.. code-block:: bash

    $ v=1234abcd
    $ echo ${v##*[[:digit:]]}
    abcd

2. To remove from the **end of the string** we have also two versions:

* ``${parameter%pattern}`` will remove the **shortest match**:

.. code-block:: bash

    $ v=1234abcd
    $ echo ${v%[[:alpha:]]}
    1234abc

* ``${parameter%%pattern}`` the **longest match**:

.. code-block:: bash

    $ v=1234abcd
    $ echo ${v%%[[:alpha:]]*}
    1234

String substitution
===================
This technique is way more useful and flexible than the last one, since it allows us to **search and replace** a matched section of a string, which means that we can also **remove** it, if we replace it by an empty string! The general syntax is::

    ${parameter/pattern/string}

For example:

.. code-block:: bash

    $ var="The quick fox jumped over the brown dog"
    $ echo ${var/fox/frog}
    The quick frog jumped over the brown dog

The command above will replace only the **first match** of the pattern, in this case ``fox``. If we want to replace all occurrences of a pattern we would use two slashes:

.. code-block:: bash

    $ echo ${var//?he/a}
    a quick fox jumped over a brown dog

Removal
-------
There's a variation of this syntax that allows us to **remove** the matched section::

    ${parameter/pattern/string}

For example:

.. code-block:: bash

    $ echo ${var/quick}
    The fox jumped over the brown dog
    $ echo ${var//?he}
    quick fox jumped over brown dog

That removes all matches of the ``?he`` three-characters word.

Anchoring the patterns
----------------------
This syntax also allows us to anchor the patterns to match at the beginning or the end of the string. For example, if we tried to replace the **extension** of a file:

.. code-block:: bash

    $ file='beautiful_jpeg.jpeg'
    $ echo ${file/%jpeg/jpg}
    beautiful_jpeg.jpg

We can anchor the pattern to the **beginning** to remove all directories before a filename:

.. code-block:: bash

    $ file='/home/bob/Music/Satie/Gnossienne no.1.flac'
    $ echo ${file/*\/}
    Gnossienne no.1.flac

To specify a **slash character** in our pattern, we have to escape it with a **backslash**, ``\/``. So the expression ``*\/`` will match **everything** until the last ``/``.

.. _`pattern matching`: https://www.gnu.org/software/bash/manual/html_node/Pattern-Matching.html
