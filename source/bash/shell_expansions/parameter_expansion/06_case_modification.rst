*****************
Case modification
*****************
This expansion modifies the case of alphabetic characters in ``parameter``. There are several **operators**:

* The ``^`` operator converts the **first character** matching the pattern **to uppercase**.
* The ``,`` operator converts the **first character** matching the pattern **to lowercase**.
* The ``^^`` operator converts **any character** matching the pattern **to uppercase**.
* The ``,,`` operator converts **any character** matching the pattern **to lowercase**.

Modifying the first character
=============================
There are a couple of options here:

* Omiting a **pattern**, will modify the case of the first character no matter what. The syntaxes look like this:

    ${parameter^} # 1st character to uppercase
    ${parameter,} # 1st character to lowercase

* Providing a **pattern** will modify the case of the first character only if it matches the provided pattern. Syntaxes:

    ${parameter^pattern} # 1st character to uppercase, if it matches the pattern
    ${parameter,pattern} # 1st character to lowercase, if it matches the pattern

The patterns follow the same `pattern matching`_ rules than `filename expansion`_. As a reminder of the basics:

* ``w`` will match a literal ``w``.
* ``[wr]`` will match any ``w`` or ``r``.
* ``[a-d]`` will match the range of characters between ``a`` and ``d``.
* ``?`` will match any single character.
* ``*`` will match any string.

Omitting pattern
----------------
For example::

    $ greeting=hello
    $ echo ${greeting^}
    Hello

If pattern is omitted, it is treated like a ``?``, which matches every character. So in the command above, the first character is modified always.

Specifying a pattern
--------------------
Let's specify a pattern::

    $ echo ${greeting^a}
    hello

The expansion above converts the first character to uppercase only if it is an ``a``; since in this case is not, no case modification takes place.

Modifying all occurrences
=========================
If we want to modify all occurrences of a certain character no matter its position; the ``^^`` operator converts **to uppercase**, while the ``,,`` converts **to lowercase**. The syntaxes look like this::

    ${parameter,,pattern}
    ${parameter^^pattern}

If we don't specify a pattern, the whole word will be modified. For example::

    $ fruit=STRAWBERRY
    $ echo ${fruit,,}
    strawberry

Let's see what happens when we specify a **pattern**::

    $ greeting=hello
    $ echo ${greeting^^l}
    heLLo

To change to **lowercase** we must to specify the pattern in uppercase::

    $ echo ${fruit,,R}
    STrAWBErrY

A more involved pattern::

    $ echo ${greeting^^[!l]}
    HEllO

Note that if we try to use the pattern ``(@l)`` to match only the first ``l``, it wouldn't work since the ``^^`` operator modify **all** occurrences of the pattern.

Testing the case of strings
===========================
These operators can be useful to test the case of a string. For example, we can figure out if a string contains **only uppercase** characters, by converting the whole string to lowercase::

    $ thing=HELLO
    $ [[ ${thing^^} == $thing ]] && echo "The string didn't contain any lowercase characters."

Then we compare the **modified string** with the **original one**, if the strings are identical, that means it hasn't been modified because it didn't contain any lowercase characters ;-)

.. note:: To check if a string contains **only lowercase characters**, we would use ``${thing,,}`` to convert it to uppercase and then change if the string has been modified.

Positional parameters
=====================
If parameter is ``@`` or ``*``, the case modification operation is applied to each `positional parameter`_ in turn, and the expansion is the resultant list.


Array parameters
================
If parameter is an **array variable** subscripted with ``@`` or ``*``, the case modification operation is applied to each member of the array in turn, and the expansion is the resultant list.


.. _`positional parameter`: https://www.gnu.org/software/bash/manual/html_node/Positional-Parameters.html#Positional-Parameters
.. _`nameref`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameters.html#Shell-Parameters
.. _`filename expansion`: https://www.gnu.org/software/bash/manual/html_node/Filename-Expansion.html
.. _`pattern matching`: https://www.gnu.org/software/bash/manual/html_node/Pattern-Matching.html
.. _`positional parameters`: https://www.gnu.org/software/bash/manual/html_node/Positional-Parameters.html#Positional-Parameters
