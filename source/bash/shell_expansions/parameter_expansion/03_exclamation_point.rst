****************************
Exclamation point expansions
****************************
As we said at the beginning, the synopsis of parameter expansion is::

    ${parameter}

When the first character of parameter is an **exclamation point** ``!``, Bash performs several types of parameter expansion.

Indirect expansion
==================
If the first character of parameter is an **exclamation point** ``!``, a level of **variable indirection** is introduced. For example:

.. code-block:: bash

    #!/usr/bin/bash

    width=34
    height=27

    param1=width
    param2=height

    echo "Area: ${!param1}m x ${!param2}m = $(( ${!param1} * ${!param2} )) square meters."

In the example above, we're referencing the variables ``width`` and ``height`` **indirectly**, through the variables ``param1`` and ``param2``.

.. note:: The **exclamation point** must immediately follow the left brace in order to introduce indirection.

The value is subject to tilde expansion, parameter expansion, command substitution, and arithmetic expansion.

.. warning:: Find some examples to clarify this last sentence...

Using a nameref as parameter
----------------------------
If ``parameter`` is a `nameref`_, this expands to the **name of the variable** referenced by parameter instead of performing **indirect expansion**. (That would be two levels of indirection) Check what we wrote in our section dedicated to :ref:`nameref-label`.

Exceptions
==========
The GNU Bash manual mentions the following types of parameter expansion as exceptions to **Indirect expansion**.  is **not** performed in two cases:

1. ``${!prefix*}`` or ``${!prefix@}``: These expressions expand to the **names of variables** whose names begin with prefix, separated by the first character of the IFS special variable. When ``@`` is used and the expansion appears within **double quotes**, each variable name expands to a separate word.

2. ``${!name[@]}`` or ``${!name[*]}``: Here we may have two situations:

    * If name is an **array variable**, expands to the **list of array indices** (keys) assigned in name. When ``@`` is used and the expansion appears within **double quotes**, each key expands to a separate word.
    * If name is **not an array**, expands to ``0`` if name is set and ``null`` otherwise. 




.. _`positional parameter`: https://www.gnu.org/software/bash/manual/html_node/Positional-Parameters.html#Positional-Parameters
.. _`nameref`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameters.html#Shell-Parameters
