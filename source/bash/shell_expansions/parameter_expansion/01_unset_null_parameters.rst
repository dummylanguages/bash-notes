************************
Unset or null parameters
************************
Bash can test if a parameter is **unset** (it doesn't exist) or **null** (contains empty string). In all of the following syntaxes, we can use two versions of the operator:

1. If the **colon** is included, the operator tests for both parameter’s **existence** and that its value is **not null**.
2. If the colon is omitted, the operator tests only for **existence**.

Substitute a default value
==========================
If parameter is unset or null, the expansion of word is substituted. Otherwise, the value of parameter is substituted. Syntax::

    ${parameter:-word}

This is quite useful when an expected positional parameter is not provided to the script.

Assign a default value
======================
If parameter is unset or null, the expansion of word is **assigned** to parameter. The value of parameter is then used. Syntax::

    ${parameter:=word}

.. note:: `Positional parameters`_ and `special parameters`_ may not be assigned to in this way.

Write to standard error
=======================
If parameter is null or unset, the expansion of word (or a message to that effect if word is not present) is written to the **standard error** and the shell, if it is not interactive, **exits**. Otherwise, the value of parameter is substituted. Syntax::

    ${parameter:?word}

The oposite of substitute
=========================
If parameter is null or unset, nothing is substituted, otherwise the expansion of word is substituted. Syntax::

    ${parameter:+word}

.. _`Positional parameters`: https://www.gnu.org/software/bash/manual/html_node/Positional-Parameters.html
.. _`special parameters`: https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html
