*********
Basic use
*********
The most basic use of this feature would be to check some **environment variable**, for example::

    $ echo $HOME
    /home/bob

Or any variable we could create::

    $ nickname=Bobby
    $ echo $nickname
    Bobby

Optional braces
---------------
In the examples above, the use of **curly braces** surrounding the variable name is **optional**. We could have written::

    $ echo ${HOME}
    /home/bob

Or::

    $ nickname=Bobby
    $ echo ${nickname}
    Bobby

In these cases it makes no difference, but apart from the most simple cases, **curly braces** are required.

.. note:: Even though **curly braces** are not required in every situation, getting used to **add them always** it's a **good programming practice**, both for consistency and to avoid surprises.

Required use of braces
======================
Curly braces are required in several scenarios:

1. To protect the **variable** to be expanded, from characters immediately following it, which could be interpreted as part of the name. For example:

.. code-block:: bash

    $ basename=foo
    $ mkdir ${basename}bar

Withouth the braces the directory would be named ``basenamebar``, instead of ``foobar``.

2. When expanding a `positional parameter`_ with **more than one digit**. For example, let's use the `set builtin`_ to create a bunch of positional parameters:

.. code-block:: bash

    $ set -- 1st 2nd 3rd 4th 5th 6th 7th 8th 9th Apple Beach

If we want to access the positional parameter at the tenth position, the word ``Apple``, and we try this:

.. code-block:: bash

    $ echo $10
    1st0

We are getting the **first parameter**, followed by a ``0``, clearly not what we wanted. Let's try with **curly braces** now:

.. code-block:: bash

    $ echo ${10}
    Apple

3. Curly braces are also required when accessing **array elements**:

.. code-block:: bash

    $ names=(Bob Lynda Tyrell)
    $ echo ${names[1]}
    Lynda


.. _`positional parameter`: https://www.gnu.org/software/bash/manual/html_node/Positional-Parameters.html#Positional-Parameters
.. _`set builtin`: https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
.. _`nameref`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameters.html#Shell-Parameters
