###################
Parameter expansion
###################

.. toctree::
	:maxdepth: 3
	:hidden:

	00_basic_use
	01_unset_null_parameters
	02_substring_expansion
	03_exclamation_point
	04_length
	05_pattern_matching
	06_case_modification
	07_operator
	08_slicing

`Parameter expansion`_ is used to expand the value of parameters. The basic form of parameter expansion is::

    ${parameter}

Where ``parameter`` can be:

* A **variable**, which is a parameter denoted by a name chosen by us.
* A positional parameter.
* A special parameter.
* An array reference.

We are going to see quite a lot of weird syntaxes in this section, some of them only apply to Bash and are not portable. If portability is not a goal, becoming familiar with **all** of these syntaxes can help to make our Bash scripts more compact.

.. note:: valuable information here: https://wiki-dev.bash-hackers.org/syntax/pe

.. _`Parameter expansion`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
