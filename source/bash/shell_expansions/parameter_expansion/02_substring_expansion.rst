*******************
Substring expansion
*******************
The **general syntax** of this type of parameter expansion is:

.. code-block:: bash

    ${parameter:offset}

Optionally, we can specify a *length*:

.. code-block:: bash

    ${parameter:offset:length}

There are different types of expansion grouped under this category, which come into play depending on the value of ``parameter``, which can be:

1. A **variable** containing a string (Can be a literal string too). Here we'll also see:

    * A **positional parameter** that evaluates to a string.
    * An **array member** that evaluates to a string.

2. The ``@`` character, which refers to all **positional arguments**.
3. An **indexed array** subscripted by ``@`` or ``*``, to refer to **all** the elements in the array.
4. An **associative array** name.

String parameters
=================
When ``parameter`` is a **string**, the expression expands to up to ``length`` characters of the value of parameter starting at the character specified by ``offset``. This is referred to as **Substring Expansion**. For example:

.. code-block:: bash

    $ string=01234567890abcdefgh
    $ echo ${string:3:2}
    34

.. note:: Substring indexing is **zero-based**.

Omitting the length
-------------------
If ``length`` is omitted, it expands to the substring of the value of parameter starting at the character specified by offset and extending to the end of the value:

.. code-block:: bash

    $ echo ${string:7}
    7890abcdefgh

.. note:: For ``length`` and ``offset`` we can use `arithmetic expansion`_.

Offset and length
-----------------
The ``offset`` specifies the initial character of the substring, and the ``length``, well the length of the substring:

.. code-block:: bash

    $ echo ${string:7:2}
    78

We get a substring of length two characters that starts at the 7th position. If we used a **negative length**:

.. code-block:: bash

    $ echo ${string:7:-2}   # From index 7 until the end, except the last two elements.
    7890abcdef

Negative offset
---------------
If ``offset`` evaluates to a number less than zero, let's say ``-7``, we get a substring that starts at the 7th character counting from the right. In other words, it contains the last seven characters. For example:

.. code-block:: bash

    $ echo ${string: -7}    # The last seven elements
    bcdefgh

.. note:: Note that a negative offset must be separated from the colon by at least one space to avoid being confused with the ``:-`` expansion.

In this context, specifying a ``length`` allows us to determine the length of the string:

.. code-block:: bash

    $ echo ${string: -7:0}  # From the last seven elements, none.
    
    $ echo ${string: -7:2}  # From the last seven elements, just two.
    bc

Negative length
---------------
A **negative length** here also excludes the last ``length`` characters:

.. code-block:: bash

    $ echo ${string: -7:-2} # From the last seven elements, except the two last ones
    bcdef

If length evaluates to a number less than zero, it is interpreted as an offset in characters from the last character in the string rather than a number of characters, and the expansion is the characters between the two offsets.

Single positional parameter
---------------------------
The string can also be a **single positional parameter**. For example:

.. code-block:: bash

    $ set -- 01234567890abcdefgh
    $ echo ${1:7}
    7890abcdefgh
    $ echo ${1:7:0}

    $ echo ${1:7:2}
    78
    $ echo ${1:7:-2}
    7890abcdef
    $ echo ${1: -7}
    bcdefgh
    $ echo ${1: -7:0}

    $ echo ${1: -7:2}
    bc
    $ echo ${1: -7:-2}
    bcdef

Nothing surprising here, it's still a string. A bit later we'll see how to use substring expansion with **all** positional parameters.

Array elements
--------------
We can also use an **array element** to specify the string:

.. code-block:: bash

    $ array[0]=01234567890abcdefgh
    $ echo ${array[0]:7}
    7890abcdefgh
    $ echo ${array[0]:7:0}

    $ echo ${array[0]:7:2}
    78
    $ echo ${array[0]:7:-2}
    7890abcdef
    $ echo ${array[0]: -7}
    bcdefgh
    $ echo ${array[0]: -7:0}

    $ echo ${array[0]: -7:2}
    bc
    $ echo ${array[0]: -7:-2}
    bcdef

As in the section, the element at ``array[0]`` is still a string. Next we'll see how to use substring in **all** the elements of an array, so we can **slice** it.

Array parameters
================
We can use substring expansion on arrays, to get slices of it. In this case, the value of ``parameter`` is not a **string** but a whole **array**. For example:

.. code-block:: bash

    $ array=(0 1 2 3 4 5 6 7 8 9 0 a b c d e f g h)
    $ echo ${array[@]:7}
    7 8 9 0 a b c d e f g h

In the example above, we are using an **array name** subscripted by ``@`` or ``*``. Since we only specify the ``offset``, it expands to all members of the array starting at the index ``7`` up until the end. If we used an ``offset`` of ``0``, we would get the whole array:

.. code-block:: bash

    $ echo ${array[@]:0}
    0 1 2 3 4 5 6 7 8 9 0 a b c d e f g h

If we also specify a ``length``, it expands to the first ``length`` members:

.. code-block:: bash

    $ echo ${array[@]:7:2}
    7 8
    $ echo ${array[@]:0:2}
    0 1

A negative offset
-----------------
A **negative offset** will expand to the last ``offset`` members of the array:

.. code-block:: bash

    $ echo ${array[@]: -3}
    f g h

In this context, using a ``length`` will give us the first ``length`` members:

.. code-block:: bash

    $ echo ${array[@]: -7:2}
    b c
    $ echo ${array[@]: -7:0}    # A length 0 gives no elements

It is an **expansion error** if ``length`` evaluates to a number **less than zero**:

.. code-block:: bash

    $ echo ${array[@]: -7:-2}
    bash: -2: substring expression < 0

.. warning:: Substring expansion applied to an **associative array** produces undefined results.

Positional parameters
=====================
When parameter is ``@``, we are dealing with all the positional parameters. It works the same as **substring expansion**, just think of each positional parameter as a character in a string.

Only offset
-----------
The simplest scenario. For example:

.. code-block:: bash

    $ set -- 1 2 3 4 5 6 7 8 9 0 a b c d e f g h
    $ echo ${@:7}
    7 8 9 0 a b c d e f g h

We get from the 7th parameter until the last one.

Offset and length
-----------------
The simplest scenario. For example:

.. code-block:: bash

    $ echo ${@:7:0}

    $ echo ${@:7:2}
    7 8
    $ echo ${@:7:-2}
    bash: -2: substring expression < 0

Note that in this case, it is an expansion **error** if ``length`` evaluates to **a number less than zero**.

Negative offsets
----------------
A negative offset means that we start counting from the end, so an ``offset`` of ``-1`` evaluates to the **last positional parameter**. We can specify lengths too:

.. code-block:: bash

    $ echo ${@: -7:0}

    $ echo ${@: -7:2}
    b c

One-based indexing
------------------
We saw how substring indexing is **zero-based**. When **positional parameters** are used, the **indexing starts at 1** by default. If ``offset`` is ``0``, and the positional parameters are used, ``$@`` is prefixed to the list.

.. code-block:: bash

    $ echo ${@:0}
    ./bash 1 2 3 4 5 6 7 8 9 0 a b c d e f g h
    $ echo ${@:0:2}
    ./bash 1

In this case Bash itself is added to the list. If we were running a script, it would be the script name.

.. _`positional parameter`: https://www.gnu.org/software/bash/manual/html_node/Positional-Parameters.html#Positional-Parameters
.. _`nameref`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameters.html#Shell-Parameters
.. _`arithmetic expansion`: https://www.gnu.org/software/bash/manual/html_node/Arithmetic-Expansion.html
