*******
Slicing
*******
Here we'll see how to create **slices** using **substring expansion**, which is a type of `parameter expansion`_. We'll apply these techniques to **strings** but they work the same with **arrays** and **positional parameters**.

Let's remind ourselves of the syntax for **substring expansion**:

.. code-block:: bash

    ${parameter:offset}

Optionally, we can specify a *length*:

.. code-block:: bash

    ${parameter:offset:length}

The **GNU Bash manual** mentions briefly:

    ``length`` and ``offset`` are **arithmetic expressions**

This sentence has great useful implications.

Using arithmetic expressions
============================
Consider the string variable:

.. code-block:: bash

    $ var='The quick brown fox'
    $ echo ${#var}
    19
    $ echo ${var:19}

    $ echo ${var:0:1}
    T
    $ echo ${var:18}
    x
    $ echo ${var:19 - 1}
    x
    $ echo ${var:- 1}
    x

We can tell it's 43 characters long; the **first character** is indexed at ``0``, and the **last character** is at index ``18``.

Taking slices
-------------
1. Let's slice the word ``quick`` out of the string. If we count, the ``q`` is at index ``4``, and the word ``quick``, plus its trailing **space** is six characters long so:

.. code-block:: bash

    $ echo "|${var:4:6}|"
    |quick |

.. note:: Note that we've included **pipe characters** surrounding the slice, to show that we've taken the trailing space after ``The``.

2. Now let's slice the first part of the string, we start from the beginning up until the ``q`` which is at index ``4``:

.. code-block:: bash

    $ echo "|${var:0:4}|"
    |The |

.. note:: Note that we've included **pipe characters** surrounding the slice, to show that we've taken the trailing space after ``The``.

We can also specify the first character with a **negative index**:

.. code-block:: bash

    $ length=${#var}
    $ echo "|${var: - $length:4}|"
    |The |


3. To slice the last part of the string (``brown fox``) we can use the start of ``quick`` as our reference:

.. code-block:: bash

    $ length=${#var}
    $ echo "|${var: 4 - $length}|"
    |quick brown fox|

We're using arithmetic here; we've assigned the lenght of the string to the ``length`` variable, so everything looks clearer. The expression ``4 - $length`` equals 15, check it out:

.. code-block:: bash

    $ echo $((4 - $length))
    -15

So we are printing the 15 last characters of the string: we start at index ``-15`` (counting from the right), and since we don't specify a length, print up until the end of the string. But we don't want the word ``quick`` in our slice, and we know that this word is five characters long, **six** if we count the trailing space, so let's use more arithmetic:

.. code-block:: bash

    $ echo "|${var: 4 + 6 - $length}|"
    |brown fox|


.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
