**************
Using operator
**************
The last `parameter expansion`_ syntax mentioned in the GNU Bash manual has the general form::

    ${parameter@operator}

Where operator is a single letter which will transfor the value of parameter or show information about it.

Quoted string
=============
The first version uses the ``Q`` operator::

    ${parameter@Q}

The expansion is a string that is the value of parameter **quoted** in a format that can be reused as input. For example::

    $ var="John's"
    $ echo $var
    John's
    $ echo -e ${var@Q}
    'John'\''s'

This is useful for example when an associative array key has an apostrophe inside::

    $ declare -A dict
    $ name="John's"
    $ dict["$name"]=45
    $ ((dict[$name]++))
    -bash: dict[John's]++: bad array subscript (error token is "dict[John's]++")

We're getting an error because when ``$name`` expands, Bash is expecting the othe single quote. We need that expansion **quoted**::

    $ ((dict[${name@Q}]++))
    $ echo ${dict[$name]}
    46

Visualize IFS
-------------
We know that when IFS is not set, by default equals to ``space tab newline``, let's visualize it::

    $ echo ${IFS@Q}
    $' \t\n'

As you can see, it is expanded as an `ANSI-C quoted string`_. We would get the same result using the ``%q`` format specifier of ``printf``::

    $ printf %q "$IFS"
    $' \t\n

Escape characters expanded
==========================
The expansion is a string that is the value of parameter with backslash escape sequences expanded as with the $'…' quoting mechanism.

P
The expansion is a string that is the result of expanding the value of parameter as if it were a prompt string (see Controlling the Prompt).

A
The expansion is a string in the form of an assignment statement or declare command that, if evaluated, will recreate parameter with its attributes and value.


Listing parameter attributes
============================
The last operator is ``a``, which produces a string consisting of flag values representing parameter’s attributes, for example::

    $ declare -ai array_integers
    $ echo ${array_integers@a}
    ai

.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
.. _`ANSI-C quoted string`: https://www.gnu.org/software/bash/manual/html_node/ANSI_002dC-Quoting.html
