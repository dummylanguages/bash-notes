***********************
Length of the parameter
***********************
The following basic syntax::

    ${#parameter}

Can be used to get the length of parameter, which may refer to several things:

* The number of characters in a string.
* The number of elements in an array.
* The number of positional parameters.





.. _`positional parameter`: https://www.gnu.org/software/bash/manual/html_node/Positional-Parameters.html#Positional-Parameters
.. _`nameref`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameters.html#Shell-Parameters
