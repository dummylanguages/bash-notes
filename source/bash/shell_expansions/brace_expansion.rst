****************
Brace expansion
****************
`Brace expansion`_ is a mechanism used to create arbitrary strings. It works expanding **patterns** which take the general form:

    preamble{sequence expression}postcript

The **preamble** and the **postcript** are optional. Let's see some examples:

* Using a **preamble**::

    $ echo background{.png,.jpeg,.jpg}
    background.png background.jpeg background.jpg

* Or a **postcript**::

    $ echo {monkey,dog,cat}.jpg
    monkey.jpg dog.jpg cat.jpg

* Or both a **preamble** and a **postcript**::

    $ echo co{mplica,nversa,llec}tion
    complication conversation collection

* Or not use any of them::

    $ echo {one,two,three}
    one two three

* We don't have to fill all the positions between commas. For example, find the lonely one::

    $ echo {pant,ab,,M}alone
    pantalone abalone alone Malone

This last one has an interesting application when creating backup files.

Ranges
======
We can generate a range of numbers inside the expression::

    $ echo {1..5}
    1 2 3 4 5

Or just **even** numbers withing a range specifying a **step value**::

    $ echo {0..10..2}
    0 2 4 6 8 10

This one is cool::
    
    $ echo {0..100..10}
    0 10 20 30 40 50 60 70 80 90 100

Or a range of characters::

    $ echo {A..H}
    A B C D E F G H

Variables
=========
We can use variables inside the curly braces, for example::

    $ e=2.71828; pi=3.14
    $ echo {$pi,$e}
    3.14 2.71828

Command substitution inside brace expansion
===========================================
We'll see `command substitution`_ is a bit later, here we want to show how we can use it inside curly braces::

    $ echo {`echo hello`,`echo world`}
    hello world

We can use the other version too::

    $ echo {$(echo hello),$(echo world)}
    hello world

Backup idiom
============
We'll use this idiom to show a use case that may be useful in different scenarios. Imagine we have a file with a very long name::

    $ touch a_very_long_filename.txt

And we want to create a backup copy of it. We could do this::

    $ cp a_very_long_filename.txt a_very_long_filename.txt.bak

Or we could use the ``{,.bak}`` idiom so we don't have to type the name of the file twice::

    $ cp a_very_long_filename.txt{,.bak}

Notice how we leave empty the position before the first comma.

Directory structures
====================
Some examples creating directory structures. Imagine we want to create something like this::

    base/
        branch-a/
        branch-b/
            
We could do::

    $ mkdir -p base/branch-a base/branch-b

But using brace expansion may save a lot of typing, specially when directory names are long::

    $ mkdir -p base/branch-{a,b}

If we change our mind about the branches and want to keep just the base folder::

    $ rmdir base/branch-{a,b}


.. _`Brace expansion`: https://www.gnu.org/software/bash/manual/html_node/Brace-Expansion.html#Brace-Expansion
.. _`command substitution`: https://www.gnu.org/software/bash/manual/html_node/Command-Substitution.html#Command-Substitution
