********************
Arithmetic expansion
********************
`Arithmetic expansion`_ allows to perform arithmetic computations with integer values and substitute in the result. The format for arithmetic expansion is::

    $(( expression ))

For example:

.. code-block:: bash

    $ echo $(( 2 + 2 ))
	4

In the command above, Bash will **evaluate** the arithmetic expression and **expand** the resulting value. At the end of it, an **exit status** is produced:

* If the arithmetic expression is formulated **correctly**, we get a ``0`` exit status. (you can check it out using the `$?`_ special parameter.)
* But if the expression contains some mistake, we'll get a ``1`` exit status, plus an error in our standard error. For example:

.. code-block:: bash

    $ echo $(( 2 / 0 ))
    -bash: 2/0: division by 0 (error token is "0")
    $ echo $?
    1

The arithmetic expression is evaluated according to the rules for `shell arithmetic`_ used by the `((`_ compound command.

Arithmetic Expansion vs Arithmetic Evaluation
=============================================
These two concepts are quite close, but a small explanation won't hurt:

1. Arithmetic expansion **evaluates** and **expands** to the value resulting from the evaluation. The syntax::

    $(( expression ))

2. Arithmetic evaluation only **evaluates**. The syntax is quite similar::

    (( expression ))

If we try to print out the result of **arithmetic evaluation**:

.. code-block:: bash

    $ echo ((5 + 3))
    -bash: syntax error near unexpected token '('
    $ echo $?
    2

We'll get an **error** and a **non-zero** exit status, because ``echo`` is expecting a **string** as an argument, and **arithmetic evaluation** doesn't expand to one, it only evaluates.

When to use each
----------------
1. Use **arithmetic expansion** whenever you need a value to print (like we saw before) or to assign to a variable. For example:

.. code-block:: bash

    $ result=$(( 3 * 4 ))
    $ echo $result
    12

2. **Arithmetic evaluation** can be used when we don't need a value, and it can be used in different situations. Inside **loops** as an iteration control variable:

.. code-block:: bash

    $ counter=0
    $ while (($counter < 3))
    > do
    > echo $counter
    > (( counter++ ))
    > done
    0
    1
    2

In the example above, we're using **arithmetic evaluation** in two places:

* The expression ``(($counter < 3))`` is evaluated, and its **exit status** is used to control the number of iterations.
* Also, we use arithmetic evaluation to increment the control variable, ``(( counter++ ))``, so we don't get trapped in an infinite loop.

In both of these cases, a string with the result is not needed at all; in fact, it would throw ``command not found`` errors when Bash tries to interpret the expanded results(``0``, ``1``...) as commands.

Arithmetic evaluation in conditional constructs
-----------------------------------------------
The ``((`` compound command is described in the `conditional constructs`_ section of the GNU Bash manual, that should give you an idea of its importance in this scenario. For example:

.. code-block:: bash

    $ if ((2 > 0))
    > then
    > echo "2 is greater than 0"
    > fi
    2 is greater than 0

The expression ``((2 > 0))`` evaluates to ``1``, so the message is printed. In this example the expression is super simple, but imagine a script that is using a complex arithmetic expression:

.. code-block:: bash

    $ set -- 2 3 4
    $ if (($1 + $2 > $3 || $1 + $3 > $2 || $2 + $3 > $1))
    > then
    > echo "It's a valid triangle."
    > fi
    2 is greater than 0

When working with hairy expressions like the one above, it's useful to remember that we can use **arithmetic expansion** to confirm that our logic is fine:

.. code-block:: bash

    $ echo $(( $1 + $2 > $3 || $1 + $3 > $2 || $2 + $3 > $1 ))
    1

.. note:: For a full list of the operators that can be used in arithmetic expansion, check :ref:`our other section <shell_arithmetic_label>` about it.

.. _`Arithmetic expansion`: https://www.gnu.org/software/bash/manual/html_node/Arithmetic-Expansion.html
.. _`shell arithmetic`: https://www.gnu.org/software/bash/manual/html_node/Shell-Arithmetic.html
.. _`((`: https://www.gnu.org/software/bash/manual/html_node/Conditional-Constructs.html
.. _`$?`: https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html
.. _`conditional constructs`: https://www.gnu.org/software/bash/manual/bash.html#Conditional-Constructs

