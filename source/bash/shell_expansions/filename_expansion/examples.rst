*******************
Examples with ``*``
*******************
Let's create a small file structure to demonstrate how the ``*`` glob works::

    $ mkdir dir_zero && cd $_
    $ mkdir -p dir{1..2}/subdir{1,2}
    $ touch file{A,B}.txt

As a result of the commands above we should have the following directory tree::

    $ tree -L 3
    .
    |-- dir1
    |   |-- subdir1
    |   `-- subdir2
    |-- dir2
    |   |-- subdir1
    |   `-- subdir2
    |-- fileA.txt
    `-- fileB.txt

Matching directories
^^^^^^^^^^^^^^^^^^^^
At the beginning of this chapter we saw how the ``*`` glob expands to all the **files** and **directories** in the current directory. If we want to match only **directories** we can use the glob ``*/``::

    $ echo */
    dir1/ dir2/

Note how ``fileA`` and ``fileB`` are not shown.

Matching subdirectories
^^^^^^^^^^^^^^^^^^^^^^^
To show **subdirectories** also, use ``**/``::

    $ for d in **/; do echo $d; done
    dir1/
    dir1/subdir1/
    dir1/subdir2/
    dir2/
    dir2/subdir1/
    dir2/subdir2/

The ``**`` or ``**/`` glob will only work if the ``globstar`` shell option is **on**. You can check if it's on running::

    $ shopt | grep globstar
    globstar       	on

If it's **off**, you can turn it on running the `shopt builtin command`_::

    $ shopt -s globstar

We can **disable** it using the ``-u`` option.

Matching directories, subdirectories and files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The title says it all, run::

    $ for df in **; do echo $df; done
    dir1
    dir1/subdir1
    dir1/subdir2
    dir2
    dir2/subdir1
    dir2/subdir2
    fileA.txt
    fileB.txt

Anchored
^^^^^^^^
All globs are implicitly anchored at both start and end. For example::

    $ echo *.txt
    fileA.txt fileB.txt

Or::

    $ echo file*
    fileA.txt fileB.txt

No matches: enabling ``nullglob``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
When there are no matches, the globs expand to their literal characters, for example::

    $ echo *.png
    *.png

Since there are no ``.png`` files in the current directory we get the glob back as output. We can change that behaviour setting up the ``nullglob`` sell option, which by default is off::

    $ shopt | grep nullglob
    nullglob       	off
    $ shopt -s nullglob
    $ shopt | grep nullglob
    nullglob       	on

Now if we check for ``.png`` files::

    $ echo *.png

We don't get any output at all. This option may seem convenient to save our script from throwing errors when globbing fails, but **null globbing** is not specified by POSIX. In portable scripts, you must explicitly check that a glob match was successful by checking that the files actually exist(check `Greg's wiki`_ for ideas).


.. _`filename expansion`: https://www.gnu.org/software/bash/manual/html_node/Filename-Expansion.html#Filename-Expansion
.. _`pattern matching`: https://www.gnu.org/software/bash/manual/html_node/Pattern-Matching.html#Pattern-Matching
.. _`shopt builtin command`: https://www.gnu.org/software/bash/manual/html_node/The-Shopt-Builtin.html#The-Shopt-Builtin
.. _`Greg's wiki`: https://mywiki.wooledge.org/glob

