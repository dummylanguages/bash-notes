##################
Filename expansion
##################

.. toctree::
	:maxdepth: 3
	:hidden:

	examples

Bash itself cannot recognize **Regular Expressions**, it depends on external utilities such as ``sed`` and ```awk``` to do so. But Bash is capable of `filename expansion`_ also known as `globbing`_, which allows us to use certain **filename wildcards** to create patterns to match filenames. For example::

    $ echo *
    background.png Documents Downloads Movies Music Pictures poem.txt Public

You may be wondering what's going on, why is the ``echo`` command even listing files and directories? That's because the ``*`` expands to all the files and directories in the **current directory**, hence filename expansion, and ``echo`` simply prints that to its standard output.

.. note:: Note that the files and directories appear **alphabetically sorted**.

The `pattern matching`_ section of the official Bash documentation explains how to use these wildcard characters. The following table contains a summary:

+-----------+---------------------------------------------+
| Wildcard  |                   Meaning                   |
+===========+=============================================+
| ``*``     | Matches any string, of any length.          |
+-----------+---------------------------------------------+
| ``?``     | Matches any single character.               |
+-----------+---------------------------------------------+
| ``[...]`` | Matches any one of the enclosed characters. |
+-----------+---------------------------------------------+

After `word splitting`_ has been done, Bash scans each word for the characters ``*``, ``?``, and ``[``. If one of these characters appears, then the **word** is regarded as a **pattern**, and replaced with an alphabetically sorted list of filenames matching the pattern.


.. _`filename expansion`: https://www.gnu.org/software/bash/manual/html_node/Filename-Expansion.html#Filename-Expansion
.. _`globbing`: https://en.wikipedia.org/wiki/Glob_(programming)
.. _`pattern matching`: https://www.gnu.org/software/bash/manual/html_node/Pattern-Matching.html#Pattern-Matching
.. _`word splitting`: https://www.gnu.org/software/bash/manual/html_node/Word-Splitting.html#Word-Splitting
