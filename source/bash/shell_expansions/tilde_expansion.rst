***************
Tilde expansion
***************
`Tilde expansion`_ is used to expand the value of:

* The **home directory** of a user.
* Directories in the `directory stack`_.

In all cases, we can use ``~`` expansion in **variable assignments**, for example::

    $ pwf=~
    $ echo $pwf
    /home/bob

Expanding home
==============
When we use the ``~`` by itself, it will expand to the value of ``$HOME`` of the current user::

    $ echo ~
    /home/bob

If ``HOME`` is unset, the home directory of the user executing the shell is substituted instead.

We can change to the home directory of any other user by using ``~`` as a prefix to a **username**::

    $ echo ~lynda
    /home/lynda

Expanding directories in the stack
==================================
We can add more symbols to the ``~`` to access directories in the `directory stack`_.

Expanding to ``PWD``: the ``~+`` prefix
---------------------------------------
To access the ``PWD`` (Present Working Directory) we add the ``+`` sign, so the prefix becomes ``~-``::

    $ echo ~+
    /home/bob/test

We can add more directory names if we need to drill down the filesystem::

    $ echo ~+/foo
    /Users/fito/test/foo

Expanding to ``OLDPWD``: the ``~-`` prefix
------------------------------------------
To expand to the path of ``OLDPWD`` we add the ``-`` sign, so the prefix becomes ``~-``. For example::

    $ pwd
    $ /home/bob
    $ cd /media
    $ pwd
    /media
    $ echo ~-
    /home/bob

Expanding to any directory in the stack
---------------------------------------
We can access any folder in the directory stack by its stack number. For example, let's imagine our stack looks like this::

    $ dirs -v
    0  ~/test/foo
    1  ~/test/bar
    2  ~/test

To access any of these directory names we could the prefix ``~N``, where **N** represents any of the directory numbers::

    $ echo ~0
    /home/bob/test/foo
    $ echo ~1
    /home/bob/test/bar
    $ echo ~2
    /home/bob/test

Using ``~+N`` has identical effect to what we has just described, it's just more verbose.

Accessing the stack in reverse order
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If we wanted to access the directories in reverse order, meaning from the last to the first one, we would use the prefix ``~-N``. Notice the last directory has the index **-0** and so on. So if this was our stack::

    $ dirs -v
    0  ~/test/foo
    1  ~/test/bar
    2  ~/test

We could access the directories starting from the last one (-0)::

    $ echo ~-0
    /home/bob/test
    $ echo ~-1
    /home/bob/test/bar
    $ echo ~-2
    /home/bob/test/foo

.. _`Tilde expansion`: https://www.gnu.org/software/bash/manual/html_node/Tilde-Expansion.html#Tilde-Expansion
.. _`directory stack`: https://www.gnu.org/software/bash/manual/html_node/The-Directory-Stack.html#The-Directory-Stack
