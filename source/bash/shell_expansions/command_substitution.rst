********************
Command substitution
********************
`Command substitution`_ allows the output of a command to replace the command itself. There are two ways of performing command substitution:

    * Using old-style backquotes:```command```
    * Using ``$(command)``

Old-style
=========
This way works like this::

    $ echo "My current working directory is: `pwd`"
    /home/bob

.. note:: This syntax is the one used by the **Bourne shell** and the **C shell**. It's also defined by the `POSIX standard`_.

New style
=========
The other way of performing command substitution is using::

    $ echo "My current working directory is: $(pwd)"
    /home/bob

We can use redirection combined with command substitution. For example, imagine we have a file::

    $ echo 'hello world' > foo.txt

And we want to get its contents using command substitution. We could do::

    $ echo "The message is $(cat foo.txt)"
    The message is hello world

Or we could just::

    $ echo "The message is $(< foo.txt)"
    The message is hello world

.. note:: This syntax is the one used by the **Korn shell** and is defined by **POSIX** as well.

.. _`Command substitution`: https://www.gnu.org/software/bash/manual/html_node/Command-Substitution.html#Command-Substitution
.. _`POSIX standard`: https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html#tag_02_06_03
