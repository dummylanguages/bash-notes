################
Shell expansions
################

.. toctree::
	:maxdepth: 3
	:hidden:

	brace_expansion
	tilde_expansion
	parameter_expansion/index
	command_substitution
	arithmetic_expansion
	word_splitting
	filename_expansion/index

Here we'll cover what in the Bash official manual is known as `shell expansions`_. There are seven types of shell expansion, which is performed in the following order:

1. `Brace expansion`_
2. `Tilde expansion`_
3. `Parameter and variable expansion`_
4. `Arithmetic expansion`_
5. `Command substitution`_, (done in a left-to-right fashion).
6. `Word splitting`_
7. `Filename expansion`_

On systems that can support it, there is an additional expansion available: `Process substitution`_, which is a way to write and read to and from a command. .

After all these expansions are performed, `quote removal`_ is performed.

A lot of these expansion (not all though) have in common something: they are introduced with the ``$`` sign. For example:

* Variable expansion, ``${}``::

	$ echo ${age}
	44

* Command substitution, ``$()``::

	$ echo "Hi, my name is $(whoami)"

* Arithmetic expansion, ``$(())``::

	$ echo "How much is 2 + 2? $((2+2))"
	How much is 2 + 2? 4

.. _`shell expansions`: https://www.gnu.org/software/bash/manual/html_node/Shell-Expansions.html#Shell-Expansions
.. _`Brace expansion`: https://www.gnu.org/software/bash/manual/html_node/Brace-Expansion.html#Brace-Expansion
.. _`Tilde expansion`: https://www.gnu.org/software/bash/manual/html_node/Tilde-Expansion.html#Tilde-Expansion
.. _`Parameter and variable expansion`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
.. _`Arithmetic expansion`: https://www.gnu.org/software/bash/manual/html_node/Arithmetic-Expansion.html#Arithmetic-Expansion
.. _`Command substitution`: https://www.gnu.org/software/bash/manual/html_node/Command-Substitution.html#Command-Substitution
.. _`Word splitting`: https://www.gnu.org/software/bash/manual/html_node/Word-Splitting.html#Word-Splitting
.. _`Filename expansion`: https://www.gnu.org/software/bash/manual/html_node/Filename-Expansion.html#Filename-Expansion
.. _`Process substitution`: https://www.gnu.org/software/bash/manual/html_node/Process-Substitution.html#Process-Substitution
.. _`quote removal`: https://www.gnu.org/software/bash/manual/html_node/Quote-Removal.html#Quote-Removal
