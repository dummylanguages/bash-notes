##############
Indexed arrays
##############

.. toctree::
	:hidden:

	creating_arrays
	referencing_arrays
	slicing_arrays
	modifying_arrays
	copying_arrays
	deleting_arrays
	array_tricks

Variables can be assigned arrays, which are ordered list of elements. Tecnically speaking they are called `indexed arrays`_, since each of these elements is referenced by an **index number**. 

Some other features of arrays are:

* They are **zero-based**, meaning the first element starts at zero index.
* The size of an array has no maximum limit.
* Elements don't have to be assigned contiguously.
* Element indices must be **integers**, or expressions that evaluate to integer values.
* In Bash, arrays are **one-dimensional**, meaning that elements in an array can be strings, or integers, but not other arrays.

.. _`indexed arrays`: https://www.gnu.org/software/bash/manual/html_node/Arrays.html
