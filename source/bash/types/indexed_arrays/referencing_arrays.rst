******************
Referencing arrays
******************
In the section before we learned how to create arrays and how to put values inside them. Here we'll see how to get out these values so we can use them for whatever we want.

.. note:: To **reference an array member** means to access the value stored at a given index position. 

Arrays are zero-based
=====================
As we said in the section before, arrays are **zero-based**, meaning the first element is numbered zero, the second as one, and so on. For example, consider the following array::

    $ declare -p veggies
    declare -a veggies=([0]="carrot" [1]="lettuce" [2]="broccoli")

We can see that ``declare`` display the index next to each element, and the first one has the index 0. The following table is self-explanatory:

+------------------+--------+---------+----------+
| Position:        | 1st    | 2nd     | 3rd      |
+------------------+--------+---------+----------+
| Element:         | carrot | lettuce | broccoli |
+------------------+--------+---------+----------+
| Index:           | 0      | 1       | 2        |
+------------------+--------+---------+----------+

Referencing single values
=========================
To get the value stored in some array member we have to use `parameter expansion`_. For example, given the following array::

    $ declare -p fruits
    declare -a fruits=([0]="lemon" [1]="orange" [2]="banana")

Remember, arrays are zero-based, so if we want to access the **first element** we use the **zero index**::

    $ echo ${fruits[0]}
    lemon

Referencing all the values
==========================
What if we want to get **all** the elements in an array? Use ``@`` or ``*`` as a subscript::

    $ echo ${fruits[*]}
    lemon orange banana

Referencing all the array members at once is useful in several situations, for example when we need to **iterate over the members**. In that case there is a huge difference between these two subscripts. Consider the :download:`following script </_code/iterating_array.sh>`: 

.. literalinclude:: /_code/iterating_array.sh
   :language: bash
   :emphasize-lines: 6
   :linenos:
   :tab-width: 2

The output of this script is::

    1: Rick Sanchez
    2: Morty Smith
    3: Mr. Meeseeks

But if we used ``"${names[*]}"`` instead, the output would be::

    1: Rick Sanchez Morty Smith Mr. Meeseeks

Notice, just a single line, result of a single iteration. All the elements appear next to each other separated by a space. The reason for that is we are using this parameter expansion between **double quotes**, and in that situation:

* ``"${names[*]}"`` expands to a **single word** with the value of each array member separated by a space.
* Whereas ``"${names[$]}"`` expands each element of the array to a **separate word**.

Getting just the indices
========================
If what we want is the list of indices in an array we can use:

    * ${!name[@]} *
    * ${!name[*]} *

For example::

    $ colors=([0]=red [2]=blue [4]=green)
    $ declare -p colors
    declare -a colors=([0]="red" [2]="blue" [4]="green")

As you can see, we have assigned 3 values at even positions, let's try to get the **indices**::

    $ echo ${!colors[@]}
    0 2 4

Size of an array
================
We can get the size of an **array** using the ``#``. For example::

    $ declare -p colors
    declare -a colors=([0]="red" [1]="dark blue" [2]="light green")

It's important to remember to use either ``*`` or ``2`` in our subscript notation::

    $ echo ${#colors[@]}
    3
    $ echo ${#colors[*]}
    3

Size of a member
----------------
The size of an array **member** is the number of characters of the value. For example, using our last example let's get the number of characters of the second element::

    $ echo ${#colors[1]}
    9

Note that the space in ``dark blue`` counts as a character too.

Negatives indices
=================
We can also reference elements in an array counting from right to left. For example, consider the following array::

    $ declare -p veggies
    declare -a veggies=([0]="carrot" [1]="lettuce" [2]="broccoli")

We can see that ``declare`` display the index next to each element, and the first one has the index 0. Well, it is possible to reference the members in an array using negative indices. The following table is self-explanatory:

+------------------+--------+---------+----------+
| Position:        | 1st    | 2nd     | 3rd      |
+------------------+--------+---------+----------+
| Elements:        | carrot | lettuce | broccoli |
+------------------+--------+---------+----------+
| Index:           | 0      | 1       | 2        |
+------------------+--------+---------+----------+
| Negative Index:  | -3     | -2      | -1       |
+------------------+--------+---------+----------+

So in order to fetch the last element we could::

    $ echo ${veggies[-1]}
    broccoli

We are not limited just to the last element. Counting right-to-left, we can access any element we wanted to::

    $ echo ${veggies[-3]}
    carrot

Why at -1?
----------
If you are wondering why the last element is referenced at index **-1**, think about this: the array has a size of **3** (it has 3 elements) but the last element has the index **2**. So the index of the last element is always the length of the array - 1:

* Size of the array: ``${#veggies[*]}``
* Index of last element: ``${#veggies[*]} - 1``

Since we can use shell arithmetic as a subscript, let's plug this last expression into the square brackets and see if we get the last element::

    $ echo ${veggies[${#veggies[*]} - 1]}
    broccoli

Indeed we get it. That's the explanation of why ``-1`` will fetch the last element, think of it as a **shortcut** for ``array[size-1]``, drop the size and you get: ``array[-1]``.


.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/bash.html#Shell-Parameter-Expansion
