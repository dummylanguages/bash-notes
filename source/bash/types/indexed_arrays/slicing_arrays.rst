**************
Slicing arrays
**************
We can select a subset of elements in an array. This is done with a particular type of `parameter expansion`_ known as **substring expansion**. There are two types of substring expansion:

1. The first one allows us to select from an element until the end the array.
2. The second one is more flexible, allowing us to select from an element until another element.

Slicing until the end of the array
==================================
To select a subset of elements in an array we can use::

    ${array_name[@]:offset}

.. note:: The official documentation uses the term ``offset``, which in this case means the index of the element from which we'll start slicing. By the way, as you may have already imagined, using ``*`` as subscript renders the same result.

For example, consider the array::

    $ week=(monday tuesday wednesday thursday friday saturday sunday)
    $ echo ${week[@]:5}
    saturday sunday

It's important to notice that ``${week[@]:5}`` expand to a **string** (hence the name substring expansion), so if you want to create an array out of that string slice::

    $ weekend=${week[@]:5}
    $ declare -p weekend
    declare -- weekend="saturday sunday"

The variable ``weekend`` contains just a single string with two words separated by a space. But don't worry, creating an actual array of a slice is easy::

    $ weekend=(${week[@]:5})
    $ declare -p weekend
    declare -a weekend=([0]="saturday" [1]="sunday")

It's just a matter of enclosing the substring expansion in between parentheses, which is the proper syntax to create an array.

Using negative indexing
-----------------------
We can use negative indices in slicing operations. For example::

    $ week=(monday tuesday wednesday thursday friday saturday sunday)
    $ echo ${week[@]: -2}

In some situations, negative indexing is more intuitive. For example, in this case we know we want the **last 2 elements** in the array, so using ``-2`` makes sense.

.. note:: It's important to leave a **at least one space** after the colon and before the negative sign! As in ``${week[@]: -2}``. This is intended to avoid being confused with the ``:-`` expansion. 

Setting both endpoints of the slice
===================================
The other version of **substring expansion** applied to arrays has the form::

    ${array_name[@]:offset:length}

Where **length** stands for the number of elements we want to slice, and **offset** the element where we start counting.

.. note:: It is an **expansion error** if **length** evaluates to a number **less than zero**. 

For example::

    $ week=([0]="monday" [1]="tuesday" [2]="wednesday" [3]="thursday" [4]="friday" [5]="saturday" [6]="sunday")
    $ weekdays=(${week[@]:0:5})
    $ declare -p weekdays
    declare -a weekdays=([0]="monday" [1]="tuesday" [2]="wednesday" [3]="thursday" [4]="friday")

We can use **negative indices** to mark the offset, just be aware that the **length** is always counted left-to-right. If we wanted to select Friday and Saturday::

    $ week=([0]="monday" [1]="tuesday" [2]="wednesday" [3]="thursday" [4]="friday" [5]="saturday" [6]="sunday")
    $ echo ${week[*]: -3:2}
    friday saturday

As you can see, we can use negative indexing for setting the offset, but from that point, the 2 elements are counted left-to-right.


.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/bash.html#Shell-Parameter-Expansion
