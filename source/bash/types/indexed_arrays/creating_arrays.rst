***************
Creating arrays
***************
There are a couple of ways of creating arrays:

* **Implicitely**, by assigning a list of values to a variable.
* **Explicitely**, using the ``declare`` command, or its POSIX-compliant synonym ``typeset``.

Assigning values
================
Another way is using **assignment statements**. There are several syntaxes available:

A single value
--------------
We can create an array by assigning a value at a certain index using **subscript notation**, which in general form looks like this::

    array_name[subscript]

In the expression above **subscript** stands for the index number of the member we want to reference. We have to enclose this index inside square brackets.

Let's create an array with a single member at **index 2**:

.. code-block:: bash

    $ rodents[2]=squirrel
    $ declare -p rodents
    declare -a rodents=([2]="squirrel")

As you can see, the **array attribute** is set automatically, we didn't declare the array explicitely. Also, note that even though arrays are zero-based, we can start assigning at any index we want.

.. note:: We'll see a bit later how this same syntax can also used to **modify** the values in an array.

Once an array is created this way, we can keep adding elements incrementally, for example::

    $ rodents[1]=hamster
    $ rodents[0]=mouse
    $ declare -p rodents
    declare -a rodents=([0]="mouse" [1]="hamster" [2]="squirrel")

A list of values
----------------
We can assign several elements simultaneously. We just have to enclose them in **parentheses** and separate them with a space:

.. code-block:: bash

    $ arr=(one two)
    $ declare -p arr
    declare -a arr=([0]="one" [1]="two")

.. note:: Do **not** use **commas** to separate array members.

Specifying explicit indexes
---------------------------
We can be specific about the index position each value should take in the array:

.. code-block:: bash

    $ names=([0]="Rick Sanchez" [1]="Morty Smith" [4]="$USER" [99]="Mr. Meeseeks")

As we said before, array elements don't have to be assigned contiguously.

Assigning empty values
----------------------
An array variable is considered set, if a subscript has been assigned a value. The **null string** is a valid value. For example:

.. code-block:: bash

    $ my_list[0]=
    $ declare -p my_list
    declare -a my_list=([0]="")

Explicitely: declaring arrays
=============================
Using the explicit way:

.. code-block:: bash

    $ declare -a arr

The statement above would assign an empty array to the variable ``arr`` and set the **array attribute** on it. We can check it out using:

.. code-block:: bash

    $ declare -p arr
    declare -a arr

This other syntax may be used:

.. code-block:: bash

    $ declare -a arr[3]
    declare -a arr

But since we are not assigning any value to that index, it is ignored, and the array is created **empty**.

.. note:: All of the assignment statements seen in the section before can be combined with explicit declarations that use the ``declare`` command.

Array of integers
-----------------
The explicit syntax may look verbose, but actually it may be useful for typing the elements in our array, for example:

.. code-block:: bash

    $ declare -ai num_list=(1 2 3)
    $ declare -p num_list
    declare -ai num_list=([0]="1" [1]="2" [2]="3")

After displaying ``num_list`` we can see the ``-ai`` attribute, which means its an array of integers. If we tried to assign anything other than an integer:

.. code-block:: bash

    $ num_list[0]=foo
    $ declare -p num_list
    declare -ai num_list=([0]="1" [1]="2" [2]="3")

But it didn't work out, because ``foo`` is not an integer value. It's worth noticing that we won't receive any warning, it fails silently:

.. code-block:: bash

    $ echo $?
    0

.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/bash.html#Shell-Parameter-Expansion
