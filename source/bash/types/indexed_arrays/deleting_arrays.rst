***************
Deleting arrays
***************
Here we'll see how to delete:

* Arrays as a whole.
* Array members.

Deleting arrays
===============
If we use ``*`` or ``@`` as subscript, we destroy the whole **array**::

    $ unset characters[*]
    $ declare -p characters
    -bash: declare: characters: not found

As with any other variable, we can also destroy the whole array just passing its name to ``unset``, no subscript notation needed::
    
    $ arr=(1 2)
    $ unset arr
    $ declare -p arr
    -bash: declare: arr: not found

Destroying members
==================
In this category we have several options available.

Single elements
---------------
We can destroy array members using the ``unset`` builtin. Consider the following array::

	$ characters=('Doofus Rick' 'Evil Morty')
	$ declare -p characters
	declare -a characters=([0]="Doofus Rick" [1]="Evil Morty")

Let's get rid of ``Doofus Rick``::

    $ unset characters[1]
    $ declare -p characters
    declare -a characters=([0]="Doofus Rick")

A range of elements
-------------------
Sometimes we want to delete a range of elements. For example, consider the following array::

    $ vowels=(a e i 44 252 31 o u)

We clearly want to get rid of the 3 numeric values::

    $ vowels=(${vowels[@]:0:3} ${vowels[@]: -2})

It is more practical in this case to use a combination of **slicing** and **concatenation** to get rid of them. In the example we are using two slices:

* ``${vowels[@]:0:3}`` expands to the first 3 vowels.
* ``${vowels[@]: -2}`` expands to the two last ones.

Then we concatenate the two resulting arrays and re-assign to ``vowels`` variable. And it works indeed::

    $ declare -p vowels
    declare -a vowels=([0]="a" [1]="e" [2]="i" [3]="o" [4]="u")

Search and destroy
------------------
This works almost as **searching and replacing** array members. Remember the expression:

    * ${parameter/pattern/string} *

In this case, we leave the **string** empty, like this:

    * ${parameter/pattern/} *

For example, we have the following array::





.. _`indexed arrays`: https://www.gnu.org/software/bash/manual/html_node/Arrays.html
.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/bash.html#Shell-Parameter-Expansion
