**************
Copying arrays
**************
Creating a copy of an array is very easy. We just have to use `parameter expansion`_ to expand all the elements in the array, inside parentheses. For instance, let's say we have the following array::

    $ fruits=(orange apple pear)
    $ declare -p fruits
    declare -a fruits=([0]="orange" [1]="apple" [2]="pear")

And we want to create a copy of it named ``snacks``::

    $ snacks=(${fruits[@]})
    $ declare -p snacks
    declare -a snacks=([0]="orange" [1]="apple" [2]="pear")

.. note:: Here, subscripting the array with ``*`` has the same effect.

Renumbering of the indices
==========================
A side effect of copying arrays the way we have described, it's that a renumbering of the indices occur. For example, imagine we have an array with **non-consecutive** elements::

    $ pets=(cat dog)
    $ pets[99]=crocodile
    $ declare -p pets
    declare -a pets=([0]="cat" [1]="dog" [99]="crocodile")

Let's make a copy of ``pets`` and see what happens with the indices::

    $ cute_pets=(${pets[@]})
    $ declare -p cute_pets
    declare -a cute_pets=([0]="cat" [1]="dog" [2]="crocodile")

In ``cute_pets`` the element ``crocodile`` is now at index ``2``.

Copying an array this way will renumber all the elements sequentially, closing any gap in the array indexing.

Don't renumber my array
=======================
If we want an array copy that doesn't close the gaps between non-consecutive indices we'll have to resort to a slightly lengthier :download:`script solution </_code/copying_array.sh>`: 

.. literalinclude:: /_code/copying_array.sh
   :language: bash
   :emphasize-lines: 8
   :linenos:
   :tab-width: 2

The important part in the script is **line 8**, where we iterate over the array indices. The expression ``${!pets[@]}`` expands to the indices of the array. Each index is assigned to the variable ``index``, which is used to assign values exactly at the same position.

The output of this script is::

    The original array:
    declare -a pets=([0]="cat" [1]="dog" [99]="crocodile")

    The copy:
    declare -a cute_pets=([0]="cat" [1]="dog" [99]="crocodile")

.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/bash.html#Shell-Parameter-Expansion
