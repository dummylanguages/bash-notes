************
Array tricks
************
Let's see some techniques that come in handy.

Creating arrays
===============

From strings
------------
For example, let's use  `brace expansion`_ to generate a **dummy string** for demonstration purposes:

.. code-block:: bash

    $ str=$( echo {A..D} )
    A B C D
    $ declare -p str
    declare -- str="A B C D"

Let's turn this string into an **array** of its characters using the `read`_ builtin:

.. code-block:: bash

    $ read -ra arr <<< "$str"
    $ declare -p arr
    declare -a arr=([0]="A" [1]="B" [2]="C" [3]="D")

Delimiters
^^^^^^^^^^
But what if we want to create the array elements based on some **delimiter** the string contains? Note that we are setting ``IFS`` for that run of the ``read`` command, after that ``IFS`` is set to its original value.

.. code-block:: bash

    $ csv='A, B, C, D'
    $ IFS=', ' read -a arr $csv

Or we can also use a combination of **parentheses** and **command substitution**:

.. code-block:: bash

    $ arr=( $(IFS=', '; echo $csv) )

.. warning:: Don't forget the ``;`` to separate commands when using **command substitution**.

From brace expansion
--------------------
We could create an array from a range, using a combination of **parentheses** and `command substitution`_:

.. code-block:: bash

    $ arr=( {A..D} )
    $ declare -p arr
    declare -a arr=([0]="A" [1]="B" [2]="C" [3]="D")

Converting arrays to strings
============================
Consider the following array:

.. code-block:: bash

    $ declare -p arr
    declare -a arr=([0]="A" [1]="B" [2]="C" [3]="D")

We could use ``echo``:

.. code-block:: bash

    $ str=$( echo ${arr[*]} )
    $ echo $str
    A B C D

Or ``printf``, if we don't want a **space** character between the elements:

.. code-block:: bash

    $ str=$( printf '%s' ${arr[*]} )
    $ echo $str
    ABCD

Although using ``echo`` with ``tr`` allows us to customize the **separator**:

.. code-block:: bash

    $ str=$( echo ${a4[*]} | tr -s ' ' '-' )
    $ echo $str
    A-B-C-D

.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/bash.html#Shell-Parameter-Expansion
.. _`brace expansion`: https://www.gnu.org/software/bash/manual/html_node/Brace-Expansion.html
.. _`read`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html#Bash-Builtins
.. _`command substitution`: https://www.gnu.org/software/bash/manual/html_node/Command-Substitution.html
