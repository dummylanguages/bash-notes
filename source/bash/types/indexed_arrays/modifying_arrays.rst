****************
Modifying arrays
****************
Here we'll see several ways to change arrays:

* Modify and delete single values.
* Concatenate arrays.

Modifying members
=================
Given any existing array, for example::

    $ declare -p fruits
    declare -a fruits=([0]="apple" [1]="orange" [2]="peach")

We can change the value of any element::

    $ fruits[2]="banana"
    $ declare -p fruits
    declare -a fruits=([0]="apple" [1]="orange" [2]="banana")

If we don't specify any index, we are modifying the first element::

    $ fruits="lemon"
    $ declare -p fruits
    declare -a fruits=([0]="lemon" [1]="orange" [2]="banana")

Note how the array doesn't get smashed by the ``lemon`` string value, that's because it has the ``-a`` atribute set. By the way, once set, the array attribute can not be turned off.

.. note:: Referencing an array variable without a subscript is equivalent to referencing with a subscript of 0.

Search and replace
==================

Adding members
==============
Nothing simpler than adding a new member to an array. Consider the array::

    $ colors=(black white)
    $ declare -p colors
    declare -a colors=([0]="black" [1]="white")

Let's add one more member::

    $ colors=(${colors[@]} red)
    $ declare -p colors
    declare -a colors=([0]="black" [1]="white" [2]="red")

We can also add several elements at the same time::

    $ colors=(${colors[@]} blue green [99]=pink)
    declare -a colors=([0]="black" [1]="white" [2]="red" [3]="blue" [4]="green" [99]="pink")

Concatenating arrays
====================
It is possible to concatenate the elements of different arrays. Let's say we have the following two arrays::

    $ drinks=(Coca-cola Pepsi)
    $ booze=(Whiskey Rum)
    $ wasted=(${drinks[@]} ${booze[@]})
    $ declare -p wasted
    declare -a wasted=([0]="Coca-cola" [1]="Pepsi" [2]="Whiskey" [3]="Rum")

In the example above we concatenated two arrays under a third variable, but we don't need to do that if we don't want:: 

    $ week=(Mon Tue Thu Fri Wed)
    $ weekend=(Sat Sun)
    $ week=(${week[@]} ${weekend[@]})

We can just concatenate two arrays into an existing one.

Search and Replace elements
===========================
The **search and replace** mechanism that can be used with strings, can be also used with arrays, using the general expression:

    * ${parameter/pattern/string} *

Where:

* **Parameter** is the name of an array.
* **Pattern** is the value of the element we want to search.
* **String** the element we want to plug in instead.

The interesting part here is **pattern**. It allows us to use `pattern matching`_ to find the member we want to replace.

For example, we have the following array with fruit, but somehow a cat sneeked in::

    $ fruits=(orange apple cat watermelon)

We can replace the cat with a juicy watermelon instead::

    $ fruits=(${fruits[@]/cat/watermelon})
    $ declare -p fruits
    declare -a fruits=([0]="orange" [1]="apple" [2]="watermelon" [3]="pear")

Modifying the case of values
============================
${parameter^pattern}
${parameter^^pattern}
${parameter,pattern}
${parameter,,pattern}




.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/bash.html#Shell-Parameter-Expansion
.. _`pattern matching`: https://www.gnu.org/software/bash/manual/html_node/Pattern-Matching.html#Pattern-Matching
