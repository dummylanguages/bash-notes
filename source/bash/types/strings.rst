*******
Strings
*******
When assigning string values to variables, we must take into consideration several scenarios:

1. A single word value.
2. A string containing spaces and more stuff.

A single word string
====================
There's nothing wrong with the assignment::

    $ nickname=bob

Since it's a one-word string that doesn't contain any special characters that need to be escaped, we don't need to quote it. I haven't found much information about it, although in Google's `Shell Style Guide`_ there's this line::

    last_line='NULL'

Which would support the use of **single quotes** for one-word strings. But if we display the variable ``nickname`` we assigned before::

    $ declare -p nickname
    declare -- nickname="bob"

We can see how Bash shows its value within **double quotes**. These are also needed if our one-word string contains an apostrophe (single quote)::

    $ whose="John's"

Arguably single quotes are more clean and **readable**, but in some cases (like the one above) we still need to double quotes for one-word strings, so maybe for **consistency** it's a good idea to use double quotes for these type of strings.

Bottom line, it is really a matter of preference in this case.

A string containing spaces
==========================
If our string contains several words separated by spaces, we must undoubtedly resort to some quoting mechanism, such as `single quoting`_::

    nickname='Rick Sanchez'

If our string has any single quotes in it we must use `double quoting`_::

    paradox="Schroedinger's Cat"

Or even two single quotes::

    who="John's and Ralph's father"

There's a lot to say about `quoting`_, check the :ref:`section in these notes about it<quoting-section>`, but basically:

* If you don't need or don't want to allow any type of expansion within your string, use **single quotes**.
* Use **double quotes** if you need some type of expansion, or your string contains single quotes within.

No string attribute
===================
Bash offers a weak typing mechanisms through the ``typeset`` and ``declare`` statements. Any of these offer the possibility of setting the type of value a variable can hold. There are attributes for **integer** values and both types of **arrays**, but not for string values.

A string containing newlines
============================

Outputting special characters
-----------------------------
If we have a string with tabs or newline characters in it, for example::

    $ cat poem.txt
    Roses are red,
    Violets are blue,
    Sugar is sweet,
    And so are you.

Now let's store this text into a **variable**::

    $ poem=$(cat poem.txt)

If we ``echo`` this ``poem``, using the ``-e`` option to enable the interpretation of backslash escapes::

    $ echo -e $poem
    Roses are red, Violets are blue, Sugar is sweet, And so are you.

We don't get the **newline characters**. But if we enclose the variable expansion in **double quotes**::

    $ echo "$poem"
    Roses are red,
    Violets are blue,
    Sugar is sweet,
    And so are you.

We get the newlines on the output, even without using the ``-e`` option. The ``cat`` command doesn't need them though::

    $ cat <<< $poem
    Roses are red,
    Violets are blue,
    Sugar is sweet,
    And so are you.

.. _embedding-special-characters:

Embedding special characters into a string
------------------------------------------
If we are creating a string variable in the command line, and we want to put **special characters** in it we have two options:

1. Use `ANSI-C quoting`_, which consists on using the expression ``$'some string'`` to create the string. For example::

    $ var=$'hello\nworld'
    $ echo "$var"
    hello
    world
    $ wc -l <<< $var
    2

Using this mechanism we can insert special character using escape sequences such as ``\t`` for TABS or ``\n`` for newline characters. For carriage returns we would use ``\r``.

2. Use one `command for changing text`_ of the Readline library named ``quoted-insert``, which is by default bound to the ``ctrl-v`` shortcut::

    $ var="field 1   <C-d><TAB>   field 2"
    $ cut -f 1 <<< $var
    field1

In the example above, we started typing the string, and pressed ``<C-d>`` and then the special character we wanted to introduce, in this case ``<TAB>`` to record it in the string.

.. note:: Use ``<C-d><C-j>`` to embed an ``LF`` character in any string, which is how **newline characters** are represented in Unix and Unix-like systems.

To insert a newline character ``\n`` escape sequence using this second method, we have to use ``<C-d><C-j>`` to insert a `Line Feed`_ character, which is the newline character in Unix. This **shortcut** has the effect of literally dropping the cursor to a **new line**, check it out::

    $ var='line1<C-v><C-j>
    line2'

We type ``line1`` press ``<C-v><C-j>`` and the cursor drops down to the next line, where we finish the string and press Enter. If we count the lines::

    $ wc -l <<< $var
    2

About line breaks
-----------------
In Unix and Linux operating systems, every time we press the ``Enter`` key, we are sending a `Carriage Return`_ character. When typing text, this ``CR`` character is embedded in the **string** or text file as a `Line Feed`_ character.

The difference can be seen by doing the following experiment::

    $ echo 'CR:^M
    > LF:
    ' > line-breaks.txt

1. In the **first line** we typed ``CR:`` and then ``<ctrl-v><ctrl-enter>`` to embed a **carriage return** character, represented by ``^M``.
2. In the **second line** we typed ``LF:`` and then ``<ctrl-v><ctrl-j>`` to embed a **line feed** character, which is **not visible** except for the fact that the cursor drops to the next line.

There's a way of seeing both the ``CR`` and the ``LF`` characters::

    $ cat -ve line-breaks.txt 
    CR:^M$
    LF:$
    $

* We can see how in the second line, after ``CR:`` we have both a **carriage return** (introduced with ``<ctrl-v><ctrl-enter>``) and a **line feed** (represented by a ``$`` sign) that we introduced pressing ``Enter``.
* On the second line we have the string ``LF:`` that we typed, and the **line feed** that we introduced with the shortcut ``<ctrl-v><ctrl-j>``.
* You can ignore the last ``$``, which is a **line feed** introduced by ``echo`` automatically.

.. note:: Nowadays **macOS** uses the ``LF`` character for line breaks, but it used to use the ``CR`` character. **Windows 10** uses ``CRLF`` for the line breaks, so if you see ``\r\n`` when opening text files coming from Windows now you know why.

Retaining whitespace when referencing: USE DOUBLE QUOTES!
=========================================================
If we want to **retain whitespace** inside of a string when referencing a variable, we must enclose it in **double quotes**. For example, compare this::

    $ var='   a string     with a lot       of whitespace'
    $ echo "$var"
    a string     with a lot       of whitespace

To this::

    $ echo $var
    a string with a lot of whitespace

In the second case we didn't enclose the variable in **double quotes**, so we lose all the leading/trailing whitespace, as well as the whitespace in between words.

.. warning:: Find out the exact reason for this behaviour.

.. _`Shell Style Guide`: https://google.github.io/styleguide/shellguide.html
.. _`quoting`: https://www.gnu.org/software/bash/manual/html_node/Quoting.html
.. _`single quoting`: https://www.gnu.org/software/bash/manual/html_node/Single-Quotes.html
.. _`double quoting`: https://www.gnu.org/software/bash/manual/html_node/Double-Quotes.html
.. _`ANSI-C quoting`: https://www.gnu.org/software/bash/manual/html_node/ANSI_002dC-Quoting.html#ANSI_002dC-Quoting
.. _`command for changing text`: https://www.gnu.org/software/bash/manual/html_node/Commands-For-Text.html
.. _`Carriage Return`: https://en.wikipedia.org/wiki/Carriage_return
.. _`Line Feed`: https://en.wikipedia.org/wiki/Newline
