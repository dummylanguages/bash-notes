*******
Numbers
*******
In Bash we can assign numbers to our variables. By default, numbers and strings are smashed in the same category when variables are created using the **assignment statement**::

    $ var=hello; num=42
    $ declare -p var num
    declare -- var="hello"
    declare -- num="42"

Notice how both values show the ``--`` flag. In this case, Bash will not treat a numeric value differently from a string value. For example::

    $ num+=2
    $ echo $num
    422

For Bash, the numeric value stored in ``num`` is treated as a string, so the ``+=`` operator performs **string concatenation** instead of adding 2 to the previous value of ``a``.

There are three builtin ways of making Bash treat numeric values as integers, in order to evaluate arithmetic expressions:

1. Setting the integer attribute.
2. Using the ``((`` compound command.
3. Or the ``let`` simple command.

.. note:: We go deeper about shell arithmetic in this other section.

The integer attribute
=====================
To be able of performing `shell arithmetic`_, we can explicitely tell Bash that we want our numeric value treated as an integer. We can do that setting the **integer attribute**::

    $ declare -i b
    $ b=2+2
    $ echo $b
    4

We can set this attribute even in numeric variables that already exist. For example, ``a`` was assigned a numeric value::

    $ declare -p a
    declare -- a="2"

The output above shows that ``a`` is just a regular value which would receive the string treatment. Let's set the integer attribute on it and display again its value/attribute::

    $ declare -i a
    $ declare -p a
    declare -i a="2"

This time ``a`` has the ``-i`` attribute **on**. By the way, we could also turn the attribute **off** using ``+i``::

    $ declare +i a
    $ declare -p n
    declare -- n="2"

To know more about shell arithmetic, in this section we go balls deep about it.

The ``((`` compound command
===========================
Setting the **integer attribute** is not the only way of making Bash perform basic arithmetic operations. When the ``((`` compound command is used, numeric values are treated as **integers**::

    $ b=5
    $ ((a=b+2*3))
    $ echo $a
    11

You can read about this command in the `conditional constructs`_ section of the manual, because once it performs the arithmetic evaluation, it returns:

* The exit code ``0``, if the result is 0.
* Code ``1``, in any other case.

Which makes it useful as a test command to use inside conditional constructs. It's not POSIX® compliant though.

Arithmetic expansion
--------------------
There's another quite similar builtin mechanism named `arithmetic expansion`_ which uses the ``((`` compound command. Preceding this command with the ``$`` will expand to the result of the arithmetic operation::

    $ b=3
    $ echo $((a=b+2*3))
    11

**Arithmetic expansion** is specified by POSIX®, so its use should be preferred over the ``((`` command.

The ``let`` command
===================
``let`` is a **simple command** included as a `Bash builtin`_.

    $ b=3
    $ let a=$b+2*3; echo $a
    11


.. note:: You can use quotes to make complex expressions more readable::

    $ let a="$b + 2 * 3"

If your concern is portability, you must know that the ``let`` command is not specified by POSIX®.

Using external utilities
========================
There are also some command line utilities *not included* in Bash, that can handle simple arithmetic and much more. To cite a few:

* The ``expr`` command, which can handle only **integers**.
* The `bc`_ utility (stands for basic calculator), which is more a calculator language than a simple command.
* The `dc`_ utility, (stands for desktop calculator), one of the oldest Unix utilities, predating even the invention of the C programming language. 

Floating-point values
---------------------
Bash builtin commands can only perform basic arithmetic operations with integers. If we have assigned some **floating point** values we'll have to ressort to other commands (not builtin into Bash) in order to perform floating-point operations. ``bc`` for example::

    $ a=3.4; b=2.3
    $ echo "$a + $b" | bc
    5.7

This is a good chance to explain why numeric values are not given a numeric attribute by default:

1. If they were automatically given the **integer attribute**, any floating-point value would have to be truncated to the integer part, losing the decimals.
2. Another option would be to introduce a **floating-point attribute**, but it's probably a design choice not to do so since Bash doesn't have the tools to perform floating-point arithmetic.

Since Bash can only handle **integers**, it prefers to leave numeric values in the same categories as strings, in case they are floating-point values, so other software can deal with those operations.

By the way, look what happens when we try to declare a floating-point value as **integer**::

    $ declare -i a=1.2
    -bash: declare: 1.2: syntax error: invalid arithmetic operator (error token is ".2")

Style
-----
Google's `Shell Style Guide`_ recommends::

    # "never quote literal integers"
    value=32

Probably it's a good idea to do the same with floating-point values.


.. _`Shell Style Guide`: https://google.github.io/styleguide/shellguide.html
.. _`Arithmetic expansion`: https://www.gnu.org/software/bash/manual/html_node/Arithmetic-Expansion.html#Arithmetic-Expansion
.. _`Bash builtin`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html#Bash-Builtins
.. _`bc`: https://en.wikipedia.org/wiki/Bc_(programming_language)
.. _`dc`: https://en.wikipedia.org/wiki/Dc_%28computer_program%29
.. _`shell arithmetic`: https://www.gnu.org/software/bash/manual/html_node/Shell-Arithmetic.html#Shell-Arithmetic
.. _`conditional constructs`: https://www.gnu.org/software/bash/manual/html_node/Conditional-Constructs.html#Conditional-Constructs
