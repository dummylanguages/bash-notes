#############
Types of data
#############

.. toctree::
	:hidden:

	strings
	numbers
	indexed_arrays/index
	associative_arrays

We can assign different types of data to variables:

* Strings
* Integers
* Indexed arrays
* Associative arrays


.. _`parameter`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameters.html#Shell-Parameters
