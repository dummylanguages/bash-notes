####################################
The command line: external libraries
####################################

.. toctree::
    :maxdepth: 3
    :hidden:

    readline/index
    history/index

Once Bash is running, we'll have available a **command line interface** ready to receive our input.

.. note:: The **command line interface** is commonly referred to just as the **command line**.

Its use is quite intuitive, just type a command, and press ``Enter`` to run it. For example::

    $ mkdir -p test/inner

In the line above we can differentiate several parts:

1. The ``$`` sign is known as the `command prompt`_, which is a sequence of one or more characters that prompts the user to take action. As we'll see :ref:`later <prompt-customization>`, the prompt can be customized to show useful information.
2. The first word, ``mkdir``, it's the command itself. This command is used for **making directories**.
3. The ``-p`` part is an **option** that allows us to create intermediate directories. In this case we are creating a directory named ``test`` and inside it, another one called ``inner``.
4. The last part is the **argument** to this command. In this case we are passing the names of the directories we want to create as an argument.

.. note:: Note that ``mkdir`` is **not** part of Bash, it's just a command interpreted by Bash. This is a good place to remind that Bash shouldn't be identified with the commands it interprets. Except for a few `shell builtin commands`_ that are installed as part of Bash, the rest of them are installed independently.

During an interactive session, we may need to **edit** our command line, for example:

* **move the cursor** along the line to fix typos, so we don't have to retype the whole line again.
* **cut/copy** and **paste** some stuff.
* Access the **command history**, pressing the up and down keys.

All of this functionality is not provided by Bash itself, but by a couple of external software libraries named `GNU Readline`_ and `GNU History`_, which are available in most Linux and Unix-like systems. In the next section, we'll cover how to use them.

.. _`command prompt`: https://en.wikipedia.org/wiki/Command-line_interface#Command_prompt
.. _`later`: https://www.gnu.org/software/bash/manual/html_node/Bash-Startup-Files.html#Bash-Startup-Files
.. _`shell builtin commands`: https://www.gnu.org/software/bash/manual/html_node/Shell-Builtin-Commands.html#Shell-Builtin-Commands
.. _`GNU Readline`: https://en.wikipedia.org/wiki/GNU_Readline
.. _`GNU History`: https://tiswww.case.edu/php/chet/readline/history.html
