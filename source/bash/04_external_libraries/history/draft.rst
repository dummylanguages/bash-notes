*****
draft
*****

History environment variables
=============================
The `Bash Variables`_ section of the Bash official manual mentions several **environment variables** used by Bash to configure the History settings:

* ``HISTFILESIZE`` sets the maximum number of lines contained in the history file, ``~/.bash_history``.
* ``HISTSIZE`` controls the number stored in memory for the current session.

In my case these two variables were set by default to::

    $ echo $HISTFILESIZE $HISTSIZE
    2000 1000

https://www.digitalocean.com/community/tutorials/how-to-use-bash-history-commands-and-expansions-on-a-linux-vps
https://www.howtogeek.com/howto/44997/how-to-use-bash-history-to-improve-your-command-line-productivity/

.. _`Bash Variables`: https://www.gnu.org/software/bash/manual/bash.html#Bash-Variables


