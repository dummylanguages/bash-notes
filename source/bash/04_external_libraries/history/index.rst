***********
GNU History
***********

.. toctree::
	:maxdepth: 3
	:hidden:

	draft

The `GNU Bash official manual`_ includes a section titled `Using History Interactively`_, which describes how to use the `GNU History Library`_ interactively, from a user’s standpoint. Interaction with the library is provided both by Bash itself and by the `GNU Readline Library`_ library.

Apart from the Bash manual, we can find instructions about how to interact with this library in:

* Several places in the **GNU Readline manual**, mainly:

	* Chapter `1.2.5 Searching for Commands in the History`_
	* Chapter `1.4.2 Commands For Manipulating The History`_

* The `GNU History Library official manual`_ dedicates its first section to explain how to `use History interactively`_. The rest of the manual contains information about how to program for this library.

By the way, both of thes are developed by the same person, Chet. 

.. _`GNU Bash official manual`: https://www.gnu.org/software/bash/manual/bash.html
.. _`Using History Interactively`: https://www.gnu.org/software/bash/manual/bash.html#Using-History-Interactively
.. _`GNU History Library`: https://tiswww.case.edu/php/chet/readline/history.html
.. _`GNU Readline Library`: https://tiswww.cwru.edu/php/chet/readline/rltop.html
.. _`1.2.5 Searching for Commands in the History`: https://tiswww.case.edu/php/chet/readline/readline.html#SEC8
.. _`1.4.2 Commands For Manipulating The History`: https://tiswww.case.edu/php/chet/readline/readline.html#SEC15
.. _`GNU History Library official manual`: https://tiswww.case.edu/php/chet/readline/history.html#SEC_Top
.. _`use History interactively`: https://tiswww.case.edu/php/chet/readline/history.html#SEC1

