Killing and Yanking (Copy/paste)
================================
We must explain what **kill** and **yank** mean:

* **Killing text** means to **cut** the text from the line. The cut text is available in the **kill ring**.
* To **yank** means to copy. In this case, we are **yanking/copying** text *from the kill ring* and **pasting** back into the line.

Killing text
------------
The default shorcuts for **killing** text commands are:

+--------------------------+------------------------------------------------------------+------------------------+
|         Shortcut         |                           Result                           |      Command name      |
+==========================+============================================================+========================+
| :kbd:`Ctrl + k`          | **Forward**, from *point* [#f0]_ to the *end of the line*  | ``kill-line``          |
+--------------------------+------------------------------------------------------------+------------------------+
| :kbd:`Ctrl + u`          | **Backwards**, from *point* to the *beginning of the line* | ``unix-line-discard``  |
+--------------------------+------------------------------------------------------------+------------------------+
| :kbd:`Ctrl + x + Rubout` | Idem                                                       | ``backward-kill-line`` |
+--------------------------+------------------------------------------------------------+------------------------+

.. [#f0] Same as the `GNU readline documentation`_, we are using the word **point** to designate the position of the cursor.

Using the :kbd:`Meta` key (aka :kbd:`Alt`) as a modifier, our commands will act upon **words**:

+----------------------------+--------------------------------------------------------------------------+------------------------+
|          Shortcut          |                                  Result                                  |      Command name      |
+============================+==========================================================================+========================+
| :kbd:`Alt + d`             | **Forward**, from *point* to the **end** of the current **word**         | ``kill-word``          |
+----------------------------+--------------------------------------------------------------------------+------------------------+
| :kbd:`Alt + Rubout` [#f1]_ | **Backwards**, from *point* to the **beginning** of the current **word** | ``backward-kill-word`` |
+----------------------------+--------------------------------------------------------------------------+------------------------+
| :kbd:`Ctrl + w`            | Idem                                                                     | ``unix-word-rubout``   |
+----------------------------+--------------------------------------------------------------------------+------------------------+

.. [#f1] :kbd:`Rubout` is the :kbd:`Backspace` key.

Yanking
-------
When we **yank**, we are pasting the most-recently-killed line from the **kill ring** (yank from the kill ring):

+-----------------+----------------------------------------+--------------+
|    Shortcut     |                 Result                 | Command name |
+=================+========================================+==============+
| :kbd:`Ctrl + y` | Yank/paste at the cursor position      | ``yank``     |
+-----------------+----------------------------------------+--------------+
| :kbd:`Alt + y`  | Rotate **kill ring** and paste new top | ``yank-pop`` |
+-----------------+----------------------------------------+--------------+

The Kill ring
-------------
The kill ring is just a buffer where Readline stores the text that we kill. There is one command related to the ring:

* ``yank-pop``, by default bound to the shorcut :kbd:`Meta + y`.

To understand how this command works, let's restart a new session in Bash and:

1. Type the word `one`, and kill it with :kbd:`Ctrl + u`.
2. Now type the word `two` and kill it too.
3. If we yank now, we will paste the last word we killed, in this case `two`.
4. Yank it again.
5. Now use the ``yank-pop`` command, :kbd:`Meta + y` to go backwards in the kill ring until you find the word you want.

In this example it's clear why it's called a ring, once it gets to the end of the buffer, continues at the beginning.


.. _`GNU readline documentation`: https://tiswww.case.edu/php/chet/readline/rluserman.html#SEC13
