The command history
===================
`GNU History`_ is a library that offers, mostly a programming interface that is used by `GNU Readline`_ to provide access to the list of commands previously typed, which is known as the **command history**.

There are a couple of interesting commands though, that allow us to use **GNU History** interactively:

+------------+---------------------------------------------------+
|  Shortcut  |                      Result                       |
+============+===================================================+
| :kbd:`!n`  | Run command with number ``n`` in the history list |
+------------+---------------------------------------------------+
| :kbd:`!-n` | Run command which is ``-n`` lines back            |
+------------+---------------------------------------------------+
| :kbd:`!!`  | Run **last** command (like :kbd:`!-1`)            |
+------------+---------------------------------------------------+

To see more of them, you can check the `Bash documentation`_ about using the History.

Essentials
----------
The :kbd:`Up Arrow` and :kbd:`Down Arrow` are the two most essential shortcuts in Readline to use the **command history**. After a failed command, it's quite common to hit the :kbd:`Up Arrow` to fetch the last command and add our modifications to it. We can go back up to the beginning of the history. The :kbd:`Down Arrow` will take us in the oposite direction, to the most recent commands.

These two commands are also assigned to the following two shortcuts:

+-----------------+------------------------------------------------------+----------------------+
|    Shortcut     |                        Result                        |     Command name     |
+=================+======================================================+======================+
| :kbd:`Ctrl + p` | Fetch the **previous** command from the history list | ``previous-history`` |
+-----------------+------------------------------------------------------+----------------------+
| :kbd:`Ctrl + n` | Fetch the **next** command from the history list     | ``next-history``     |
+-----------------+------------------------------------------------------+----------------------+

Sometimes, it's a bit tedious having to hit repeatedly the arrow keys to get to a distant command. We can navigate to both extremes of the list with these two commands:

+-----------------+---------------------------------------------+--------------------------+
|    Shortcut     |                   Result                    |       Command name       |
+=================+=============================================+==========================+
| :kbd:`Meta + <` | Move to the **first line** in the history   | ``beginning-of-history`` |
+-----------------+---------------------------------------------+--------------------------+
| :kbd:`Meta + >` | Move to the **end** of the the history list | ``end-of-history``       |
+-----------------+---------------------------------------------+--------------------------+

Note than both shortcuts use `angle brackets`_, so you'll have to actually hit also the :kbd:`Shift` key, in order to get the bracket. So the shortcuts become:

* :kbd:`Meta + Shift + ,` for :kbd:`Meta + <`
* And :kbd:`Meta + Shift + .` for :kbd:`Meta + >`

Types of search
---------------
Readline provides commands for `searching`_ through the command history for lines containing a specified string. There are two search modes:

* incremental 
* non-incremental. 

.. note:: Using the ``history`` command with the ``-c`` option will delete **all** our command history (useful for practicing these commands)

Incremental search
------------------
Incremental searches begin before the user has finished typing the search string. As each character of the search string is typed, Readline displays the next entry from the history matching the string typed so far. An incremental search requires only as many characters as needed to find the desired history entry.

There are a couple of commands for this type of search:

+-----------------+-----------------------------------------------------------+----------------------------+
|    Shortcut     |                          Result                           |        Command name        |
+=================+===========================================================+============================+
| :kbd:`Ctrl + r` | Search **backward** in the history                        | ``reverse-search-history`` |
+-----------------+-----------------------------------------------------------+----------------------------+
| :kbd:`Ctrl + s` | Search **forward** in the history                         | ``forward-search-history`` |
+-----------------+-----------------------------------------------------------+----------------------------+
| :kbd:`Ctrl + s` | Terminate incremental search                              | ``bla``                    |
+-----------------+-----------------------------------------------------------+----------------------------+
| :kbd:`Ctrl + s` | Abort an incremental search and restore the original line | ``bla``                    |
+-----------------+-----------------------------------------------------------+----------------------------+

Once you have started doing an incremental search, hitting again :kbd:`Ctrl + r` (or :kbd:`Ctrl + s`) will show the next entry matching the search string typed so far.

Forward search not working
^^^^^^^^^^^^^^^^^^^^^^^^^^
When trying to do a ``forward-search-history`` using the :kbd:`Ctrl + s` shortcut, you may found that the shortcut doesn't work. The reason for that is that, by default, :kbd:`Ctrl + s` is used to **stop** output control.

When **output control** is enabled, :kbd:`Ctrl + s` is used to send a control code named ``XOFF`` and **stop output**, and :kbd:`Ctrl + q` to send the ``XON`` code and **start output** again.

We can disable this output control functionality with::

    $ stty -ixon

A good idea would be to set it up permanently in our shell configuration.

The XON/XOFF codes: A bit of history
""""""""""""""""""""""""""""""""""""
In data communications, `flow control`_ is the process of managing the rate of data transmission between two nodes to prevent a fast sender from overwhelming a slow receiver. `Software flow control`_ is a method of flow control used in computer `data links`_, especially `RS-232 serial`_ so that, as we said, the receiving node is not overwhelmed with data from transmitting node.

Software flow control is used extensively by low-speed devices, especially older printers and dumb terminals, to indicate they are temporarily unable to accept more data. XOFF/XON are still sometimes used manually by computer operators, to pause and restart output which otherwise would scroll off the display too quickly. 

Although the ASCII standard did not reserve any control characters for use as XON/XOFF, it does provide four generic "device control" characters (DC1 through DC4). The `Teletype Model 33 ASR`_ adopted two of these, **DC3** and **DC1**, for use as XOFF and XON, respectively. This usage was copied by others, and is now a **de facto standard**.

+----------+---------------------+---------+-----------------+
|   Code   |       Meaning       |  ASCII  |    Shortcut     |
+==========+=====================+=========+=================+
| ``XOFF`` | Pause transmission  | ``DC3`` | :kbd:`Ctrl + s` |
+----------+---------------------+---------+-----------------+
| ``XON``  | Resume transmission | ``DC1`` | :kbd:`Ctrl + q` |
+----------+---------------------+---------+-----------------+

Non-incremental search
----------------------
Non-incremental searches read the entire search string before starting to search for matching history lines. The search string may be typed by the user or be part of the contents of the current line. 

+-----------------+------------------------------------------------+--------------------------------------------+
|    Shortcut     |                     Result                     |                Command name                |
+=================+================================================+============================================+
| :kbd:`Meta + p` | Search **backward**, moving up in the history  | ``non-incremental-reverse-search-history`` |
+-----------------+------------------------------------------------+--------------------------------------------+
| :kbd:`Meta + n` | Search **forward**, moving down in the history | ``non-incremental-forward-search-history`` |
+-----------------+------------------------------------------------+--------------------------------------------+

Configuring the command history
-------------------------------
By default, every command we type on a shell session will be saved to the command history. This behaviour can be disabled for a particular session using the builtin command::

    $ set -o history

There are several **environment variables** related to the command history:

* ``HISTFILE`` is the name of the file where the history is written. By default this variable is set to `~/.bash_history`. When Bash starts up, the history is initialized from this file.
* ``HISTFILESIZE`` may be set, to specify the maximum numbers of lines the **history file** can have. Default usually is set to **500**, so lines that exceed this number are truncated to leave place to newer commands.
* ``HISTSIZE`` specifies the number of commands to save in the **history list**. When a shell exits, the commands on this list are appended to the **history file**.

It's important to note that the **history file** is not updated while the session is running. Only as we exit the shell, the **history list**(kept in memory) is written(appended) to the end of the history file. By default, the command list is **appended**, but we could choose to overwrite the **history file** after each shell session with the setting::

    $ shopt -s histappend



.. _`GNU Readline`: https://tiswww.case.edu/php/chet/readline/rluserman.html
.. _`GNU History`: https://tiswww.case.edu/php/chet/readline/history.html
.. _`searching`: https://tiswww.case.edu/php/chet/readline/rluserman.html#SEC8
.. _`flow control`: https://en.wikipedia.org/wiki/Flow_control_(data)
.. _`Software flow control`: https://en.wikipedia.org/wiki/Software_flow_control
.. _`data links`: https://en.wikipedia.org/wiki/Data_link
.. _`RS-232 serial`: https://en.wikipedia.org/wiki/RS-232
.. _`Teletype Model 33 ASR`: https://en.wikipedia.org/wiki/Teletype_Model_33
.. _`angle brackets`: https://en.wikipedia.org/wiki/Bracket
.. _`Bash documentation`: https://www.gnu.org/software/bash/manual/html_node/Using-History-Interactively.html#Using-History-Interactively
