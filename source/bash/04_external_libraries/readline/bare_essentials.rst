Bare essentials
===============
The most basic interaction with the command line includes the following keys:

+-------------------------+-----------------------------------------------------+--------------------------+
|        Shortcut         |                       Result                        |       Command name       |
+=========================+=====================================================+==========================+
| :kbd:`Right arrow`      | Move **one character** to the **right**             | ``forward-char``         |
+-------------------------+-----------------------------------------------------+--------------------------+
| :kbd:`Left arrow`       | Move **one character** to the **left**              | ``backward-char``        |
+-------------------------+-----------------------------------------------------+--------------------------+
| :kbd:`Backspace` [#f0]_ | **Delete** character to the **left** of the cursor  | ``backward-delete-char`` |
+-------------------------+-----------------------------------------------------+--------------------------+
| :kbd:`DEL`              | **Delete** character to the **right** of the cursor | ``delete-char``          |
+-------------------------+-----------------------------------------------------+--------------------------+
| :kbd:`Ctrl-l`           | Clears the screen (like the ``clear`` command)      | ``clear-screen``         |
+-------------------------+-----------------------------------------------------+--------------------------+
| :kbd:`Ctrl + _`         | **Undo** the last editing command [#f1]_            | ``undo``                 |
+-------------------------+-----------------------------------------------------+--------------------------+

.. [#f0] In computing the `delete character`_ is sometimes known as :kbd:`Rubout`. The :kbd:`Backspace` key is modernly assigned to this function.
.. [#f1] (Same as :kbd:`Ctrl + x + u`). This command undoes the edits by increments, which are separately remembered for each line.

When we finish editing the line, to **run the command** we just have to hit the :kbd:`Return` key, no matter where the cursor is located.

Autocompletion
==============
Readline offers a feature that `autocomplete`_ the full name of a command, filename or directory, without having to fully type it. This is very useful, in case we don't quite remember the exact name of something, or it's just too long. These are the shortcuts:

+-----------------+-----------------------------------------------------------------+--------------------------+
|    Shortcut     |                             Result                              |       Command name       |
+=================+=================================================================+==========================+
| :kbd:`TAB`      | Complete the name before the cursor                             | ``complete``             |
+-----------------+-----------------------------------------------------------------+--------------------------+
| :kbd:`Meta + ?` | Display a list of possible completions of the text before point | ``possible-completions`` |
+-----------------+-----------------------------------------------------------------+--------------------------+
| :kbd:`Meta + ?` | Insert **all** possible completions at point                    | ``insert-completions``   |
+-----------------+-----------------------------------------------------------------+--------------------------+

From these shortcuts, :kbd:`TAB` is by far the one used the most.

.. _`delete character`: https://en.wikipedia.org/wiki/Delete_character
.. _`autocomplete`: https://tiswww.case.edu/php/chet/readline/rluserman.html#SEC19
