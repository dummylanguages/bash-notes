**********************************
GNU Readline: command line editing
**********************************

.. toctree::
	:maxdepth: 3
	:hidden:

	bare_essentials
	moving_cursor
	changing_text
	killing_yanking
	command_history
	vi_mode
	completion/index

According to the `GNU Readline`_ official website, this library is available to any interactive programs with a command line interface, such as Bash and other shells, the MySQL command-line client and that type of programs.

**Readline** has available a lot of commands for different purposes:

* For `moving the cursor`_.
* For `changing text`_.
* For `manipulating the command history`_.
* Keyboard macros.
* Miscellaneous commands.

.. note:: For licensing reasons, **GNU Readline** is not available on **macOS**. So if you are a mac user, know that in your system there is another library that provides a similar functionality: The `Editline Library`_ (``libedit``).


This commands are bound to key sequences (shortcuts), and can be rebound using the ``bind`` command, or the ``~/.inputrc`` file. We'll see how to do this later.
The shortcuts we are going to see are enabled by **default**, and are known as **emacs mode**. Later we'll see that there's also a **vim mode** available.


.. _`GNU Readline`: https://tiswww.cwru.edu/php/chet/readline/rltop.html
.. _`moving the cursor`: https://tiswww.case.edu/php/chet/readline/rluserman.html#SEC5
.. _`changing text`: https://tiswww.case.edu/php/chet/readline/rluserman.html#SEC16
.. _`manipulating the command history`: https://tiswww.case.edu/php/chet/readline/rluserman.html#SEC15
.. _`GNU readline documentation`: https://tiswww.case.edu/php/chet/readline/rluserman.html#SEC13
.. _`Editline Library`: http://thrysoee.dk/editline/
