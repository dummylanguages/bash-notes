Changing text
=============
The **GNU Readline**'s documentation has a section named `changing text`_, which contain commands for:

* Switching the position of characters and words(transposing).
* Changing the case of words.
* Deleting characters.

Quoted insert
-------------
Among the commands for **changing text**, there's an interesting command that doesn't fit into any of the above categories, named ``quoted-insert``. This command may seem a bit superfluous, but actually is **super useful** for embedding **new line** and **tab** characters into string variables in the command line. The other option is to use `ANSI C quoting`_.

.. note:: By default is bound to the shortcut :kbd:`Ctrl + v`, and it's used so we can type characters that otherwise would be read as commands.

For example, we want to insert an ``LF`` character in the middle of a string variable::

    $ greeting='Hello
    > world!'

* We type ``greeting='Hello`` press :kbd:`Ctrl + v` and :kbd:`Ctrl + j`.
* The cursor drops down to the line below, where we finish the string typing ``world!'`` and :kbd:`Enter`.

We can check our string::

    $ echo "$greeting"
    Hello
    world!

.. note:: In Unix-like operating systems, the **Line Feed character** (``LF``) is interpreted as a **new line character**

It works the same with **tab characters** and other special characters. Check our other section about :ref:`embedding-special-characters` if you want to read more about it.

Transposing
-----------
Transposing means to switch the position of two adjacent characters or words, one at each side of the cursor. Just a couple of commands:

+-----------------+--------------------------------------------------+---------------------+
|    Shortcut     |                      Result                      |    Command name     |
+=================+==================================================+=====================+
| :kbd:`Ctrl + t` | Transpose the **characters** surrounding *point* | ``transpose-chars`` |
+-----------------+--------------------------------------------------+---------------------+
| :kbd:`Meta + t` | Transpose the **words** surrounding *point*      | ``transpose-words`` |
+-----------------+--------------------------------------------------+---------------------+

If the cursor is at the end of the line, then these commands will transpose the last two characters/words before the cursor.

Changing case
-------------
To change the **case** of the word:

+-----------------+-------------------------------------------------+---------------------+
|    Shortcut     |                     Result                      |    Command name     |
+=================+=================================================+=====================+
| :kbd:`Meta + u` | **Uppercase** the current (or following) word   | ``upcase-word``     |
+-----------------+-------------------------------------------------+---------------------+
| :kbd:`Meta + l` | **Lowercase** the current (or following) word.  | ``downcase-word``   |
+-----------------+-------------------------------------------------+---------------------+
| :kbd:`Meta + c` | **Capitalize** the current (or following) word. | ``capitalize-word`` |
+-----------------+-------------------------------------------------+---------------------+

.. note:: I've noted that these commands change the case from *point* until the next space. If we are in the middle of a word, they don't change the case of the characters before the cursor.

For all of these 3 commands we can pass a negative argument to change the case of previous words. For example, imagine we have typed the commands::

    $ do something fun

And the cursor is at the ``u`` of ``fun``: 

1. Pressing :kbd:`Alt + -2`, will select two words before the one where point is, in this case ``do``.
2. Now pressing :kbd:`Alt + u` will uppercase the word ``do``, without moving the cursor position.

Deleting characters
-------------------
We saw these two commands at the beginning(bare essentials). They literally **delete** characters, without saving them to the kill ring:

+-------------------------+------------------------------------------------+--------------------------+
|        Shortcut         |                     Result                     |       Command name       |
+=========================+================================================+==========================+
| :kbd:`Backspace` [#f0]_ | Delete character to the **left** of point      | ``backward-delete-char`` |
+-------------------------+------------------------------------------------+--------------------------+
| :kbd:`DEL`              | Delete character to the **right** of point     | ``delete-char``          |
+-------------------------+------------------------------------------------+--------------------------+

.. [#f0] A **positive** numeric argument will **kill** the characters instead of deleting them.

.. _`changing text`: https://tiswww.case.edu/php/chet/readline/rluserman.html#SEC16
.. _`ANSI C quoting`: https://www.gnu.org/software/bash/manual/html_node/ANSI_002dC-Quoting.html#ANSI_002dC-Quoting
