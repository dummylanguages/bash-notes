**********
Completion
**********

.. toctree::
	:maxdepth: 3
	:hidden:

	completion_commands
	programmable_completion

`GNU Readline`_ provides two types of completion:

1. The **default completion** is achieved by several `commands for completion`_, being ``complete`` the most basic one, usually bound to the :kbd:`tab` key.
2. It also provides a more sophisticated feature known as `programmable completion`_.


.. _`GNU Readline`: https://tiswww.cwru.edu/php/chet/readline/rltop.html
.. _`commands for completion`: https://www.gnu.org/software/bash/manual/html_node/Commands-For-Completion.html
.. _`programmable completion`: https://www.gnu.org/software/bash/manual/html_node/Programmable-Completion.html
