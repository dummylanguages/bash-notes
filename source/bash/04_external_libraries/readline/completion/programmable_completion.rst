***********************
Programmable completion
***********************
This type of completion was made available in Bash in `v2.04`_, and it is described in the `Programmable completion`_ section of the GNU Bash manual. We saw how the **standard completion** applies to commands, variables and files. Programmable completion is specific to commands, for example::

    $ cmd [tab][tab]        # Lists all sub-commands of "cmd"
    $ cmd --[tab][tab]      # Lists all options applicable to "cmd"
    $ cmd sub --[tab][tab]  # Lists all options applicable to "sub"
    $ cmd sub [tab][tab]    # Lists all arguments applicable to "sub"
    $ cmd su[tab]           # Completes to cmd sub
    $ cmd --opt[tab]        # Completes to cmd --option

As you can see, programmable completion would autocomplete **subcommands** and **options** related to some command. All the subcommands and options for a particular command are defined in a **completion specification** (aka ``compspec``). Once the user types a command and press :kbd:`tab`, Bash invokes the ``compspec`` tied to that command, and displays the returned completions to the user. Bash doesn't know whether the completion is a sub-command, an option or anything else; the specification is the one responsible for inspecting the current state of the command line and returning a set of possible completions.

Once a specification has been created, it's tied up to a certain command using the ``complete`` Bash builtin::

    $ complete [compspec] [cmd]

Check the GNU Bash manual to learn about ``complete`` and the other `Programmable Completion Builtins`_.

Completion scripts
==================
In practice, we don't usually need to use the ``complete`` command to enjoy the benefits of autocompletion. Usually, this command is invoked from a **completion script**, which is just a Bash script which includes one or more ``complete`` statements for a specific command. Thus, if you source a completion script, the ``complete`` commands are executed in the current shell, and completion gets activated for the corresponding command.

.. note:: Check this tutorial about how to create **completion scripts**: https://iridakos.com/programming/2018/03/01/bash-programmable-completion-tutorial

Usually, completion scripts are supplied by the same people who wrote the utility they serve. For example, if for some reason you don't have available autocompletion functionality for your ``git`` command, be aware that `Git`_ provides a completion script for Bash named `git-completion.bash`_. We can search our system for this file::

    $ find / -type f -name "git-completion.bash" 2> /dev/null
    /Library/Developer/CommandLineTools/usr/share/git-core/git-completion.bash

And source it in our Bash configuration file::

    git_completion_file=/Library/Developer/CommandLineTools/usr/share/git-core/git-completion.bash
    [[ -f "${git_completion_file}" ]] && . "${git_completion_file}"

.. note:: Some people would create a **symbolic link** to this file. That's another valid approach, but make sure you create a **valid symbolic link**. Also, in the future could be misleading if the link is broken.

Or we can **download** to our home directory and source it::

    [[ -f .git-completion.bash ]] && . .git-completion.bash

We should inmediately have available all the handy autocompletion functionality for the ``git`` command.

Scripts must match our Bash version
-----------------------------------
If you are using an outdated version of Bash (like the default one included in macOS), you may be out of luck when trying to use certain completion scripts. Usually developers make clear which version of Bash is compatible with the script. Just be aware of that.

The bash-completion Project
===========================
Some completion scripts depend on a third-party project called `bash-completion`_, which is an independent project that has existed for almost as long as Bash programmable completion itself (at least since 2003). It is independent from Bash, but has become something like a **standard**, that other developers use to write completion scripts that depend on code defined in the ``bash_completion`` script .

This project is structured around a **main script** and a **directory of scripts** for commonly used utilities:

1. The ``bash_completion`` script is a Bash script that, among some other things, provides convenience functions for other completion scripts.

2. The `completions directory`_ included in the project, contains a bunch of scripts for commonly used tools like ``ssh``, ``wget``, ``curl``, ``gzip``, and many others. Developers continuously contribute scripts to this folder. These completion scripts use functions that are defined in the ``bash_completion`` script.

To make the whole thing work, we have to have both in our system. Then in our Bash configuration we have to ``source`` the ``bash_completion`` script. Once we've done that, we also have enabled all the completion scripts in the completions directory.

Apart from the completion scripts included in folder maintained by the project, nowadays a lot of third-party completion scripts depend on the bash-completion project. For example, `Docker`_ is well known for its verbose commands, so a **completion script** becomes really useful. The Docker developer make available a series of completion scripts for different shells on their Github project. We can download the `completion script for Bash`_, and source it in our Bash configuration, but we have to make sure that the ``bash-completion`` **main script** has been sourced previously. Otherwise, we will get a ``command not found error``, because the Docker script invokes functions that are defined in the ``bash_completion`` script and that are unknown to Bash.

Installing bash-completion
--------------------------
The simplest way to install ``bash-completion`` is through our system's **package manager**, although the Github page contains instructions for a manual installation.

* In macOS we can use `homebrew`_. Just be aware of the existence of two packages:

    *  `bash-completion@1`_ for Bash 3.2 (Default in macOS)
    *  `bash-completion@2`_ for Bash 4.1+

In my case, I updated Bash, so I'll run::

    $ brew install bash-completion@2
    [...]
    Add the following line to your ~/.bash_profile:
    [[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"

As you can see, the package manager informed us of the need of sourcing the ``bash_completion.sh`` in our Bash configuration, so we just have to add the lines::

    [[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] &&
    . "/usr/local/etc/profile.d/bash_completion.sh"

Folder for individual scripts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The **homebrew formula** also installs a folder with completion scripts for individual utilities that depend on ``bash_completion`` main script.

.. note:: This folder does **not** contain all the scripts that the **bash_completion project** makes available.

This folder is placed in ``/usr/local/etc/bash_completion.d``. If we install some program that includes their own **completion scripts** (that depend on ``bash_completion``), we can create **symbolic links** to the folder::

    $ ln -s /Applications/Docker.app/Contents/Resources/docker.bash-completion \
    /usr/local/etc/bash_completion.d/docker

The ``bash_completion`` **main script**, automatically sources all the **completion scripts** located in that folder, so we don't have to source them in an individual basis. This will save us work in the long run, because **homebrew formulae** will automatically install their completion scripts to this directory.

.. warning:: It works, but don't count on that for the future; I didn't found information about it in the **bash_completion** GitHub page.

Folder for local scripts
^^^^^^^^^^^^^^^^^^^^^^^^
The **bash_completion** GitHub page mentions a folder with **local scripts** which take precedence over the one shipped by **bash_completion** project. The completion files in this directory are loaded **on-demand** based on invoked commands' names, so be sure to name your completion file with the same name the command they're designed to complete. This folder is located in the ``completions`` subdir of ``$BASH_COMPLETION_USER_DIR``. This variable defaults to::

    $XDG_DATA_HOME/bash-completion

If ``$XDG_DATA_HOME`` is not set::

    ~/.local/share/bash-completion

.. note:: In my case the folder is ``~/.local/share/bash-completion/completions``.

For example, let's download this `tmux-bash-completion`_ script(which depends on ``bash_completion``), and place it inside our local folder::

    $ curl https://github.com/imomaliev/tmux-bash-completion/blob/master/completions/tmux \
    -o ~/.local/share/bash-completion/completions/tmux

.. note:: I think reloading our Bash configuration is necessary after placing the file.

.. _`GNU Readline`: https://tiswww.case.edu/php/chet/readline/readline.html
.. _`commands for completion`: https://www.gnu.org/software/bash/manual/html_node/Commands-For-Completion.html
.. _`programmable completion`: https://www.gnu.org/software/bash/manual/html_node/Programmable-Completion.html
.. _`v2.04`: http://freshmeat.sourceforge.net/projects/bashcompletion
.. _`Programmable Completion Builtins`: https://www.gnu.org/software/bash/manual/html_node/Programmable-Completion-Builtins.html
.. _`Git`: https://git-scm.com/
.. _`git-completion.bash`: https://github.com/git/git/blob/master/contrib/completion/git-completion.bash
.. _`completions directory`: https://github.com/scop/bash-completion/tree/master/completions
.. _`Docker`: https://docker.com
.. _`completion script for Bash`: https://github.com/docker/cli/blob/master/contrib/completion/bash/docker
.. _`bash-completion`: https://github.com/scop/bash-completion
.. _`homebrew`: https://brew.sh
.. _`bash-completion@1`: https://formulae.brew.sh/formula/bash-completion
.. _`bash-completion@2`: https://formulae.brew.sh/formula/bash-completion@2
.. _`tmux-bash-completion`: https://github.com/imomaliev/tmux-bash-completion

