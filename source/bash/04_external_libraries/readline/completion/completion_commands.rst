*******************
Completion commands
*******************
`GNU Readline`_ also provides several **commands for completion**, being ``complete`` the most basic one, usually bound to the :kbd:`tab` key. The GNU Bash manual describes these commands in a section titled `Letting Readline Type For You`_.

The **default completion** allows us to auto-complete:

* command names
* variables
* filenames

For example::

    $ ec[tab]             # Completes to echo
    $ echo $PA[tab]       # Completes to echo $PATH
    $ cd Downl            # Completes to cd Downloads

If we don't get our completion after hitting :kbd:`tab` **once**, it mayb be because there are multiple possible completions for what we've typed so far. In that case, hitting :kbd:`tab` **twice** will get us a list with all the available completions.

.. _`GNU Readline`: https://tiswww.case.edu/php/chet/readline/readline.html
.. _`Letting Readline Type For You`: https://www.gnu.org/software/bash/manual/html_node/Commands-For-Completion.html
.. _`v2.04`: http://freshmeat.sourceforge.net/projects/bashcompletion
