Moving the cursor
=================
The following table includes some useful shortcuts to move the cursor:

+-----------------+------------------------------------------------+------------------------+
|    Shortcut     |                     Result                     |      Command name      |
+=================+================================================+========================+
| :kbd:`Ctrl + a` | To the start of the line                       | ``beginning-of-line``  |
+-----------------+------------------------------------------------+------------------------+
| :kbd:`Ctrl + e` | To the end of the line                         | ``end-of-line``        |
+-----------------+------------------------------------------------+------------------------+
| :kbd:`Ctrl + f` | One **character** forward (like :kbd:`Left`)   | ``forward-char``       |
+-----------------+------------------------------------------------+------------------------+
| :kbd:`Ctrl + b` | One **character** backward (like :kbd:`Right`) | ``backward-char``      |
+-----------------+------------------------------------------------+------------------------+

It's a loose convention in the Unix world than :kbd:`Ctrl` keystrokes operate on **characters** while :kbd:`Meta` keystrokes operate on **words**. The following table uses :kbd:`Meta`:

+-----------------+-----------------------+-------------------+
|    Shortcut     |        Result         |   Command name    |
+=================+=======================+===================+
| :kbd:`Meta + f` | One **word** forward  | ``forward-word``  |
+-----------------+-----------------------+-------------------+
| :kbd:`Meta + b` | One **word** backward | ``backward-word`` |
+-----------------+-----------------------+-------------------+

In **macOS** terminal application, there's a setting to **Use Option as Meta key**.

.. note:: Modern systems lack a dedicated :kbd:`Meta` key. Instead we can use :kbd:`Alt` or :kbd:`Esc`.

