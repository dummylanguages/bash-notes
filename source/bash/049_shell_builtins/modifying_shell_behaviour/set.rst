.. _set-builtin-label:

*******************
The ``set`` builtin
*******************
According to the `GNU Bash Manual`_, the ``set`` builtin is so complicated that deserves to be treated in its `own section`_. Basically, ``set`` allows us to set or unset **options** and **positional parameters**.

.. note:: The ``set`` command is `POSIX compliant`_.

Basic use
=========
Basically, the ``set`` builtin allows us to set **positional parameters**. For example::

    $ set -- one 33 'another argument'
    $ echo "$@"
    one 33 another argument

It is a good practice to use the ``--`` option right before the arguments to let ``set`` know, that the next argument is not one of its options. We could have written the line above as::

    $ set one 33 'another argument'
    $ echo "$@"
    one 33 another argument

And we would have gotten the same results, but some day along the line we may want to pass an argument named ``-a`` for example, and guess what, that's one of the arguments of the ``set`` builtin. So using ``--`` we are telling ``set``, bro, don't interpret the ``-a`` as one of your options, it's just a positional argument. For example::

    $ set -- -a -b -e
    $ echo "$@"
    -a -b -e
    $ echo "$#"
    3

In the command above, even though ``-a``, ``-b`` and ``-e`` are valid **set options**, thanks to the ``--`` option, ``set`` interprets them as **positional arguments**. (``$#`` tells us the number or positional arguments recently set)

Unsetting
=========
We can unset all positional parameters by using the ``--`` option by itself, without any arguments. For example::

    $ set --

.. _`GNU Bash Manual`: https://www.gnu.org/software/bash/manual/html_node
.. _`own section`: https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
.. _`POSIX compliant`: https://pubs.opengroup.org/onlinepubs/9699919799/