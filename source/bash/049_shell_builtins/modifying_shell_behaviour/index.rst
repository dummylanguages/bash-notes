#########################
Modifying shell behaviour
#########################

.. toctree::
    :maxdepth: 3

    set


Apart from the `Bourne Shell Builtins`_ and the `Bash Builtin Commands`_, the GNU Bash manual contains a section dedicated to builtins that `modify shell behaviour`_.


.. _`Bourne Shell Builtins`: https://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Builtins.html
.. _`Bash Builtin Commands`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html
.. _`modify shell behaviour`: https://www.gnu.org/software/bash/manual/html_node/Modifying-Shell-Behavior.html

