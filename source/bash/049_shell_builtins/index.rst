######################
Shell Builtin Commands
######################

.. toctree::
    :maxdepth: 3
    :hidden:

    bash_builtins/index
    bourne_shell_builtins/index
    modifying_shell_behaviour/index

Builtin commands are programs contained within the shell itself. When the name of a builtin command is invoked, the shell executes the command directly, without invoking another program. Builtin commands are necessary to implement functionality impossible or inconvenient to obtain with separate utilities.

Some of the builtins that Bash implements come from the Bourne Shell, other ones are unique to or have been extended in Bash. Some of the builtins are covered in other sections, such as:

* Job Control Builtins, are builtin commands which provide the Bash interface to the job control facilities.
* Directory Stack Builtins, are commands to manipulate the directory stack.
* Bash History Builtins, are commands to interact with the History library.
* Programmable Completion Builtins, are commands related to the programmable completion facilities.

.. _`Shell Builtin Commands`: https://www.gnu.org/software/bash/manual/html_node/Shell-Builtin-Commands.html#Shell-Builtin-Commands

