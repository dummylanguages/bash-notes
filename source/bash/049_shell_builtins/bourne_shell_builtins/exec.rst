********************
The ``exec`` command
********************
One of the most useful `Bourne Shell Builtins`_ is the ``exec`` command. Its synopsis::

    exec [-c] [-l] [-a name] [command [arguments ...]] [redirection ...]

* The ``-c`` option causes command to be executed with an **empty environment**.
* If the ``-l`` option is supplied, the shell places a **dash** at the beginning of the zeroth argument passed to command. This is what the ``login`` program does.
* If ``-a name`` is supplied, the shell passes ``name`` as the zeroth **argument** to command. If **command** cannot be executed for some reason:

    * A **non-interactive shell** exits, unless the ``execfail`` shell option is enabled. In that case, it returns failure.
    * An **interactive shell** returns failure if the file cannot be executed. A subshell exits unconditionally if ``exec`` fails.

* If *command* is supplied, it replaces the shell without creating a new process.
* If no command is specified, *redirections* may be used to affect the current shell environment. If there are no redirection errors, the return status is zero; otherwise the return status is non-zero.

Basic use
=========
There are **two main ways** of using ``exec``:

1. Passing a **command**, which will replace the current shell process with the one of the command we are passing. When this command finishes executing, the process exits. There are a lot of use cases for this, a simple one::

    $ exec rbash

Replace the current bash shell with `rbash`_, the **restricted bash login shell**. Because the original bash shell is destroyed, the user will not be returned to a privileged bash shell when rbash exits.

2. Passing a **redirection**, which is quite useful inside scripts to redirect **output** and/or **error** standard streams to log files and such. In this case ``exec`` does **not** destroy the current shell process.

.. warning:: We won't cover redirections in this section, they're already covered in our section about :ref:`file descriptors <file-descriptors-label>`

When do I use ``exec``
======================
This command has two main applications:

Wrapper scripts
---------------
``exec`` is used in shell scripts that do some set-up work and then invoke another program written in another language, which doesn't need the shell to run. Once this second program is started, it replaces the shell process, so we don't have an extra process hanging around uselessly and taking up resources.

.. note:: This type of scripts are known as **wrapper scripts**.

Without ``exec`` we would have two processes, the one of the setup script (even when it has finished and doesn't serve any purpose) and the one of the program we were trying to setup. One way to get rid of the setup process is writing at the end of the script ``exit $?``

If our script invokes ``exec command``, and command cannot be executed for some reason, a non-interactive shell exits, unless the ``execfail`` shell option is enabled. In that case, it returns failure. If you want to set that option::

    $ shopt -s execfail

More info
=========
http://www.softpanorama.org/Tools/exec.shtml

.. _`Bourne Shell Builtins`: https://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Builtins.html#Bourne-Shell-Builtins
.. _`rbash`: https://www.gnu.org/software/bash/manual/html_node/The-Restricted-Shell.html