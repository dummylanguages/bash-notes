********************
The ``hash`` command
********************
The ``hash`` command is another `Bourne Shell Builtins`_. Its synopsis::

    hash [-r] [-p filename] [-dt] [name]

https://www.putorius.net/bash-hash-command.html

.. _`Bourne Shell Builtins`: https://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Builtins.html#Bourne-Shell-Builtins