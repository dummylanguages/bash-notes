#####################
Bourne shell builtins
#####################
The `Bourne Shell Builtins`_ are those commands implemented in Bash to maintain compatibility with the ones used by the Bourn shell.

.. toctree::
    :maxdepth: 3

    period
    exec
    hash

.. _`Bourne Shell Builtins`: https://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Builtins.html#Bourne-Shell-Builtins

