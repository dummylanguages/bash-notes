****************************
The ``read`` builtin command
****************************
``read`` is one of the `Bash Builtin Commands`_ that really comes in handy in a lot of situations. This command can read either:

1. From the **standard input**
2. From the **file descriptor** supplied as an argument to the ``-u`` option

Reading from standard input
===========================
Let's keep it simple at first::

    $ read
    Hello world!
    $ echo $REPLY
    Hello world!

When we issue the ``read`` command, we can start typing until we press :kbd:`Enter`. Then we can get our text referencing the ``REPLY`` special variable.

Reading from here strings
-------------------------
**Here strings** offer a way of feeding ``read`` (or any command) a string of text, as if it was coming from **standard input**. For example::

    $ read <<< "Hello world!"
    $ echo $REPLY
    Hello world!

Reading from file descriptors
=============================
We can ``read`` from an **input fildes** using the ``-u`` option::

    $ exec 3<<< 'Hello world!'
    $ read -u 3
    $ echo $REPLY
    Hello world!

Don't forget to close the fildes when you finish using it: ``exec 3<&-``.

Variables
=========
Regardless of the source of ``read`` (standard input or a file descriptor), if we specify **variable names**, the input will be split according to Bash `word splitting`_ rules, and assigned to each variable. For example::

    $ echo "Write your name, age and favourite band please."
    $ read name age band
    Bob 42 The Rolling stones
    $ echo "Nice to meet you $name, I'm also $age and $band is a great band!"
    Nice to meet you Bob, I'm also 42 and The Rolling stones is a great band!

As you can see, the line written by the user is split among the variables:

* ``name`` is assigned the first word, **Bob**
* ``age`` is assigned the second word, **42**
* ``band`` is assigned the rest of the words, **The Rolling Stones**

There are three possible scenarios:

1. When the number of variables is **equal** to the number of words, each variable is assigned its respective word.
2. When the number of variables is **smaller** than the resulting words, (our example) each variable is assigned a word and the last variable is assigned all the remaining words.
3. When the number of variables is **bigger** than the number of words, some variables will remain empty.

The ``REPLY`` special variable
------------------------------
If we don't use any variable, for example::

    $ read <<< 'This is a line'
    $ echo $REPLY 
    This is a line

The `REPLY`_ Bash special variable is assigned the **whole line**. The purpose of this variable is simply to provide a name for the input received by ``read`` builtin command when **no variable** has been specified.

.. note:: ``REPLY`` is not **POSIX®**, you need to set ``IFS`` to the empty string to get the whole line for shells that don't know ``REPLY``.

Options
=======
We've already explained how the ``-u`` option works. Let's take care of the rest of the available options.

Raw mode
--------
In normal circumstances, when we are reading from **standard input**, we can keep typing text until we press :kbd:`Enter`. For example::

    $ read
    Some text
    $ echo $REPLY
    Some text

If we need to keep typing text in new lines we can use the backslash ``\`` to **escape** the :kbd:`Enter`::

    $ read
    line1 \line2 \
    > line3
    $ echo $REPLY
    line1 line2 line3

As you can see, the ``\`` character used to escape :kbd:`Enter`, and the one before are **not** ``read``. But if we want to record these characters literally, we must use the ``-r`` option::

    $ read -r
    line1 \line2 \
    $ echo $REPLY
    line1 \line2 \

If this option is given, backslash does not act as an escape character, instead they are considered to be part of the line. In particular, a **backslash-newline** pair may not then be used as a line continuation.

.. note:: The ``-r`` option is the only **POSIX®** compatible, so if you need portability in your scripts, this is pretty much the only option you can use.

Input line terminator
---------------------
By default, a **newline** character is used to terminate the input, but we can change that using the ``-d`` option::

    $ read -d #
    line1
    line2
    #

Right after we press ``#`` our input terminates, and we get back our prompt. The input sent consists of::

    $ echo $REPLY
    line1 line2

If we use an **empty string** as a delimiter, ``read`` will terminate a line when it reads a **NUL** character.

.. warning:: Somewhere I read something cool to do with this very last feature.

Creating arrays
---------------
We already mentioned that when we specify variables, the input is split into words. There's an option to create an **array** out of the **word splitting** results::

    $ read -a fruits
    orange apple banana
    $ echo "fruits has ${#fruits[*]} elements: ${fruits[0]}, ${fruits[1]} and ${fruits[2]}."
    fruits has 3 elements: orange, apple and banana.

When using this option, specifying an extra variable name (apart from the name of the array), is useless; the array is set, but not the variable.

Prompting for input
-------------------
A very useful option for scripts that need user input is ``-p``. It displays a prompt message, without a trailing newline, before attempting to read any input. For example::

    $ read -p "What's your name? " username
    $ Bob
    $ echo "Welcome $username"
    Welcome Bob

Not echoing input
-----------------
When the ``-s`` option is used, user's input won't be echoed to the terminal. That's quite useful when asking users for passwords::

    $ read -s -p "Type your password: " pwd; echo
    $ echo "Your secret password is: $pwd (Shh.. don't tell anybody)"
    Your secret password is: 1234 (Shh.. don't tell anybody)

By the way, the last ``echo`` at the end of the first line is just to put a new line at the end of the user's input.

Taking advantage of Readline library features
---------------------------------------------
Using the ``-e`` option allows you to use the Readline library functionality while editing the input. This is the library that provides shell features that allow us to move the cursor, delete words, delete from cursor to end of line, etc.

When using this option, we can use one really useful. ``-i <STRING>`` preloads the input buffer with text from ``<STRING>``. This option useful is really useful when offering the user a default option that she can either accept or modify. For example::

    $ read -e -i /home/bob/Downloads chosen_folder
    /home/bob/Downloads
    $ echo "File downloaded to: $chosen_folder"
    File downloaded to: /home/bob/Downloads

In the example above, while modifying the destination folder, the user can use Readline features such as pressing :kbd:`TAB` to use autocompletion for the directory.

Internal Field Separator
------------------------
``IFS`` is one of the special `Bourne shell variables`_ used by Bash. Its role is to serve as the **separator** when the shell performs **word splitting**. When this variable is not set, Bash uses a sequence of :kbd:`<space><tab><newline>` characters to split words, but . For example::

    $ read
        word1 word2      word3
    $ echo $REPLY
    word1 word2 word3

Note that any **sequence of whitespace characters**:

1. At the **beginning or end** of the input will be ignored since they are **not separating words**.
2. In **between words** is treated as one single delimiter.

Another example using **variables**::

    $ read a b
        var1 this    is      var2
    $ echo "$a"
    var1
    $ echo "$b"
    this    is      var2

Note that variable ``b`` retains the whitespace only because is considered a single field. This is also the behaviour when we manually set ``IFS=$' \t\n'``.

.. note:: Note that setting ``IFS=$' \t\n'`` means that either a :kbd:`space`, :kbd:`tab` or :kbd:`newline` will be considered as a delimiter. But not that the sequence **space/tab/newline** is a delimiter. ``IFS`` is not a single, multicharcter delimiter; instead, it is a collection of single-character delimiters. 

Custom ``IFS``
^^^^^^^^^^^^^^
Imagine we want to read the fields of a `CSV`_ file. We may want to set up ``IFS=,``. For example::

    $ IFS=',' read f1 f2 f3 f4 <<< '     field1, This    is    field2,, Last field       '
    $ echo "|$f1|"
    |     field1|
    $ echo "|$f2|"
    | This    is    field2|
    $ echo "|$f3|"
    ||
    $ echo "|$f4|"
    | Last field       |

Note that:

* If we use several ``,`` together, they are treated as separate delimiters, and the field in between is considered empty.
* The leading whitespace is preserved, as well as the internal whitespace inside each field.

Reading lines
^^^^^^^^^^^^^
At the beginning we saw how issuing ``read`` on a multi-line file makes it advance line by line through the file. We can automate this with a `while`_ loop. For example::

    $ while read line; do echo "|$line|"; done <<< $'  line    1  \n  line    2  \n  line    3  '
    |line    1|
    |line    2|
    |line    3|

But note how we are losing the **leading/trailing whitespace** on each line.

.. note:: To preserve white space at the beginning or the end of a line, it's common to specify ``IFS=`` (with no value) immediately before the ``read`` command. After reading is completed, the ``IFS`` returns to its previous value.

To avoid that, we can set the value of ``IFS`` to an **empty value** so that whitespace is not interpreted as a line delimiter::

    $ while IFS= read line; do echo "|$line|"; done <<< $'    line    1    \n    line    2    \n    line    3    '
    |    line    1    |
    |    line    2    |
    |    line    3    |

Note that ``IFS= read`` sets the environment variable ``IFS`` (to an empty value) only for each execution of ``read``.

.. note:: That's because this is an instance of the general `simple command`_ syntax defined by **POSIX®**: a (possibly empty) sequence of **variable assignments** followed by a command and its arguments (also, you can throw in redirections at any point). Since ``read`` is a **built-in**, the variable never actually ends up in an external process's environment. Note that ``read`` is **not** a **special built-in**, so the assignment does last only for its duration.


.. _`Bash Builtin Commands`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html
.. _`word splitting`: https://www.gnu.org/software/bash/manual/html_node/Word-Splitting.html#Word-Splitting
.. _`REPLY`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html
.. _`Bourne shell variables`: https://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Variables.html#Bourne-Shell-Variables
.. _`simple command`: https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html#tag_02_09_01
.. _`CSV`: https://en.wikipedia.org/wiki/Comma-separated_values
.. _`while`: https://www.gnu.org/software/bash/manual/html_node/Looping-Constructs.html#Looping-Constructs