#############
Bash builtins
#############
`Bash Builtin Commands`_ are those commands that are unique to or have been extended by POSIX or Bash. 

.. toctree::
    :maxdepth: 3

    echo_builtin
    printf_builtin
    read_builtin


.. _`Bash Builtin Commands`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html#Bash-Builtins

