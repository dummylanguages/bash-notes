.. _printf-label:

******************************
The ``printf`` builtin command
******************************
According to `Wikipedia`_:

    ``printf`` (short for "**print f**ormatted") is a **shell builtin** and **utility program** that formats and prints data.

That means that there are **several implementations** of the command; the ones I know about are:

* ``printf`` is one of the `Bash Builtin Commands`_.
* There is also a version included in the `GNU Core Utilities`_.
* The BSD implementation included in **macOS**, `FreeBSD`_ or `OpenBSD`_. This version appeared for the first time in `4.3BSD-Reno`_

Here we'll cover the Bash builtin version, whose synopsis is::

    printf [-v var] format [arguments]

As you can see, the only required argument is ``format`` which could be just a string::

    $ printf 'Hello world!\n'
    Hello world!

Note that, as opposed to ``echo``, ``printf`` doesn't automatically add a **newline** character at the end of the string; if we want one, we have to supply it ourselves.

Basic use
=========
By default, this command writes the formatted arguments to the **standard output** under the control of ``format``, for example::

    $ var='world!'
    $ printf "Hello %s \n" $var
    Hello world!

In the command above, ``format`` is represented by the string ``Hello %s \n``. This string must be enclosed in **double quotes** or **single quotes**, and contains three types of objects:

* **Plain characters**, which are simply copied to standard output. (``Hello``)
* **Escape sequences**, which are converted and copied to the standard output. (``\n``)
* **Format specifications**, which determine how the arguments given after ``format`` are gonna be formatted and printed.(``%s``)

Assigning output to a variable
==============================
The ``-v`` option causes the output to be assigned to a **variable**, rather than being printed to the standard output. For example::

    $ printf -v var 'Hello %s \n' 'world!'
    $ printf "$var"
    Hello world!

Escape sequences
================
The **format string** interprets escape sequences. All escape sequences start with a **backslash**. The table below contains a summary.

+---------------+---------------------------+
| Specification |          Meaning          |
+===============+===========================+
| ``\"``        | Escape **double quote**   |
+---------------+---------------------------+
| ``\\``        | Escape **backslash**      |
+---------------+---------------------------+
| ``\a``        | Alert. Rings a bell.      |
+---------------+---------------------------+
| ``\b``        | Backspace                 |
+---------------+---------------------------+
| ``\c``        | Produce no further output |
+---------------+---------------------------+
| ``\e``        | Escape                    |
+---------------+---------------------------+
| ``\f``        | Form feed                 |
+---------------+---------------------------+
| ``\n``        | Newline                   |
+---------------+---------------------------+
| ``\r``        | Carriage return           |
+---------------+---------------------------+
| ``\t``        | Horizontal tab            |
+---------------+---------------------------+
| ``\v``        | Vertical tab              |
+---------------+---------------------------+


Format specifications
=====================
The Bash documentation remits us to the `printf(1)`_ man page for a list of format specifications. There they inform us we can get the full documentation at http://www.gnu.org/software/coreutils/printf.

.. note:: In C language `printf`_ is one of the main output functions. In that context format specifications are known as **format specifiers** or **conversion specifications**.





.. _`Wikipedia`: https://en.wikipedia.org/wiki/Printf_(Unix)
.. _`Bash Builtin Commands`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html
.. _`GNU Core Utilities`: https://www.gnu.org/software/coreutils/manual/html_node/printf-invocation.html#printf-invocation
.. _`FreeBSD`: https://www.freebsd.org/cgi/man.cgi?query=printf&apropos=0&sektion=0&manpath=FreeBSD+12.1-stable&arch=default&format=html
.. _`OpenBSD`: https://man.openbsd.org/printf.1
.. _`4.3BSD-Reno`: https://en.wikipedia.org/wiki/History_of_the_Berkeley_Software_Distribution
.. _`printf(1)`: https://manpages.debian.org/buster/coreutils/printf.1.en.html
.. _`printf`: https://en.cppreference.com/w/c/io/fprintf
