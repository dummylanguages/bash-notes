****************************
The ``echo`` builtin command
****************************
``echo`` is one of the `Bash Builtin Commands`_. Its synopsis is:

    echo [-neE] [arg …]

* Outputs the *args*, separated by spaces, terminated with a **newline**. If ``-n`` is specified, the trailing newline is suppressed.
* The **return status** is ``0`` unless a write error occurs.

Print new line at the end
=========================
By default, ``echo`` prints a **newline character** at the end of its arguments::

    $ echo hello world
    hello world
    $

In the example above we passed two arguments, ``hello`` and ``world``, which were printed as such, with a **newline character** at the end. If we want to suppress this newline character, we have to use the ``-n`` option::

    $ echo -n hello world
    hello world$ # Prompt is here!

As you can see, when the output is printed, there's not newline at the end of it, that's why the **prompt** appears right next to it.

.. note:: The ``-n`` option it's really useful when writing scripts to adjust the behaviour of ``echo``. Sometimes we may not want that **newline character** at the end.

Escape characters expansion
===========================
``echo`` may or may not expand **escape characters** embedded in a string. What do we mean by that? A escape character is a sequence such as ``\n``, which may or may not be interpreted a **new line character** or ``\t``, which may or may not be interpreted as a **tab character**. For example::

    $ echo "Name\tAge\nBob\t45"
    Name\tAge\nBob\t45\n

The output above shows how these characters were showed literally. That means that in our system, by default, ``echo`` does not expand **escape characters**. But we can make it expand them using the ``-e`` option::

    $ echo -e "Name\tAge\nBob\t45"
    Name	Age
    Bob	    45

Default behaviour
-----------------
The ``xpg_echo`` shell option may be used to dynamically determine whether or not echo expands **escape characters** by **default**. We can **check** the status of this option by running::

    $ shopt xpg_echo
    xpg_echo       	off

We can turn it **on** using ``shopt -s``::

    $ shopt -s xpg_echo
    $ echo "Name\tAge\nBob\t45"
    Name    Age
    Bob     45

As you can see, in the example above we didn't have to use the ``-e`` option to make ``echo`` expands the **escape characters**. To **disable** the option::

    $ shopt -u xpg_echo

.. note:: Check the GNU Bash official manual for a **list of options** for the `shopt builtin`_. At the end of it you can read about ``xpg_echo``.

Escape sequences
================
The following table contains the list of **escape sequences** that ``echo`` can interpret:

+==================+========================+
| Escape sequence  | Meaning                |
+------------------+------------------------+
| ``\a``           | alert (bell)           |
+------------------+------------------------+
| ``\b``           | backspace              |
+------------------+------------------------+
| ``\c``           | supress further output |
+------------------+------------------------+
| ``\e`` or ``\E`` | escape                 |
+------------------+------------------------+
| ``\f``           | form feed              |
+------------------+------------------------+
| ``\n``           | new line               |
+------------------+------------------------+
| ``\r``           | carriage return        |
+------------------+------------------------+
| ``\t``           | horizontal tab         |
+------------------+------------------------+
| ``\c``           | vertical tab           |
+------------------+------------------------+
| ``\\``           | backslash              |
+------------------+------------------------+

There are as well some escape characters to print **eight-bit characters**, check them out:

+=================+====================================================================+
| Escape sequence | Meaning                                                            |
+-----------------+--------------------------------------------------------------------+
| ``\0nnn``       | an eight-bit character whose value is the octal value ``nnn``      |
+-----------------+--------------------------------------------------------------------+
| ``\xHH``        | an eight-bit character whose value is the hexadecimal value ``HH`` |
+-----------------+--------------------------------------------------------------------+
| ``\uHHHH``      | a Unicode (ISO/IEC 10646) character (one to four hex digits)       |
+-----------------+--------------------------------------------------------------------+
| ``\UHHHHHHHH``  | a Unicode (ISO/IEC 10646) character (one to eight hex digits)      |
+-----------------+--------------------------------------------------------------------+

For example, if we wanted to print the emoji 😉, we could use its Unicode::

    $ echo -e '\U1F609'
    😉

Using ``echo`` is discouraged
=============================
According to `POSIX`_, this command has not been deprecated because of its extremely widespread use in historical applications. 
New applications are encouraged to use :ref:`printf <printf-label>` instead of ``echo``.

It is not possible to use echo **portably** across all POSIX systems unless both the ``-n`` option and any **escape sequences** are omitted.

Read more about ``echo`` and ``printf``:

* https://www.in-ulm.de/~mascheck/various/echo+printf/
* https://unix.stackexchange.com/questions/65803/why-is-printf-better-than-echo?noredirect=1&lq=1

Tricks
======
Imagine you have a file that contains some **escape sequences** that you want to expand::

    $ $ echo 'Name\tAge\nBob\t45' > special.txt
    $ cat -ve special.txt
    Name\tAge\nBob\t45$

As you can see, using the ``-ve`` options with ``cat``, shows us the **newline character** (represented with a ``$``) that ``echo`` added at the end of the string when redirecting its output to the ``special.txt`` file. But notice also how the **escape characters** are just characters with no special meaning.

We could use ``echo`` combined with `command substitution`_  to display the contents of such a file::

    $ echo -e $(cat special.txt)
    Name    Age
    Bob     45

Or using the old command expansion syntax::

    $ echo -e `cat special.txt`
    Name    Age
    Bob     45

As an alternative to expanding the output of ``cat``, we could redirect the file to the input of cat using `input redirection`_ (the ``<`` operator)::

    $ echo -e $(< special.txt)
    Name    Age
    Bob     45

Or ``echo -e `< special.txt``` if you prefer the old syntax.

.. _`Bash Builtin Commands`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html
.. _`shopt builtin`: https://www.gnu.org/software/bash/manual/html_node/The-Shopt-Builtin.html#The-Shopt-Builtin
.. _`POSIX`: https://pubs.opengroup.org/onlinepubs/009695399/utilities/echo.html
.. _`command substitution`: https://www.gnu.org/software/bash/manual/html_node/Command-Substitution.html
.. _`input redirection`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Redirecting-Input