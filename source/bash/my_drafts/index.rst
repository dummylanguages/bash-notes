###############
Shell operation
###############
In this section we are going to see how the shell does its thing, to be more precise, what steps follows the interpreter in order to execute a command:

1. It reads its input from:

	* A file. (Shell script)
	* An interactive shell session.
	* The string supplied as an **argument** when ``bash`` is `invoked`_ with the ``-c`` option.

2. **Token recognition** is the phase when the interpreter breaks the input into tokens: **words** and **operators**, which are separated by metacharacters. **Quoting** and **alias expansion** are performed during this phase.
3. Then the shell parses the input into `simple commands`_ and `compound commands`_.
4. `Shell expansions`_.
5. `Redirections`_.
6. `Command execution`_.
7. The shell optionally, it waits for the command to complete and collects its `exit status`_.


.. _`invoked`: https://www.gnu.org/software/bash/manual/html_node/Invoking-Bash.html#Invoking-Bash
.. _`simple commands`: https://www.gnu.org/software/bash/manual/html_node/Simple-Commands.html#Simple-Commands
.. _`compound commands`: https://www.gnu.org/software/bash/manual/html_node/Compound-Commands.html#Compound-Commands
.. _`Shell expansions`: https://www.gnu.org/software/bash/manual/html_node/Shell-Expansions.html#Shell-Expansions
.. _`Redirections`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Redirections
.. _`Command execution`: https://www.gnu.org/software/bash/manual/html_node/Executing-Commands.html#Executing-Commands
.. _`exit status`: https://www.gnu.org/software/bash/manual/html_node/Exit-Status.html#Exit-Status
