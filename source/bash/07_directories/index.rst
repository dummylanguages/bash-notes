###########
Directories
###########

.. toctree::
	:maxdepth: 3
	:hidden:

	directory_stack


.. _`directory stack`: https://www.gnu.org/software/bash/manual/html_node/The-Directory-Stack.html#The-Directory-Stack
