***************
Directory stack
***************
The `directory stack`_ is is a list of recently-visited directories. At a minimum, it contains the **current directory**, which is always the "top" of the directory stack.

There are three `builtin commands`_ that allow us to manipulate this stack:

* The ``pushd`` builtin adds directories to the stack as it changes the current directory.
* The ``popd`` builtin removes specified directories from the stack and changes the current directory to the directory removed.
* The ``dirs`` builtin displays the contents of the directory stack.

The ``dirs`` command
====================
When we use this command by itself, it prints the contents in horizontal. A couple of useful options are:

* The ``-v`` option print the directory stack with one entry per line, prefixing each entry with its index in the stack.
* Using ``-c`` clears the directory stack by deleting all of the elements. 


.. _`directory stack`: https://www.gnu.org/software/bash/manual/html_node/The-Directory-Stack.html#The-Directory-Stack
.. _`builtin commands`: https://www.gnu.org/software/bash/manual/html_node/Directory-Stack-Builtins.html#Directory-Stack-Builtins
