******************
Special parameters
******************
There are certain parameters in Bash that can only be referenced; assignment to them is not allowed.

Below is a summary:

* ``$*`` Expands to the positional parameters, starting from one.
* ``$@`` Expands to the positional parameters, starting from one.
* ``$#`` Expands to the number of positional parameters in decimal.
* ``$?`` Expands to the exit status of the most recently executed foreground pipeline.
* ``$-`` Expands to the current option flags as specified upon invocation, by the set builtin command, or those set by the shell itself (such as the -i option).
* ``$$`` Expands to the process ID of the shell. In a ``()`` **subshell**, it expands to the process ID of the invoking shell, not the subshell.
* ``$!`` Expands to the process ID of the job most recently placed into the background, whether executed as an asynchronous command or using the bg builtin.
* ``$0``  Expands to the name of the shell or shell script.
* ``$_`` At shell startup, set to the absolute pathname used to invoke the shell or shell script being executed as passed in the environment or argument list. Subsequently, expands to the last argument to the previous simple command executed in the foreground, after expansion. 

Check the GNU Bash manual for more information about `Special parameters`_.

.. _`Special parameters`: https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html
