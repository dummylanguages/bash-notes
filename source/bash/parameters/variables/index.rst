#########
Variables
#########

.. toctree::
	:maxdepth: 3
	:hidden:

	01_variables
	02_local_var
	03_environment_var
	04_shell_var
	05_namerefs


In Bash, a `parameter`_ denoted by a *name* is referred to as a **variable**. All variables have:

* A **value**, which can be the **null string**
* Zero or more **attributes**.

Variable names may consist solely of **letters**, **numbers**, and **underscores**. For example::

	var=13
	var2=coffee
	_another_var2=tea

They can begin only with a **letter** or **underscore**. Some **illegal** names would be::

	2var=coffee			# Can't start with a number
	another-var=tea		#Can't contain an underscore

Also, variables are **case-sensitive**::

	var=1
	Var=2
	$ echo $var
	1
	$ echo $Var
	2


.. _`parameter`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameters.html#Shell-Parameters
