***************
Name references
***************
As a reminder, in Bash a **variable** is a parameter denoted by a name. A variable has a **value** and zero or more **attributes**. A variable can be assigned the ``nameref`` attribute using the ``-n`` option to any of the following `Bash Builtin Commands`_:

1. ``typeset``
2. ``declare``
3. ``local``

A variable with the ``nameref`` attribute is a variable that references another variable. That allows variables to be manipulated indirectly:  whenever the **nameref variable** is referenced, assigned to, unset, or has its attributes modified (other than using or changing the nameref attribute itself), the operation is actually performed on the **real variable**. For example:

.. code-block:: bash

    $ var="Some value"
    $ declare -n ref=var
    $ echo $ref     # Referencing the nameref variable
    Some value
    $ ref="Another value"   # Assigning to the nameref variable
    $ echo $var
    Another value
    $ unset ref     # Unsetting the nameref variable
    $ declare -p var
    -bash: declare: var: not found

.. _nameref-label:

When to use namerefs
====================
The use cases for **nameref variables** are quite rare.

Within shell functions
----------------------
A **nameref** is commonly used within shell **functions** to refer to a variable whose name is passed as an argument to the function. For instance:

.. code-block:: bash

    #!/bin/bash

    var="Old value"

    function change_value () {
        local -n nr=$1
        nr="A new value"
        echo "Calling ${FUNCNAME[0]}... Now ${!nr} is: ${nr}"
    }

    echo "Before calling the function, var: $var"
    change_value var   # Let's call the function, passing var as argument
    echo "Outside the function, var: $var"

We have successfully changed the value of ``var`` from inside the function. Even though ``nr`` was created as a ``local`` variable, it was given the ``nameref`` attribute, which allows it to modify the value of ``var``.

Note that when we are doing **indirect parameter expansion**, if we use a **nameref** (``${!nr}``), it expands to the **name of the variable** instead of the value that would result of performing the complete indirect expansion. In this case, ``${!nr}`` expands to ``var`` but not to the value of ``var`` (that would be ``A new value``).

.. note:: Just to be clear, in normal circumstances (without using a **nameref variable**), when we invoke a function passing some variable as an argument, we are just passing the **value**. The variable outside the function is left alone.

As the control variable in loops
--------------------------------
If the **control variable** in a for loop has the ``nameref`` attribute, the list of words to iterate over can be a list of shell variables, and a name reference will be established for each word in the list, in turn, when the loop is executed. For example:

.. code-block:: bash

    $ declare -n nr
    $ i=1       # A simple loop counter
    $ for nr in var1 var2 var3      # nr=var1, nr=var2, nr=var3
    > do
    > nr=$((i++))       # We assign values through the nameref variable
    > done
    $ echo $var1 $var2 $var3
    1 2 3

Referencing array variables
===========================
**Array variables** cannot be given the ``nameref`` attribute. However, nameref variables can reference array variables and subscripted array variables.

Unsetting nameref variables
===========================
Namerefs can be unset using the ``-n`` option to the ``unset`` builtin (see Bourne Shell Builtins). For example:


Otherwise, if ``unset`` is executed with the name of a nameref variable as an argument (without using ``-n``), the variable referenced by the nameref variable will be unset. For example:

.. _`Bash Builtin Commands`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html#Bash-Builtins
