Environment variables
=====================
There are two types of variables in Bash:

1. We have **normal variables**, the ones that have a scope limited to the shell where they have been created and are not inherited by subshells.
2. **Environment variables**, which are available system-wide to all processes.

Normal variables only live as long as the current shell session lives. For example, if we define the variable::

    $ shortlived='original value'
    $ echo $shortlived
    original value

But if we start a new instance of Bash (by simply calling ``bash`` again) that will start a new shell session, known as a **subshell**. And if we try to access the variable::

    $ bash
    $ echo $shortlived

    $ declare -p shortlived
    bash: declare: shortlived: not found

That variable doesn't exist in the subshell. For the same reason, shell variables defined within a **script** dissapear once the script has finished execution. That makes sense, because whenever we launch a script, this will run in a new process, a subshell.

.. note:: A **subshell** is a new shell instance which runs in a new process. This new process is a **child** of the process which launched it, its **parent**.

An `environment variable`_ is a variable that is available system-wide and is inherited by all spawned child processes and shells.

The Environment
---------------
The `environment`_ is the set of variables that condition how a process runs. Each process runs in its own environment, and child processes inherit a copy the environment of their parent.  

That is not true the other way around, i.e., a child process can add up new variables to its copy of environment, but these will not be available in the original environment of its parent.

Adding variables to the environment
-----------------------------------
Use the ``export`` command. For example::

    $ export foo=bar
    $ echo $foo
    bar

The variable ``foo`` is in the environment. Let's start a subshell and see if we can reference ``foo``::

    $ bash
    $ echo $foo
    bar

And sure enough, we can access it in the subshell. Now let's define a variable in the subshell::

    $ export subfoo=baz
    $ declare -p subfoo
    declare -x subfoo="baz"

.. note:: Note the ``-x`` option in the output of ``declare``, we'll see why in a minute.

But if we exit the subshell, we can see how ``subfoo`` does not exist in the parent shell::

    $ declare -p subfoo
    bash: declare: subfoo: not found

Exporting normal variables to the environment
---------------------------------------------
Once we have set a **normal variable**, for example::

    $ catchphrase='wubba lubba dub dub'
    $ declare -p catchphrase
    declare -- catchphrase="wubba lubba dub dub"

We can export it to the **environment** using the ``-x`` option with the ``declare`` command (or ``typeset``)::

    $ declare -x catchphrase
    $ printenv catchphrase
    wubba lubba dub dub

.. note:: The ``printenv`` can also be used to check the value of a particular **environment variable**. By itself will output the whole environment.

Deleting environment variables
------------------------------
As with common variables, we can also use the ``unset`` command to get rid of environment variables::

    $ printenv foo
    bar
    $ unset foo
    $ declare -p foo
    bash: declare: foo: not found

The ``env`` or ``printenv`` commands can be used to display environment variables. 

.. _`environment variable`: https://en.wikipedia.org/wiki/Environment_variable
.. _`environment`: https://www.gnu.org/software/bash/manual/html_node/Environment.html#Environment