******************
Creating variables
******************
There are different ways of creating a variable:

1. Just assigning a value to a name.
2. Using any declaration statement.

Assigning values
================
The most basic form of setting up a variable is through the **assignment statement**::

    $ name=value

Note that there is **no space** between the variable **name**, the ``=``, and the assigned **value**. We can retrieve the value stored in the variable (if any) preceding its name with the ``$`` sign. For example::

    $ echo $name
    value

.. note:: The ``$`` at the beginning of the variable is known as `parameter expansion`_; this other section has more information about it.

Empty variable
--------------
If value is not given, the variable is assigned the **null string**. For example::

    $ empty=
    $ set | grep '^empty'
    empty=

.. note:: To avoid a cluttered ouput, use the **caret** sign ``^`` at the beginning of the **pattern** that we ``grep``.

Declaring and setting attributes
================================
There are two `Bash builtin commands`_ that  that allow us to declare variables and set **attributes** at the same time:

* ``declare``
* ``typeset`` is just a synonym supplied for compatibility with the `KornShell`_.

Assigning a value at the same time we use any of these commands is **optional**. We can do this::

    $ declare -i age=33

But we can also declare a variable, and assign it a value down the line::

    $ declare -i age
    $ age=33

Attributes
----------
Both commands take the same options. Some of them are used to set **attributes** on variables, which are summarized in the table below:

+--------+----------------------------------------------------------------------------------------------+
| Option |                                          Attribute                                           |
+========+==============================================================================================+
| ``-a`` | It sets the variable as an **indexed array**.                                                |
+--------+----------------------------------------------------------------------------------------------+
| ``-A`` | It sets the variable as an **associative array**.                                            |
+--------+----------------------------------------------------------------------------------------------+
| ``-g`` | It sets the variable as an **associative array**.                                            |
+--------+----------------------------------------------------------------------------------------------+
| ``i``  | It sets the **integer** attributes on the variable.                                          |
+--------+----------------------------------------------------------------------------------------------+
| ``n``  | It sets the **nameref** attribute, making the variable a name reference to another variable. |
+--------+----------------------------------------------------------------------------------------------+
| ``r``  | It sets the **readonly** attribute, making the variable a constant.                          |
+--------+----------------------------------------------------------------------------------------------+
| ``t``  | It sets the **trace** attribute. Useful only for functions.                                  |
+--------+----------------------------------------------------------------------------------------------+
| ``l``  | It converts the value/s of a variable to **lowercase** on assignment.                        |
+--------+----------------------------------------------------------------------------------------------+
| ``u``  | It converts the value/s of a variable to **uppercase** on assignment.                        |
+--------+----------------------------------------------------------------------------------------------+

Using ``+`` instead of ``-`` turns off the attribute instead, with some exceptions:

* ``+a`` and ``+A`` may not be used to destroy array variables.
* ``+r`` will **not** remove the **readonly** attribute. 

When used in a **function**, ``declare`` by itself (without any option) makes each name **local**, as with the ``local`` command, unless the ``-g`` option is used.


Displaying variables
--------------------
These commands (``declare`` and ``typeset``) have available other options used to **display** names and their attributes.

.. note:: It's common to refer to variables as **names**, since they can be assigned values as well as functions.

The table below summarizes the display options:

+--------+----------------------------------------------------------------------------+
| Option |                                  Displays                                  |
+========+============================================================================+
| ``-p`` | The **attributes** and **values** of each name.                            |
+--------+----------------------------------------------------------------------------+
| ``-f`` | Shell **functions** and their definitions.                                 |
+--------+----------------------------------------------------------------------------+
| ``-F`` | Shell **functions**, but only their **names**, whithout their definitions. |
+--------+----------------------------------------------------------------------------+

For example, if we want to check the existence of a variable, we can do::

    $ greeting="Good day sir"
    $ declare -p greeting
    declare -- greeting="Good day sir"

The ``-p`` option shows both the name and value of a variable, as long as the name stores not a function. If the name references a function, we should use either the ``f`` or the ``F`` options. The difference between them is simple::

    $ f1() { echo 'hello world'; }
    $ declare -f f1
    f1 () 
    { 
        echo 'hello world'
    }

Using ``-f`` displays the whole function definition, whereas using ``-F``::

    $ declare -F f1
    f1

It displays only the function name. These two options are the only way to display functions, using ``-p`` is useless here::

    $ declare -p f1
    bash: declare: f1: not found

Deleting variables
==================
It's not necessary to exit the shell to get rid of a shell variable. We just have to use the ``unset`` command, which is a `Bourne shell builtin`_::

    $ unset shortlived
    declare -p shortlived
    bash: declare: shortlived: not found

.. _`KornShell`: http://kornshell.com/
.. _`Bash builtin commands`: https://www.gnu.org/software/bash/manual/bash.html#Bash-Builtins
.. _`Bourne shell builtin`: https://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Builtins.html#Bourne-Shell-Builtins
.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html#Shell-Parameter-Expansion
