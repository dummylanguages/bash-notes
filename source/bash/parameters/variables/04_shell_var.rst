Shell variables
===============
Under the denomination `shell variables`_, the Bash official manual describes a list of `environment variables`_ that Bash maintains and needs for different purposes. They are classified under two categories:

* `Bash variables`_, set or used by Bash, and not relevant for other shells.
* `Bourne shell variables`_, maintained by Bash for compatibility with the Bourne shell.

The list is extensive, I would like to mention in the first group:

* ``RANDOM``: Each time this parameter is referenced, a random integer between 0 and 32767 is generated. Assigning a value to this variable seeds the random number generator.

And in the second:

* ``PATH``: A colon-separated list of directories in which the shell looks for commands. A zero-length (null) directory name in the value of PATH indicates the current directory. A null directory name may appear as two adjacent colons, or as an initial or trailing colon.


.. _`shell variables`: https://www.gnu.org/software/bash/manual/html_node/Shell-Variables.html#Shell-Variables
.. _`environment variables`: https://en.wikipedia.org/wiki/Environment_variable
.. _`Bash variables`: https://www.gnu.org/software/bash/manual/html_node/Bash-Variables.html#Bash-Variables
.. _`Bourne shell variables`: https://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Variables.html#Bourne-Shell-Variables