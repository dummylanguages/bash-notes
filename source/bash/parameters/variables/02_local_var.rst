Global and local variables
==========================
In Bash, all variables, by default, have a **global** scope, meaning that their values are accessible everywhere, as long as the shell session it's alive. For example, if we declare::

    $ nickname=Rick

That value is gonna be accessible at any point in time, as long as the current shell session is alive.

Inside functions
----------------
If you have experience with other programming languages, you may think that variables defined inside functions, are not visible outside them. But that's not the case in Bash, for example, imagine we create the variable::

    $ nickname=Rick
    $ echo $nickname
    Rick

Now we create a **function** (inside our current shell session) and define inside it a variable with the same name but another value::

    $ f1() { nickname=Morty; echo $nickname; }
    f1
    Morty

You may think that the variable ``nickname`` defined inside the function is different than the homologous defined outside it... not in Bash::

    $ echo $nickname
    Morty

As you can see, when we run the function, it smashed the value that we assigned it outside of it ``nickname``. That makes total sense, since as we said, a **global** variable is in scope for the whole shell session, and that function was defined without leaving the session.

Inside scripts
--------------
When we launch a script, a new subshell is created. This subshell is a new shell session so variables defined before will not be in scope inside the script, and variables defined inside the script (while it's running) won't be available outside it once the script finishes executing.

When scripts grow bigger, we usually need to define a lot of variables inside them and we have to be careful when choosing names for them. For example, consider the :download:`following script </_code/global.sh>`:

.. literalinclude:: /_code/global.sh
   :language: bash
   :emphasize-lines: 4, 11
   :linenos:
   :tab-width: 2

* In **line 4** we have defined a variable named ``x``.
* In lines **11** we are referencing ``x``, even though it was defined inside the function.

The output of this script::

    inside f1, x=something
    outside the function, x=something

Even though the variable ``x`` is defined inside the function ``f1``, its value is overshadowing the one we assigned before. One way to solve this issue would be to use different variable names, which would be super practical in this script. The thing is that real life scripts are usually more complex that our trivial example, and they tend to use a lot of variables; keeping track of all of their names to avoid overshadowing is gonna become cumbersome pretty fast.

Creating local variables
------------------------
To avoid the mentioned overshadowing problem, Bash offers mechanisms to limit the scope of variables defined inside functions. We just have to declare them using one of the following commands:

* We can use the ``local``, easy to remember for this situation.
* Or we can use the ``typeset`` command, or its synonymous, ``declare``.

For example, consider the :download:`following script </_code/locals.sh>`:

.. literalinclude:: /_code/locals.sh
   :language: bash
   :emphasize-lines: 3, 6, 11
   :linenos:
   :tab-width: 2

* In **line 3** we have defined a variable named ``x`` which has a **global scope**, so it is visible inside both functions.
* In lines **6** and **11** we have defined two variables also named ``x``, but both of them have **local scope**, meaning they are different from each other and from the one with global scope.

The output of this script::

    inside f1, x=something
    inside f2, x=something else
    inside f1, x=something
    inside f2, x=something else
    outside the function, x=global scope

Declaring a variable as **local** allows us to reuse the same variable names in different functions without overshadowing.

These three commands take the ``-g`` option, which would make the variable **global**. That is equivalent to not using the command at all, which may look unnecessarily **verbose** but may be interesting if we want to achieve **consistency** in our variable declarations by always using the ``declare`` or ``typeset`` commands.

 .. _`environment variable`: https://en.wikipedia.org/wiki/Environment_variable
 .. _`environment`: https://www.gnu.org/software/bash/manual/html_node/Environment.html#Environment