*********************
Positional parameters
*********************
`Positional parameters`_ are the **arguments** we pass to commands, and they are denoted by **numbers**.

The ``set`` builtin
===================
Positional parameters are usually used inside scripts, but we can demonstrate their use in the command line, using the ``set`` builtin. Basically, the ``set`` builtin allows us to set **positional parameters**. For example::

    $ set -- one 33 'another argument'

.. note:: Check our other section dedicated to the :ref:`set-builtin-label` for the meaning of the ``--`` option.

The command above is setting three positional parameters:

1. ``one``
2. ``33``
3. ``another argument``

We can access each of these parameters using **parameter expansion**::

    ${parameter}

Where ``parameter`` is a number; for example::

    $ echo "${1}"
    one
    $ echo "${3}"
    another argument

For one digit positional arguments the **curly braces** are **optional**, so in the examples above we could have use ``$1`` and ``$3``. Curly braces must be used to access positional arguments that need more than one digit, for example::

    $ set -- 1 2 3 4 5 6 7 8 9 A B C D E

We can omit the curly braces to access the positional argument at the first nine positions::

    $ echo $4
    4

But we need them to access positional arguments starting at the 10th position::

    $ echo ${10}
    A

.. warning:: Positional parameters can not be assigned with **assignment statements**. We can use the ``set`` and ``shift`` builtin to do that.

Use in scripts
==============
At the end of the last section we said that positional parameters can not be assigned with **assignment statements**. Apart from the ``set`` command, they are automatically assigned when we pass **arguments** to some script. For example:

.. code-block:: bash

    #!/bin/bash

    if [[ ${#} -eq 0 ]]
    then
      echo "You didn't pass any arguments."
    elif [[ ${#} -eq 1 ]]
    then
      echo "Your passed 1 argument: ${1}"
    else
      echo "You called this script with ${#} arguments: ${@}"
      echo "Your first argument was: ${1}"
      echo "Your last argument was: ${@: -1}"
    fi

The logic of this script is quite simple:

* If we don't pass any argument, the first ``if`` clause runs.
* If we pass only one argument, the ``elif`` clause runs.
* If we pass more than one arguments, the ``else`` clause runs.

Special parameters
------------------
In the last script we are using several **special parameters**:

* ``${#}`` expands to the amount of positional arguments, always a **number** (can be zero if no arguments are passed). We are using it in several places, to test if any argument was passed (first test) and also to print the number of arguments.
* ``${@}`` expands to all the values passed as arguments. We used it in the ``else`` clause to print them all at the end of the first ``echo``.

The following table summarizes the **special parameters** that can be used with **positional parameters**:

+-------------------+-----------------------------------------------+
| Special parameter |                  Expands to                   |
+===================+===============================================+
| ``${#}``          | Number of positional parameters in decimal.   |
+-------------------+-----------------------------------------------+
| ``${@}``          | All positional parameters, starting from one. |
+-------------------+-----------------------------------------------+
| ``${*}``          | All positional parameters, starting from one. |
+-------------------+-----------------------------------------------+

To learn the details about the difference between ``${@}`` and ``${*}``, check ``${@}`` the section about `special parameters`_ in the GNU Bash manual.

Expansion of positional parameters
==================================
The GNU Bash manual contains a section named `parameter expansion`_, where they explain all the types of expansions. Regarding **positional parameters** there are two syntaxes available::

    ${parameter}

Where parameter is a number, such as ``$1`` or ``$4``; that's the basic type of expansion we saw at the beginning.

Substring expansion
-------------------
But there's another more involved syntax::

    ${@:offset:length}

This syntax is a type of **substring expansion** applied to positional arguments. For example, let's set some positional arguments::

    $ set -- AA BB CC DD EE FF

* If we wanted to get all the arguments starting with the **third** one we would do::

    $ echo "${@:3}"
    CC DD EE FF

* We are using an ``offset`` of ``3``, which specifies the starting point. We are not using a ``length`` so we get all the parameters until the end. Let's say we just want the **fourth** and **fifth** elements::

    $ echo ${@:3:2}
    CC DD

* **Negative offsets** are allowed; an offset of ``-1`` evaluates to the **last** positional parameter. For example::

    $ echo ${@: -1}
    FF

Note the space in this case between the ``:`` and the negative sign; for negative offsets the space is a must. We also use a negative offset in the ``else`` clause of the script we saw before; we used ``${@: -1}`` to access the **last positional parameter**.

Another script
--------------
Let's use another script to demonstrate how to use **substring expansion** with positional parameters:

.. code-block:: bash

    #!/bin/bash

    if [[ ${#} -eq 0 ]]
    then
    echo "You didn't pass any arguments."
    elif [[ ${#} -eq 1 ]]
    then
    echo "You only passed 1 argument: ${1}"
    else
    echo "You called this script with ${#} arguments:"

    for (( i=1; i <= ${#}; i++ ))
    do
        echo "* Argument $i: ${@:$i:1}"
    done
    fi

Note how we can access each individual positional parameter using **substring expansion**. A simpler version of this script that doesn't use substring expansion, could be:

.. code-block:: bash

    #!/bin/bash

    if [[ ${#} -eq 0 ]]
    then
    echo "You didn't pass any arguments."
    elif [[ ${#} -eq 1 ]]
    then
    echo "You only passed 1 argument: ${1}"
    else
    echo "You called this script with ${#} arguments:"

    i=1
    for param in ${@}
    do
        echo "* Argument $i: $param"
        (( i++ ))
    done
    fi

We are iterating over all positional parameters using the ``${@}`` special argument. We're also using a **counter** named ``i``.

.. _`Positional parameters`: https://www.gnu.org/software/bash/manual/html_node/Positional-Parameters.html
.. _`parameter expansion`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
.. _`special parameters`: https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html
