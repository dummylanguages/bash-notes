##########
Parameters
##########

.. toctree::
	:maxdepth: 4
	:hidden:

	variables/index
	positional_parameters
	special_parameters


In Bash a `parameter`_ is an entity that stores values. There are three types of parameters:

* `Variables`_, which are parameters denoted by a name.
* `Positional parameters`_ are the arguments we passed to commands, and they are denoted by numbers.
* `Special parameters`_.


.. _`Variables`: https://en.wikipedia.org/wiki/Variable_(computer_science)
.. _`parameter`: https://www.gnu.org/software/bash/manual/html_node/Shell-Parameters.html#Shell-Parameters
.. _`Positional parameters`: https://www.gnu.org/software/bash/manual/html_node/Positional-Parameters.html#Positional-Parameters
.. _`Special parameters`: https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html#Special-Parameters
