******************
The ``while`` loop
******************
The `while construct`_ allows for repetitive execution of a list of commands, as long as the command controlling the while loop executes successfully (**exit status** of **zero**). The syntax is:

    while *test-commands*; do *consequent-commands*; done

For example:

.. code-block:: bash

    while [ $answer != 'q' ]
    do
    read -p "Type 'q' to quit: " answer
    done

The script above will continue running until the user types the letter ``q``.

Iterating over lines
====================
One of the most common usages of the while loop is to read a file, data stream, or variable **line by line**. That's achieved by combining ``while`` with ``read``, one of `Bash Builtin Commands`_.

Let's create a small dummy file to iterate over its lines::

    $ echo {1..4}$' line\n' | sed 's/^ *//; $d' > notes.txt
    $ cat notes.txt 
    1 line
    2 line
    3 line
    4 line

Iterating over a file
---------------------
When we want to iterate a file, we use the ``<`` redirection operator:

.. code-block:: bash

    while read -r line
    do
      echo "$line"
    done < notes.txt

What happens here is that on each iteration (each line contained in notes.txt) ``while`` runs the ``read`` command, which returns ``0`` (true) after it successfully reads a valid line. When it reaches the end of the file we are redirecting in (``< notes.txt``), then ``read`` can't read anything and returns a **non-zero exit code** which causes ``while`` to stop iterating.

That's the basic idea, but next we're gonna go over some scenarios that may arise.

.. note:: The source for these **recipes** to iterate over lines can be found in http://mywiki.wooledge.org/BashFAQ/001

Trimmed whitespace
^^^^^^^^^^^^^^^^^^
If the lines contain leading/trailing **whitespace**, this gets trimmed. For example:

.. code-block:: bash

    $ sed 's/^/\t/' notes.txt > tabs.txt
    $ cat tabs.txt 
        1 line
        2 line
        3 line
        4 line

Now each line in our file is preceded by a **leading tab**, and if we try to iterate over the lines using the same loop:

.. code-block:: bash

    $ while read -r line; do echo "$line"; done < tabs.txt
    1 line
    2 line
    3 line
    4 line

.. note:: Don't forget to double quote the ``"$line"`` reference, or quit using ``echo`` and switch to ``printf '%s\n' "$line"``.

We lose the **leading tabs**. To fix the trimed whitespace problem we have specify ``IFS=`` or ``IFS=''`` inmediately before the ``read`` command. After reading is completed, the ``IFS`` returns to its previous value:

.. code-block:: bash

    $ while IFS= read -r line; do echo "$line"; done < tabs.txt
        1 line
        2 line
        3 line
        4 line

Non-POSIX file
^^^^^^^^^^^^^^
Imagine the last line of our does not comply with the `line POSIX standard`_, i.e. does not terminate in a **newline character**. For example:

.. code-block:: bash

    $ cat tabs.txt | head -c -1 > no_newline.txt
    $ cat no_newline.txt 
	1 line
	2 line
	3 line
	4 line$ #prompt starts here!

If we try to iterate over the lines using the last loop:

.. code-block:: bash

    $ while IFS= read -r line; do echo "$line"; done < no_newline.txt
        1 line
        2 line
        3 line
    $

We just lost the **last line**! This is because the lack of newline made ``read`` returns a **non-zero exit code** when it suddenly encountered EOF, leaving the broken partial line in the ``line`` variable. You can print it if you do::

    $ echo "$line"
    4 line

A simple **fix** is adding a **logical OR** to the ``while``:

.. code-block:: bash

    $ while IFS= read -r line || [[ -n "$line" ]]; do echo "$line"; done < no_newline.txt
    	1 line
    	2 line
    	3 line
    	4 line

Since it returns a non-zero exit code, the first part of the ``||`` statement becomes ``false``, so the second part is checked; that's where we run the command ``[[ -n "$line" ]]``, which makes ``read`` to read the contents of ``"$line"``, which at that point is the last line that caused the *error*.

Using file descriptors
^^^^^^^^^^^^^^^^^^^^^^

.. warning:: FINISH IT

Iterating over a variable
-------------------------
Sometimes we have a bunch of text inside a variable, and we want to iterate over its lines. We have two ways to do this:

* Using the `here string`_ redirection operator ``<<<``:

.. code-block:: bash

    lines=$'\tOne line\n\tAnother line'
    while read -r line || [[ -n "$line" ]]
    do
        echo "$line"
    done <<< "$lines"

* Printing the variable into ``while`` **standard input**, using a pipe:

    $ printf '\tOne line\n\tAnother line' | { while read -r line || [[ -n "$line" ]]
    > do echo "$line"; done; [[ $line ]] && echo "$line"; }
    One line
    Another line


.. _`while construct`: https://www.gnu.org/software/bash/manual/bash.html#Looping-Constructs
.. _`Bash Builtin Commands`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html
.. _`line POSIX standard`: https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_206
.. _`here string`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Here-Strings
