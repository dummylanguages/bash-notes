####################
Iteration statements
####################

.. toctree::
    :maxdepth: 3
    :hidden:

    while_loop
    for_loop

As we mentioned in the :ref:`control statements` section, **iteration statements** are another way of `controlling the flow`_ of a program. Iteration statements, also known as **loops**, allow us to define a statement (quite often a group of statements) to be executed repeatedly. Each repetition is known as an **iteration**.

Bash supports the following looping constructs:

* ``until``
* ``while``
* ``for``

The number of repetitions may be controlled in several ways:

1. Using a **count**, the way ``for`` loops do.
2. While a **condition** is true or false; which is the way ``until`` and ``while`` loops work respectively.
3. It's also possible to iterate over the elements in a collection, using another variant of ``for``.

.. _`controlling the flow`: https://en.wikipedia.org/wiki/Control_flow

