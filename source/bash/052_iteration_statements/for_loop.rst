************
The for loop
************
There are a couple of versions of the ``for`` loop in Bash:

1. We can loop over lists of items.
2. There's also a syntax that resembles a **C-style** ``for`` loop.

Iterating over a list of items
==============================
A list of items is anything that expands to a list of words, such as:

* A hard-coded **list of words**.
* A **sequence** generated with brace expansion.
* Any **command** that outputs a list of words.
* An expanded **array**.
* **Positional parameters**.

A list of words
---------------
We can iterated over a hard-coded list of words. For example:

.. code-block:: Bash

    $ for fruit in apple orange strawberry
    > do
    > echo $fruit
    > done
    apple
    orange
    strawberry

In the command above we are looping over a list of strings; on each iteration the variable ``fruit`` is bound to each of the strings in the list.

.. warning:: Use a **space** as a separator; a **comma** or anything else will throw an error.

We could have also use a list of **numbers** but in that case using a sequence is way more flexible.

Sequence of numbers
-------------------
Instead of hard-coding a list of numbers to iterate over, we can use brace expansion to generate a sequence. For example::

.. code-block:: Bash

    $ for number in {1..3}
    > do
    >   echo "line: ${number}"
    > done
    line: 1
    line: 2
    line: 3

In the example above it may look we're not gaining much by generating just 3 numbers, but imagine having to hard-code 300, or 3000 ;-) Sequences also allow to use an **increment** value, for example, imagine we want to loop over the first 4 even numbers:

.. code-block:: Bash

    $ for number in {0..6..2}
    > do
    >   echo "line: ${number}"
    > done
    line: 0
    line: 2
    line: 4
    line: 6

It's possible to generate a **sequence** using the `seq`_ command, so in the example above, we could generate the same sequence using ``$(seq 0 2 6)``,  instead of ``{0..6..2}``.

A command output
----------------
In the example above, we mentioned how we can use a command to generate a list of words to loop over, for example::

    $ seq 0 2 6
    0
    2
    4
    6

Any command that generates a list of words can be used to that effect:

    $ for folder in $(ls /)
    > do
    >   echo "Folder: $folder"
    > done
    Folder: bin
    Folder: boot
    Folder: dev
    Folder: etc
    [...]

Arrays
------
We can iterate over array elements:

.. code-block:: Bash

    $ arr=(red 'dark blue' 'light green')
    $ for color in "${arr[@]}"
    > do
    >   echo "Color: $color"
    > done
    Color: red
    Color: dark blue
    Color: light green

Here it's important to draw attention over two essential details:

* We must use the ``@`` subscript, so that each element is expanded into a **separate string**. Using ``*`` will expand all elements into the **same string**!
* It's good practice to always surround in **double quotes** the array expansion, like ``"${arr[@]}"``, so that **elements that contain a space** (such as ``dark blue`` or ``light green``) are not split in two.

To iterate over the **indices** of any array (indexed or associative) we would use ``${!arr[@]}``.

Positional parameters
---------------------
The syntax for iterating over positional parameters is quite similar:

.. code-block:: Bash

    $ set -- 'one hundred' 'two thousand' 'three millions'
    $ for arg in "$@"
    > do
    >   echo "Arg: $arg"
    > done
    Arg: one hundred
    Arg: two thousand
    Arg: three millions

.. warning:: What we said on the arrays section about enclosing in **double quotes** for words that contain a space also holds true here! Use ``"$@"`` and not just ``$@``.

Omitting ``in``
^^^^^^^^^^^^^^^
An interesting quirk we should mention here, it's that we can rewrite the command above like this:

.. code-block:: Bash

    $ for arg
    > do
    >   echo "Arg: $arg"
    > done
    Arg: one hundred
    Arg: two thousand
    Arg: three millions

We don't have to use ``"$@"``; when ``in`` is not present: the loop will automatically iterate over **all** the positional parameters.

Discarding some
^^^^^^^^^^^^^^^
Sometimes we may want to discard some positional parameters, for example:

.. code-block:: Bash

    #!/usr/bin/env bash

    function is_member () {
    local word="$1"
    shift # Once the 1st pos. param. is assigned, discard it.

    # Omit 'in' to iterate over ALL remaining pos. param.
    for i
    do
      if [[ "$word" == "$i" ]]
      then
        return 0
      fi
    done

    return 1  # Return 1 if there's no match.
    }

    main () {
    local word=$1

    # Using the return code of the function is_member
    if is_member "$@"  # Don't forget the double quotes!!!
    then
      echo "$word is a member of the array."
    else
      echo "$word is NOT a member of the array."
    fi
    }

    main "$@"

In the script above, we use the function ``is_member`` as the test command for the ``if`` statement in ``main``, and we pass it **all** the arguments. In the ``is_member`` function once we assign the first argument to the variable ``word``, we discard it. We need to do that because in the ``for`` loop we iterate over **all** the remaining positional parameters. Let's run the script::

    $ bash test.sh 'dark blue' "${arr[@]}"
    dark blue is a member of the array.

.. warning:: Forgetting the **double quotes** when passing the pos. parameters to ``is_member`` will break the functionality of the script.

C-style ``for`` loop
====================
An alternate form of the for command is also supported::

    for (( expr1 ; expr2 ; expr3 )) ; do commands ; done

* First, the arithmetic expression ``expr1`` is evaluated according to the rules of shell arithmetic.
* Next, the arithmetic expression ``expr2`` is then evaluated repeatedly until it evaluates to zero.  below
* Each time ``expr2`` evaluates to a **non-zero value**, commands are executed and the arithmetic expression ``expr3`` is evaluated. 

For example:

.. code-block:: Bash

    for (( i=0 ; i<=3 ; i++ ))
    > do echo $i
    > done
    0
    1
    2
    3

.. note:: If any expression is omitted, it behaves as if it evaluates to ``1``, and we get an error.

We can manipulate the loop's behaviour with either the ``break`` or ``continue`` `builtin`_.

The ``continue`` builtin
------------------------
This commands allows us to resume the next iteration of an enclosing ``for`` loop (also of a ``while``, ``until``, or ``select`` loop). If a **number** is supplied, the execution of the nth enclosing loop is resumed. The **number** must be greater than or equal to ``1``. The return status is **zero** unless the number is less than ``1``.

The ``break`` builtin
---------------------

.. _`control flow`: https://en.wikipedia.org/wiki/Control_flow
.. _`seq`: https://en.wikipedia.org/wiki/Seq_(Unix)
.. _`builtin`: https://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Builtins.html

