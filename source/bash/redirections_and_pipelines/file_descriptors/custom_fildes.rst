***********************
Custom file descriptors
***********************
The GNU Bash manual contains a section about `duplicating`_ file descriptors. In the section before I showed how to create a duplicate of a standard file descriptor, meaning a new file descriptor that points to the original terminal device file.

In this section I'm gonna explain how to create **new** file descriptors that point to files other than the original terminal device file.

Creating an input file descriptor
=================================
Consider the following file::

    $ echo 'Hello world!' > greeting.txt

Let's create an **input fildes** that we could use to feed the contents of the file above into the input a process::

    $ exec 3< greeting.txt

The line above creates a new input file descriptor (``3``), that it's pointed to the ``greeting.txt`` file. That file descriptor is now available in our shell session to be used when we need it::

    $ ls -l /proc/$$/fd
    total 0
    lrwx------ 1 bob bob 64 Apr  8 14:58 0 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  8 14:58 1 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  8 14:58 2 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  8 15:13 255 -> /dev/pts/0
    lr-x------ 1 bob bob 64 Apr  8 14:58 3 -> /home/bob/greeting.txt

Note also the file permissions, ``lr-x``. Let's use it to **input** that file into some process, ``cat`` for example::

    $ cat <&3
    Hello world!

As you can see, we are using an input file descriptor as an argument for ``cat``, the same as if we were passing the ``greeting.txt`` file itself. An **input** file descriptor can only be used once, if we try::

    $ cat <&3
    $

Position in a file descriptor
-----------------------------
Why an **input fildes** can be **read** only once? A file descriptor has a **position**, when we finish reading it, we reach the end of the file. In our last example, after our first ``cat``, the position is at the end of the data, so when we try to ``cat`` again, we get nothing.

But check this out::

    $ exec 3<<< 'line1
    > line2
    > line3'

First of all, above we have created an input fildes out of a **here string**, how cool is that?! Now let's ``read`` from it::

    $ read <&3 ; echo "$REPLY"
    line1
    $ read <&3 ; echo "$REPLY"
    line2
    $ read <&3 ; echo "$REPLY"
    line3

In the example above we ``read`` a line at a time, (normally you would use ``read -r``) until we get to the end of the file. We could use a `Perl`_ function named `sysseek`_ as an easy way to rewind the position to the start of the file::

    perl -e 'sysseek(stdin,0,0);' <&3
    $ read <&3 ; echo "$REPLY"
    line1
    $ read <&3 ; echo "$REPLY"
    line2
    $ read <&3 ; echo "$REPLY"
    line3

There, we could reuse again the **input fildes**, we just had to rewind it ;-)

.. note:: `sysseek`_ does its magic by calling a system function named `lseek`_

Nothing happens, but if we list the file descriptors, it's still there.

Without using Perl magic, the input fildes has to be closed and open again to be used more than once.

Let's **close** it::

    $ exec 3>&-

About here strings
------------------
In bash, `here strings`_ are implemented via temporary files, and are referenced via **file descriptor** by the shell itself. That file descriptor is inherited by the command that we run the **here string** on, and duplicated onto file descriptor ``0``, standard input. For example::

    $ exec 3<<< "TEST"

Above we have created an **input fildes**, if we check the list of file descriptors::

    $ ls -l /proc/$$/fd
    total 0
    lrwx------ 1 bob bob 64 Apr  9 17:13 0 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  9 17:13 1 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  9 17:13 2 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  9 17:22 255 -> /dev/pts/0
    lr-x------ 1 bob bob 64 Apr  9 17:13 3 -> '/tmp/sh-thd.hzu3j8 (deleted)'

We can see that the temporary file used to store the here string is already **deleted**. Let's try to use this **input fildes** anyways::

    $ cat <&3
    TEST

Oops, we could plug it into ``cat`` and read from it, how's that possible. A file is “deleted” when there is no more directory entry pointing to that file. Such file still exists on the disk, the `inode`_ for the file, as well as the file's content, are not deleted until the file is **closed**. However, it's still possible to duplicate existing descriptors to that file, which is what redirection on ``/proc/PID/fd/FD`` does.

Open a file for writing using a custom file descriptor
======================================================
Let's create an **output fildes**::

    $ exec 4> foo.txt
    $ ls -l /proc/$$/fd
    [...]
    l-wx------ 1 bob bob 64 Apr  8 14:58 4 -> /home/bob/foo.txt

This time the permissions look different, ``l-wx``. Now we can attach the **output fildes** to some command, and its output will end up in the file::

    $ echo 'Some text' >&4
    $ cat foo.txt
    Some text

The thing with **output file descriptors** is that they can be reused several times::

    $ echo 'Some text' >&4
    $ echo 'Some text' >&4
    $ cat foo.txt
    Some text
    Some text
    Some text

Anyways, let's **close** it::

    $ exec 4>&-

.. note:: According to the GNU Bash docs, we are moving the file descriptor to standard output

Open a file both for writing and reading
========================================
For example::

    $ exec 3<> foo.txt

Check the **permissions**::

    $ ls -l /proc/$$/fd
    [...]
    lrwx------ 1 bob bob 64 Apr  8 14:58 3 -> /home/bob/foo.txt

Let's **write** to the file attaching the **output fildes** to some command::

    $ echo 'New text' >&3
    $ cat foo.txt
    New text

We can write several times::

    $ ls >&3
    $ cat foo.txt
    New text
    file1
    file2
    [...]

Let's **read** the contents of the file by redirecting the **input** of some command to this fildes::

    $ cat <&3
    New text

Note that an input fildes can only be consumed once.

Hmm... gotta find out why is appending text to the file instead of smashing its contents. Let's **read** from it::

    $ cat <&3

Let's **close** it now with ``exec 4>&-``.


.. _`duplicating`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Duplicating-File-Descriptors
.. _`Perl`: https://www.perl.org/
.. _`sysseek`: https://perldoc.perl.org/5.30.1//functions/sysseek.html
.. _`lseek`: https://manpages.debian.org/buster/manpages-dev/lseek.2.en.html
.. _`inode`: https://en.wikipedia.org/wiki/Inode
.. _`here strings`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Here-Strings
