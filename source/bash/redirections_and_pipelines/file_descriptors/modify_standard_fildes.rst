***********************************
Modifying standard file descriptors
***********************************
It's not a good idea to modify the standard streams file descriptors. Next we're gonna conduct several experiments to show what happens when we change the files where these file descriptors are pointed to.

Experiment: Redirect standard input
===================================
Consider the following file::

    $ cat foo.txt 
    apple
    orange

Let's redirect the **standard input** file descriptor so it points to this file::

    $ exec 0< foo.txt 
    $ apple
    bash: apple: command not found
    $ orange
    bash: orange: command not found
    $ exit

It's like typing the contents of the file in the keyboard (**standard input**) and pressing enter. Naturally, Bash doesn't find the commands ``apple`` and ``orange``. When it finishes, no matter if the commands are valid or not ``exec`` issues an ``exit``, and our shell exits.

.. note:: I think that instead of redirecting what I'm doing is smashing ``0`` with a new file descriptor that is pointed to a file.

Experiment: Redirect standard output
====================================
If we try and redirect **standard output**, we'll lose control over the terminal. For example::

    $ ls
    file1 file2
    $ exec 1> foo.txt
    $ ls
    $ # no output!!

A way to get output back, would be to use that extra file descriptor(``255``) that Bash keeps around for situations like this one::

    $ ls 1>&255
    file1 file2

In the command above we're making ``ls`` to send its output to file descriptor ``255``, which is connected to the terminal. Note that ``ls >&255`` is equivalent. Let's check where the file descriptors of the shell are pointed::

    $ ls -l /proc/$$/fd

Oops, we forgot that we don't have standard output! Let's point standard output back to the terminal device file::

    $ exec 1>&255

The command above, takes advantage of that fd backup(``255``) pointed to the terminal. By the way, we can check the output of ``foo.txt``::

    file1 file2
    total 0
    lrwx------ 1 bob bob 64 Apr  9 14:02 0 -> /dev/pts/0
    l-wx------ 1 bob bob 64 Apr  9 14:02 1 -> /home/bob/foo.txt
    lrwx------ 1 bob bob 64 Apr  9 14:02 2 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  9 14:05 255 -> /dev/pts/0

The file shows the output of the commands we run while we had file descriptor ``1`` pointed to the file.

Experiment: Redirect standard error
-----------------------------------
If we try and redirect **standard error**, we'll lose control over the terminal::

    exec 2> foo.txt

.. note:: find out why


https://wiki.bash-hackers.org/howto/redirection_tutorial


.. _`duplicate`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Duplicating-File-Descriptors
.. _`REPLY`: https://www.gnu.org/software/bash/manual/html_node/Bash-Variables.html#Bash-Variables
.. _`read`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html#Bash-Builtins
