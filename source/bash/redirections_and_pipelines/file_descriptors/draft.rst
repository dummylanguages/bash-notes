*****
Draft
*****
Consider the following file::

    $ cat foo.txt
    line1
    line2

Let's create an **input fildes** that we can use to read lines from this file::

    $ exec 3< foo.txt

This input fildes can only be used once::

    $ read <&3
    $ echo $REPLY
    line1
    $ read <&3
    $ echo $REPLY
    line2

Each time we use it, we are reading a line, until the file it's empty. After that we can't use it anymore, so we can close it::

    $ exec 3>&-

.. _`duplicate`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Duplicating-File-Descriptors
.. _`REPLY`: https://www.gnu.org/software/bash/manual/html_node/Bash-Variables.html#Bash-Variables
.. _`read`: https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html#Bash-Builtins
