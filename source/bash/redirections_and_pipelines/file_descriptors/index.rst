.. _file-descriptors-label:

################
File descriptors
################
.. toctree::
	:maxdepth: 3
	:hidden:

	duplicating_standard_fildes
	modify_standard_fildes
	custom_fildes
	draft


As a recap, when a program is executed it becomes a **process**. We can pass information from one process to another one using three communication channels named **standard streams**. By default, these standard streams are connected to the terminal's **device file**, not directly, but through **file descriptors**. These initials file descriptors are **symbolic links** that, by default, are pointed to the terminal's **device file**.

In the section before we've seen how `redirections`_ are all about pointing these standard **file descriptors** to other files.

In this section we'll learn what we can do with the file descriptors themselves:

* How to change the files the standard file descriptors are pointed to. (not a good idea)
* How to create new file descriptors.
* How to point a file descriptor to another file descriptor. This same technique is used to close them.


.. _`Redirections`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Redirections
