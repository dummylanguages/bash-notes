*************************************
Duplicating standard file descriptors
*************************************
Apart from redirecting these three initial file descriptors, we can `duplicate`_ them, meaning create new file descriptors that are pointed to the same device file.

.. note:: Is this useless?

Duplicate standard input
========================
To duplicate the **standard input** file descriptor we would do::

    $ exec 3<&0

.. note:: Basically we are saying, create a file descriptor ``3`` and pointing to the address ``&0``, i.e. to the file that file descriptor ``0`` is pointing to (``/dev/pts0``).

We can check that the file descriptor has been created and where is pointed to by running::

    $ ls -l /proc/$$/fd
    total 0
    lrwx------ 1 bob bob 64 Apr  8 14:58 0 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  8 14:58 1 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  8 14:58 2 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  8 15:13 255 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  8 14:58 3 -> /dev/pts/0

Since file descriptor ``0`` was pointed (and still is) to the file ``/dev/pts0``, now file descriptor ``3`` is pointing to that file too. We can try it out running::

    $ cat <&3
    Hello
    Hello
    Bye
    Bye

By the way, this fildes can be reused as many times as we want. I haven't found the use for this technique though.

To **close** file descriptor ``3``, we just have to make it a duplicate of ``-``::

    $ exec 3<&-

.. note:: These examples may look silly and pointless; just keep them in mind for what's coming a bit later.

Duplicate standard output
=========================
To duplicate the **standard output** file descriptor we would do::

    $ exec 4>&1
    $ ls >&4
    file1 file1

Duplicate standard error
========================
Same logic::

    $ exec 5>&2
    $ ls + >&5
    ls: cannot access '+': No such file or directory


.. _`duplicate`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Duplicating-File-Descriptors
