##########################
Redirections and pipelines
##########################

.. toctree::
   :maxdepth: 3
   :hidden:
    
   standard_streams/index
   redirections/index
   file_descriptors/index
   pipelines/index

Both `pipelines`_ and `redirections`_ have something in common, they redirect streams:

* With **pipes** we redirect the streams of **commands** into the streams of other **commands**.
* **Redirections** allow us to redirect the streams from **commands** into **files**.

They can, and usually are, be combined with each other.

But before going into more detail about them, let's use the next section to explain what do we mean by streams.

.. _`pipelines`: https://www.gnu.org/software/bash/manual/html_node/Pipelines.html#Pipelines
.. _`redirections`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Redirections