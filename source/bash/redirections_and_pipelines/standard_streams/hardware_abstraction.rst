********************
Hardware abstraction
********************
Before explaining what a stream is, let's explain a couple of background concepts.

Before hardware abstraction
===========================
Many early computer systems did not have any form of `hardware abstraction`_. This meant that programmers had to know how every hardware device in a system worked and include code to ensure that the program could communicate with the device in question.

Unix systems implement the concept of hardware abstraction at a couple of different levels:

* At the kernel level we have `device drivers`_.
* On top of that there is another subsystem which makes use of `device files`_

Device drivers
--------------
Device drivers are part of the **kernel** and they provide a software interface to hardware devices. This interface enable the operating system to perform low-level operations on the device without needing to know precise details about the hardware being used. 

Some devices are **real**, such as a keyboard or a mouse, others are **virtual**, meaning that they don't represent real hardware, like for example a terminal emulator.

Device files
------------
**Device files** are interfaces to **device drivers** that appears in the file system as if they were ordinary files. They are consistent with the Unix design principle that `everything is a file`_.

Device files are part of a subsystem that historically has had different implementations:

* `HAL`_ is a software library that was used by UNIX-like operating systems for hardware abstraction, nowadays it's been replaced by newer alternatives.
* `devfsd`_ was one of these replacements, but had several disadvantages.
* `udev`_ is a popular device manager for the Linux kernel, distributed as part of systemd.
* `eudev`_ is a fork of udev created by the Gentoo Linux project in order to udev functionality available without systemd.

The purpose of the functionality implemented by any these subsystems is to abstract even further the interaction with devices. This way developers can write software that interact with devices using standard `system calls`_, instead of low-level operations that the driver provides. This abstraction layer simplifies many programming tasks, and make software more independent of specific hardware devices. 

Your terminal is also a file
============================
Even though a **terminal emulator** is not a real piece of hardware, it certainly is represented in the filesystem as a **device file**. You can find out the device file of your current terminal session by running::

    $ tty
    /dev/pts/0

The line above was the ouput I got in a Linux system. The specific output will depend on what's the device file of your terminal at any given point. In **macOS**, terminal devices are named a bit different than in Linux::

	$ tty
	/dev/ttys002


.. _`hardware abstraction`: https://en.wikipedia.org/wiki/Hardware_abstraction
.. _`everything is a file`: https://en.wikipedia.org/wiki/Everything_is_a_file
.. _`HAL`: https://en.wikipedia.org/wiki/HAL_(software)
.. _`devfsd`: https://en.wikipedia.org/wiki/Devfsd
.. _`udev`: https://en.wikipedia.org/wiki/Udev
.. _`eudev`: https://wiki.gentoo.org/wiki/Eudev
.. _`system calls`: https://en.wikipedia.org/wiki/System_call
