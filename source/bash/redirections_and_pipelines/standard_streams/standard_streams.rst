*************************************
Standard streams and File descriptors
*************************************
In Unix-like operating systems, when we launch a **program**, a `process`_ is created and automatically has available, three input and output communication channels:

* Standard input
* Standard output
* Standard error

These three I/O connections are known as `standard streams`_, and each of them is assigned a `file descriptor`_ represented by a non-negative integer. Check the following table:

+-----------------+-----------------+
| Standard stream | File descriptor |
+=================+=================+
| Standard input  | 0               |
+-----------------+-----------------+
| Standard output | 1               |
+-----------------+-----------------+
| Standard error  | 2               |
+-----------------+-----------------+

When we execute a command in an interactive shell session, by default, its standard streams are connected to a `text terminal`_, which in modern times is a `terminal emulator`_.

.. figure:: /_images/text_terminal.jpg
    :scale: 70%
    :align: center

.. note:: ``stdin``, ``stdout`` and ``stderr`` are the names of the standard streams in the `C programming language`_. 

Standard input
--------------
The standard input is the stream of data (often text) that a program receives. Not all programs require or admit any input. For example, the ``ls`` program (which display file names contained in a directory) may take command-line arguments, but that data is not an input stream. 

Standard output
---------------
Standard output is the stream where a program writes its output data. Not all programs generate output. For example, the ``mv`` command is silent on success.

Standard error
--------------
Standard error is another stream typically used by programs to output error messages or diagnostics. Even though it is also an **output stream**, it is independent of the standard output and can be redirected separately.

The usual destination is the **display** of the text terminal which started the program to provide the best chance of being seen even if its standard output has been redirected.

File descriptors
================
Each **process** has associated a **file descriptor table** that contains an index of all its file descriptors; this table is maintained by the **kernel**. A process is born by default with three file descriptors, the ones that correspond to the mentioned standard streams, but we can create additional file descriptors. In Unix-like operating systems, file descriptors can refer to:

* Regular files and directories.
* Block and character devices (also called **special files**)
* Unix domain sockets, and **named pipes**.
* Anonymous pipes.
* Network sockets.

There's another system-wide table that records the **mode** in which the file (or other resource) has been opened: for reading, writing, appending, and possibly other modes; this is called the **file table**.

There's a third table called the **inode table** that describes the actual underlying files.

.. figure:: /_images/file_descriptor_tables.png
   :scale: 35%
   :align: center

Putting together all we have explained so far, when we run a command a **process** is launched. This process has available three **standard streams**, each identified by a **file descriptor**. By default, these file descriptors point to the **device file** of the terminal where the process was launched. Assuming that Bash is our default shell, when we open a terminal the **bash process** is launched and has available the three standard streams.

Let's launch a shell session, and check the processes::

    $ ps
    PID TTY          TIME CMD
      1 pts/0    00:00:00 bash
     13 pts/0    00:00:00 ps

We can safely ignore the process with PID ``13``, since that's the process of the ``ps`` command itself. The interesting part is the first line under the headers; from this line we can tell:

* The PID of ``bash`` command is ``1``.
* We can also see the **terminal** associated with the process, ``pts/0``.

On Linux, the set of **file descriptors** open in a process can be accessed under the path ``/proc/PID/fd/``, where PID is the process identifier. If we run::

    $ ls -l /proc/1/fd
    total 0
    lrwx------ 1 bob bob 64 Apr  7 17:20 0 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  7 17:20 1 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  7 17:20 2 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  7 17:34 255 -> /dev/pts/0

1. We can see that the process with PID ``1`` (our bash session), has 4 **file descriptors**:

* File descriptor ``0`` is **standard output**.
* File descriptor ``1`` is **standard input**.
* File descriptor ``2`` is **standard error**.
* File descriptor ``255`` is just another file descriptor that Bash uses for when the other ones are not available. For example::

    $ ls foo.txt bar.txt 1>/dev/null 2>/dev/null

The file ``foo.txt`` exists, but ``bar.txt`` does not. In normal circumstances we would have get standard output and error, but since we are redirecting both, we get none. Now, if we redirect to the available file descriptor (``255``)::

    $ ls foo.txt bar.txt 1>/dev/null 2>/dev/null &>/proc/$$/fd/255
    ls: cannot access 'bar.txt': No such file or directory
    foo.txt

We get both outputs back, because this file descriptor(``255``) is a symbolic link to our terminal (``/dev/pts/0``).

Anyways, these are entries in the **file descriptors** table described in the diagram.

2. We can also see the permissions ``lrwx``; the ``l`` means these file descriptors are links to a device file, ``/dev/pts/0``.
These are entries in the **file table**.

3. We can also see that these file descriptors point to the device file of the terminal where they were launched, ``/dev/pts/0``. This agrees with the information we get when we run the ``tty`` command::

    $ tty
    /dev/pts/0

This is an entry in the **Inode table**.

Children processes
==================
Anytime we type a **command** and press enter, Bash (the command interpreter) forks a **child process** that inherits all the streams of the **parent process**, the bash process. That can be easily checked if we launch a subshell::

    $ bash
    $ ps
    PID TTY          TIME CMD
      1 pts/0    00:00:00 bash
     27 pts/0    00:00:00 bash
     35 pts/0    00:00:00 ps

Let's check now the file descriptors of this subshell::

    ls -l /proc/27/fd
    total 0
    lrwx------ 1 bob bob 64 Apr  7 17:43 0 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  7 17:43 1 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  7 17:43 2 -> /dev/pts/0
    lrwx------ 1 bob bob 64 Apr  7 17:43 255 -> /dev/pts/0

As expected, the same file descriptors pointing to the same device files.

Manipulating file descriptors
=============================
As we'll explain in the next sections, Bash allows us to manipulate streams through their **file descriptors** and point them to:

1. Other **files**, in the case of **redirections**.
2. The streams of other **commands**, when using **pipelines**.


.. _`process`: https://en.wikipedia.org/wiki/Process_(computing)
.. _`file descriptor`: https://en.wikipedia.org/wiki/File_descriptor
.. _`standard streams`: https://en.wikipedia.org/wiki/Standard_streams
.. _`text terminal`: https://en.wikipedia.org/wiki/Computer_terminal#Text_terminals
.. _`terminal emulator`: https://en.wikipedia.org/wiki/Terminal_emulator
.. _`device driver`: https://en.wikipedia.org/wiki/Device_driver
.. _`device file`: https://en.wikipedia.org/wiki/Device_file
.. _`C programming language`: https://en.wikipedia.org/wiki/C_(programming_language)