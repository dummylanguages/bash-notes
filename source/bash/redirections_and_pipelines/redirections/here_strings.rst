************
Here strings
************
`Here strings`_ make the shell to read input from a **string** (hence the name) or anything that expands to one. Most of the times it's used with **variables**, for example::

    $ greeting='Hello world!'
    $ cat <<< $greeting
    Hello world!

The `cat`_ command displays the contents of a **file** to standard output. In normal circumstances, if you try to feed it a string or a variable it wouldn't work::

    $ cat 'Hello world!'
    cat: 'Hello world!': No such file or directory

We get an error because ``cat`` was expecting a filename. But with the **here string** operator::

    $ cat <<< 'Hello world!'
    Hello world!

It works flawlessly, because we are supplying the string (or something that expands to one) into the **standard input** of the command.

Strings, but not only
=====================
That's what **here strings** are for, they allow us to plug strings into commands that in normal circumstances wouldn't take them. Whatever we input into this operator undergoes:

* Tilde expansion
* Parameter and variable expansion
* Command substitution
* Arithmetic expansion
* And quote removal

`Filename expansion`_ and `word splitting`_ are **not** performed.

That means that this would work(Tilde expansion)::

    $ cat <<< ~
    /home/bob

But this would not::

    $ cat <<< *
    *

The result of a given valid expansion is supplied as a **single string**, with a **newline appended**, to the command on its standard input.

Redirecting into file descriptors
=================================
We can also use this operator to redirect strings into `file descriptors`_. For example::

    Oops! Finish this

.. _`Here strings`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Here-Strings
.. _`cat`: https://www.gnu.org/software/coreutils/manual/coreutils.html#cat-invocation
.. _`Filename expansion`: https://www.gnu.org/software/bash/manual/html_node/Filename-Expansion.html
.. _`word splitting`: https://www.gnu.org/software/bash/manual/html_node/Word-Splitting.html
.. _`file descriptors`: https://www.foo.com
