**************
Here documents
**************
This type of redirection has a weird name. `Here documents`_ make the shell to read input from the current source until a line containing only word (with no trailing blanks) is seen.

Redirecting heredocs to files
=============================

Redirecting heredocs to input fildes
====================================
Let's say we want to create a heredoc and redirect it to an **input file descriptor**, easy::

    $ cat 3<<EOF 0<&3
    All of this text goes to
    standard output and to
    an input file descriptor
    EOF

.. _`Here documents`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Here-Documents
