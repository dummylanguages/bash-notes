Redirecting the error stream
============================
The **error stream** is also an **output stream**, but we can redirect it independently. For example if we try to print the contents of an unexisting file we'd get an error::

    $ cat nope.txt 
    cat: nope.txt: No such file or directory

The figure below represents where the standard error is pointing by default.

.. figure:: /_images/default_stderr.png
    :scale: 70%
    :align: center

To redirect this stream we have to point out its **file descriptor** to any file we deem appropriate::

    $ cat nope.txt 2> /tmp/cat_error.txt

This time we didn't get any error message. The number **2** in the ``2>`` operator represents the **file descriptor** of the **error stream**. We could print the contents of the ``cat_error.txt`` file to check that the error output ended up there::

    $ cat /tmp/cat_error.txt
    cat: nope.txt: No such file or directory

And indeed it did. The figure below shows where the stream was redirected.

.. figure:: /_images/redirected_stderr.png
    :scale: 70%
    :align: center


.. _`command grouping`: https://www.gnu.org/software/bash/manual/html_node/Command-Grouping.html
