*******************************************
Redirecting the output and the error stream
*******************************************
It is possible to `redirect both the output and ther error streams`_ to the same file. Consider the following lines::

    $ echo 'hello world' > foo.txt
    $ cat nope.txt foo.txt
    cat: nope.txt: No such file or directory
    Hello world!

The **output** above shows two messages:

* The first line is the output of the **error stream**. It's warning us about the non-existence of the file ``nope.txt``
* The second line is the **output stream**, which successfully shows the contents of ``some-file``

That's because both output streams (output and error) point to the terminal's device file, as the figure below shows.

.. figure:: /_images/cat_default_streams.png
    :scale: 70%
    :align: center

Sometimes we may want to store both of these outputs **together** into some file, in these cases we can use the ``&>`` operator::

    $ cat nope.txt foo.txt &> /tmp/two_streams.txt

Let's verify the contents of the ``two_streams.txt`` file to check that both output streams ended up there::

    $ cat /tmp/two_streams.txt
    cat: nope.txt: No such file or directory
    Hello world!

And indeed it did. The figure below shows where both streams were redirected.

.. figure:: /_images/redirecting-stdout-and-stderr.png
    :scale: 70%
    :align: center

Several formats available
=========================
Even though the **GNU Bash manual** mentions the ``&>`` syntax as the **preferred** one, it goes on to mention two **alternative syntaxes**:

1. One is ``>&``::

    $ cat nope.txt foo.txt >& /tmp/two_streams.txt
    $ cat /tmp/two_streams.txt 
    cat: nope.txt: No such file or directory
    Hello world!

2. The other one is::

    $ cat nope.txt foo.txt > /tmp/two_streams.txt 2>&1

According to the `Bash Hackers Wiki`_, this should be the preferred one, since is more explicit and POSIX compliant.

Order matters!
==============
If you opt for the **POSIX compliant syntax**, keep in mind that order matters. Don't forget that writing this::

    $ cat nope.txt foo.txt > /tmp/two_streams.txt 2>&1

Is equivalent to writing this::

    $ cat nope.txt foo.txt 1> /tmp/two_streams.txt 2>&1

Which means:

1. Redirect **Standard output** to the file. (``1> /tmp/two_streams.txt``)
2. Then redirect **Standard error** to **Standard output**, which is already pointing to the file.

So we get what we want, redirect both output and error to the file.

.. note:: This is cool too: ``$ cat nope.txt foo.txt 2> /tmp/two_streams.txt 1>&2``

Wrong order!
------------
But if we write::

    $ cat nope.txt foo.txt 2>&1 > /tmp/two_streams.txt

It may seem we are redirecting both the **error** and the **output** streams to the **input** stream of the file. But don't forget that the command is equivalent to::

    $ cat nope.txt foo.txt 2>&1 1> /tmp/two_streams.txt

So what's really happening is:

1. Redirect **error** to **output** (``2>&1``), and since output points to the terminal, now error is pointing to the terminal too. (Don't forget file descriptor are **symbolic links**!!)
2. Redirect **output** to the file, (``1> /tmp/two_streams.txt``) but only the output, because the error stays pointed to the terminal.

    $ cat nope.txt foo.txt 2>&1 > /tmp/two_streams.txt
    cat: nope.txt: No such file or directory

So that's why using the wrong order, **Standard error** leaks to the terminal.

.. note:: The **GNU Bash manual**, in the 1st paragraph of the `Redirections`_ section clearly says: Redirections are processed in the order they appear, **from left to right**.

Appending Standard Output and Standard Error
============================================
Instead of overwriting the file each time, we may find convenient to just append to it. In that case, we just have to add an extra ``>`` to the operators seen above. For example::

    $ cat nope.txt foo.txt &>> /tmp/two_streams.txt

Or using the more explicit and **POSIX compliant** version::

    $ cat nope.txt foo.txt >> /tmp/two_streams.txt 2>&1

Remember, ``>>`` is short for ``1>>``, the same that ``>`` is short for ``1>``.

.. warning:: Don't try to use ``>>&``, it throws an error.

.. _`command grouping`: https://www.gnu.org/software/bash/manual/html_node/Command-Grouping.html
.. _`redirect both the output and ther error streams`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Redirecting-Standard-Output-and-Standard-Error
.. _`Redirections`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html
.. _`Bash Hackers Wiki`: https://wiki.bash-hackers.org/scripting/obsolete
