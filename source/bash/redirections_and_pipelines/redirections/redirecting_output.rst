*****************************
Redirecting the output stream
*****************************
``echo`` is a Bash builtin command that writes its argument to the standard output, the terminal's display::

    $ echo 'Hello world'
    Hello world

When we run this command, its file descriptors point to the terminal's device file, as the figure below shows.

.. figure:: /_images/default_fd.png
    :scale: 70%
    :align: center

The message ``Hello world`` is printed on our terminal's display because, by default, that's where the standard output of any process points to. Let's point it to a file::

    $ echo 'Hello world' 1> some-file
    $ cat file-one
    Hello world

In the first line above we have redirected the output stream of the ``echo`` command to ``some-file``, that's why when we ``cat`` the contents of the file, we can see the ``Hello world`` string has been written to the file. In the operator ``1>``, the **1** refers to the **file descriptor** for standard output. The following figure summarizes the redirection:

.. figure:: /_images/redirect_stdout.png
    :scale: 70%
    :align: center

.. note:: The ``>`` operator is a **shortcut** for the ``1>`` operator.

Extreme care must be taken when using this operator, since we may end up losing valuable information if we point the redirection to some existing important file. To reduce the chances of this incidence, next we are gonna see how to set up a safety net.

Prevent overwriting
===================
We just have to set the ``noclobber`` option::

    $ set -o noclobber

When this option is set, if we try redirecting output to a file that exists, we'll get an error::

    $ echo 'Smash it!' 1> some-file
    bash: file: cannot overwrite existing file

Even when this option is on, if we need to overwrite some **existing file**, we can use the ``1>|`` **redirection operator**, or its shortcut ``>|``::

    $ echo 'Got smashed!' >| some-file
    $ cat some-file
    Got smashed!

This operator overrides the ``noclobber`` option.

Truncating a file
=================
Sometimes we may want to truncate a file on purpose (we may be demonstrating something, or cleaning out some log files). We can just do::

    $ cat foo.txt
    Hello world!
    $ > foo.txt
    $ cat foo.txt
    $

Appending to a file
===================
Whenever we want to **append** the output of some command to the end of some file we can use the ``1>>`` operator (or its shortcut ``>>``::

    $ date +"%T" >> some-file
    $ cat some-file
    Got smashed!
    21:02:26

We can see how we added the time to the contents of ``some-file``, right at the end.

.. note:: The **append redirections** are not affected by the ``noclobber`` option. Even when this option is set, we can append to the end of any file without trouble. Care must be taken in order to avoid corrupting important files with extraneous information, even if it's just appended at the end. 

Redirecting the output of multiple commands
===========================================
If we want to redirect the output of multiple commands at the same time, we can use the `command grouping`_ feature of bash to send the output of all the commands to a single file. We have a couple of options here:

1. We can group them inside **curly braces**::

    $ { sha1sum foo.txt ;sha512sum foo.txt ;md5sum foo.txt ;} >checksum.txt

2. Or we can use **parentheses**, which causes a subshell environment to be created, and each of the commands in the list to be executed in that subshell::

    $ ( sha1sum foo.txt ;sha512sum foo.txt ;md5sum foo.txt ) >checksum.txt

Also, instead of the semicolons, we may the logic operators ``&&`` or ``||`` (which stands for **AND** and **OR** operations respectively):

* If you use ``&&`` to connect two commands, the second one will only be executed, if the first one succeeds (exit code is 0). If it fails, none of the following commands will run.
* The ``||`` however works the only way around only runs the second command if the first one failed (exit code is not 0)

.. _`command grouping`: https://www.gnu.org/software/bash/manual/html_node/Command-Grouping.html
