############
Redirections
############
.. toctree::
	:maxdepth: 3
	:hidden:

	redirecting_output
	redirecting_error
	redirecting_output_and_error
	here_docs
	here_strings


As we know, when we run a command, by default, all of its standard streams are connected to the terminal where we launch it. In practice, that means that each of the **file descriptors** associated with each stream point to the terminal's **device file**.

`Redirections`_ are all about pointing these **file descriptors** to other files.

.. _`Redirections`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Redirections
