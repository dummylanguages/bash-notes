#########
Pipelines
#########
.. toctree::
	:maxdepth: 3
	:hidden:

	draft


We've seen how `Redirections`_ are all about pointing these **file descriptors** to other files.
`pipelines`_ are all about pointing the standard output of a process into the standard input of another one.


.. _`Redirections`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Redirections
.. _`pipelines`: https://www.gnu.org/software/bash/manual/html_node/Pipelines.html#Pipelines
