*****
draft
*****


Pipes run in parallel
=====================

Pipes create subshells
======================
If you try to do this::

    $ name=bob
    $ read -p "What's your name? " name | echo "Hi $name"


When the ``lastpipe`` option is set, and job control is not active, the shell runs the last command of a pipeline not executed in the background in the current shell environment. You can check this option::

    $ shopt lastpipe
    lastpipe       	off

And enable it with ``shopt -s lastpipe``.

Named Pipes
===========
mkfifo ~/pipe


https://www.jumpingbean.co.za/blogs/mark/bash-subshells-ghost-in-the-subshell
http://www.softpanorama.org/Scripting/pipes.shtml

.. _`pipelines`: https://www.gnu.org/software/bash/manual/html_node/Pipelines.html#Pipelines
