*************
Invoking Bash
*************
We don't have to make Bash our default shell in order to use it. If it's installed in our system we can use it by just typing the ``bash`` command.

When invoking Bash this way, we can pass a lot of options that will affect Bash behaviour. These **options** can be divided in two groups:

* **Single-character** options, the same that we can use with the `set`_ builtin command.
* Several **multi-character** options, which to be recognized must be specified **after** the single-character options.

Read the `invoking Bash`_ section of the official docs for more information.

.. _`invoking Bash`: https://www.gnu.org/software/bash/manual/html_node/Invoking-Bash.html#Invoking-Bash
.. _`set`: https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
