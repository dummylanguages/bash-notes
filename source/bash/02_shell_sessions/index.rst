##############
Shell sessions
##############

.. toctree::
	:maxdepth: 3
	:hidden:

	shell_session
	01_default_shell
	02_invoking_bash

In modern computers, **Bash**, as well as any other shell, can be started using one of the two following programs:

* Launching a `terminal emulator`_, usually by clicking some icon on the desktop, or using a shortcut of the window system.
* Using a `virtual console`_, which may be accessed before the GUI is loaded or, in some cases, like FreeBSD or Linux based operating systems, it's available all the time using some shortcuts.

Once the terminal or the console is running, they will start a session in your default shell. In this section we'll explain what exactly is a shell session, and along the way we'll explain other things such as::

* What is a process and a process group.
* How can we use signals to control processes.


.. _`terminal emulator`: https://en.wikipedia.org/wiki/Terminal_emulator
.. _`virtual console`: https://en.wikipedia.org/wiki/Virtual_console