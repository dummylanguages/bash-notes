*************
Shell session
*************
When a user launches a terminal emulator, the shell is running in a **single process** known as a **shell session**. All the commands the user runs are **children processes** of the session, which is the parent process. A shell session is a way of organizing all the processes the user starts from the command line.

.. note:: The following link is a gold mine of information: https://www.informit.com/articles/article.aspx?p=397655

Inside a session, a user may start a process group, for example when piping commands. The parent of this process group is also a child of the session.

In a POSIX-conformant operating system, a `process group`_ denotes a collection of one or more processes. Among other things, a process group is used to control the distribution of a signal; when a signal is directed to a process group, the signal is delivered to each process that is a member of the group. 

Similarly, a **session** denotes a collection of one or more process groups.

`pmisc`_ Debian package contains miscellaneous utilities that use the proc FS

`Signals`_ and the four common `signal-handling environments`_

Every session is tied to a **terminal** from which processes in the session get their **input** and to which they send their **output**. The terminal to which a session is related is called the **controlling terminal** (or controlling tty) of the session. A terminal can be the controlling terminal for only one session at a time.

The controlling terminal for a session can be changed, although this is usually done only by processes that manage a user's initial logging into a system.

 **Job control** allows users to suspend the current task (known as the **foreground task**) while they go and do something else on the same terminal. Sometimes the suspended task is a sequence of processes working together, the system needs to keep track of which processes should be suspended when the user wants to suspend "the" foreground task. **Process groups** allow the system to keep track of which processes are working together and hence should be managed together (as a single job) via job control. A **process group leader** is the process whose pid is the same as its **process group ID** (or ``pgid``).

What is a process
=================
A process is a program running. All processes are identified by a positive integer known as **pid** (process ID).
All processes(except init) have a parent process. When a child process dies, its exit code is stored in the kernel's process table until the parent process requests it. 

The **init process** is the first process started when a machine is booted and is assigned a ``pid`` of ``1``. One of the primary jobs of the init process is to collect the exit statuses of processes whose parents have died, allowing the kernel to remove the child processes from the process table. Processes can find their pid and their parent's pid through the ``getpid()`` and ``getppid()`` functions.

A process' credentials
----------------------
In Linux all user and group names are mapped to integer numbers known as **User IDs** (uids) and **group IDs** (gids). These mappings are kept in /etc/passwd and /etc/group, respectively.

The kernel knows nothing about the names, it is concerned only with the integer representation. The ``0`` **uid** is reserved for the system administrator, normally called **root**. All normal security checks are disabled for processes running as root (that is, with a uid of 0), giving the administrator complete control over the system.

For starters, a process may be considered to have a **single uid** associated with it, although more about this later. It also has a **primary gid**, and also belongs to a set of **supplemental groups**.

Flexible management of uids and gids
------------------------------------
Some programs need a flexible management of uids and gids. **System daemons** are programs that are always running on a system and perform some action at the request of an external stimulus. For example, most World Wide Web (http) daemons are always running, waiting for a client to connect to it so that they can process the client's requests. Most daemons need to run with root permissions but perform actions at the request of a user who may be trying to compromise system security through the daemon.

The **ftp daemon** initially runs as **root** and then switches its uid to the uid of the user who logs into it (most systems start a new process to handle each ftp request, so this approach works quite well). But under some circumstances, however, the **ftp daemon** must open a network connection in a way that only root is allowed to do, but it cannot simply switch back to root, because user processes cannot give themselves superuser access. 

To solve this dilemma, a process actually has three uids: its real uid, saved uid, and effective uid. Normally, a process's effective, real, and saved uid's are all the same, but in some cases it can change: 

1. when the ftp daemon's starts, all its IDs are set to 0, giving it root permissions.
2. When a user logs in, the daemon sets its **effective uid** to the uid of the user, leaving both the saved and real uids as 0.
3. When the ftp daemon needs to perform an action restricted to root, it sets its **effective uid** to ``0``, performs the action, and then resets the **effective uid** to the ``uid of the user`` who is logged in.

.. note:: The ftp daemon does not need the **saved uid** at all, but other programs take advantage of this mechanism, ``setuid`` and ``setgid`` are two examples.

1. The **effective uid** is used for all security checks and is the only uid of the process that normally has any effect.



.. _`process group`: https://en.wikipedia.org/wiki/Process_group
.. _`pmisc`: https://packages.debian.org/buster/psmisc
.. _`signals`: https://en.wikipedia.org/wiki/Signal_(IPC)
.. _`signal-handling environments`: https://sites.cs.ucsb.edu/~almeroth/classes/W99.276/assignment1/signals.html