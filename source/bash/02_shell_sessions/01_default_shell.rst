*************
Default shell
*************
In any case, once we start a session on the terminal or console, we'll have a shell running which may be Bash or not. We can find out the **default shell** running::

    $ echo $SHELL

The ``SHELL`` environment variable stores the default shell for the current user in our system. The value of this variable comes from a couple of sources, depending on the operating system:

    1. In **Linux systems**, our default shell is set in the ``/etc/passwd`` file, like this line for user bob::

        bob:L2NOfqdlPrHwE:504:504:Bob Belcher:/home/bob:/bin/bash

    2. In **macOS** the situation is a bit more complex, since they use a system called `Apple Open Directory`_ to store and organize information about a computer network's users and resources. 

In any case, we'll see a command to change the default shell no matter the file where this is set.

Listing available shells
========================
The list of the available shells on our system is stored in the ``/etc/shells`` file. If you want to check that list just run::

    $ cat /etc/shells

    /bin/bash
    /bin/csh
    /bin/ksh
    /bin/sh
    /bin/tcsh
    /bin/zsh

Adding a shell to the list
==========================
In order to be able to change the default shell for some user, we have to include that shell in the list of shells mentioned above. To do that run::

    $ echo /usr/local/bin/bash | sudo tee -a /etc/shells

Once the shell is in the list, we can set it up as the default.

Changing the default shell
==========================
To change the default shell for a given user we can use the `change shell`_ command::

    $ chsh -s /usr/local/bin/bash

The line above will change the login shell for **current user** to the latest version of Bash that we have installed to ``/usr/local/bin``.

.. _`change shell`: https://en.wikipedia.org/wiki/Chsh
.. _`Apple Open Directory`: https://en.wikipedia.org/wiki/Apple_Open_Directory