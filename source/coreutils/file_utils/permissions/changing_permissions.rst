********************
Changing permissions
********************
Generally, the **mask** only affects file `permissions`_ during the creation of **new files**. Permissions can be changed later by users and programs using `chmod`_.

Classes
=======
**Permissions** on Unix-like systems are managed in three distinct **scopes** or **classes**. These scopes are known as:

* **User**, which refers to the user that created the file or directory.
* **Group**. Files and directories are assigned a group, which define the file's **group class**.
* **Others** are the users who are not the **owner**, nor a member of the **group**.

Permissions
===========
Unix-like systems implement three specific **permissions** that apply to each class:

* The **read** permission grants the ability to read a file. When set for a **directory**, this permission grants the ability to read the names of files in the directory, but not to find out any further information about them such as contents, file type, size, ownership, permissions.
* The **write** permission grants the ability to **modify a file**. When set for a **directory**, it means creating files, deleting files, and renaming files. Note that this requires that **execute** is also set; without it, the write permission is meaningless for directories.
* The **execute** permission grants the ability to execute a **file**. This permission must be set for **executable programs**, in order to allow the operating system to run them. When set for a **directory**, the execute permission is interpreted as the search permission: it grants the ability to access file contents and meta-information if its name is known, but not list files inside the directory, unless **read** is set also.

Listing in long format
======================
**Modes** are the filesystem permissions given to **user**, **group** and **others** classes to access files under Unix. They are shown when listing files in **long format**::

	$ ls -l
	total 2
	drwxr-xr-x 2 bob wheel 4096 Feb  8 14:53 pictures
	-rw-r--r-- 1 bob wheel    1 Feb  8 14:16 notes.txt

The first field of each line contains a string such as ``drwxr-xr-x``. This is known as the **file mode string**, which is composed of 4 fields:

* The first field indicates the **type of file**. The first line shows a ``d``, which indicates a directory. A ``-`` means a regular file, an ``l`` a symbolic link, etc.
* The next **three characters**, in the case of the first line ``rwx`` represent the permissions of the **owner** of the file.
* The second **three characters**, in the case of the first line ``r-x`` represent the permissions of the **group** of the file.
* Lastly, the last **three characters**, in this case also ``r-w``, represent the permissions of **others**; users that are not the owner or don't belong in the group of the file.

.. figure:: /_images/coreutils/files/long_format.jpg

.. note:: Check what we wrote in :ref:`ls long format` to learn more about it.

The ``chmod`` command
=====================
In Unix and Unix-like operating systems, `chmod`_ is the **command** and **system call** which is used to change the `access permissions`_ of **file system objects** (files and directories).

.. note:: A ``chmod`` command first appeared in AT&T `Unix version 1`_. 

The name ``chmod`` is an abbreviation of **change mode**, because that's exactly what it does, changing the **modes** (aka permissions) of a file. There are two available syntaxes to change the modes of a file:

* Octal modes
* Symbolic modes

Symbolic modes
--------------
The symbolic mode is composed of three components, which are combined to form a single string of text::

	$ chmod [references][operator][modes] file

References
^^^^^^^^^^
References refer to the **classes** or scopes mentioned at the beginning. The following table summarizes them:

+-----------+--------+------------------------------------------------------------------+
| Reference | Class  |                           Description                            |
+===========+========+==================================================================+
| ``u``     | user   | file owner                                                       |
+-----------+--------+------------------------------------------------------------------+
| ``g``     | group  | members of the file's group                                      |
+-----------+--------+------------------------------------------------------------------+
| ``o``     | others | users other than the file's owner or members of the file's group |
+-----------+--------+------------------------------------------------------------------+
| ``a``     | all    | all three of the above, same as ``ugo``                          |
+-----------+--------+------------------------------------------------------------------+

Operator
^^^^^^^^
The operators are:

+----------+------------------------------------------------------------------------------+
| Operator | Description                                                                  |
+==========+==============================================================================+
| ``+``    | adds the specified modes to the specified classes                            |
+----------+------------------------------------------------------------------------------+
| ``-``    | removes the specified modes from the specified classes                       |
+----------+------------------------------------------------------------------------------+
| ``=``    | the modes specified are to be made the exact modes for the specified classes |
+----------+------------------------------------------------------------------------------+

Modes
^^^^^
There are three **basic modes** which correspond to the basic permissions:

+-------+---------+--------------------------------------------+
| Mode  | Name    | Description                                |
+=======+=========+============================================+
| ``r`` | read    | read a file or list a directory's contents |
+-------+---------+--------------------------------------------+
| ``w`` | write   | write to a file or directory               |
+-------+---------+--------------------------------------------+
| ``x`` | execute | execute a file or recurse a directory tree |
+-------+---------+--------------------------------------------+

Octal modes
-----------
``chmod`` accepts up to four octal digits:

* The **leading digit** is optional, specifies the special ``setuid``, ``setgid``, and **sticky** flags.
* The **three rightmost** digits define permissions for the file **user**, the **group**, and **others**.

The following table summarizes them:

+---------+---------+--------+-------------------------+
| **rwx** | Binary  | Octal  | Permission              |
+=========+=========+========+=========================+
| ``rwx`` | ``111`` | ``7``  | read, write and execute |
+---------+---------+--------+-------------------------+
| ``rw-`` | ``110`` | ``6``  | read and write          |
+---------+---------+--------+-------------------------+
| ``r-x`` | ``101`` | ``5``  | read and execute        |
+---------+---------+--------+-------------------------+
| ``r--`` | ``100`` | ``4``  | read only               |
+---------+---------+--------+-------------------------+
| ``-wx`` | ``011`` | ``3``  | write and execute       |
+---------+---------+--------+-------------------------+
| ``-w-`` | ``010`` | ``2``  | write only              |
+---------+---------+--------+-------------------------+
| ``--x`` | ``001`` | ``1``  | execute only            |
+---------+---------+--------+-------------------------+
| ``---`` | ``000`` | ``0``  | none                    |
+---------+---------+--------+-------------------------+

Example
-------
For example, consider the following file::

	$ ls -l
	total 24
	drwxr-xr-x 2 bob foogrp 4096 Feb  8 14:53 test0
	[...]

As you can see, the initial ``d`` indicates this is a **directory**. Let's say we want to change its modes to:

* User:   ``rwx``, which is the same as ``111`` or ``7``. (This is fine, nothing to change)
* Group:  ``--x``, which in binary would be ``001`` and in octal ``1``.
* Others: ``--x``, the same as above.

We could use the **symbolic syntax**::

	$ chmod go=--x test0

Or the **octal syntax**::

	$ chmod 711 test0

The first ``7`` is for the **user**, and means ``rwx``. The two ``1`` are for **group** and **others** and mean ``--x``.

Another example
---------------
Another one. Let's say we have the following modes set for the file ``test1``::

	-rw-r--r-- 1 bob foogrp    1 Feb  8 14:16 test1

And we want to have:

* User:   ``rwx``, which is the same as ``111`` or ``7``. (This is fine, nothing to change)
* Group:  ``--x``, which in binary would be ``001`` and in octal ``1``.
* Others: ``r--``, which in binary would be ``100`` and in octal ``4``.

Using the **symbolic syntax**, we have to remove ``x`` from the **group**, and add ``r`` to **others**::

	$ chmod g-r+x test1

Using the **octal syntax**::

	$ chmod 714 test1

The ``714`` means:

* The first ``7`` is for the **user**, and means ``rwx``.
* The second ``1`` is for **group** and means ``--x``(binary ``001``).
* The third ``4`` is for **others** and means ``r--`` (binary ``100``).

.. _`permissions`: https://en.wikipedia.org/wiki/File-system_permissions#Traditional_Unix_permissions
.. _`chmod`: https://en.wikipedia.org/wiki/Chmod
.. _`access permissions`: https://en.wikipedia.org/wiki/File-system_permissions
.. _`Unix version 1`: https://en.wikipedia.org/wiki/Ancient_UNIX
