################
File permissions
################
In this sections we'll go over the coreutils for:

* Changing file permissions (``chmod``)
* Changing file ownership (``chown``)
* Changing file group ownership (``chgrp``)

.. toctree::
	:maxdepth: 3
	:numbered: 3
	:hidden:

	umask
	changing_permissions


.. _`umask`: https://en.wikipedia.org/wiki/Umask
.. _`Version 7 Unix`: https://en.wikipedia.org/wiki/Version_7_Unix
