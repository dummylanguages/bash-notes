********************
Note about ``umask``
********************
In Unix-like systems, each file has a set of attributes that control who can **read**, **write** or **execute** it. When a program creates a **new file**, the file permissions are restricted by what is known as a `mask`_. Each **process** (a running program) has its own **mask** and is able to change its settings using a function call (**umask function**). When the process is a shell, the **mask** is set with the `umask command`_.

.. note:: When a file is created on a Unix-like system, its **permissions** are restricted by the `mask`_ of the process that created it.

The **mask**, the `umask command`_ and the **umask function** were not part of the original implementation of UNIX. The operating system evolved in a relatively small computer-center environment, where security was not an issue. It eventually grew to serve hundreds of users from different organizations. The **mask** and the ``umask`` command were introduced around **1978**, in the `Version 7 Unix`_, so it could allow sites, groups and individuals to choose their own defaults. The mask has since been implemented in most, if not all, of the contemporary implementations of Unix-like operating systems.

.. _`umask command`: https://en.wikipedia.org/wiki/Umask
.. _`mask`: https://en.wikipedia.org/wiki/Mask_(computing)
.. _`Version 7 Unix`: https://en.wikipedia.org/wiki/Version_7_Unix
