.. _chroot command:

**********************
The ``chroot`` command
**********************
According to `Wikipedia`_, in Unix operating systems, **chroot** is an operation that changes the apparent root directory for the current running process and its children. A program that is run in such a modified environment cannot access files outside the designated directory tree. The term ``chroot`` may also refer to:

* The chroot system call.
* The `chroot`_ wrapper program. Even thought there are several versions of these command, we'll be looking at the one included in the GNU Core Utilities.

Applications
============
Changing the root of a process may have different **applications** such as:

* Create a test environment for software development and testing.
* System recovery: reinstallation of the bootloader files on your system.
* Provide some old legacy software with old libraries that may clash with the ones in the host.
* Creating what is known known as **chroot jails**: When we change the root directory for a group or user, they lose access to the real **root directory** in the system.

Command syntax
==============
The basic syntax of the command have several versions depending on what we're using it for. For example::

  $ chroot [options]  /path/to/new/root  command

The mandatory arguments are:

* The path to the directory we want to set as the **new root**.
* The point of changing a root is to make a process believe that the new directory is the root folder; in this sense, ``command`` represents the name of the program we want to run under the new root. If we don't specify one, the **default** will be the process from where we invoked ``chroot``, most of the times your current **shell**.

If you try to run::

  $ sudo chroot new_root
  chroot: failed to run command ‘/bin/bash’: No such file or directory

The reason for that is the new root can't access any file that lives above itself; we'll need to copy all the necessary libraries and executables under the new root, in order to be able to use them.

Command options
---------------
Aditionally, there are a couple of options:

* ``--groups=groups`` is used to to override the supplementary groups to be used by the new process. to override the supplementary groups to be used by the new process. The items in the list (names or numeric IDs) must be separated by commas.
* ``--userspec=user[:group]``. By default, ``command`` is run with the same credentials as the invoking process. Use this option to run it as a different user and/or with a different primary group.
* ``--skip-chdir``. Use this option to not change the working directory to ``/`` after changing the root directory to newroot, i.e., inside the chroot.

Example: SFTP
=============
Let's imagine we want to share files with some friends through `SFTP`_, but we want to limit their access to our file-system; we just want to give them access to a particular folder.

.. note:: For the rest of this section we'll assume you have `OpenSSH server`_ installed in your system.

Add users and groups
--------------------
First thing is gonna be to create a **group**, let's call it ``sftpusers``::

  $ sudo groupadd sftpusers

Then we can create a user, let's say ``bob``, and add it to the ``sftpusers``::

  $ sudo useradd bob -g sftpusers -s /sbin/nologin -p 1234 bob

Note that we're creating ``bob`` with:

* ``-g`` an initial login group, ``sftpusers``.
* ``-s /sbin/nologin`` with no login shell; if his is just for using SFTP, he's not gonna need login privileges.
* ``-p`` a silly password.

He won't have a home directory either.

SSH configuration
-----------------
Prior to editing the configuration file, you should make a copy of the original file and protect it from writing so you will have the **original settings** as a reference and to reuse as necessary::

  $ sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.original
  $ sudo chmod a-w /etc/ssh/sshd_config.original

Now need to tell the ``ssh`` service what to do when SFTP users log in. Open the ``/etc/ssh/sshd_config`` file and add the following at the end::

  Subsystem sftp internal-sftp

  Match Group sftpusers
  ForceCommand internal-sftp
  ChrootDirectory /home
  X11Forwarding no
  AllowTcpForwarding no

It’s important that you add these settings as a separate set of entries, and that you use the ``Match`` syntax to indicate that this section only applies to users in this group. The configuration we just added means:

* The ``ForceCommand`` makes ssh choose its built-in facility to provide SFTP service (which you can control independently).
* ``ChrootDirectory`` tells sshd where to restrict the user to.
* ``Subsystem sftp internal-sftp`` tells ``sshd`` to load the internal sftp service and make it available.

You might need to make sure that this Subsystem is not defined already by commenting out this line earlier in the config file::

  # override default of no subsystems
  # Subsystem sftp /usr/libexec/openssh/sftp-server

Once the changes have been saved, we'll have to restart ``sshd``::

  # systemctl restart sshd

Restrict user to his home
-------------------------
If we test the setup, we'll see that ``bob`` can see all of the other users’ directories as well. However, he can’t navigate to those directories. You can direct the chrooted user to their own home directory by changing the ``ChrootDirectory`` line in the ``sshd_config`` file like this::

  ChrootDirectory /

This quick change makes it look to ``bob`` as though he’s in his own home directory, and he won’t be able to see any other user’s files.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Chroot
.. _`chroot`: https://www.gnu.org/software/coreutils/manual/html_node/chroot-invocation.html
.. _`Version 7 AT&T UNIX`: https://en.wikipedia.org/wiki/AT%26T_UNIX
.. _`SFTP`: https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol
.. _`OpenSSH server`: https://www.openssh.com/
