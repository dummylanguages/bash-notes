.. _touch command:

*********************
The ``touch`` command
*********************
In Unix and Unix-like systems, `touch`_ is a command used to update the **access date** and/or **modification date** of a computer **file** or **directory**.

.. note:: A ``touch`` utility first appeared in `Version 7 AT&T UNIX`_. Today, the command is available for a number of different operating systems, including many Unix and Unix-like systems, **DOS**, **Microsoft Windows**, etc.

The `Single Unix Specification (SUS)`_ specifies that ``touch`` should change the **access time** (``atime``), **modification time** (``mtime``), or both, for a file. The file is identified by a pathname supplied as a single argument, for example::

    $ touch foo

The outcome of this command may be different depending on if the file ``foo`` exists or not:

* If the file identified **does not exist** it is created, and both the ``atime`` and ``mtime`` are set to the **current time**.
* In the case of an **existing file**, touch uses the **current time** just to update the ``atime`` and the ``mtime`` of the file.

.. note:: Later we'll see how can we set specific timestamps to whatever time in point we want, both in the future or in the past.

Creating files
==============
As we mentioned in the last section, ``touch`` can be used for creating files, as long as the files passed as argument do not exist. For example::

    $ touch test1 test2

Assuming that the files ``test1`` and ``test2`` don't exist, the command above creates them, setting the **timestamps** to the **current time**.

.. note:: The file it is created with **default permissions**.

We can pass as many arguments as needed. If you use **Bash**, a nifty trick to create files with a similar name, is to use `brace expansion`_ as in the following example::

    $ touch test{1..3}
    ls -1
    file1
    file2
    file3

Changing a file timestamps
==========================
By default, ``touch`` changes both **modification** and **access** times, but we may use two **flags** to modify these times time individually:

* The ``-a`` is for changing the time of **last access** of the file (``atime``).
* The ``-m`` flag is for changing the time of the **last modification** (``mtime``).

Selecting both flags is equivalent to the default.

Specifying date and time
------------------------
Again, by default, the **timestamps** are set to the **current time**, but using the ``-t`` flag allows us to specify a different time and date. The argument for this option is an argument string that has the following general format::

    [[CC]YY]MMDDhhmm[.SS]

Where:

* ``CC`` The first two digits of the year (the **century**).
* ``YY`` The second two digits of the year.  If ``YY`` is specified, but ``CC`` is not, a value for ``YY`` between 69 and 99 results in a ``CC`` value of 19.  Otherwise, a ``CC`` value of 20 is used.
* ``MM`` The **month of the year**, from 01 to 12.
* ``DD`` the **day of the month**, from 01 to 31.
* ``hh`` The **hour of the day**, from 00 to 23.
* ``mm`` The **minute of the hour**, from 00 to 59.
* ``SS`` The **second of the minute**, from 00 to 61.

.. note:: If the ``CC`` and ``YY`` letter pairs are not specified, the values default to the **current year**. If the ``SS`` letter pair is not specified, the value defaults to ``0``.

For example, let's say we want to change the timestamps to **June the 1st** at **23:42** we would run::

    $ touch -t 06012342 foo.txt

Where:

* ``06`` represents the sixth month, **June**.
* ``01`` the first day of the month.
* ``23`` hours.
* ``42`` minutes.

Since we didn't specify **century** or **year**, it defaults to the current year. The **seconds** default to ``0``. We can check the result using the `stat`_ command (system call)::

    $ stat foo.txt
    File: foo.txt
    Size: 40              Blocks: 8          IO Block: 4096   regular file
    [...]
    Access: 2021-02-08 19:34:10.247771742 +0000
    Modify: 2022-06-01 23:42:00.000000000 +0000
    Change: 2021-02-08 19:34:10.111770735 +0000

.. warning:: By the way, if you try to see the **time** using ``ls -l``, you may not see it. To know why, read what we wrote :ref:'here <ls long format>` about it.

Adding or subtracting time
==========================
The ``-A`` flag adjusts the access and modification time stamps for the file by the specified value. The argument for this option is an argument string that has the following general format::

    [-][[hh]mm]SS

* ``-`` is optionally use to make the adjustment **negative**: the new time stamp is set to be before the current one.
* ``hh`` is used for specifying the **number of hours**, from 00 to 99.
* ``mm`` is used for the **number of minutes**, from 00 to 59.
* ``SS`` is for the **number of seconds**, from 00 to 59.

.. note:: This flag is intended for use in modifying files with incorrectly set time stamps.

The ``-A`` flag implies the ``-c`` flag: if any file specified does not exist, it will be silently ignored.

Other options
=============

Symbolic links
--------------
Add the ``-h`` flag to adjust the timestamps of **symlinks**, for example::

    $ touch -h -t 06012342 foolink.txt

.. note:: The ``-h`` flag is short for ``--no-dereference``.

It's worth mentioning, that this flag does not change the timestamps of the **referenced file**. Needless to say, it's only useful in systems that allow changing the timestamps of a symlink.

Using a file's timestamps as a reference
----------------------------------------
Let's imagine we want to set the timestamps of file ``bar`` equal to the ones of file ``foo``, then we'd do::

    $ touch bar -r foo

The ``-r`` is short for ``--reference=FILE``. In this case the reference file is ``foo`` we're using its timestamps to set the ones of ``bar``.

Do not create non-existing files
--------------------------------
The default behaviour of ``touch`` is to create the file if it doesn't exist. We can avoid that using the ``-c`` flat. The ``touch`` utility does not treat this as an error.  No error messages are displayed and the exit value is not affected.

.. note:: The ``-c`` flag is short for ``--no-create``.

Exit status
===========
The ``touch`` utility exits ``0`` on **success**, and ``>0`` if an **error** occurs.

.. _`touch`: https://en.wikipedia.org/wiki/Touch_(command)
.. _`Version 7 AT&T UNIX`: https://en.wikipedia.org/wiki/AT%26T_UNIX
.. _`brace expansion`: https://www.gnu.org/software/bash/manual/html_node/Brace-Expansion.html#Brace-Expansion
.. _`stat`: https://en.wikipedia.org/wiki/Stat_(system_call)
.. _`Single Unix Specification (SUS)`: https://en.wikipedia.org/wiki/Single_Unix_Specification
