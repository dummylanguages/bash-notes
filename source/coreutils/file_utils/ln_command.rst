.. _ln command:

******************
The ``ln`` command
******************
In Unix and Unix-like systems, `ln`_ is a command-line utility used to create a `hard link`_ or a `symbolic link`_ (symlink) to an existing file or directory.

.. note:: The ``ln`` command can however be used to create **symbolic links** to **non-existent files**.

The ``ln`` command by **default** creates **hard links**, and when called with the ``-s`` flag creates **symbolic links**.

Hard links vs soft links
========================
A **hard link** points to the **inode** of a given file. They allow multiple filenames to be associated with the same file, since a hard link is just another name for the same inode. If you **delete** the **original file**, you can still access its content through any of its hard links. That’s because both target file and hard link has the same inode and thus they point to the same data block.

.. note:: When a file has several **hard links** deleting the original name of the file won’t delete its data as long as it has hard link.

**Symbolic links**, on the other hand, are just **special files** that refer to other files by name.

Hard links to directories
-------------------------
Most operating systems prevent **hard links to directories** from being created since such a capability could disrupt the structure of a file system and interfere with the operation of other utilities. But in the ones that allow them, we must use the flag:

* ``-d``
* ``-F``
* ``--directory``

Any of these flags allow  the **superuser** to attempt to hard link directories.

.. warning:: It may fail due to system restrictions, even for the superuser.

By the way, all **directories** are created with two **hard links**:

* One to ``.``, the **current directory**.
* Another one to ``..``, the **parent directory**.

.. _`ln`: https://en.wikipedia.org/wiki/Ln_(Unix)
.. _`hard link`: https://en.wikipedia.org/wiki/Hard_link
.. _`symbolic link`: https://en.wikipedia.org/wiki/Symbolic_link
.. _`Version 7 AT&T UNIX`: https://en.wikipedia.org/wiki/AT%26T_UNIX
.. _`brace expansion`: https://www.gnu.org/software/bash/manual/html_node/Brace-Expansion.html#Brace-Expansion
.. _`stat`: https://en.wikipedia.org/wiki/Stat_(system_call)
.. _`Single Unix Specification (SUS)`: https://en.wikipedia.org/wiki/Single_Unix_Specification
