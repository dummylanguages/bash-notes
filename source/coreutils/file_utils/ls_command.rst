.. _ls command:

******************
The ``ls`` command
******************
In Unix and Unix-like systems, `ls`_ is a command-line utility used to **list** computer files(and subdirectories) in a directory (aka folder). An ``ls`` command appeared in the `Unix Version 1`_ and the name inherited from a similar command in Multics also named ``ls``, short for the word **list**. ``ls`` is specified in `POSIX`_ and the `Single Unix Specification (SUS)`_.

Today, the two popular versions of ``ls`` are the one provided with the `GNU coreutils package`_, and that released by various `BSD`_ variants (like the one in **macOS**). Both are free software and open source, and have only **minor syntax differences**.

Basic use
=========
Let's suppose we start with the following file tree::

  test_folder/
  ├── file1
  ├── file2
  └── subdir1
    ├── file3
    └── file4

When invoked without any arguments, ``ls`` lists the files in the **current working directory**. For example, if the output of ``pwd`` is ``test_folder``::

  $ ls
  file1   file2   subdir1

.. note:: Unix and Unix-like operating systems maintain the idea of a `current working directory`_, which is the one where the user is currently positioned at a given point in time.

If another directory is specified, then ``ls`` will list the files there::

  $ ls subdir1
  file3 file4

Options
=======
Sometimes, the bare format that ``ls`` displays when used without options, is not enough for our needs. In that case there are available a series of options. Here we'll cover some of them, for the remaining ones ask the ``man``.

One entry per line
-------------------
Using the flag ``1`` (the number one) force output to be one entry per line::

  $ ls -1
  file1
  file2
  subdir1

List recursively
----------------
If we want to check the contens of the subdirectories we can use the ``-R`` option::

  $ ls -R
  file1   file2   subdir1

  ./subdir1:
  file3 file4

Types of files
--------------
If we need to know the type of each listed file, we can use the ``-F`` flat. It appends a character revealing the nature of a file, for example, ``*`` for an executable, or ``/`` for a directory. For example::

  $ ls -F
  file1*   file2    subdir1/

As you can see, ``file1`` is **executable** and ``subdir1`` is a folder. **Regular files** have no suffix.

Hidden files
------------
By default, ``ls`` doesn't show `hidden files`_, which in Unix are signified by a preceding ``.`` in their names.
Using ``-a`` lists **all files** in the given directory, including **hidden files**. For example::

  $ ls -a
  .       ..      file1   file2   .hidden   subdir1

As you can see, ``.hidden`` is a hidden file. Also, ``.`` represents the **current directory** and ``..`` represents the **parent directory** of the current one.

.. _ls long format:

Long format
-----------
The ``-l`` flag displays the contents of a directory in what is known as **long format**. For example::

  $ ls -l
  total 2
  drwxr-xr-x 2 bob wheel 4096 Feb  8 14:53 pictures
  -rw-r--r-- 1 bob wheel    1 Feb  8 14:16 notes.txt

Let's go over the fields of the first line:

1. The string ``drwxr-xr-x`` is known as the **file mode string**.
2. The number of **hard links** that the inode of the file.
3. The third field describes the **owner** of this directory, in this case, the user ``bob``.
4. The fourth field describes the **group**, in this case, the group ``wheel``.
5. Then we have the **size**, in this case ``4096`` **bytes**.
6. Then we have the **modification date and time**.
7. Lastly the **filename**. 

.. figure:: /_images/coreutils/files/long_format.jpg

.. _ls full time:

Full time
---------
The **GNU version** of ``ls`` has available an interesting options named ``--full-time`` to show the full file time (year, month, day, hour, minute, seconds, nanoseconds, timezone offset) regardless of when that time is. For example::

  $ ls --full-time
  total 0
  -rwxr--r-- 1 bob wheel 0 2019-02-09 10:15:46.921410533 +0000 foo.txt

This option is useful sometimes. That's because ``ls`` has two time display formats:

* For timestamps from the **past 6 months**: ``month, day, hour, minute``.
* For other timestamps (**in the future**, or from **more than 6 months ago**): ``year, month, day``.

The intent is to gain horizontal space and not overwhelm the user with information. Showing “from this year” is  more obvious than showing the current year and letting the user notice that it is indeed the current year. The  exact time of day rarely matters for very old files. Dates in the future are rare.

Sort by access time
-------------------
If you need to **sort** by **access time**, newest first, use the ``-u`` option.

Example
=======
Imagine we are asked for a command that will list all files and directories in your
current directory with the following requisites:

* It should **not list**: **hidden files**, the **current directory** (a single dot), or the **parent directory** (a double dot).
* Results must be separated by a **comma**.
* They also must be ordered by **access time**.
* **Directories** must be followed by a slash character.

The command should be::

  $ ls -pum

Where:

*``-p``: append ``/`` indicator to **directories** (short for ``--indicator-style=slash``).
*``-u``: order by **access time** (newest first).
*``-m``: fill width with a **comma separated list** of entries.


.. _`ls`: https://en.wikipedia.org/wiki/Ls
.. _`GNU coreutils package`: https://www.gnu.org/software/coreutils/coreutils.html
.. _`BSD`: https://en.wikipedia.org/wiki/Berkeley_Software_Distribution
.. _`Unix Version 1`: https://en.wikipedia.org/wiki/Ancient_UNIX
.. _`POSIX`: https://en.wikipedia.org/wiki/POSIX
.. _`Single Unix Specification (SUS)`: https://en.wikipedia.org/wiki/Single_Unix_Specification
.. _`current working directory`: https://en.wikipedia.org/wiki/Working_directory
.. _`hidden files`: https://en.wikipedia.org/wiki/Hidden_file_and_hidden_directory
