.. _gnu coreutils section:

##############
Core Utilities
##############
The `GNU Core Utilities`_ are the basic **file**, **shell** and **text manipulation** utilities of the GNU operating system. These are the core utilities which are expected to exist on every operating system.

.. note:: Previously these utilities were offered as three individual sets of GNU utilities, ``Fileutils``, ``Shellutils``, and ``Textutils``. Those three were combined into the single set of utilities called ``Coreutils``. 

Here we'll use this fact to separate their study. For that we'll use a Wikipedia entry named `List of GNU Core Utilities`_.

.. toctree::
  :maxdepth: 2
  :hidden:

  file_utils/index
  shell_utils/index
  text_utils/index


.. _`GNU Core Utilities`: http://www.gnu.org/software/coreutils/
.. _`List of GNU Core Utilities`: https://en.wikipedia.org/wiki/List_of_GNU_Core_Utilities_commands
