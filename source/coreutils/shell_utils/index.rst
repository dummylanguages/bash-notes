###############
Shell Utilities
###############
Here we'll go over the `GNU Core Utilities`_ that used to be included in the ``Shellutils`` package.


.. _`GNU Core Utilities`: http://www.gnu.org/software/coreutils/
