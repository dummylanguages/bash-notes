***********************
Putting it all together
***********************
Let's write some scripts combining all the commands we've seen.

Generating dummy text files
===========================
How about generating some files with dummy text to practice our skills further?

The following commands generate random numbers::

    $ ten_numbers=$(shuf -i 111-999 -n 10)
    158
    438
    328
    [...]

If you don't have ``shuf`` try ``jot``::

    $ ten_numbers=$(jot -r 10 1000 9990)
    8137
    6426
    7140
    [...]

The following function generates a **single line** with a random word in it::

    $ function generate-word (){ sed -n "$RANDOM p" /usr/share/dict/words }
    $ generate-word
    Inuktitut's

.. note:: If you don't have the ``words`` file, you can install it (in Debian based systems), with the package ``wamerican``. if you need a specific language go for ``wspanish`` package, and use the ``spanish`` file, or the ``wbritish`` package, and use the ``british-english`` file.

Let's generate 10 random lines, and store them in the ``ten_words`` variable::

    $ ten_words=$(for i in {1..10}; do generate-word; done)

Now let's create a function to generate a random date::

    $ function generate-date () {
    > date "+%B %d %A %Y %T" --date="$(shuf -i 24-48 -n 1) month ago $(shuf -i 1-12 -n 1) hour ago"
    > }

And then a variable to store 10 dates::

    $ for i in {1..10}; do ten_dates+="$(generate-date)\n"; done

Note that this time we have used a different approach to store the dates into the ``ten_dates`` variable. We are using the ``+=`` operator, so we can introduce small formatting details, like the extra ``\n`` at the end of each date, which the ``generate-date`` function was not giving us.

Now, we can use `process substitution`_ to put them together using the `paste`_ command, and finally write everything into a **CSV file** (note that we haven't written a file so far)::

    $ paste <(echo "$ten_numbers") <(echo -e "$ten_dates") <(echo "$ten_words")  > dummy.txt

If we display the contents of ``dummy.txt``::

    $ cat dummy.txt 
    876    October 02 Monday 2017 11:01:57    Tartary's
    509    February 02 Thursday 2017 17:01:57   Amos
    974    April 02 Saturday 2016 09:01:57    appallingly
    [...]

We have our 10 records to play with. Feel free to modify the commands above to add more fields.




.. _`process substitution`: https://www.gnu.org/software/bash/manual/html_node/Process-Substitution.html
.. _`paste`: https://www.gnu.org/software/coreutils/manual/coreutils.html#paste-invocation
