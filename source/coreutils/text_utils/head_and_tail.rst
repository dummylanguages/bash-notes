*********************
``head`` and ``tail``
*********************
Here we're gonna study together the ``head`` and ``tail`` commands, not only because they are conceptually related, but also because in practice they're often used in tandem to get a range of lines.

We're gonna cover the GNU version of both commands that are distributed in the `GNU Core Utilities`_ package, but their original versions date back to 1977 when they appeared as part of `PWB/UNIX`_. Some operating systems such as **macOS** or `FreeBSD`_ use these BSD versions. See `FreeBSD man head`_ and `FreeBSD man tail`_ for more info.

A dummy file to practice
========================
First of all, let's generate a file with 20 lines of **dummy text** so we can demonstrate these commands::

    $ function generate-line (){ sed -n "$RANDOM p" /usr/share/dict/words; }
    $ for i in {1..20}; do echo "$i:$(generate-word)" >> twenty_lines.txt; done

``head``
========
The `head`_ command prints, by **default**, the **first 10 lines** of a given file. Synopsis::

    head [option]... [file]...

If we want to specify an amount of lines other than 10, we use the ``--lines`` option(``-n``) followed by the number of lines we want. For example::

    $ head -n 3 twenty_lines.txt
    1:Christy
    2:airwaves
    3:Fahrenheit

Only in GNU head
----------------
When we want to get rid of a certain amount of lines at the end of the file, we can use this same option with a negative number, for example::

    $ head -n -16 twenty_lines.txt
    1:Christy
    2:airwaves
    3:Fahrenheit
    4:chimes

This option is really useful to get rid of the last line of a file::

    $ head -n -1 foo.txt
    ...

Note that the BSD version of ``head`` lack this option so if you are in macOS, to get rid of the last line you can use ``sed '$d' foo.txt``.

.. note:: Instead of initial lines, the ``head`` command can also print the initial given amount of **bytes** and other units of data. Run `man head`_ or check the `GNU head documentation`_ for more info.

Passing several files
---------------------
If we pass several files, by default ``head``(and ``tail``) will print a **header** before the lines of each file::

    $ head -n 2 twenty_lines.txt twenty_lines.txt 
    ==> twenty_lines.txt <==
    1:Christy
    2:airwaves

    ==> twenty_lines.txt <==
    1:Christy
    2:airwaves

To get rid of these headers use the ``--quiet`` option, or its equivalents ``-q`` and ``--silence``.

.. note:: This option works the same in the ``tail`` command.

``tail``
========
The `tail`_ command prints, by **default**, the **last 10 lines** of a given file. Synopsis::

    tail [option]... [file]...

If we want to specify an amount of lines other than 10, we use the ``--lines`` option(``-n``) followed by the number of lines we want. For example::

    $ tail -n 3 twenty_lines.txt
    18:GE
    19:biceps's
    20:bogie

We can also print the last part of the file starting from any line we want if we prepend a ``+`` sign to the line number::

    $ tail -n +16 twenty_lines.txt
    16:anarchic
    17:amortizing
    18:GE
    19:biceps's
    20:bogie

Watch a file
------------
The ``tail`` command has a super useful option to keep printing the last part of a file that keeps growing, like for example log files. Let's create a function that adds a line to the ``log.txt`` file and stops 2 seconds::

    $ function generate-logs () {
    > for i in {1..10}; do echo "$i:$(generate-word)" >> logs.txt; sleep 2; done;
    > }

Now we can call this function to run in the **background**, ``generate-logs &``, and watch for changes in the file using the ``--follow`` option(``-f``)::

    $ tail -f log.txt
    1:cloves
    2:bolts
    3:McNamara
    [...]

Note that even when the ``generate-logs`` function has finished execution, ``tail`` is still looking for changes in the ``log.txt`` file, press ``Ctrl-c`` to kill it. To avoid that, the **GNU version** of ``tail`` offers the ``--pid=pid`` option, that allows us to specify the process ID of the program responsible of writing to the file::

    $ generate-logs & tail --pid=$! -f logs.txt

If we specify a ``pid`` that is not in use or that does not correspond to the process that is writing to the tailed files, then ``tail`` may terminate long before any files stop growing or it may not terminate until long after the real writer has terminated.

.. note:: ``$!`` is a Bash `special parameter`_ that expands to the process ID of the command most recently placed in the background.

This command has several more options that may interest you. Run `man tail`_ or check the `GNU tail documentation`_ for more information.
Printing a range of lines
=========================
``head`` and ``tail`` can be combined to display a range of lines. For example, to print lines 7 to 10 (both included) of file ``twenty_lines.txt``::

    $ head -n 10 twenty_lines.txt | tail -n +7
    7:cashing
    8:arias
    9:awning
    10:blatantly

* The ``head`` command allows us to print from the beginning up to the **higher boundary** (line 10).
* Whereas ``tail`` allows us to print from the **lower boundary** (line 5) up till the end of the file.

Alternative commands
--------------------
Printing a range of lines is a very typical operation. Other alternatives to achieve this same result are::

    $ sed -n '7,10p' twenty_lines.txt
    7:cashing
    8:arias
    9:awning
    10:blatantly

Or using ``awk``::

    $ awk 'NR==7, NR==10' twenty_lines.txt
    [...]

In both examples above, both commands will keep parsing the whole file till the end, which in this case isn't a big deal since our file only has 20 lines, but imagine a file with 500 thousand lines. In those, situations it's a good idea to specify the line where both commands should stop execution, ideally the line right after the last line we want to print::

    $ sed -n '7,10p;11q' twenty_lines.txt
    [...]

With ``awk`` we can specify the lines where the file starts and stop being read with the ``FNR`` thing::

    $ awk 'FNR==7, FNR==10' twenty_lines.txt
    [...]

Or use the ``exit`` **AWK command** on line number 11::

    $ awk 'NR==7, NR==10; NR==11 {exit}' twenty_lines.txt

By the way, using ``perl`` would be::

    $ perl -ne 'print if 7..10' twenty_lines.txt 

.. _`GNU Core Utilities`: https://www.gnu.org/software/coreutils/
.. _`PWB/UNIX`: https://en.wikipedia.org/wiki/PWB/UNIX
.. _`FreeBSD`: https://www.freebsd.org/
.. _`FreeBSD man head`: https://www.freebsd.org/cgi/man.cgi?query=head&apropos=0&sektion=0&manpath=FreeBSD+7.3-RELEASE+and+Ports&arch=default&format=html
.. _`FreeBSD man tail`: https://www.freebsd.org/cgi/man.cgi?query=tail&apropos=0&sektion=0&manpath=FreeBSD+7.3-RELEASE+and+Ports&arch=default&format=html
.. _`head`: https://www.gnu.org/software/coreutils/manual/coreutils.html#head-invocation
.. _`man head`: https://manpages.debian.org/buster/coreutils/head.1.en.html
.. _`GNU head documentation`: https://www.gnu.org/software/coreutils/manual/coreutils.html#head-invocation
.. _`tail`: https://www.gnu.org/software/coreutils/manual/coreutils.html#tail-invocation
.. _`special parameter`: https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html#Special-Parameters
.. _`man tail`: https://manpages.debian.org/buster/coreutils/tail.1.en.html
.. _`GNU tail documentation`: https://www.gnu.org/software/coreutils/manual/coreutils.html#tail-invocation
