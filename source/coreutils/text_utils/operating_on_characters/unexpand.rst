************************
The ``unexpand`` command
************************

.. _`GNU Core Utilities manual`: https://www.gnu.org/software/coreutils/manual/coreutils.html
.. _`operating on characters`: https://www.gnu.org/software/coreutils/manual/coreutils.html#Operating-on-characters
.. _`unexpand`: https://www.gnu.org/software/coreutils/manual/coreutils.html#unexpand-invocation
.. _`tr`: https://www.gnu.org/software/coreutils/manual/html_node/tr-invocation.html#tr-invocation
