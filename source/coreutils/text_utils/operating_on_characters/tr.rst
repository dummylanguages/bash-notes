******************
The ``tr`` command
******************
`tr`_ is a command in Unix, Plan 9, Inferno, and Unix-like operating systems. It is an abbreviation of **translate** or **transliterate**, and it is used to:

* Replace characters.
* Squeze several characters into one.
* Remove character.

Implementations
===============
There are several implementations of this command, but the main ones are:

* The original version of ``tr`` was introduced in `Version 4 Unix`_.
* Then there's the version bundled in the `GNU Core Utilities`_ package.

.. note:: Check `Wikipedia`_ for more info about other implementations.

Usage
=====
The synopsis of this command is::

    tr [option]… set1 [set2]

As you can see, this command doesn't take any **file** as argument, it just reads the byte stream from its **standard input** and writes the result to its **standard output**, so it's usually used as part of a pipe.

Replace characters
------------------
For example:

* Replace one character by another one, for example **tabs** by **commas**::

    $ cat file.txt | tr '\t' ',' > file.csv

Squeeze characters
------------------
Let's see several examples:

* Replace multiple **spaces** (or whatever character) with a single one::

    $ tr -s " " < file

As part of a pipe::

    $ cat file.txt | tr -s ' ' > file2.txt

Remove characters
-----------------
To remove a **newline** character::

    $ echo 'hello world' | tr -d '\n'

.. _`Version 4 Unix`: https://en.wikipedia.org/wiki/Research_Unix
.. _`GNU Core Utilities`: https://www.gnu.org/software/coreutils
.. _`Wikipedia`: https://en.wikipedia.org/wiki/Tr_(Unix)#Implementations
.. _`GNU Core Utilities manual`: https://www.gnu.org/software/coreutils/manual/coreutils.html
.. _`operating on fields`: https://www.gnu.org/software/coreutils/manual/coreutils.html#Operating-on-fields
.. _`cut`: https://www.gnu.org/software/coreutils/manual/coreutils.html#cut-invocation
.. _`paste`: https://www.gnu.org/software/coreutils/manual/coreutils.html#paste-invocation
.. _`join`: https://www.gnu.org/software/coreutils/manual/coreutils.html#join-invocation
.. _`tr`: https://www.gnu.org/software/coreutils/manual/html_node/tr-invocation.html#tr-invocation
