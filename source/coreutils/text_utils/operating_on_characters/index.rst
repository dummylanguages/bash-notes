#######################
Operating on characters
#######################

.. toctree::
    :maxdepth: 3
    :hidden:

    tr
    expand
    unexpand


The `GNU Core Utilities manual`_ has a section titled `operating on characters`_ dedicated to three commands:

* `tr`_, which 
* `expand`_, to
* `unexpand`_, which 

In this section we'll review these commands and mentioned other ones that are usually used in combination.

.. _`GNU Core Utilities manual`: https://www.gnu.org/software/coreutils/manual/coreutils.html
.. _`operating on characters`: https://www.gnu.org/software/coreutils/manual/coreutils.html#Operating-on-characters
.. _`tr`: https://www.gnu.org/software/coreutils/manual/coreutils.html#tr-invocation
.. _`expand`: https://www.gnu.org/software/coreutils/manual/coreutils.html#expand-invocation
.. _`unexpand`: https://www.gnu.org/software/coreutils/manual/coreutils.html#unexpand-invocation
