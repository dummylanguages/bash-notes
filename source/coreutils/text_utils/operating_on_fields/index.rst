###################
Operating on fields
###################

.. toctree::
    :maxdepth: 3
    :hidden:

    cut
    paste
    join


The `GNU Core Utilities manual`_ has a section titled `operating on fields`_ dedicated to three commands:

* `cut`_, which prints selected parts of lines.
* `paste`_, to merge fields of different files.
* `join`_, which joins lines that have a common field.

In this section we'll review these commands and mentioned other ones that are usually used in combination.

.. _`GNU Core Utilities manual`: https://www.gnu.org/software/coreutils/manual/coreutils.html
.. _`operating on fields`: https://www.gnu.org/software/coreutils/manual/coreutils.html#Operating-on-fields
.. _`cut`: https://www.gnu.org/software/coreutils/manual/coreutils.html#cut-invocation
.. _`paste`: https://www.gnu.org/software/coreutils/manual/coreutils.html#paste-invocation
.. _`join`: https://www.gnu.org/software/coreutils/manual/coreutils.html#join-invocation
