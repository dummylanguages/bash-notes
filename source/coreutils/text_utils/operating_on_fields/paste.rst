*********************
The ``paste`` command
*********************
This command allows us to print the fields of a file next to the fields of another one, based of the sequential order on which they appear in the files. For example::

    $ cat names.txt
    1   Rick Sanchez    70
    2   Morty Smith     14
    3   Jerry Smith     53

    $ cat occupations.txt
    1   Scientist
    2   Student
    3   Galactic Infiltrator

Imagine we want to output the **second column** of the occupations file next to the first file::

    $ paste -d ':' names.txt <(cut -f 2 occupations.txt)
    1	Rick Sanchez	70:Scientist
    2	Morty Smith		14:Student
    3	Jerry Smith		53:Galactic infiltrator

Note that we've used the ``--delimiters`` option to ``paste`` the last column after a ``:``. If you wanted all fields separated by ``:``(or any other character) the ``tr`` command comes in handy::

    $ paste -d ':' names.txt <(cut -f 2 occupations.txt) | tr -s '\t' ':'
    1:Rick Sanchez:70:Scientist
    [...]

The serial option
=================
The ``--serial`` option may seem like not a very useful one, but in fact it's awesome in some scenarios. For example, imagine we have massaged some text file and ended up with a single column of numbers such a::

    $ cat results.dat
    4.4
    3.1
    1.7
    0.5

We can set them horizontally and set a ``+`` sign as a delimiter (instead of the default tab character)::

    $ paste -s -d + results.dat > sum.dat
    4.4+3.1+1.7+0.5

The ``-delimiter`` option allows us to specify a delimiter, the ``+`` in this case because we want to add the numbers, but it could be a ``*`` for example. Now we can pipe into the `bc`_ command to add them up::

    $ echo sum.dat | bc
    9.7

Alternative
-----------
As usual ``awk``::

    $ awk '{s+=$1} END {printf "%.0f\n", s}' results.dat
    9.7

.. _`GNU Core Utilities manual`: https://www.gnu.org/software/coreutils/manual/coreutils.html
.. _`operating on fields`: https://www.gnu.org/software/coreutils/manual/coreutils.html#Operating-on-fields
.. _`cut`: https://www.gnu.org/software/coreutils/manual/coreutils.html#cut-invocation
.. _`paste`: https://www.gnu.org/software/coreutils/manual/coreutils.html#paste-invocation
.. _`join`: https://www.gnu.org/software/coreutils/manual/coreutils.html#join-invocation
.. _`tr`: https://www.gnu.org/software/coreutils/manual/html_node/tr-invocation.html#tr-invocation
.. _`bc`: https://www.gnu.org/software/bc/manual/html_mono/bc.html
