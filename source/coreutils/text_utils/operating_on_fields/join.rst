********************
The ``join`` command
********************
This command join lines of two files that have a common field, known as the **join field**, and prints the result to standard output. This is really useful when the records in both files have an **ID** field.

.. note:: As opposed to the ``paste`` command, that joins fields based on the sequential order they appear on the file.

For example, we have the following three records in a file::

    $ cat scrambled-names.txt
    2   Morty Smith     14
     3  Jerry Smith     53
    1   Rick Sanchez    70

And we want to combine them with information contain in records that have the same **ID** field in this other file::

    $ cat scrambled-occupations.txt
    7   Engineer
    2   Student
    1   Scientist
    9   Carpenter
    3   Galactic Infiltrator

``join`` requires **sorted** input files, so the first step is to make sure is that they are sorted according to the **join field**.

Sorting input files
===================
The `sort`_ command is another **GNU Core Utility** that allows to order all the lines from a given set of files according to some **key field**. When used in combination with ``join``, we want to sort the files based on the same field we're gonna use later to ``join`` them, or in other words, we gotta make sure the **key field** is the same that the **join field**.

To specify the **key field** we use the ``--key`` option::

    $ sort -k 1 scrambled-names.txt
     3	Jerry Smith	53
    1	Rick Sanchez	70
    2	Morty Smith	14

As you can see from the output, something's not quite right. The reason for that is that the record **number 3** has a leading space that we have to tell ``sort`` to ignore. We can do that with the ``--ignore-leading-blanks`` option, ``-b`` for short::

    $ sort -b -k 1 scrambled-names.txt > names-sorted.txt
    1	Rick Sanchez    70
    2	Morty Smith     14
     3	Jerry Smith     53

Joining
=======
Once we have sorted the input files, we can join them::

    $ join names.sorted occupations.sorted
    1 Rick Sanchez 70 Scientist
    2 Morty Smith 14 Student
    3 Jerry Smith 53 Galactic infiltrator

Note how the lines of the occupations file that didn't have a corresponding field on the names file are totally ignored and not printed out.

Defaults
========
When no options are given, this is how ``join`` behaves:

* The **join field** is the **first field** in each line.
* Fields in the input can be separated by one or more **blanks**, but fields with **leading blanks** on the line **ignored**.??
* Fields in the **output** are separated by a **space**.
* Each output line consists of the **join field**, the remaining **fields from file1**, then the remaining **fields from file2**.



.. _`GNU Core Utilities manual`: https://www.gnu.org/software/coreutils/manual/coreutils.html
.. _`operating on fields`: https://www.gnu.org/software/coreutils/manual/coreutils.html#Operating-on-fields
.. _`sort`: https://www.gnu.org/software/coreutils/manual/html_node/sort-invocation.html#sort-invocation
.. _`paste`: https://www.gnu.org/software/coreutils/manual/coreutils.html#paste-invocation
.. _`join`: https://www.gnu.org/software/coreutils/manual/coreutils.html#join-invocation
.. _`tr`: https://www.gnu.org/software/coreutils/manual/html_node/tr-invocation.html#tr-invocation
