*******************
The ``cut`` command
*******************
This command is used to select parts of lines and print them to the standard output. We can print out:

* **Characters** in a line, using the ``--characters`` or ``-c`` options.
* **Bytes**, using ``--bytes`` or ``-b``.
* **Fields**, using the ``--fields`` option, ``-f`` for short.

Cutting fields separated by tabs
--------------------------------
This last feature is the most interesting one so let's focus on it. For example, let's create a file with three fields separated by **tab characters**::

    $ cat <<< $'field1\tfield2\tfield3\nfieldA\tfieldB\tfieldC' > tab-fields.txt
    $ cat tab-fields.txt 
    field1	field2	field3
    fieldA	fieldB	fieldC

Let's print the second field of both lines::

    $ cut -f 2 tab-fields.txt
    field2
    fieldB

Cutting fields separated by any character
-----------------------------------------
Let's create another file with also three fields, only that this time we'll separate the fields with **commas**::

    $ cat <<< $'field1,field2,field3\nfieldA,fieldB,fieldC' > comma-fields.txt
    $ cat comma-fields.txt 
    field1,field2,field3
    fieldA,fieldB,fieldC

If we try now to ``cut`` the second field::

    $ cut -f 2 comma-fields.txt
    field1,field2,field3
    fieldA,fieldB,fieldC

.. note:: **Fields** are separated by a ``TAB`` character by **default**.

It doesn't work because, by default, ``cut`` only see the fields separated by **tabs**, so we have to tell it that this time we are using another **delimiter**::

    $ cut -f 2 -d ',' comma-fields.txt
    field2
    fieldB

The ``--delimiter`` option allows us to specify the delimiter.

Excluding fields
----------------
If we want to display most of the fields, it's more practical to specify those that we don't want to see printed out. The ``--complement`` option is a **GNU extension** that allows to do that::

    $ cut -f 2 --complement --delimiter=',' comma-fields.txt
    field1,field3
    fieldA,fieldC

Inconsistent use of delimiters
------------------------------
Imagine someone has made an inconsistent use of delimiters when introducing records in a file::

    $ printf '1\tRick Sanchez\t\t70
    > 2\t\tMorty Smith\t14
    > 3\tJerry Smith\t\t53
    > ' > names.txt

If we print the file out::

    $ cat names.txt 
    1	Rick Sanchez		70
    2		Morty Smith	14
    3	Jerry Smith		53

Let's try to ``cut`` the second field::

    $ cut -f 2 names.txt
    Rick Sanchez

    Jerry Smith

We can see that ``Morty Smith`` is missing, because we used two **tabs** when recording his name. For ``cut``, this record contains a **tab character** in its second field.

**Solution**: Fortunately solving this issue is quite simple using a combination of redirection, piping and the `tr`_ command::

    $ tr -s '\t' < names.txt | cut -f 2
    Rick Sanchez
    Morty Smith
    Jerry Smith

In the line above we are using the ``tr`` command with the ``--squeeze-repeats`` option (``-s`` for short), which replaces each sequence of a repeated character that we specified ``'\t'``, with a single occurrence of that character.


.. _`GNU Core Utilities manual`: https://www.gnu.org/software/coreutils/manual/coreutils.html
.. _`operating on fields`: https://www.gnu.org/software/coreutils/manual/coreutils.html#Operating-on-fields
.. _`cut`: https://www.gnu.org/software/coreutils/manual/coreutils.html#cut-invocation
.. _`paste`: https://www.gnu.org/software/coreutils/manual/coreutils.html#paste-invocation
.. _`join`: https://www.gnu.org/software/coreutils/manual/coreutils.html#join-invocation
.. _`tr`: https://www.gnu.org/software/coreutils/manual/html_node/tr-invocation.html#tr-invocation
