###################
The ``cat`` command
###################
`cat`_ has been in Unix from the very beginning, `Research Unix`_. Nowadays is a standard in all Unix and Linux distributions, and it is also included in the `GNU core utilities`_. The synopsis of the command::

    cat [option] [file] ...

Output files
============
The most basic use of ``cat`` consists in reading files sequentially, writing their contents to the standard ouput. For example::

    $ cat sample.txt
    The quick green frog
    jumped over the lazy dog.

A couple of really useful **options** for this use case may be:

* ``--number`` (``-n``) to display a number before **all** lines.
* ``--number-non-blank`` (``-b``), will display all lines, but show a number only before **non-blank** lines.

Counting lines can be useful to get information we can use with other programs such as ``sed``.

Reading from standard input
===========================
If instead of a file, we pass ``-`` or nothing, ``cat`` will read from its **standard input**, and print back to its **standard ouput**. Press ``Ctrl-d`` to send the `EOT`_ character. For example::

    $ cat -n -
    hello world
    <ctrl-d>
        1 hello world

This may look pointless, but we'll see in a second that is a super useful technique to create files on the fly.

Creating a file
---------------
We can create a new file with ``cat`` redirecting its **standard output** to a file (using ``>``, we are `redirecting the output`_ of ``cat``). For example::

    $ cat - > test.txt
    hello world
    <ctrl-d>

The command above starts reading what we type in the **standard input** (we used ``-`` to make it explicit) and redirects its output to the ``test.txt`` file.

.. warning:: If the file already exists, all its contents will be truncated! (fancy word for smashed)

Appending to a file
-------------------
For the reason mentioned above, it's safer to use the ``>>`` operator to `append the redirected output`_ to a file::

    $ cat >> test.txt
    hello again
    <ctrl-d>

This way, if the file exists we won't be truncating its contents, just **appending** to them. If that was a mistake, it will be easier to fix.

.. note:: This technique (and the one before) allow us to achieve what **here documents** do, with less keystrokes.

Concatenating files
===================
The name of the ``cat`` command is derived from its function to con**cat**enate files. For example, let's create a couple of files::

    $ cat > part1.txt
    Hello <ctrl-d>
    $ cat > part2.txt
    world!
    <ctrl-d>

Note where we have press ``<ctrl-d>`` in the commands above. Now we have two files ``part1.txt`` and ``part2.txt`` that we can **concatenate**:

    $ cat part1.txt part2.txt
    Hello world!

If we wanted to, we could redirect the output into a third file::

    $ cat part1.txt part2.txt > total.txt
    $ cat total.txt 
    Hello world!

Getting creative with redirections
==================================
Let's show a couple of techniques for creating documents on the fly:

* `Here strings`_ combined with ``cat``, for when we want to dump a short string into a file.
* `Here documents`_, for when we need to add several lines.

Here Strings
------------
We could have created the files above using a combination of redirection operators. For example::

    $ cat > total.txt <<< 'Hello world!'
    $ cat total.txt
    Hello world!

The command above is redirecting the output of ``cat`` into the file, and getting its input from a **here string**, using the ``<<<`` operator. If we wanted to **append** text to the file we would use the ``>>`` operator::

    $ cat >> total.txt <<< 'Hello again!'
    $ cat total.txt
    Hello world!
    Hello again!

Here Documents
--------------
Some people combine **here documents** and the ``cat`` command to create files on the command line, without having to pull an editor::

    $ cat << EOF >> total.txt
    > Keep writing, and when you want to quit,
    > write EOF on a line by itself, and press Enter
    > EOF<enter>

Note that the here document is the ``<< EOF`` part, where we can replace ``EOF`` for any string we wanted to. Then we are redirecting the output of the here document using the ``>>`` to **append** the text to the ``total.txt`` file. If instead of appending, you prefer to create a new file or smash an existing one, redirect to the file with ``>``.

.. note:: I much prefer to use the other technique mentioned in the **concatenate files** section.

Cat abuse
=========
It is common among novice Unix shell scripters (noobs) to use ``cat`` to pipe the ouput of a file into a command, that could have taken the same filename as an argument, for example::

    $ cat -n total.txt | grep 'again!'
    2	Hello again!

We could have run the ``grep`` command on that same file, saving up some keystrokes and one extra running process along the way::

    $ grep -n 'again!' total.txt 
    2:Hello again!

``grep`` even has the ``-n`` option to print line numbers next to the matched line, just with a slightly different format.

.. note:: There's a fictitional award named the `Useless Use of Cat Award`_ granted in Unix forums and other geek places like that, to people who unneccessarily use ``cat`` when there's no need for it.

.. _`cat`: https://en.wikipedia.org/wiki/Cat_(Unix)
.. _`Research Unix`: https://en.wikipedia.org/wiki/Research_Unix
.. _`GNU core utilities`: https://www.gnu.org/software/coreutils/
.. _`EOT`: https://en.wikipedia.org/wiki/End-of-Transmission_character
.. _`redirecting the output`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Redirecting-Output
.. _`append the redirected output`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Appending-Redirected-Output
.. _`Here strings`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Here-Strings
.. _`Here documents`: https://www.gnu.org/software/bash/manual/bash.html#Here-Documents
.. _`Useless Use of Cat Award`: http://porkmail.org/era/unix/award.html
