##############
Text Utilities
##############
Here we'll go over the `GNU Core Utilities`_ that used to be included in the ``Textutils`` package.


.. toctree::
  :maxdepth: 3

  cat
  operating_on_characters/index
  operating_on_fields/index
  Output of parts of files      <head_and_tail>
  Practicing                    <all_together>


.. _`GNU Core Utilities`: http://www.gnu.org/software/coreutils/
