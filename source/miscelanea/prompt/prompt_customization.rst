.. _prompt-customization:

********************
Prompt customization
********************
Bash has four prompts that can be customized:

* ``PS1`` is the **primary prompt** which is displayed before each command, thus it is the one most people customize.
* ``PS2`` is the **secondary prompt** displayed when a command needs more input (e.g. a multi-line command).
* ``PS3`` is not very commonly used. It is the prompt displayed for Bash's select built-in which displays interactive menus. Unlike the other prompts, it does not expand Bash escape sequences. Usually you would customize it in the script where the select is used rather than in your .bashrc.
* ``PS4`` is also not commonly used. It is displayed when debugging bash scripts to indicate levels of indirection.

All of the prompts are customized by setting the corresponding variable to the desired string (usually in ``~/.bashrc``), for example::

    PS1='$ '

In the example above, we've configured our primary prompt using a plain string. If you reload your configuration file ``source ~/.bashrc`` you'll see how the prompt is now a ``$`` sign followed by a space.

Bash escape sequences
=====================
Apart from using static plain strings, we can also use `Bash escape sequences`_ that expand to special strings. For example, ``\u`` is expanded into the **current username** and ``\H`` is expanded to the **hostname**. So if we set::

    PS1='\H @ \u $ '

Our prompt may show something like ``bob @ thinkpad``, assuming that your username is ``bob`` and the **hostname** is ``thinkpad``.

Terminfo escape sequences
=========================
Aside from the escape characters recognized by Bash, most terminals recognize special escape sequences that affect the terminal itself rather than printing characters. These escape sequences vary from terminal to terminal, and they are documented in the terminfo database. To see what capabilities your terminal supports, run::

    $ infocmp

To find out a description of what each capability (the part before the ``=`` sign) does, check `terminfo`_. For example, the ``setaf`` command sets the **foreground color** of whatever text is printed after it. To get the escape code for a capability, you can use the ``tput`` command. For example::

    $ tput setaf 2

It would print the escape sequence to set the foreground color to **green**. 

To practically incorporate these capabilities into your prompt, you can use Bash's command substitution and string interpolation. For example::

    GREEN="\[$(tput setaf 2)\]"
    RESET="\[$(tput sgr0)\]"

    PS1="${GREEN}my prompt${RESET}> "

.. note:: Wrapping the tput output in ``\[`` and ``\]`` is recommended by the Bash man page. This helps Bash ignore non-printable characters so that it correctly calculates the size of the prompt.

ANSI escape sequences
=====================
Unfortunately, valid ANSI escape sequences may be missing from your terminal's terminfo database. This is especially common with escape sequences for newer features such as 256 color support. In that case you cannot use ``tput``, you must input the escape sequence manually. 

Check Wikipedia for a list of `ANSI escape sequences`_. Every escape sequence starts with a literal escape character, which you can input using the Bash escape sequence ``\e``. So for example, ``\e[48;5;209m`` sets the background to a peachy color (if you have 256 color support) and ``\e[2;2H`` moves the cursor near the top-left corner of the screen. 

.. figure:: /_images/sgr-code.png
   :align: center

Check this page for more info: https://wiki.archlinux.org/index.php/Bash/Prompt_customization

.. _`Bash escape sequences`: https://www.gnu.org/software/bash/manual/html_node/Controlling-the-Prompt.html
.. _`ANSI escape sequences`: https://en.wikipedia.org/wiki/ANSI_escape_code#Escape_sequences
.. _`terminfo`: https://invisible-island.net/ncurses/terminfo.src.html