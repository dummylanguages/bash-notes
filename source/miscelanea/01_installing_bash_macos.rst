Installing Bash in macOS
========================
In most of Unix systems, Bash is already installed, you can chech that out by simply::

    $ bash --version

If your version is outdated and you prefer the latest one, using the **package manager** of your distribution would be the best option. But if you prefer to compile and install it **from source**, next we explain how.

Installing from source
----------------------

1. Download the source code from: http://ftp.gnu.org/gnu/bash/

2. Change to your ``~/Downloads`` folder and unpack::

    $ tar zxvf ~/Downloads/bash-4.4.18.tar.gz

3. Change to the resulting directory::

    $ cd bash-4.4.18

4. Tell the ``configure`` file where we want to install the binary::

    $ ./configure --prefix=/usr/local

5. Compile::

    $ make

6. Install::

    $ make install

Making it the default shell
---------------------------
To set the new version of Bash as our default shell we have 2 ways:
1. Using the Command line.
2. Using the GUI of your system. We'll explain only macOS.

From the Command line
^^^^^^^^^^^^^^^^^^^^^
Add the new Bash to the **list of acceptable shells for chpass**. This list is kept in ``/etc/shells``, and it will require an administrator password to edit it::

    $ sudo echo /usr/local/bin/bash >> /etc/shells

.. warning:: If you get a **permission error** using ``cat``, edit the file with ``sudo vim``.

Set the new Bash as the default shell with chsh::

    $ chsh -s /usr/local/bin/bash

The change should be inmediate, check the new version.

From System Preferences (macOS)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Some simple steps:

1. We open **System Preferences**
2. Under **User and Groups** we *Right-click* on the Current User
3. Under **Advanced Options**, change **Login shell** to ``/usr/local/bin/bash``. Press **OK**.
4. **Log out** and **in** again.

