************
Bash plugins
************

Bash autocompletion
-------------------

::

$ brew install bash-completion

An put this on your ``.bash_profile``::

	if [ -f `brew --prefix`/etc/bash_completion ]; then
	    . `brew --prefix`/etc/bash_completion
	fi

And don't forget to source ``.bash_profile`` with::

	source .bash_profile



.. _`SSH`: https://en.wikipedia.org/wiki/Secure_Shell
