macOS and libedit
=================
To install **GNU Readline** in macOS there are a couple of options:

* Using `homebrew`_, a popular package manager.
* Compiling and installing manually.

Installing using homebrew
"""""""""""""""""""""""""
We can easily install **GNU Readline** in macOS using `homebrew`_, a popular package manager. 

.. note:: If you don't have **homebrew** install run the following command to install it::

  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"


And then to install **GNU Readline**::

  brew install readline

When this command finishes, we are warned with a **caveat**::

  ==> Caveats
  readline is keg-only, which means it was not symlinked into /usr/local,
  because macOS provides the BSD libedit library, which shadows libreadline.
  In order to prevent conflicts when programs look for libreadline we are
  defaulting this GNU Readline installation to keg-only.

  For compilers to find readline you may need to set:
    export LDFLAGS="-L/usr/local/opt/readline/lib"
    export CPPFLAGS="-I/usr/local/opt/readline/include"

  ==> Summary
  🍺  /usr/local/Cellar/readline/8.0.1: 48 files, 1.5MB

.. note:: **Keg-only** means the formula is installed only into the Cellar; it is not linked into ``/usr/local``. This means most tools will not find it. We don’t do this for stupid reasons. You can still link in the formula if you need to with:

    $ brew link --force readline
    Linking /usr/local/Cellar/readline/8.0.1... 17 symlinks created


.. _`homebrew`: https://brew.sh/
