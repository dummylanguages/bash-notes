#!/usr/bin/env bash
printf "Type your age: "
read age

if [ $age -lt 18 ]
then
  echo "Sorry, you are to young to use our services."
  echo "Exit code: $?"
fi