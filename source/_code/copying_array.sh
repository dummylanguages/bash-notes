#!/usr/bin/env bash

pets=(cat dog)
pets[99]=crocodile
echo "The original array:"
declare -p pets

for index in ${!pets[@]}
do
  cute_pets[index]=${pets[index]}
done

echo -e "\nThe copy:"
declare -p cute_pets