#!/usr/bin/env bash

names=("Rick Sanchez" "Morty Smith" "Mr. Meeseeks")
counter=1

for n in "${names[@]}"
do
  echo "$((counter++)): $n"
done