#!/usr/bin/awk -f

# We need to press enter so the BEGIN block finishes
BEGIN { print "Press enter:" }

{ print "Don't panic" }
{ exit }

END { print "Bye" }