BEGIN { print "Month\tFrequency" 
        print "=====\t=========" 
}

NF { 
    if (! ($1 in month_freq)) names[i++] = $1
    month_freq[$1]++
}
# We use the last value of 'i' as loop boundary
END { 
    for (j = 0; j < i; j++) 
        print names[j] "\t " month_freq[ names[j] ] 
}