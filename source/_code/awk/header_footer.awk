BEGIN { print "***This is a silly header.***" }

{ print }       # It could also be shortened as // or as just 1

END { print "***This is a silly footer.***" }
