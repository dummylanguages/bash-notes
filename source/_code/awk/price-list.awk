BEGIN {
    FS=":"
    printf("Car\t\tPrice\n")
    printf("---\t\t-----\n")
}

{   
    printf("%-10s\t$%'.2f\n", $1, $2)
}