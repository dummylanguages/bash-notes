#!/usr/bin/env bash

x='global scope'

f1() {
  local x=something
  echo "inside f1, x=$x"
}

f2() {
  local x='something else'
  echo "inside f2, x=$x"
}

# call the functions
f1
f2
f1
f2

echo "outside the function, x=$x"