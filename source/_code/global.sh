#!/usr/bin/env bash

f1() {
  x=something
  echo "inside f1, x=$x"
}

# call the function
f1

echo "outside the function, x=$x"