#!/bin/bash

declare -a fruits=('apple' 'melon' 'banana')

fruit_up()
{
	for f in "$@"
	do
		printf "I like %s\n" "$f"
	done
}

foo "${fruits[@]}"
