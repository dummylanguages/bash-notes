#!/bin/bash

declare -a fruits=('apple' 'melon' 'banana')

fruit_up()
{
	declare -a my_array=($1)	# do not use double quotes
	for f in "${my_array[@]}"
	do
		printf "I like %s\n" "$f"
	done
}

fruit_up "${fruits[*]}"
