#!/usr/bin/env bash

echo "You passed this script $# parameters:"
counter=1

# Loop over all parameters
for i in $@
do
  echo "Param. $counter: $i"
  ((counter++))
done

echo 
echo "Parameter 10 = $0"        # No problem
echo "Parameter 10 = ${10}"
echo "Parameter 11 = $11"       # Here you should use ${11}