#!/usr/bin/env bash
read -p 'Choose A, B or C: ' OPTION

case $OPTION in
  A)
    echo You chose option A...
    ;;
  B)
    echo You chose option B...
    ;;
  C)
    echo You chose option C...
    ;;
  *)
    echo Invalid option!
esac