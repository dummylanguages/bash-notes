#!/usr/bin/env bash
printf "Do you like ice-cream? (Y/N): "
read answer

if [[ "$answer" = "Y" || "$answer" = "y" ]]
then
  printf "Great, me too! What's your favourite flavour? "
  read flavour

  if [[ "$flavour" = "Chocolate" || "$flavour" = "chocolate" ]]
  then
    echo "Chocolate is also my favourite!"
  fi
fi
