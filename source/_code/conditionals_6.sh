#!/usr/bin/env bash
PASSWORD=1234

read -p 'Type your password: ' INPUT

if [ "$PASSWORD" = "$INPUT" ]
then
  echo Correct password!    # Branch A
else 
  echo Incorrect password!  # Branch B
fi