#!/usr/bin/env bash

f1() {
  x=something
  echo "inside f1, x=$x"
}

f2() {
  echo "inside f2, x=$x"
}

# call the functions
f1
f2