#!/usr/bin/env bash
read -p 'Choose A, B or C: ' OPTION

if [ $OPTION == "A" ]; then
  echo You chose option A...
elif [ $OPTION == "B" ]; then
  echo You chose option B...
elif [ $OPTION == "C" ]; then
  echo You chose option C...
else
  echo Invalid option!
fi