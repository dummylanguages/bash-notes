#!/usr/bin/env bash
read -p 'Type a word: ' WORD

printf "Your word contains "

case $WORD in
  [Aa])
    printf "an A, "
    ;&
  [Bb])
    printf "a B, "
    ;&
  [Cc])
    printf "a C "
    ;&
  *)
    printf "in it.\n"
    ;;
esac