.. Bash notes documentation master file, created by
   sphinx-quickstart on Sat Jan 11 18:09:29 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :numbered: 3
   :caption: Bash
   :hidden:

   bash/01_intro/index
   bash/02_shell_sessions/index
   bash/03_startup_files/index
   bash/04_external_libraries/index
   bash/049_shell_builtins/index
   bash/parameters/index
   bash/types/index
   bash/050_scripting/index
   bash/051_selection_statements/index
   bash/052_iteration_statements/index
   bash/shell_expansions/index
   bash/shell_arithmetic/index
   bash/quoting/index
   bash/redirections_and_pipelines/index
   bash/07_directories/index
   Job Control <job_control/index>

.. toctree::
  :maxdepth: 2
  :caption: Core Utilities
  :hidden:

  coreutils/index

.. toctree::
   :maxdepth: 2
   :caption: Text Manipulation
   :hidden:

   text_processing/index


.. toctree::
   :maxdepth: 2
   :caption: Miscelanea
   :hidden:

   Prompt customization       <miscelanea/prompt/index>
   Readline in macOS          <miscelanea/03_readline_macos>
   Installing Bash in macOS   <miscelanea/01_installing_bash_macos>
   Bash plugins               <miscelanea/02_plugins>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
