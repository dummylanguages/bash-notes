########
``diff``
########
In computing, the utility ``diff`` is a data comparison tool that computes and displays the differences between the contents of files. The utility displays the changes in one of several standard formats, such that both humans or computers can parse the changes, and use them for patching.

.. note:: The `Wikipedia entry for diff`_ is a good source of information about this utility.

History
=======
``diff`` was developed in the **early 1970s** on the Unix operating system which was emerging from Bell Labs in Murray Hill, New Jersey. The final version, first shipped with the `5th Edition of Research Unix`_ in **1974**

Late in **1984** it was released a separate utility, `patch`_, which had the ability to modify files with **output from diff**. 

In diff's early years, common uses included comparing changes in the source of software code and markup for technical documents, verifying program debugging output, comparing filesystem listings and analyzing computer assembly code.

Usage
=====
The ``diff`` command is invoked from the command line, passing it the names of two files::

	$ diff original new

The output of the command represents the changes required to transform the original file into the new file. For example, consider the contents of :download:`sample.txt </_code/patch/original>`:

.. literalinclude:: /_code/patch/original

Imagine we've modified this file, and saved it as :download:`sample.txt </_code/patch/new>`, with the following contents:

.. literalinclude:: /_code/patch/new

The ``diff`` utility can show us the **differences** between the two files::

	$ diff original new
	2,3c2,4
	< I'm the 2nd line, my name is redshirt.
	< Bottom line: I want to change.
	\ No newline at end of file
	---
	> These lines contain the changes
	> introduced in the new update.
	> Bottom line: I feel like new.

As you can see, the ``diff`` utility does not produce colored output; its output is plain text. However, many tools can show the **output with colors** by using **syntax highlighting**. To take advantage of this, we can redirect the output of ``diff`` to a file, like this::

	$ diff original new > updated.diff

Now we can open :download:`update.diff </_code/patch/update.diff>` in any editor that supports syntax highlighting:

.. literalinclude:: /_code/patch/update.diff
	:language: diff

In this **traditional output format**:

* ``a`` stands for **added**
* ``d`` for **deleted**
* ``c`` for **changed**
* **Line numbers** of the original file appear **before** ``a``, ``d``, or ``c``.
* **Line numbers** of the new file appear **after**.
* The ``<`` sign indicates line of the **original file** that have been **changed** or **deleted** (they appear in **red** color)
* The ``>`` sign indicates line of the **new file** (in **green** color)

By default, **unchanged lines** (common to both files) are not shown. Lines that have moved are shown as added at their new location and as deleted from their old location. However, some diff tools highlight moved lines. 

.. _`Wikipedia entry for diff`: https://en.wikipedia.org/wiki/Diff
.. _`5th Edition of Research Unix`: https://en.wikipedia.org/wiki/Research_Unix
.. _`patch`: https://en.wikipedia.org/wiki/Patch_%28Unix%29
