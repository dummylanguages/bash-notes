##############
Patching files
##############

.. toctree::
    :maxdepth: 3
    :hidden:

    diff

Patching a file is the process of updating a file based on the instructions contained in a separate file, called a **patch file**. There are several of utilities involved in the whole process, but the ``patch`` command is the one that originated the name of the process.

.. note:: The ``patch`` command is listed among the `POSIX utilities`_

Implementations
===============
There are several implementations of ``patch``:

* The original version was written by `Larry Wall`_, the creator of the Perl language.
* `GNU Diffutils`_ is a package of several programs related to finding differences between files. One of these utilities is ``patch``. **macOS** uses the GNU version of ``patch``.

.. note:: The `Wikipedia entry for patch`_ is a good source of information on the topic.

Purpose
=======
For example, let's say we have a pretty long text file, with thousands of lines, that we'll call **source_v1**::

    $ cat source_v1
    hello

Now someone decides to edit the contents of this file, introducing small changes here and there, and saves the new version as **source_v2**::

    $ cat source_v2
    hello world

If this person wanted to distribute the new version of the file, he or she could just send the new version, **source_v2**. But that may be unconvenient if the file is big and the updates are frequent.

.. note:: The ``patch`` command was frequently used for updating of source code to a newer version. 

Creating a patch file
=====================
Another option would be to feed **source_v1** and **source_v2** as the input of a program named ``diff``, in order to generate a **small text file** that consists only of the **list of differences** between both files. For example::

    $ diff source_v1 source_v2 > update.diff

The resulting file is commonly known as a **patch file** (or **patch** for short). A **patch** (named ``update.diff`` in this example) is way smaller in size than the updated **source_v2**, hence is easier to distribute.

.. note:: Patch files often have the ``.diff`` extension. **Patches** are also known as **diff** files.

Applying a patch
================
Once we get a **patch** we can change to the directory where our **source_v1** and apply the **patch** (named **update.diff** in this example)::

    $ patch < update.diff
    patching file source_v1

.. warning:: Don't confuse the ``patch`` command with the **patch** file.

Now if we check the contents of ``source_v1`` it must contains all the changes introduced in ``source_v2``::

    $ cat source_v1
    hello world

.. note:: Updating files with ``patch`` is often referred to as applying the patch or simply patching the files.

Output to a file
----------------
The command::

    $ patch < update.diff
    patching file source_v1

Will update the contents if file ``source_v1`` with the updates contained in the diff file. But what if we want to keep the old version and get the patched file with another name, let's say ``source_updated``; well, we can use the ``--output`` option, or ``-o`` for short::

    $ patch source_v1 update.diff -o source_updated

That would leave the ``source_v1`` untouched, and will output its updated (patched) version onto ``source_updated``.


.. _`GNU Diffutils`: https://www.gnu.org/software/diffutils/
.. _`Wikipedia entry for patch`: https://en.wikipedia.org/wiki/Patch_%28Unix%29
.. _`POSIX utilities`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/patch.html
.. _`Larry Wall`: https://en.wikipedia.org/wiki/Larry_Wall
