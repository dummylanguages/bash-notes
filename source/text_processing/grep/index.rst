####################
The ``grep`` command
####################

.. toctree::
    :maxdepth: 3
    :hidden:

    basic_use
    options/index


``grep`` is a command-line utility for searching **strings of text** either in **files** or **output streams**. Its name comes from the `ed`_ command ``g/re/p`` (**globally** search a **regular expression** and **print**), which has the same effect: doing a global search with the regular expression and printing all matching lines.

The ``grep`` original program was written by Ken Thompson and first included in Version 4 Unix. Soon after appeared several variations such as:

* ``egrep``
* ``fgrep``
* ``pcrgrep``

Binaries of these variants persist in most modern systems, however their explicit usage has been deprecated and the functionalities of these variants are included in grep as the command-line switches ``-E``, ``-F`` and ``-P`` respectively.

Almost every installation of Unix, and Linux regardless of distribution, has some version of this tool. The two most predominant versions are:

* `GNU Grep`_, used in Linux systems. 
* `BSD Grep`_, used in macOS, and some other Unix systems, like `FreeBSD`_.

.. note:: You can read more about ``grep`` in:

 * The `Wikipedia`_ entry for ``grep``
 * The official `GNU Grep manual`_.
 * If you run `man grep`_.

.. _`ed`: https://en.wikipedia.org/wiki/Ed_(text_editor)
.. _`Wikipedia`: https://en.wikipedia.org/wiki/Grep
.. _`GNU Grep`: https://www.gnu.org/software/grep/
.. _`BSD Grep`: https://docs.oracle.com/cd/E36784_01/html/E36870/grep-1.html#scrolltoc
.. _`FreeBSD`: https://wiki.freebsd.org/BSDgrep
.. _`GNU Grep manual`: https://www.gnu.org/savannah-checkouts/gnu/grep/manual/grep.html
.. _`man grep`: https://manpages.debian.org/buster/grep/grep.1.en.html
