**************************
Output Line Prefix Control
**************************
In this section we'll go over what the official manual calls `output line prefix control`_ options.

Print line numbers
==================
If we want to know the line number of our matches, we can ask ``grep`` to prefix each line of output with its line number (based 1)::

    $ ls / | grep -n 'home'
    5:home

Here we are listing the **root directory** and piping its output into ``grep``. We wanted to know in what line is our ``home`` directory.

.. _`output line prefix control`: https://www.gnu.org/savannah-checkouts/gnu/grep/manual/grep.html#Output-Line-Prefix-Control
