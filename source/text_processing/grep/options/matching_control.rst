************************
Matching control options
************************
In this section we'll go over what the official manual calls `matching control`_ options.

Ignore case
===========
If we want to match a pattern no matter the case we should use the ``--ignore-case`` option (``i`` or ``y`` for short)::

    $ grep -i "the" sample.txt 
    The quick brown fox
    jumps over the lazy dog.


.. _`matching control`: https://www.gnu.org/savannah-checkouts/gnu/grep/manual/grep.html#Matching-Control
