********************
Context Line Control
********************
In this section we'll go over what the official manual calls `context line control`_ options. These are options to show **context lines**, meaning non-matching lines that are near a matching line.

Print trailing context lines
============================
If we want to print some lines **after** a matched line we can use::

    $ ls / | grep -n -A 2 'home'
    5:home
    6-lib
    7-lib64

Here we are printing **2 lines** after the matched line using the ``-A`` and then the number of lines. The long version would be ``--after-context=2``.

Print preceding context lines
=============================
If we want to print some lines preceding a matched line we can use::

    $ ls / | grep -n -B 2 'home'
    3-dev
    4-etc
    5:home

Here we are printing **2 lines** before the matched line using the ``-B`` and then the number of lines. The long version would be ``--before-context=2``.

Print surrounding context lines
===============================
If we want to print some lines surrounding a matched line we can use::

    $ ls / | grep -n -C 2 'home'
    3-dev
    4-etc
    5:home
    6-lib
    7-lib64

Here we are printing **2 lines before** and **2 lines after** the matched line using the ``-C`` and then the number of lines. The long version would be ``--context=2``.


.. _`context line control`: https://www.gnu.org/software/grep/manual/grep.html#Context-Line-Control
