*************
Grep programs
*************
As we mentioned in the introduction, soon after the original ``grep`` appeared new variations of this program:

* `egrep`_
* `fgrep`_
* `pcrgrep`_

Direct invocation of either ``egrep`` or ``fgrep`` is deprecated, but executables are still provided to allow historical applications that rely on them to run unmodified.

Grep variations
===============
There are four major variants of grep, that differ only by the **regular expression** syntax they used. These versions are controlled by the following options:

Basic regexp
------------
Interpret patterns as basic regular expressions (BREs). This is the default, but if we want to be specific we can use the options ``-G`` or ``--basic-regexp``.

Extended regexp
---------------
Interpret patterns as extended regular expressions (EREs). The options are ``-E`` or ``--extended-regexp``.

Perl-compatible regular expressions
-----------------------------------
The ``--perl-regexp`` (``-P`` for short) makes ``grep`` interpret patterns as `Perl-compatible regular expressions`_.

Fixed strings
-------------
If we want ``grep`` to interpret our **patterns** as fixed strings, not regular expressions, we gotta use the ``--fixed-strings`` options (``-F`` for short).

.. note:: ``-F`` is specified by POSIX.)


.. _`egrep`: https://www.ibm.com/support/knowledgecenter/en/ssw_aix_72/e_commands/egrep.html
.. _`fgrep`: https://www.ibm.com/support/knowledgecenter/en/ssw_aix_72/f_commands/fgrep.html
.. _`pcrgrep`: https://www.pcre.org/original/doc/html/pcregrep.html
.. _`Perl-compatible regular expressions`: https://www.pcre.org/original/doc/html/index.html
