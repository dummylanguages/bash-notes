**********************
General output control
**********************
In this section we'll go over what the official manual calls `general output control`_ options.

Count matching lines
====================
If we are only interested in the numbers of line that match our pattern, we must use the ``--count`` option (``c`` for short)::

    $ grep -i "the" sample.txt 
    The quick brown fox
    jumps over the lazy dog.

Print matching files
====================
If we want to know what files contains lines that match our pattern, we would use the ``--files-with-matches`` option (``-l`` for short)::

    $ grep -l '[.]$' sample.txt poem.txt 
    sample.txt
    poem.txt

Apparently both files have at least a line that ends in a dot.

.. _`general output control`: https://www.gnu.org/savannah-checkouts/gnu/grep/manual/grep.html#General-Output-Control
