############
Grep options
############

.. toctree::
    :maxdepth: 3

    grep_programs
    matching_control
    general_output_control
    output_line_prefix_control
    context_line_control


.. _`ed`: https://en.wikipedia.org/wiki/Ed_(text_editor)

