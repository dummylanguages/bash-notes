**********
Using grep
**********
``grep`` is a really simple command line utility that scans text, and prints the **lines** that match a given **pattern**.

What is a pattern
=================
A **pattern** can be both:

* A **literal string**.
* A **regular expression**.

For example, consider the following text::

    $ cat sample.txt
    The quick brown fox
    jumps over the lazy dog.

1. We could search for a **fixed string** of text::

    $ grep "brown" sample.txt 
    The quick brown fox

2. Or we could search for a **regular expression**::

    $ grep '[.]$' sample.txt 
    jumps over the lazy dog.

Above we have used a **regular expression**, ``[.]$``, to match a dot at the end of any line, and as an output we get the only line that contains a dot.

Grepping streams
================
In the examples above, we have make ``grep`` to scan **files** for patterns, but we can also ``grep`` the **output stream** of another command. When demonstrating how to grep the **output streams** of any command, it's typical to pipe the output of the ``cat`` command into ``grep``::

    $ cat grep-help.txt | grep GNU
    GNU grep home page: <http://www.gnu.org/software/grep/>
    General help using GNU software: <http://www.gnu.org/gethelp/>

This is a simplistic example just for demonstrating purposes, since it's way more efficient to use ``grep`` directly on the file.

.. _`redirection operator`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html
