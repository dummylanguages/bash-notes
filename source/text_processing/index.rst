###############
Text Processing
###############
Wikipedia has a category named `Unix text processing utilities`_, where there are listed command line utilities, used in UNIX and UNIX-like operating systems, for processing text. We'll use that list as a **base** for our exploration, but we'll leave out those tools already covered in the :ref:`GNU Coreutils section <gnu coreutils section>`;some other times we'll be including not listed ones.


.. toctree::
  :maxdepth: 3

  regular_expressions/index
  ed_editor/index
  grep/index
  sed/index
  awk/index
  patch/index


.. _`Unix text processing utilities`: https://en.wikipedia.org/wiki/Category:Unix_text_processing_utilities