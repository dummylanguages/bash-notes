###################
The ``sed`` command
###################

.. toctree::
    :maxdepth: 3
    :hidden:

    basic_use
    mode_of_operation
    addresses
    sed_commands/index
    hold_buffer

The `Wikipedia entry for the sed command`_ describes ``sed`` as a Unix utility that parses and transforms text, using a simple, compact programming language.

.. note:: Aparently, ``sed`` was initially developed from 1973 to 1974, and was based on the scripting features of the interactive editor `ed`_ and the earlier `qed`_. `Version 7 Unix`_ was the first to include ``sed`` which evolved as the natural successor to the popular `grep`_ command. Later on, `GNU sed`_ added several new features, including in-place editing of files.

Before diving deep into it, we have to mention that, nowadays, there are two common ``sed`` implementations:

* The **BSD version**, used in several Unix and also **macOS**.
* The **GNU version**, used in Linux and can be installed in any Unix.

There are some differences in the options available in these two implementations. The GNU version adds some extensions not present in the BSD implementation.

Some interesting sites about ``sed``:

* `GNU sed manual`_.
* `Bruce Barnett's sed tutorial`_.
* `sed.sf.net`_ is an amazing source of information about ``sed``.
* https://www.youtube.com/watch?v=P4ZcBrJ38I8

.. _`Wikipedia entry for the sed command`: https://en.wikipedia.org/wiki/Sed
.. _`ed`: https://en.wikipedia.org/wiki/Ed_(text_editor)
.. _`qed`: https://en.wikipedia.org/wiki/QED_(text_editor)
.. _`Version 7 Unix`: https://en.wikipedia.org/wiki/Version_7_Unix
.. _`grep`: https://en.wikipedia.org/wiki/Grep
.. _`GNU sed`: https://www.gnu.org/software/sed/
.. _`GNU sed manual`: https://www.gnu.org/software/sed/manual/
.. _`Bruce Barnett's sed tutorial`: https://www.grymoire.com/Unix/Sed.html
.. _`sed.sf.net`: http://sed.sourceforge.net/
