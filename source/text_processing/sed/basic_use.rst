*********
Basic Use
*********
There are two main basic ways of using ``sed``:

* Using commands that fit into one line, known as``sed`` **one-liners**.
* Putting our ``sed`` commands into a separate file known as a ``sed`` **script**.

One liners
==========
This use is quite common and basically consists of connecting the **output** of some command to the **input** of ``sed``, using a shell pipeline. For example::

  $ echo 'hello world' | sed 's/world/Bob/'
  hello Bob

In the example above, we're piping the output of the ``echo`` command into the input of ``sed``; the latter is using the **substitute** command to replace the first occurrence of the word ``world`` by the word ``Bob``.

Several instructions simultaneously
-----------------------------------
If we need to pack several instructions into one line, we have several options:

* Use a **semicolon** to separate them.
* Use the ``-e`` option to write them separately.
* Use multiline feature of our **shell**.

Example of the first would be::

  $ echo 'hello world' | sed 's/hello/Hi/; s/world/Bob/'
  Hi Bob

Which can also be written as two separate instructions, preceded by ``-e``::

  $ echo 'hello world' | sed -e 's/hello/Hi/' -e 's/world/Bob/'
  Hi Bob

Finally, to use the multiline capability of our shell we just have to press ``Enter`` after the first **single quote**::

  $ echo 'hello world' | sed '
  s/hello/Hi/
  s/world/Bob/'
  Hi Bob

.. note:: Another option would be piping several calls to ``sed`` one after another, but this is a bit unefficient.

Scripts
=======
Another way of using ``sed`` is putting the instructions into a separate file; that's especially useful when they don't fit on a single line. For example, let's imagine we have the following ``sed`` script::

  $ cat script.sed
  s/world/Bob/

We could run it with::

  $ echo 'hello world' | sed -f script.sed
  hello Bob

Note how we need to use the ``--file`` option (``-f`` for short) before the script name.

Wrapper Scripts
---------------
It's also common to use ``sed`` commands embedded into **shell scripts**, which act like a wrapper around out ``sed`` program. This sort of script allows us to run the instructions using just the name of the script as a command.

Separate input file
===================
So far we've seen ``sed`` used on the **output** of another command, but it's quite normal to run it on **input files**, for example::

  $ cat sample.txt 
  hello world
  $ sed 's/world/Bob/' sample.txt
  hello Bob

As you can see, we just have to specify the input file as **last argument**.


.. _`EOT`: https://en.wikipedia.org/wiki/End-of-Transmission_character
.. _`pipelines`: https://en.wikipedia.org/wiki/Pipeline_(Unix)
.. _`Turing-complete`: https://en.wikipedia.org/wiki/Turing-complete
.. _`s command`: https://www.gnu.org/software/sed/manual/sed.html#The-_0022s_0022-Command
