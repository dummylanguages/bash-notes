***************
The hold buffer
***************




.. _`addresses`: https://www.gnu.org/software/sed/manual/sed.html#sed-addresses
.. _`numeric addresses`: https://www.gnu.org/software/sed/manual/sed.html#Numeric-Addresses
.. _`range`: https://www.gnu.org/software/sed/manual/sed.html#Range-Addresses
.. _`GNU sed`: https://www.gnu.org/software/sed
.. _`regular expressions`: https://en.wikipedia.org/wiki/Regular_expression
.. _`Basic Regular Expression (BRE)`: https://www.gnu.org/software/sed/manual/sed.html#BRE-syntax
.. _`Extended Regular Expression (ERE)`: https://www.gnu.org/software/sed/manual/sed.html#ERE-syntax

