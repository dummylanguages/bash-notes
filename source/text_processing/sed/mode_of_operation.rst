*****************
Mode of operation
*****************
``sed`` is a **line-oriented** text processing utility: it reads text, line by line, from an **input stream** or **file**. The line in question is put into an internal buffer called the **pattern space**. Then ``sed`` applies whatever **operations** have been specified to that line in the pattern space. The following diagram describes this process:

.. figure:: /_images/sed/sed-pattern-space.jpg
   :align: center

After the operations have been applied, the line is printed, and the pattern space is emptied to leave space for the following line (if there's one). This cycle continues as long as there are lines in the input.

Input
=====
As its name suggest, the **stream editor** (``sed``) takes its **input** from a **stream**. To generate this stream we have several choices:

* Use a **file**.
* Use the **output stream** of other command.
* Use our terminal's default **standard input stream**, ``stdin``.

Standard input stream
---------------------
This last approach is not particularly useful but we'll show it anyways::

    $ sed 's/Bob/world!/'
    hello Bob
    hello world!
    ^D

In the first line we typed the command and press **enter** to run it. This way the command starts receiving input; now every time we press **enter** at the end of a line, we are sending it as **input** to ``sed``, which will replace every occurrence of the word ``Bob`` with the word ``world``. To finish, we just have to press ``Ctrl + d`` to indicate the `EOT`_ (end of transmission).

The output of ``sed`` goes to the **standard output** by default, but it can be captured in a file, for example::

    $ sed 's/Bob/world/' > output_file
    hello Bob

Again, we pressed **enter** after entering the command, then started typing and pressed ``Ctrl - d`` to finish. This time it seems we got no output, but that's because we were redirecting it to the ``output_file``. Let's check it out::

    $ cat output_file 
    hello world!

Using the output stream of other command
----------------------------------------
Using `pipelines`_, we can redirect the output stream of other commands so it becomes the input of ``sed``. For example, imagine we have the :download:`following file </_code/sed/sample.txt>` file:

.. literalinclude:: /_code/sed/sample.txt

Let's imagine the files is thousands of lines long, and we want to replace text in some lines::

    $ grep 'fox' sample.txt | sed s/fox/squirrel/
    The quick brown squirrel

In the command above we're using ``grep`` to match a certain line. Then we use redirect the **standard output** of ``grep`` into the **standard input** of ``sed`` using a **pipe**.

We could continue chaining the standard streams more commands using pipes. For example::

    $ grep 'fox' sample.txt | sed s/fox/squirrel/ | tr ‘a-z’ ‘A-Z’
    THE QUICK BROWN SQUIRREL

Using files
-----------
Finally, we can input text into ``sed`` using a **file**, or set of files. For example::

    $ sed -n '2,4 p' sample.txt poem.txt
    jumped over the lazy dog.
    Roses are red,
    Violets are blue,

The command above prints the range of lines from the **second** to the **fourth** lines (both included) of both of the files. As a result is printed the second line of the first file, and the first two lines of the second file.

.. note:: The purpose of this seemingly pointless example is to show how we can input several files at the same time.

Operations
==========
The **operations** applied by ``sed`` to each line in the pattern space are specified by a **sed script**. We can think of a sed script as the body of a loop that iterates through the lines of a stream, where the loop itself and the loop variable (the current line number) are implicit and maintained by ``sed``. 

.. note:: ``sed`` implements a `Turing-complete`_ programming language with about 25 different commands.

We have two ways of specifying the sed script:

* It can either be specified on the command line (using the ``-e`` option)
* It can be supplied in a **separate file** (``-f`` option).

Feeding commands through the command-line
-----------------------------------------
The ``-e`` option (short for ``--expression``) indicates that we want to pass our **sed script** on the command line. Since this is the **default**, we don't have to use explicitely use ``-e``. For example, let's run a very basic script on the :download:`sample.txt </_code/sed/sample.txt>` file::

    $ sed -n '1p' sample.txt
    The quick brown fox

In the command above we have not used the ``-e`` option because we have only used one **command**.

.. warning:: You can omit the ``-e`` option when your script consists of a **single command**.

If we want to pass **several commands**, we have two options:

* Using ``-e`` for each command::

    $ sed -e 's/fox/frog/' -e 's/dog/fish/' sample.txt    
    The quick brown frog
    jumps over the lazy fish.

* Separate the commands using a semicolon ``;``::

    $ sed 's/fox/frog/ ; s/dog/fish/' sample.txt
    The quick brown frog
    jumps over the lazy fish.

As you can see, even though we're running two commands, we still can get away without using ``-e`` if we separate the commands with a semicolon.

Feeding commands using a script
-------------------------------
As opposed to the ``--expression`` option, we can also use the ``--file`` option, ``-f`` for short. This option takes a script file as an argument. For example, consider the following script::

    $ cat script.sed 
    s/dog/@@@/g
    s/fox/dog/g
    s/@@@/fox/g

The script has three lines, and we're running the `s command`_ (as in substitute) on the three of them. We can use this script to feed commands to ``sed``::

    $ sed --file script.sed sample.txt 
    The quick brown dog
    jumps over the lazy fox.

As you can see, we've swapped the strings **dog** and **fox** using a **temporal string**, ``@@@``.

.. _`EOT`: https://en.wikipedia.org/wiki/End-of-Transmission_character
.. _`pipelines`: https://en.wikipedia.org/wiki/Pipeline_(Unix)
.. _`Turing-complete`: https://en.wikipedia.org/wiki/Turing-complete
.. _`s command`: https://www.gnu.org/software/sed/manual/sed.html#The-_0022s_0022-Command
