############
sed commands
############

.. toctree::
    :maxdepth: 3
    :hidden:

    print
    delete
    substitute

`GNU sed`_ supports both **standard POSIX** commands and some **GNU extensions**. Check the official manual for the `sed commands summary`_.

.. note:: By default, ``sed`` will read the **sed command** that we pass through the command-line, so there's no need to be explicit about it using the ``--expression`` or ``-e`` options. Here we'll be omitting that option.

.. _`GNU sed`: https://www.gnu.org/software/sed/
.. _`sed commands summary`: https://www.gnu.org/software/sed/manual/sed.html#sed-commands-list
