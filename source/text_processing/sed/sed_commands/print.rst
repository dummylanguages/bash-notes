*************
Print command
*************
We can just print the lines in a file using the ``p`` command::

    $ sed 'p' sample.txt
    The quick brown fox
    The quick brown fox
    jumps over the lazy dog.
    jumps over the lazy dog.

``sed`` prints each line twice:

1. Once because we are using the ``p`` command (print), which explicitely prints the contents of the pattern space.
2. Another one because at the end of each cycle, by default, ``sed`` prints the contents of the pattern space to its output stream, before emptying it for the next cycle.

Now consider the following one-liner::

    $ sed '' sample.txt 
    The quick brown fox
    jumped over the lazy dog.

.. note:: Even when we're not using any **sed command** at all, ``sed`` reads from the file, line by line, and put each line into an internal buffer called the **pattern space**. Then, by **default**, at the end of each **cycle** and before emptying the buffer, it prints it to its standard output.

Silencing the pattern space
---------------------------
We can silence the printing of the **pattern space** by using the ``--silent`` or the ``--quiet`` options (``-n`` for short)::

    $ sed -n 'p' sample.txt 
    The quick brown fox
    jumps over the lazy dog.

Print the number of the line
============================
To print the number line of the line in the pattern space::

    $ sed -n '=' sample.txt
    1
    2

.. _`GNU sed`: https://www.gnu.org/software/sed/
.. _`list of commands`: https://www.gnu.org/software/sed/manual/sed.html#sed-commands-list


