******************
Substitute command
******************
According to the official manual of GNU sed, the `s command`_ is probably the most important one and has a lot of different options. The syntax of the s command is::

    s/regexp/replacement/flags

* The ``s`` at the beginning stands for **substitute** and it's the command itself.
* The **three slashes** ``/.../.../`` are the default **delimiters**.
* ``regexp`` is a **regular expression** used to search for a pattern in a line.
* ``replacement`` is the string we want to substitute in when the regular expression matches.
* ``flags`` are 

Changing the delimiters
=======================
Conventionally, the substitute command uses **slashes** as delimiters. But that may produce ugly expressions such as::

    $ pwd
    /home/bob/Pictures
    $ pwd | sed -n "s/\/home\/bob\/Pictures/Robert's pics folder/ p"
    Robert's pics folder

The first part of the command ``"s/\/home\/bob\/Pictures/Robert's pics/ p"`` is known as a **picket fence**, because the slash delimiters have to be escaped with a backslash. We can avoid that using other delimiters, in this case I tend to use the **pipe character**::

    $ pwd | sed -n "s|/home/bob/Pictures|Robert's pics folder| p"
    Robert's pics folder

But other people may use **underscores**::

    $ pwd | sed -n "s_/home/bob/Pictures_Robert's pics folder_ p"
    Robert's pics folder

Or **colons**::

    $ pwd | sed -n "s:/home/bob/Pictures:Robert's pics folder: p"
    Robert's pics folder

.. note:: This ``sed`` feature allows us to avoid what is known as `leaning toothpic syndrome`_.

Deleting strings
================
We've already seen how the ``d`` command can be used to **delete lines**. But what if we want to delete only certain strings? In that case we can use the **substitute command**, for example::

    $ sed 's/brown //' sample.txt 
    The quick fox
    jumps over the lazy dog.

We are matching the string ``brown□`` and replacing it with **nothing** which is equivalent to deleting it.

.. _`s command`: https://www.gnu.org/software/sed/manual/sed.html#The-_0022s_0022-Command
.. _`sed addresses`: https://www.gnu.org/software/sed/manual/sed.html#sed-addresses
.. _`numeric addresses`: https://www.gnu.org/software/sed/manual/sed.html#Numeric-Addresses
.. _`range`: https://www.gnu.org/software/sed/manual/sed.html#Range-Addresses
.. _`Basic Regular Expression (BRE)`: https://www.gnu.org/software/sed/manual/sed.html#BRE-syntax
.. _`Extended Regular Expression (ERE)`: https://www.gnu.org/software/sed/manual/sed.html#ERE-syntax
.. _`leaning toothpic syndrome`: https://en.wikipedia.org/wiki/Leaning_toothpick_syndrome

