************
Delete lines
************
The official manual of GNU sed, contains a `sed commands list`_ where the ``d`` command is cryptically described as:

    Delete the pattern space; immediately start next cycle.

Let's see some examples to see how it actually works. This is the sample text we'll use::

    $ cat poem.txt 
    Roses are red,
    Violets are blue,
    Sugar is sweet,
    And so are you.

Deleting lines by number
========================
To delete the **first line** of a text input::

    $ sed '1d' poem.txt 
    Violets are blue,
    Sugar is sweet,
    And so are you.

To get rid of the **last line**::

    $ sed '$d' poem.txt 
    Roses are red,
    Violets are blue,
    Sugar is sweet,

To delete the **first** and the **last** lines::

    $ sed '1d;$d' poem.txt
    Violets are blue,
    Sugar is sweet,

To delete all lines except the **first one**::

    $ sed '1!d' poem.txt
    Roses are red,

Deleting ranges of lines
========================
The **delete command** also works on ranges::

    $ sed '2,3d' poem.txt 
    Roses are red,
    And so are you.

Deleting pattern matched lines
==============================
To delete lines that start with a specified character::

    $ sed '/^R/ d' poem.txt
    Violets are blue,
    Sugar is sweet,
    And so are you.

Or lines that finish with some character::

    $ sed '/[,]$/ d' poem.txt
    And so are you.

We could delete the last line, only if it matches some pattern::

    $ sed '${/you/ d}' poem.txt
    Roses are red,
    Violets are blue,
    Sugar is sweet,


.. _`sed commands list`: https://www.gnu.org/software/sed/manual/sed.html#sed-commands-list
