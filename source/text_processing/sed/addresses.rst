**************************
Addresses: selecting lines
**************************
In ``sed`` we can select on which lines the **sed commands** will be executed using a mechanism known as `addresses`_. We can select lines based on their numbers or by text matching.

Selecting lines by number
=========================
Consider the following file::

    $ cat name-email.csv
    1,Donny Cable,dcable0@youtu.be
    2,Reine Maymand,rmaymand1@weebly.com
    3,Rhett Meese,rmeese2@weather.com
    [...]

We can use `numeric addresses`_ to select lines. If we want to print just the **second line**::

    $ sed -n '2p' name-email.csv
    2,Reine Maymand,rmaymand1@weebly.com

To select the **last line** we would use the ``$`` sign::

    $ sed -n '$ p' name-email.csv
    10,Clio Titlow,ctitlow9@chronoengine.com

The GNU version of the command introduces a extension that is referred in the manual as **first-step**. It allows us to select the **first line** from where we'll be counting a given **step** . For example, to select all the **odd lines** we would start counting at line number 1, then count two and so on::

    $ sed -n '1~2 p' name-email.csv
    1,Donny Cable,dcable0@youtu.be
    3,Rhett Meese,rmeese2@weather.com
    5,Chrisse Konert,ckonert4@bigcartel.com
    7,Katti Ligoe,kligoe6@twitpic.com
    9,Cassi Yong,cyong8@seattletimes.com

To select the **even lines** we would have to start counting from the zeroth line, then counting two would place us in the second line and so on::

    $ sed -n '0~2 p' name-email.csv
    2,Reine Maymand,rmaymand1@weebly.com
    4,Theda Perrat,tperrat3@virginia.edu
    6,Eb McIllrick,emcillrick5@berkeley.edu
    8,Laurice Powton,lpowton7@noaa.gov
    10,Clio Titlow,ctitlow9@chronoengine.com

Selecting a range of lines
==========================
We can select lines using their numbers or using regular expressions.

Ranges using line numbers
-------------------------
We can select a `range`_ of lines using their position in a file::

    $ sed -n '2,4 p' name-email.csv
    2,Reine Maymand,rmaymand1@weebly.com
    3,Rhett Meese,rmeese2@weather.com
    4,Theda Perrat,tperrat3@virginia.edu

Above we are matching lines from **2** to **4**.

If we wanted to print from the seventh line to the end of the file, we would run::

    $ sed -n '7,$ p' name-email.csv
    7,Katti Ligoe,kligoe6@twitpic.com
    8,Laurice Powton,lpowton7@noaa.gov
    9,Cassi Yong,cyong8@seattletimes.com
    10,Clio Titlow,ctitlow9@chronoengine.com

Ranges using regular expressions
--------------------------------
We can set up both extremes of the range using regexes, for example::

    $ sed '/Rhett/,/Cassi/ d' name-email.csv 
    1,Donny Cable,dcable0@youtu.be
    2,Reine Maymand,rmaymand1@weebly.com
    10,Clio Titlow,ctitlow9@chronoengine.com

In the example above we have deleted the range of lines in between ``/Rhett/`` and ``/Cassi/``, both matched lines included.

Ranges using regexes and numbers
--------------------------------
It's also possible to mix numbers and regexes to define ranges, for example::

    $ sed '2,/Cassi/ d' name-email.csv 
    1,Donny Cable,dcable0@youtu.be
    10,Clio Titlow,ctitlow9@chronoengine.com

Selecting lines by text matching
================================
`GNU sed`_ allows us to specify addresses using `regular expressions`_. It has support for two different syntaxes:

* The default syntax is `Basic Regular Expression (BRE)`_.
* If the ``-E`` or ``-r`` options are used, it's used the `Extended Regular Expression (ERE)`_ syntax.

Consider the following poem::

    $ cat poem.txt
    Roses are red,
    Violets are blue,
    Sugar is sweet,
    And so are you.

If we wanted to get the second line we could use::

    $ sed -n '/blue/ p' poem.txt
    Violets are blue,

Case-insensitive
----------------
To make the regex work in a **case-insensitive** way, the ``I`` **modifier** has to be used::

    $ sed -n '/sugar/ Ip' poem.txt
    Sugar is sweet,

Excluding lines
===============
We may need to exclude some lines from matching::

    $ sed -n '2!p' poem.txt
    Roses are red,
    Sugar is sweet,
    And so are you.

Appending the ``!`` character to the end of an address specification (before the command letter) negates the sense of the match. It can also be used with **ranges**::

    $ sed  -n '2,3!p' poem.txt 
    Roses are red,
    And so are you.


.. _`addresses`: https://www.gnu.org/software/sed/manual/sed.html#sed-addresses
.. _`numeric addresses`: https://www.gnu.org/software/sed/manual/sed.html#Numeric-Addresses
.. _`range`: https://www.gnu.org/software/sed/manual/sed.html#Range-Addresses
.. _`GNU sed`: https://www.gnu.org/software/sed
.. _`regular expressions`: https://en.wikipedia.org/wiki/Regular_expression
.. _`Basic Regular Expression (BRE)`: https://www.gnu.org/software/sed/manual/sed.html#BRE-syntax
.. _`Extended Regular Expression (ERE)`: https://www.gnu.org/software/sed/manual/sed.html#ERE-syntax

