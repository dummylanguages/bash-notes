*******************
Redirecting output
*******************
Sometimes we need to extract some information from input files and store it in output files. 
Most AWK implementations (with some exception we'll check later) never modify the **input files** and don't have a way to do so, so in order to generate output files we need to resort to some external mechanism. 

https://www.gnu.org/software/gawk/manual/html_node/Redirection.html

Using shell's redirection operators
===================================
The most common way of doing that is to use our shell's `redirection operators`_:

* ``>`` to **write** a new file or **overwrite** an existing one.
* ``>>`` to **write** a new file or **append** an existing one.

For example, imagine we want to replace some words in the :download:`sample.txt </_code/awk/sample.txt>`::

    $ awk '{
    > gsub(/brown fox/, "green frog")
    > gsub(/dog/,"koi fish")
    > print }'
    > sample.txt > sample2.txt

We've used the ``gsub`` `string manipulation function`_ named to replace some text, and the ``>`` to write the output to the :download:`sample2.txt </_code/awk/sample2.txt>` file, check it out::

.. literalinclude:: /_code/awk/sample2.txt

Save to file in Place (``gawk``)
================================
In **version 4.1.0**, GNU Awk added a new command line option named ``--include`` (``-i`` for short) which allows to load **AWK library files**. One of the source libraries that is distributed with GNU awk is the `inplace extension`_.

So for example, if we wanted to save changes to ``sample.txt``, without having to make a copy of it, we could run::

    $ awk -i inplace '{gsub(/brown fox/, "green frog"); gsub(/dog/,"koi fish"); print}' sample.txt

.. note:: These **extensions** are enabled and handled by ``.awk`` files located in the ``/usr/share/awk`` directory.

If we set the ``INPLACE_SUFFIX`` to any string, the extension would make a **backup** of the original file with the string as a filename suffix. For example::

    $ awk -i inplace -v INPLACE_SUFFIX=.bak '{gsub(/green frog/, "brown fox"); gsub(/koi fish/,"dog"); print}' sample.txt

In the command above we'll restoring back the contents of the ``sample.txt`` and we're doing it in place, meaning, without the need of creating an intermediate file.

.. note:: The ``-v`` option for assigning variables before program execution begins.

Save to file in Place: ``mktemp``
=================================
If for some reason our version of ``awk`` doesn't include any handy extension, we can work around by using the `mktemp`_ command::

    $ tmpfile=$(mktemp)
    $ cp sample2.txt "$tmpfile"
    $ awk '{ gsub(/brown fox/, "green frog")
    > print}' "$tmpfile" > sample2.txt

Basically what we've done is:

1. Create a "virtual" file named ``tmpfile`` using the ``mktemp`` comand.
2. Copy the contents of the original file into ``tmpfile``.
3. Use ``tmpfile`` as the **input file** and output into ``sample2.txt``.

.. note:: All files and directories created by ``mktemp`` are saved in the system’s temporary directory, i.e ``/tmp``; we don't have to manually clean them up, they will be gone once we reboot our system.

.. _`redirection operators`: https://www.gnu.org/software/bash/manual/html_node/Redirections.html
.. _`string manipulation function`: https://www.gnu.org/software/gawk/manual/gawk.html#String-Functions
.. _`inplace extension`: https://www.gnu.org/software/gawk/manual/gawk.html#Extension-Sample-Inplace
.. _`mktemp`: https://www.gnu.org/software/coreutils/manual/html_node/mktemp-invocation.html#mktemp-invocation
