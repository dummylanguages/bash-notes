.. _`printing output`:

#################
Generating output
#################

.. toctree::
    :maxdepth: 3
    :hidden:

    print
    printf
    redirecting_output

In essence, an AWK program processes some **input data**, and generates some **output data**; in both cases the data is **text**. In this section we'll cover those AWK features that allow us to manipulate the output, such as:

* The ``print`` statement for printing **simple output**.
* The ``printf`` statement for printing **formatted output**.
* How to redirect output to files and pipes using:

    * AWK operators.
    * The shell's piping and redirection operators.

.. _`output separators`: https://www.gnu.org/software/gawk/manual/html_node/Output-Separators.html
