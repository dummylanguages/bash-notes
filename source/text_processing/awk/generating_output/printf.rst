.. _`printf statement`:

************************
The ``printf`` statement
************************
The ``printf`` statement gives us more precise control over the output format than what is provided by ``print``. With ``printf`` you can specify:

* The **width** to use for each item.
* Various formatting choices for numbers such as:

    * What **output base** to use
    * Whether to print an **exponent**
    * Whether to print a **sign**
    * How many digits to print after the decimal point

Basic use
=========
The general form of ``printf`` statement is as follows::

    printf format, item1, item2, …

The difference between ``printf`` and ``print`` is the ``format`` argument, which is known as the **format string**. This is a string of text intertwined with some **format specifiers** that determine how to output each of the other arguments. There is one format specifier per item.

.. note:: The **format string** is very similar to that in the ISO C library function ``printf()``.

The ``printf`` statement does not automatically append a **newline** to its output. It outputs only what the format string specifies. So if a newline is needed, you must include a ``\n`` in the format string::

    $ awk 'BEGIN { printf "Roses are red,\nViolets are blue,\nYou get the idea\n" }'
    Roses are red,
    Violets are blue,
    You get the idea
    $

Format specifiers
=================
A **format specifier** starts with the character ``%`` and ends with a `format-control letter`_ which specifies what kind of value to print.

+==================+======================================================+
| Specifier        | Purpose                                              |
+==================+======================================================+
| ``%c``           | Prints a number as a character                       |
+------------------+------------------------------------------------------+
| ``%d`` or ``%i`` | Prints an integer                                    |
+------------------+------------------------------------------------------+
| ``%e`` or ``%E`` | Prints a number in scientific (exponential) notation |
+------------------+------------------------------------------------------+
| ``%f``           | Prints a number in floating-point notation.          |
+------------------+------------------------------------------------------+
| ``%g`` or ``%G`` | Prints a number in in either scientific notation.    |
+------------------+------------------------------------------------------+
| ``%o``           | Prints an unsigned octal integer.                    |
+------------------+------------------------------------------------------+
| ``%s``           | Prints a string                                      |
+------------------+------------------------------------------------------+
| ``%u``           | Prints an unsigned decimal integer. (C compatibility)|
+------------------+------------------------------------------------------+
| ``%x`` or ``%X`` | Print an unsigned hexadecimal integer.               |
+------------------+------------------------------------------------------+
| ``%%``           | Print a single ``%``                                 |
+------------------+------------------------------------------------------+

Format modifiers
----------------
Some format specifiers may include a `format modifier`_ in between the ``%`` and the **format-control letter**.

The width modifier
^^^^^^^^^^^^^^^^^^
This is a number specifying the desired **minimum width** of a field. By default, the field will aling to the right and will be padded with spaces to the left. For example::

    $ awk 'BEGIN {printf "%9d\n", 42}'
        42

The program above sets a minimum width of nine characters for the numeric field, which in this case has two digits, and seven space characters of padding to the left.

.. note:: By **default** the **field** is printed **to the right** and the **padding** goes **to the left**.

In some cases we may want to add **padding to the right** of a **left-aligned field**. For example, consider the following :download:`input file </_code/awk/price-list>`: 

.. literalinclude:: /_code/awk/price-list

This is what we did in the :download:`price-list.awk </_code/awk/price-list.awk>` program:

.. literalinclude:: /_code/awk/price-list.awk
    :language: awk

The ``%-10s`` format specifer is setting a width of **ten characters** with the field aligned to the left. The ``%'.2f`` is doing a couple of things:

 * The ``'`` is for adding a thousands comma.
 * The ``.2`` is setting a **precission** of two decimals after the decimal point.

And this was the output::

    $ awk -f price-list.awk price-list
    Car             Price
    ---             -----
    Cybertruck      $39,900.00     
    Model 3         $36,200.00 
    Model S         $70,620.00  
    Model Y         $49,200.00
    Model X         $81,190.00
    Roadster        $200,000.00


.. _`format-control letter`: https://www.gnu.org/software/gawk/manual/html_node/Control-Letters.html
.. _`format modifier`: https://www.gnu.org/software/gawk/manual/html_node/Format-Modifiers.html
