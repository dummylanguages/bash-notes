.. _`print statement`:

***********************
The ``print`` statement
***********************
The general form of ``print`` statement is as follows::

    print item1, item2, …

The entire list of items may be optionally enclosed in **parentheses**. We can specify as many items as we want, either strings or numbers.

.. warning:: The **parentheses** are necessary if any of the item expressions uses the ``>`` character; otherwise it could be confused with an **output redirection** (we'll go about that later).

For example::

    $ awk 'BEGIN { print "hello", "world" }'
    hello world

The command above prints the two items ``hello`` and ``world`` separated by a space. The **comma** is a **separator** for the items; without the comma, both strings would be considered as a **single item** and would be printed without any space in between::

    $ awk 'BEGIN { print "hello" "world" }'
    helloworld

.. note:: The ``print`` statement adds a **newline** character at the end of the output known as the **output record separator**. If you don't want a **newline** character at the end of your output, use ``printf``.

.. warning:: The ``print`` statement is a **statement** and **not an expression**. That means that we can’t use it in the **pattern** part of a rule.

Valid items
===========
The items to print can be:

* **Constant strings**. Don't forget to add **double-quote characters** around your string, otherwise your text would be taken as an AWK expression, and you will probably get an **error**.
* **Constant numbers**, which, by the way, don't need double quotes; numbers are converted to strings and then printed.
* **Fields** of the current record (such as ``$1``).
* **Variables** or **array elements**.
* Any AWK **expression**.  

The simple statement ``print`` with **no items** is equivalent to ``print $0``: it prints the entire current record. 

.. note:: To print a **blank line**, use ``print ""``. 

Output separators
=================
Output separators are just strings that are added to the output of our ``print`` statement, to separate **fields** and **records**. 

.. warning:: These separator strings will **not** affect the output of the ``printf`` statement (note the final **f**), they only affect the output of ``print``.

In the :ref:`BEGIN and END blocks section <BEGIN and END blocks>` we saw how, usually, the **output separators** are set in the **begin block**, before the program start going through the input records.

Output Field Separator
----------------------
As we mentioned in the beginning, a ``print`` statement contains a list of items separated by **commas**. These **items** will be printed separated by a **single space character**, because that's the default value (``" "``) of the **output field separator**. We can change this default to any string we want by setting the ``OFS`` built-in variable.

Output Record Separator
-----------------------
The output from an entire ``print`` statement is called an **output record**. Each ``print`` statement outputs one output record, and then outputs a string called the **output record separator**. The initial value of this string is ``"\n"`` (i.e., a **newline character**), that's why each ``print`` statement normally prints a separate line.

.. warning:: The **output record separator** is **not** printed when we use the ``printf`` statement; it is printed **only** after the ``print`` statement.

Example
-------
Even though these two variables are usually set in the **BEGIN block**, you may find a situation where that's not what you need. Imagine we wanted to print a **header** and then print the records below **double-spaced**, but only after the first record::

    $ echo -e "Value 1, Value 2\nValue 3, Value 4\n" |
    > awk 'BEGIN { 
    > print "Column 1\tColumn 2"
    > print "---------\t--------"; FS=", " }
    > NF { OFS="\t\t"; $1=$1; ORS="\n\n"; print }'
    Column 1        Column 2
    ---------       --------
    Value 1         Value 2

    Value 3         Value 4

We'd like to point out several things:

* We only use the **BEGIN block** to set the ``FS`` variable.
* Both ``OFS`` and ``ORS`` were set inside the **action** of our rule.
* In the **pattern** we used ``NF`` to filter out **empty lines**.
* Note the use of the ``$1 = $1`` idiom to rebuild the record after changing the **field separator**.
* Finally, ``ORS`` was changed inside the action, right before the ``print`` statement.

.. _`user defined functions`: https://www.gnu.org/software/gawk/manual/gawk.html#User_002ddefined
