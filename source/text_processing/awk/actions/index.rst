###########
AWK actions
###########

.. toctree::
    :maxdepth: 3
    :hidden:

    expressions
    control_statements/index

As a refresher, an AWK program is made up of one or several **rules**, and a **rule** is a ``pattern { action }`` combination:

* The **pattern** is what AWK uses to determine if a line is a match.
* The **action** is one or more AWK statements, enclosed in braces (``{ statement }``) that AWK performs when a line is matched.

The braces around an action must be used even if the action contains only one statement. If we are not gonna write any statement we have two possibilities with different consequences:

1. An **empty action** is an empty pair of curly braces, and it means **do nothing**::

    /foo/  { }     # match foo, do nothing

2. An **ommitted action** is equivalent to ``{print $0 }``::

    /foo/          # match foo, print the record

Each statement specifies one thing to do. If our action contains several statements, we can separate them with **newlines** or with **semicolons**.

AWK supports the following types of statements:

* `Expressions`_ such as function calls or the `assignment expression`_.
* `Control statements`_ to control flow of our AWK programs.
* **Input statements** such as `getline`_, `next`_ and `nextfile`_.
* **Output statements** such as `print`_ and `printf`_.
* The `delete statement`_, for deleting array elements.

.. _`Expressions`: https://www.gnu.org/software/gawk/manual/html_node/Expressions.html
.. _`assignment expression`: https://www.gnu.org/software/gawk/manual/html_node/Assignment-Ops.html
.. _`Control statements`: https://www.gnu.org/software/gawk/manual/html_node/Statements.html
.. _`getline`: https://www.gnu.org/software/gawk/manual/html_node/Getline.html
.. _`next`: https://www.gnu.org/software/gawk/manual/html_node/Next-Statement.html
.. _`nextfile`: https://www.gnu.org/software/gawk/manual/html_node/Nextfile-Statement.html
.. _`print`: https://www.gnu.org/software/gawk/manual/html_node/Print.html
.. _`printf`: https://www.gnu.org/software/gawk/manual/html_node/Printf.html
.. _`delete statement`: https://www.gnu.org/software/gawk/manual/html_node/Delete.html

