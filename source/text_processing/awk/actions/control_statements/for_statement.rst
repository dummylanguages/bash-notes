*****************************
The ``for`` control statement
*****************************
In AWK there are two forms of ``for`` statement:

1. The first form is quite similar to the statement in C::

    for (initialization; condition; increment)
        body

2. The second one is used for iterating over all the **indices** of an array::

    for (i in array)
        do something with array[i]

In this section we'll go over both of these forms.

.. note:: In the :ref:`arrays section` we also have an example of iterating over arrays using the ``for`` loop.

The general form
================
Let's write a program to extract the month names from the :download:`inventory-shipped </_code/awk/inventory-shipped>` test file::

    $ awk 'NR < 13 { arr[NR] = $1; len = NR } 
    > END { for (i = 1; i <= len; i++) print arr[i] }' inventory-shipped 
    Jan
    Feb
    Mar
    [...]

Let's explain what's going on in the program above:

* In the rule, we use the ``NR < 13`` pattern expression so the program only run for the first 12 records; next, the action populates the array with the first field of each record. The ``len`` variable keeps the number of the last month added.
* In the **END block** we've used a ``for`` loop to iterate over the elements of the array in an orderly fashion.

We can set more than one variable in the **initialization** part of the loop (``i = 0``), using a **multiple assignment statement** such as ``x = y = 0``. But if we have to initialize variables whose initial values are not equal, we'll have to do it as separate statements preceding the for loop.

The same is true of the **increment** part of the loop (``i++``), the only way of incrementing additional variables is by using separate statements at the end of the loop. (The C language allows setting several variables in this part, using C’s comma operator, but it is not supported in awk. 

The alternative form
====================
There is an alternative version of the ``for`` loop, for iterating over all the indices of an array::

    $ awk 'NR < 13 { arr[NR] = $1; len=NR }
    > END { for (i in arr) print arr[i]}' inventory-shipped 
    Feb
    Mar
    Apr
    May
    Jun

But as you can see, the elements are printed in no particular order. So don't use this form if you're counting on the order of the elements.

.. _`output separator`: https://www.gnu.org/software/gawk/manual/gawk.html#Output-Separators
