##################
Control statements
##################

.. toctree::
    :maxdepth: 3
    :hidden:

    for_statement

`Control statements`_, such as ``if``, ``while``, and so on, control the flow of execution in awk programs. Most of awk’s control statements work the same way as their counterpart statements in C. 

.. _`Control statements`: https://www.gnu.org/software/gawk/manual/html_node/Statements.html

