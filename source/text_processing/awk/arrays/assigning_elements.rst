.. _arrays section:

******************
Assigning elements
******************
At the beginning we've seen a naive way of initializing arrays, i.e, assigning each element individually. That's rarely the case, so here we'll see other more practical ways of filling up arrays.

Small pitfall
=============
Let's start with the wrong foot::

    $ echo "Mon Tue Wed Thu Fri" | awk 'BEGIN {RS=" "} { work_days[idx] = $0; idx++ }
    > END { for (i=0; i < idx; i++) print work_days[i] }'

    Tue
    Wed
    Thu
    Fri

Unfortunately, the very first line of input data did not appear in the output! Yes, we're using the variable ``idx`` unitialized, but uninitialized variables have the numeric value zero. So, awk should have printed the value of ``work_days[i]`` right?.

.. note:: In AWK, **array subscripts** are always **strings**.

The issue here is that **subscripts** for AWK arrays are always **strings**. So when our uninitialized variable is converted to a string, it gets the value ``""`` (empty string), not zero. In further iterations the ``idx++`` makes sure it gets the right value. Here's the proper way of writing our program::

    $ echo "Mon Tue Wed Thu Fri" | awk 'BEGIN {RS=" "} { work_days[idx++] = $0 }
    > END { for (i=0; i < idx; i++) print work_days[i] }'
    Mon
    Tue
    Wed
    Thu
    Fri

Here, even though ``idx`` is incremented **after** (``idx++``, post-increment) the first element of the array is initialized, using the **autoincrement operator** inside the brackets, forces ``idx`` variable to get a numeric value (``0``) from the beginning. The numeric ``0`` is then converted to the string ``"0"`` array subscript.

Frequency array
===============
What if we want to count the frequency that a given value appears in a file. Let's use the :download:`inventory-shipped </_code/awk/inventory-shipped>` file for that::

    $ awk 'NF { month_freq[$1]++ }
    END { for (i in month_freq) print i "\t" month_freq[i] }
    ' inventory-shipped
    Jun     1
    Apr     2
    Oct     1
    [...]

Let's go over the program above:

* First of all, since this file contains some empty lines, we're using ``NF`` as a **pattern**: if the line has no fields, ``NF`` evaluates to **zero**, and the action don't run.
* We're using the **months names** as **subscripts**, autoincrementing their values when they appear in a record.
* Notice that the order the months are printed is not the same that the order in which they were created. The next section solves that.

Printing in creation order
--------------------------
To solve that small inconvenience we were talking about in the last section, we don't have to do much. Check out the :download:`month_freq.awk </_code/awk/month_freq.awk>` program:

.. literalinclude:: /_code/awk/month_freq.awk
    :language: awk

If we run it, we can see how the months appeared in they same order they were added to the array::

    $ awk -f month_freq.awk inventory-shipped
    Month   Frequency
    =====   =========
    Jan      2
    Feb      2
    Mar      2
    [...]

The built-in ``split()`` function
=================================
All `POSIX-compliant`_ AWK implementations include a `string manipulation function`_ named ``split()``. This function splits an argument string into array elements ``a[1], a[2], ..., a[n]``, and returns ``n``, the number of elements. The **general form** of this function is::

    split(string, array [, fieldsep ])

Where:

* ``string`` is the argument string.
* ``array`` is the name of the array.
* ``fieldsep``, is a **regexp** describing where to split string. If ``fieldsep`` is omitted, the value of ``FS`` is used.

For example::

    $ awk 'BEGIN { split("cul-de-sac", arr, "-"); for (i in arr) print arr[i] }'
    de
    sac
    cul

Splits the string "cul-de-sac" into three fields using ``-`` as the **separator**. It sets the contents of the array ``arr`` as follows::

    arr[1] = "cul"
    arr[2] = "de"
    arr[3] = "sac"

.. note:: Arrays created with ``split()`` are **one-based** indexed, i.e, indices start at **one**!

Nonetheless, the ``for in`` statement prints them in no particular order. To solve this we would have to write::

    $ awk 'BEGIN { len = split("cul-de-sac", arr, "-")
    > for (i = 1; i <= len; i++) print arr[i] }'
    cul
    de
    sac

Note how the array starts at ``1`` and not ``0`` like in a lot of other programming languages.

.. _`String manipulation function`: https://www.gnu.org/software/gawk/manual/html_node/String-Functions.html
.. _`POSIX-compliant`: https://pubs.opengroup.org/onlinepubs/9699919799/