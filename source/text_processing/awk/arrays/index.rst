######
Arrays
######

.. toctree::
    :maxdepth: 3
    :hidden:

    assigning_elements

In AWK, an `array`_ is a collection of values called **elements**. Array elements can be assigned values just like awk variables, using the general form::

    array[index-expression] = value

For example::

    $ awk 'BEGIN { arr[0]="Monday"; arr[3]="Thursday"; print arr[0], arr[3] }'
    Monday Thursday

Above we have used an array named ``arr``. **Array names** have the same syntax as **variable names**; any valid variable name would also be a valid array name. As you can see, there was no need of specifying the type of the array or its size, as in other languages. Also, note that the array is **sparse**, which just means some indices are missing. It has elements ``0`` and ``3``, but doesn’t have elements ``1``, or ``2``. That's because in AWK arrays are **associative**, check this out::

    $ awk 'BEGIN { arr["first"]="Monday"; arr[-3.5]="Thursday"; print arr["first"], arr[-3.5] }'
    Monday Thursday

.. note:: Arrays in AWK are **associative**.

Since Arrays in AWK are **associative**, the indices don’t have to be integers, we can use any number, or even a string as an index. In fact, **array subscripts** are always **strings**. In the example, the number ``-3.5`` isn’t double-quoted, because awk automatically converts it to a string. 

.. note:: In AWK, **array indices** are **strings**.

The last thing we want to mention is that a reference to an element that does not exist automatically creates that array element, with the null string as its value, for example::

    $ awk 'BEGIN { freq[1.2]=3; freq[2.2]=9
    > if (freq[3.3] == "") print "Frequency does not exist!" }'
    Frequency does not exist!

The test above is incorrect for several reasons:

1. First, it creates ``freq[3.3]`` if it didn’t exist before, setting its value to the null string.
2. Second, it is valid (if a bit unusual) to set an array element equal to the empty string.
3. Finally, the test evaluate to true in every case.

The proper way to check if a particular index exists or not without creating that element is to use::

    $ awk 'BEGIN { freq[1.2]=3; freq[2.2]=9
    > if (! (3.3 in freq)) print "Frequency does not exist!" }'
    Frequency does not exist!

.. _`array`: https://www.gnu.org/software/gawk/manual/html_node/Arrays.html#Arrays
