##############
AWK one-liners
##############

.. toctree::
    :maxdepth: 3
    :hidden:

    awk_one_liners

A good approach when starting with AWK is to use practical examples. There's a text file named `HANDY ONE-LINE SCRIPTS FOR AWK`_ which contains a bunch of useful one-line AWK commands. This file has been explained by Peteris Krumins in his blog `catonmat`_

.. _`HANDY ONE-LINE SCRIPTS FOR AWK`: https://www.pement.org/awk/awk1line.txt
.. _`catonmat`: https://catonmat.net/awk-one-liners-explained-part-one
