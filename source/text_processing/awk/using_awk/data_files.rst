*************************
Data files for practicing
*************************
The best way to learn AWK is to practice a lot. Even though we're mostly gonna be using ``nawk`` (The One True Awk), the `gawk distribution`_ includes several sample files that are used by `The GNU Awk User’s Guide`_ to demonstrate AWK features. These files will come in handy to practice AWK features, regardless of the version of the program we use.

The first step is to **download** the `latest version of the gawk distribution`_::

    $ wget https://ftp.gnu.org/gnu/gawk/gawk-5.0.0.tar.gz

Extract the files::

    $ tar -zxvf gawk-5.0.0.tar.gz

According to the guide, the files are included in ``awklib/eg/data`` directory, so let's check the contents::

    $ ls -1 gawk-5.0.0/awklib/eg/data
    class_data1
    class_data2
    guide-mellow.po
    guide.po
    inventory-shipped
    mail-list

Especially useful are the :download:`mail-list </_code/awk/mail-list>` and :download:`inventory-shipped </_code/awk/inventory-shipped>` files, so it's a good idea to copy these two files to a convenient directory for practice. Now we can mess up all we want with the files. If they get too messy, we can always copy them over again.

.. _`gawk distribution`: https://www.gnu.org/software/gawk/manual/gawk.html#Gawk-Distribution
.. _`The GNU Awk User’s Guide`: https://www.gnu.org/software/gawk/manual/html_node/index.html
.. _`latest version of the gawk distribution`: https://ftp.gnu.org/gnu/gawk/
