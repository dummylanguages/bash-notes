.. _`basic use`:

***************************
Basic use: the command-line
***************************
Writing short programs in the command-line is by far the most common way of using AWK. Since they have to fit in one line, programs written this way are also known as AWK **one-liners**.

.. note:: There's a well known online compilation of AWK program in this format named `HANDY ONE-LINE SCRIPTS FOR AWK`_, aka **the AWK one-liners**. There are even books explaining this compilation, like this one from `catonmat`_.

The general syntax for an AWK one-liner is::

    $ awk 'program' input-files

Although is also quite common to see them as part of a shell pipeline::

    $ some_command | awk 'program'

Our first AWK program
=====================
For example, imagine we have the file :download:`sample.txt </_code/awk/sample.txt>`, which contains the following text:

.. literalinclude:: /_code/awk/sample.txt

Let's use it as input for our first AWK program::

    $ awk '// { print }' sample.txt
    The quick brown fox
    jumps over the lazy dog.

The command above has three parts:

1. ``awk`` is the call to the **interpreter**.
2. ``'// { print }'`` is the **program**.
3. The last argument is the name of the **input file**, in this case named ``sample.txt``.

.. warning:: When writing **one-liners**, the program instructions must be enclosed in **single quotes** to protect them from the shell. Instructions almost always contain **curly braces** and/or **dollar signs**, which are interpreted as **special characters** by the shell.

Regarding the program itself, it is composed of two parts::

    /pattern/ {action}

The combination *pattern/action* mentioned above is known in AWK parlance as a **rule**; the program of our example consists of a single rule.

Rule separator
==============
In some cases, we may want to fit several **AWK rules** in the same line. No problem, just use a **semicolon** to separate them. For example::

    $ awk '// {print}; 1 {print}' sample.txt
    The quick brown fox
    jumps over the lazy dog.
    The quick brown fox
    jumps over the lazy dog.

The sample text is printed twice, once for each of the rules.

.. note:: When writing AWK programs in separate files, we usually write a **rule per line**. In that format, space is not a constraint and the code looks more clear.

Using the secondary prompt
==========================
Sometimes we can take advantage of the continuation prompt offered by our shell in order to write our program in several lines. For example::

   $ echo 'hello world' | awk '
   > /hello/
   > {gsub(/hello/, "hi")}
   > {print}'           
   hello world
   hi world

Using the prompt this way allows us to write a rule per line, which makes the program look a bit clearer, while at the same time we get more room for it.

.. note:: As you can see in the last example, we can use the **output** of another command (``echo`` in this case) to provide the **input** of ``awk``; using ``awk`` together with other commands as part of a `shell pipeline`_ is quite common.

.. _`HANDY ONE-LINE SCRIPTS FOR AWK`: https://www.pement.org/awk/awk1line.txt
.. _`shell pipeline`: https://en.wikipedia.org/wiki/Pipeline_%28Unix%29
.. _`catonmat`: https://catonmat.net/awk-one-liners-explained-part-one
