#################
Ways of using AWK
#################

.. toctree::
    :maxdepth: 3
    :hidden:

    basic_use
    separate_file
    standalone_programs
    shell_script_embedded
    data_files

There are several ways of using AWK:

* We can write AWK programs directly in the command line. If we use this approach, we have to fit our programs within to the length of a line (AWK **one-liners**).

* Sometimes our programs may need more than a couple of lines. In that case, may be more convenient to write the program in a **separate file**.

* Lastly we can also embed an AWK program in a **shell script**, which works like a **wrapper** around our AWK program.

Regardless of the way we run our program, it's always necessary to feed it some **input**. This input is just a bunch of text that may come:

* Most of the times from a **file** whose name you specify as an argument after your AWK program command line.
* Also quite often, the **input** of ``awk`` comes from the **output** of another command which is piped into the **input** of ``awk``.
* From our terminal's **standard input**, which by **default** is the keyboard; start typing and press ``Ctrl + d`` to indicate the `EOF`_. Using ``awk`` this way is quite rare.

.. _`EOF`: https://en.wikipedia.org/wiki/End-of-file
