******************************
AWK programs in separate files
******************************
Sometimes **AWK programs** don't fit in a single line. In these cases, may be more convenient to put the program into a **separate file**. Such a file can be run:

* Either as **stand-alone** program, by adding a `shebang line`_.
* Or just as a set of rules that will be invoked from the command line.

In this section we'll cover the second scenario (check the next section for more information about :ref:`AWK stand-alone programs <awk stand-alone programs>`.

The ``--file`` option
=====================
In order to tell ``awk`` to read a program from a file, we have to use the ``--file`` option (or ``-f`` for short)::

    $ awk -f program.awk input-file

For example, these are the contents of :download:`header_footer.awk </_code/awk/header_footer.awk>` (the ``.awk`` extension is optional but recommended):

.. literalinclude:: /_code/awk/header_footer.awk

Note how we are omitting the **pattern** in our rule. We can run the program in the file with::

    $ awk -f header_footer.awk sample.txt
    ***This is a silly header.***
    The quick brown fox
    jumps over the lazy dog.
    ***This is a silly footer.***

In the case of AWK programs contained in **separate files**, there's no need of making them **executable**.

Pipeline integration
====================
The fact of having the AWK program in a separate file is no obstacle to be used as part of a `shell pipeline`_, the same as we would do with one-liner programs::

    $ ls ~ | awk -f header_footer.awk 
    ***This is a silly header.***
    Applications
    Desktop
    Documents
    Downloads
    Library
    Movies
    Music
    Pictures
    Public
    ***This is a silly footer.***

.. _`shell pipeline`: https://en.wikipedia.org/wiki/Pipeline_%28Unix%29
.. _`shebang line`: https://en.wikipedia.org/wiki/Shebang_(Unix)
