.. _awk embedded:

**********************************************
How to embed AWK programs in our shell-scripts
**********************************************
Sometimes we may need to integrate an **AWK programs** into a **shell script** that also may make use of other UNIX tools. This way we can invoke our AWK program by running the shell script, and has the added benefit of not having to keep track of a separate file for the AWK program, that could be misplaced.

.. note::  Such a shell script works like a **wrapper** around our AWK program: it contains a line that invokes the ``awk`` command, as well as the AWK program right after.

For example, consider the following :download:`dont_panic.sh </_code/awk/dont_panic.sh>` shell script:

.. literalinclude:: /_code/awk/dont_panic.sh

Note how we have to make use of the ``awk`` command, as well as the **single quotes** around our AWK rule (or rules). For that reason, if you need to use a single quote (or an apostrophe) you have to use a ``\047`` code; that will expand to a single quote character when the program is run. Otherwise you would get an error since ``awk`` will interpret the apostrophe in ``Don't`` as the end of the **rule**.

.. note:: The ``\047`` is the **octal code** for the **single quote character**.

As with any other script, we have to make it executable, then we can run it::

    $ chmod +x dont_panic.sh 
    $ ./dont_panic.sh 
    This is a shell script that gives good advice!
    Don't panic

The first line is yielded by a Bash command (``echo``), whereas the second one (``Don't panic``) is result of the AWK ``print`` **action**.

Common pitfall
==============
The **single quote** may bite you even if it appears in an AWK comment! For example, imagine we add a comment to our last script:

.. literalinclude:: /_code/awk/dont_error.sh

Now save it as :download:`dont_error.sh </_code/awk/dont_error.sh>` and run it again with the new name::

    $ ./dont_error.sh 
    This is a shell script that gives good advice!
    Don't panic
    ./dont_error.sh: line 7: unexpected EOF while looking for matching `''
    ./dont_error.sh: line 8: syntax error: unexpected end of file
