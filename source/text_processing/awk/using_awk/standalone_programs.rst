.. _`awk stand-alone programs`:

************************
AWK stand-alone programs
************************
AWK **one-liners** are really useful tools used daily by programmers, the problem is, that once you run the line, puff! your witty program is gone. Not in vain, the `gawk user's guide`_ refers to them as *one-shot throwaway awk programs*.

.. note:: AWK is a `Turing-complete`_ programming language, and even the early Bell Labs users of AWK often wrote well-structured large AWK programs.

Sometimes we may want to write self-contained AWK scripts that we can later invoke from the command-line as many times as needed. In this section we'll see how we can do that.

The AWK shebang
===============
To create a stand-alone AWK program, we just have to put the rules in a file, adding a `shebang`_ line at the very top::

    #!/usr/bin/awk -f
    
That line tells the system know what program should be used to interpret the contents of the script.

.. warning:: Unfortunately, it seems that the ``-f`` flag is non-standard; that would make scripts using this shebang non-portable accross all Unix systems.

Hello world
===========
Let's start with the classic most basic program:

.. literalinclude:: /_code/awk/hello_world.awk

.. note:: As opposed to AWK programs contained in mere separate files (the ones you need to run with the ``-f`` option), stand-alone AWK programs must be given **executable permission**.

Before running the :download:`hello_world.awk </_code/awk/hello_world.awk>` script, we have to give it **executable** permission::

    $ chmod +x hello_world.awk

Now we can run it (without the ``-f`` option)::

    $ ./hello_world.awk 
    Hello, world

As you can see, the script prints the ``Hello, world`` message to the standard output and exits when there's nothing else to do.

.. note:: For more useful scripts, you may want to keep them centralized in a folder somewhere in your system's PATH so they can be invoked from anywhere.

No single quotes needed
-----------------------
One thing you may have noticed is that we didn't have to use **single quotes** to delimit our script. That makes sense because the sole purpose of the **single quotes** mechanism is to make the shell interpret the AWK rule as if it were a simple string of text. 

.. _`Turing-complete`: https://en.wikipedia.org/wiki/Turing_completeness
.. _`shebang`: https://en.wikipedia.org/wiki/Shebang_(Unix)
.. _`gawk user's guide`: https://www.gnu.org/software/gawk/manual/html_node/One_002dshot.html#One_002dshot
