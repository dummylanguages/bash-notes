.. _BEGIN and END blocks:

******************************************
The ``BEGIN`` and ``END`` special patterns
******************************************
The ``BEGIN`` and ``END`` special patterns are used to provide startup and cleanup actions for an AWK program.

.. note:: The ``BEGIN`` and ``END`` special patterns are also known as the ``BEGIN`` and ``END`` **blocks**.

To better understand how they work, we have to grok how an AWK program flows, which we explain next.

Basic flow of an AWK program
============================
The following diagram shows how the **AWK interpreter** (``awk`` for short) does its work:

.. figure:: /_images/awk/awk_workflow.jpg
   :align: center

1. Execute the ``BEGIN`` block.
2. The AWK interpreter ``awk`` reads a line from the input stream (file, pipe, or stdin) and stores it in memory.
3. Execute **awk commands** sequentially on the input. By default ``awk`` execute commands on every **line**, but if we provide a pattern, only the lines that match will be affected by the **actions**.
4. Repeat the process until the end of the file is reached.
5. Execute the ``END`` block.

So as you can see, the ``BEGIN`` block runs **before** any input line is read, and it's a special kind of pattern which is not tested against any of the input records. The ``END`` block runs **after** all lines have been read.

.. _`modifying_separators`:

Modifying Records and Fields Separators
=======================================
Since the ``BEGIN`` block runs **before** any input line is read, it is used relatively often to modify the default separators for records and fields.

Modifying the Input Record Separator
------------------------------------
As you know, by default, the **record separator** is a **newline** character, but it can be modified by setting up the ``RS`` variable.  Often, the right time to do this is at the beginning of execution, before any input is processed, so that the very first record is read with the proper separator. To do this, use the special `BEGIN pattern`_.

For example::

    $ poem='Roses are red, Violets are blue, You get the idea.'
    $ awk 'BEGIN {RS=", "}{print $0}' <<< $poem
    Roses are red
    Violets are blue
    You get the idea.

.. note:: Note how we used the Bash `here string`_ (``<<<``) to use the string stored in the ``poem`` variable as the standard input to ``awk``. 

If we set ``RS`` to an empty space we'd get::

    $ awk 'BEGIN {RS=" "}{print $0}' <<< $poem
    Roses
    are
    red,
    Violets
    are
    blue,
    Sugar
    [...]

Each word on its own line.

Modifying the Input Field Separator
-----------------------------------
By default, input fields are separated by whitespace, but in some situations, we have to deal with input where the fields are separated by other symbols such as commas, in the case of a `CSV file`_. We can modify the **field separator** by setting up the ``FS`` variable. 

For example, imagine we want to extract the **names** (first field) of the :download:`mail-list.csv </_code/awk/mail-list.csv>` file that we created in the last section::

    $ awk 'BEGIN {FS = ","} { print $1 }' mail-list.csv
    Amelia
    Anthony
    Becky
    [...]

We just told ``awk`` to read the fields as separated by a **comma**, and then print the first field.

Modifying the Output separators
-------------------------------
As we mention in the :ref:`print statement section <print statement>`, the **output separators** are usually set in the **begin block**, before the program start going through the input records.

.. warning:: The **output separators** only affect the output of ``print`` statement; they do **not** affect the output of the ``printf`` statement (note the final **f**).

Output Record Separator
^^^^^^^^^^^^^^^^^^^^^^^
Let's say we want to output our records **double spaced**::

    $ echo -e "one\ntwo\nthree" | awk 'BEGIN {ORS="\n\n"} 1'
    one

    two

    three

The ``echo -e "one\ntwo\nthree"`` command generates three lines (records), which we just print changing the ``ORS`` to ``"\n\n"`` instead of the **default** ``"\n"``

Output Field Separator
^^^^^^^^^^^^^^^^^^^^^^
What if we need to convert the :download:`mail-list </_code/awk/mail-list>` into a `CSV file`_. We can do that by setting the value of the ``OFS`` variable to any string we want, in this case a ``","``, since we need our fields as **comma separated values** we could run::

    $ awk 'BEGIN { OFS = "," } { $1 = $1; print }' mail-list > mail-list.csv

Now if we check the contents of :download:`mail-list.csv </_code/awk/mail-list.csv>`, to see if it worked::

    $ cat mail-list.csv 
    Amelia,555-5553,amelia.zodiacusque@gmail.com,F
    Anthony,555-3412,anthony.asserturo@hotmail.com,A
    [...]

If you are wondering about the ``$1 = $1`` expression, its purpose is to force ``awk`` to rebuild the record. The `Gawk User's Guide`_ explains the trick:

    When ``awk`` reads a full record, it stores it in ``$0`` exactly as it was read from the input. This includes any leading or trailing whitespace, and the exact whitespace (or other characters) that separates the fields.

    It is a **common error** to try to change the field separators in a record simply by setting ``FS`` or ``OFS``, and then expecting a plain ``print`` or ``print $0`` to print the modified record.

    But this does not work, because nothing was done to change the record itself. Instead, you must force the record to be rebuilt, typically with a statement such as ``$1 = $1``, as described earlier. 


.. _`Gawk User's Guide`: https://www.gnu.org/software/gawk/manual/html_node/Changing-Fields.html
.. _`gawk distribution`: https://www.gnu.org/software/gawk/manual/gawk.html#Gawk-Distribution
.. _`download`: https://ftp.gnu.org/gnu/gawk/
.. _`EOF`: https://en.wikipedia.org/wiki/End-of-file
.. _`BEGIN pattern`: https://www.gnu.org/software/gawk/manual/gawk.html#BEGIN_002fEND
.. _`here string`: https://tldp.org/LDP/abs/html/x17837.html
.. _`fields`: https://www.gnu.org/software/gawk/manual/gawk.html#Fields
.. _`Specifying How Fields Are Separated`: https://www.gnu.org/software/gawk/manual/gawk.html#Field-Separators
.. _`output separator`: https://www.gnu.org/software/gawk/manual/gawk.html#Output-Separators
.. _`CSV file`: https://en.wikipedia.org/wiki/Comma-separated_values
