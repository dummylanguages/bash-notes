******
Ranges
******
In AWK, a `range pattern`_ is made of two patterns separated by a comma::

    begpat, endpat
    
They are used to match ranges of consecutive input records. The first pattern, ``begpat``, controls where the range begins, while ``endpat`` controls where the pattern ends.

A range pattern starts out by matching ``begpat`` against every input record. When a record matches ``begpat``, the range pattern is turned on, and the range pattern matches this record as well. As long as the range pattern stays turned on, it automatically matches every input record read. The range pattern also matches ``endpat`` against every input record; when this succeeds, the range pattern is turned off again for the following record. Then the range pattern goes back to checking ``begpat`` against each record.

* writeexample

The record that turns on the range pattern and the one that turns it off both match the range pattern. If you don’t want to operate on these records, you can write if statements in the rule’s action to distinguish them from the records you are interested in.

* writeexample

It is possible for a pattern to be turned on and off by the same record. If the record satisfies both conditions, then the action is executed for just that record. For example, suppose there is text between two identical markers (e.g., the ‘%’ symbol), each on its own line, that should be ignored. A

* writeexample

.. _`range pattern`: https://www.gnu.org/software/gawk/manual/html_node/Ranges.html
