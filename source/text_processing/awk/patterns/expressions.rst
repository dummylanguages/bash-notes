***********
Expressions
***********
using ``NF`` as a **pattern**: if the line has no fields, ``NF`` evaluates to **zero**, and the action don't run.

Another expression is to use ``length`` with some comparison operator to filter out lines by their length in characters.

.. _`output separator`: https://www.gnu.org/software/gawk/manual/gawk.html#Output-Separators
