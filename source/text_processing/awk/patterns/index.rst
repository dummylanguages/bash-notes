############
AWK patterns
############

.. toctree::
    :maxdepth: 3
    :hidden:

    regular_expressions
    ranges
    expressions
    begin_end

There are several types of AWK patterns:

* **Regular expressions**, the ones that go between ``//``.
* **Expressions**, such as ``2 - 1``.
* **Ranges**, which are pairs of patterns separated by a comma, specifying a range of records.
* Some **special patterns** such as:

    * ``BEGIN`` and ``END``
    * ``BEGINFILE`` and ``ENDFILE``

* The **empty pattern**, which matches every record. 

.. _`shell pipeline`: https://en.wikipedia.org/wiki/Pipeline_%28Unix%29

