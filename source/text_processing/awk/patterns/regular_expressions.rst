*******************
Regular Expressions
*******************
`Regular expressions`_ enclosed in slashes (``/regex/``) is an **AWK pattern** that matches every input record whose text belongs to that set. The simplest regular expression is a sequence of letters, numbers, or both, that matches any string that contains that sequence. Thus, the pattern ``/foo/`` matches any input record containing the three adjacent characters ``foo`` anywhere in the record.

.. note:: Almost all implementations of AWK (``awk``, ``nawk``, and ``gawk``) use `Extended Regular Expressions`_, aka **ERE**.

In this section we're going to cover some specifics about regular expressions in AWK, so we'll assume a basic background knowledge. Feel free to visit our :ref:`general section about regular expressions <regular expressions>` to get up to speed. 

Matching against the full record
================================
For example::

    $ awk '/Jan/' inventory-shipped 
    Jan  13  25  15 115
    Jan  21  36  64 620

The program above, tests the regular expression ``/Jan/`` against the entire text of each record, in the :download:`inventory-shipped </_code/awk/inventory-shipped>` test file. Normally, it only needs to match some part of the text in order to succeed. Then we are printing the full records that matched the regexp.

.. note:: When a regexp is enclosed in slashes, such as ``/foo/``, we call it a **regexp constant**, much like ``5.27`` is a **numeric constant** and ``"foo"`` is a **string constant**. 

Matching expressions
====================
If instead of matching against the entire current input record we need to match against a particular **field** we can use the ``~`` and ``!~`` operators to create expressions that specify the string to match against. The following example matches all input records with the uppercase letter ``J`` somewhere in the first field::

    $ awk '$1 ~ /J/' inventory-shipped
    Jan  13  25  15 115
    Jun  31  42  75 492
    Jul  24  34  67 436
    Jan  21  36  64 620

The expression ``$1 ~ /J/`` matches, or selects, all input records with the uppercase letter ‘J’ somewhere in the **first field**. We'd have gotten the same output if we had written like this::

    $ awk '{ if ($1 ~ /J/) print }' inventory-shipped

.. note:: The two operators ``~`` and ``!~`` perform regular expression comparisons. Expressions using these operators can be used as patterns, or in ``if``, ``while``, ``for``, and ``do`` statements.

And if we need to match the records whose first field doesn't contain an uppercase ``J`` we would do::

    $ awk '$1 !~ /J/' inventory-shipped
    Feb  15  32  24 226
    Mar  15  24  34 228
    Apr  31  52  63 420



.. _`regular expressions`: https://en.wikipedia.org/wiki/Regular_expression
.. _`Extended Regular Expressions`: https://pubs.opengroup.org/onlinepubs/9699919799/

