#########
Variables
#########

.. toctree::
    :maxdepth: 3
    :hidden:

    control_variables
    informative_variables
    user_defined

AWK offers two types of `variables`_:

* **User-defined variables**: the ones that allow the user to store values at one point in your program for use later in another part of your program.
* `Built-in variables`_, which are variables that come already predefined. There are 3 categories in this group:

    * Variables to control ``awk``. These can be modified by the user to control the way ``awk`` does certain things.
    * Informative variables, that provide information but can't be modified by the user.

.. _`variables`: https://www.gnu.org/software/gawk/manual/gawk.html#Variables
.. _`Built-in variables`: https://www.gnu.org/software/gawk/manual/gawk.html#Built_002din-Variables
