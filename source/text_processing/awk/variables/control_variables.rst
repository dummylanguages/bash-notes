*****************
Control variables
*****************
Here we'll go over the **built-in variables** that we can modify to control the way ``awk`` behaves.  These can be modified by the user to control the way ``awk`` does certain things.

``CONVFMT``
===========
This variable contains a string that controls the conversion of numbers to strings. It works by being passed as the first argument to the ``sprintf()`` function. By default its value is ``"%.6g"``.

``FS``
======
This variable allows us to control the **input field separator**. By default its value is ``" "``, a string consisting of a **single space**. Tis value means that any sequence of **spaces**, **TABs**, and/or **newlines** is a single separator. It also causes spaces, TABs, and newlines at the beginning and end of a record to be ignored.

We can set the value of ``FS`` on the command line using the ``-F`` option::

    $ awk -F, 'program' input-files

``OFMT``
========
A string that controls conversion of numbers to strings for printing with the ``print`` action. It works by being passed as the first argument to the ``sprintf()`` function. By default its value is ``"%.6g"``.

``OFS``
=======
The output field separator (see section Output Separators). It is output between the fields printed by a print statement. Its default value is " ", a string consisting of a single space.

``ORS``
=======
The output record separator. It is output at the end of every print statement. Its default value is "\n", the newline character. (See section Output Separators.) 

``RS``
======
The **input record separator**. Its default value is a string containing a single newline character, which means that an input record consists of a single line of text. It can also be the **null string**, in which case records are separated by runs of blank lines. If

``SUBSEP``
==========
The **subscript separator**. It has the default value of ``"\034"`` and is used to separate the parts of the indices of a multidimensional array. Thus, the expression ``‘foo["A", "B"]’`` really accesses ``foo["A\034B"]``.

.. _`variables`: https://www.gnu.org/software/gawk/manual/gawk.html#Variables
.. _`Built-in variables`: https://www.gnu.org/software/gawk/manual/gawk.html#Built_002din-Variables
