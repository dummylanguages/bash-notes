*********************
Informative variables
*********************
Here we'll go over the **built-in variables** that that ``awk`` sets automatically on certain occasions in order to provide information to your program.

``ARGC`` and ``ARGV``
=====================
The command-line arguments that may be passed to ``awk`` when running a program are stored in an array called ``ARGV``. ``ARGC`` is the number of arguments present. For example::

    $ awk 'BEGIN {for (i = 0; i < ARGC; i++) print ARGV[i]}' foo bar
    awk
    foo
    bar

In this case ``foo`` and ``bar`` don't even exist, that's why we used a ``BEGIN`` block, to avoid the ``awk: can't open file foo`` error.

``ENVIRON``
===========
An **associative array** containing the values of the environment. The array indices are the environment variable names; the elements are the values of the particular environment variables. For example::

    $ awk 'BEGIN {print ENVIRON["HOME"]}'
    /Users/bob


``FILENAME``
============
The name of the current input file. When no data files are listed on the command line, awk reads from the standard input and FILENAME is set to ``"-"``. When input is made of several files, ``FILENAME`` changes each time a new file is read. Inside a ``BEGIN`` rule, the value of ``FILENAME`` is ``""``, because there are no input files being processed yet. Note, though, that using ``getline`` inside a BEGIN rule can give FILENAME a value.

``FNR``
=======
The **current record number** in the **current file**. ``awk`` increments ``FNR`` each time it reads a new record, and resets ``FNR`` to **zero** each time it starts a new input file.

``NF``
======
The **number of fields** in the current input record. ``NF`` is set each time a new record is read, when a new field is created, or when ``$0`` changes (see section Examining Fields).

Unlike most of the variables described in this subsection, assigning a value to ``NF`` has the potential to affect awk’s internal workings. In particular, assignments to ``NF`` can be used to create fields in or remove fields from the current record.

``NR``
======
The **number of input records** awk has processed since the beginning of the program’s execution. ``awk`` increments ``NR`` each time it reads a new record.

``RLENGTH``
===========
``RLENGTH`` is set by invoking the ``match()`` function. Its value is the **length of the matched string**, or ``-1`` if no match is found.

``RSTART``
==========
``RSTART`` is set by invoking the ``match()`` function. Its value is the index of the character where the matched substring starts, or **zero** if no match was found. 

.. _`variables`: https://www.gnu.org/software/gawk/manual/gawk.html#Variables
.. _`Built-in variables`: https://www.gnu.org/software/gawk/manual/gawk.html#Built_002din-Variables
