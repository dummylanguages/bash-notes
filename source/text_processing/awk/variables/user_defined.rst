**********************
User-defined variables
**********************
We can set variables using the ``-v`` option, for example::

  $ echo 'hello world' | awk -v myvar='Bob' '{printf "%s %s", $1, myvar}'
  hello Bob

The ``-v`` option has to precede each variable we want to use, for example::

  $ echo 'I like' | awk -v f1='apples' -v f2='bananas' '{printf "%s %s, and %s", $0, f1, f2}'
  I like apples, and bananas

If we hadn't used ``-v`` before ``f2='bananas'``, the interpreter would have thought that was the name of an input file or directory, and we would have gotten an error.

Array trick
===========
Even though passing an **array** is not possible, we can resort to a bit of ingenuity and accomplish something similar using the ``split()`` function. For example::

  echo 'I like' | awk -v fruits='apples,bananas,berries' '
  > {split(fruits, arr, ",")}
  > {printf "%s %s, %s, and %s.", $0, arr[1], arr[2], arr[3]}'
  I like apples, bananas, and berries.

.. note:: Remember that **arrays** in AWK are **associative**.

Passing Environment Variables
=============================
A way of passing into our AWK program any environment variable could be using shell expansion, for example::

  $ echo | awk -v home="$HOME" '
  > {printf "My home is at %s\n", home}'
  My home is at /home/javi

But keep reading for a better way.

The ``ENVIRON`` array
=====================
One of the builtin variables is ``ENVIRON``, which contains all the **environment variables** defined in our system indexed by their names. For example, the value of ``ENVIRON["HOME"]`` could be something like ``/home/bob``. Let's create a small program to pretty print all the environment variables defined in our system::

  $ echo | awk '{for (v in ENVIRON) printf "%s = %s\n", v, ENVIRON[v]}'
  OLDPWD = /home/javi
  LC_TIME = en_US.UTF-8
  LC_MONETARY = en_US.UTF-8
  [...]


.. _`variables`: https://www.gnu.org/software/gawk/manual/gawk.html#Variables
.. _`Built-in variables`: https://www.gnu.org/software/gawk/manual/gawk.html#Built_002din-Variables
