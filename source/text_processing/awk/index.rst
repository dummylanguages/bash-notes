###
AWK
###

.. toctree::
    :maxdepth: 3
    :hidden:

    using_awk/index
    basic_concepts/index
    patterns/index
    actions/index
    generating_output/index
    variables/index
    arrays/index
    functions/index
    one_liners/index


AWK is **programming language** created at Bell Labs in the 1970s, by **Alfred Aho**, **Peter Weinberger**, and **Brian Kernighan**. Its name is derived from the surnames of its authors:

* **A** for Aho
* **W** for Weinberger
* **K** for Kernighan

Even though AWK is a **domain-specific language** for text processing and data extraction and it was especially designed to write one-liner programs, the AWK language is `Turing-complete`_ and even the early Bell Labs users of AWK often wrote well-structured large AWK programs.

.. figure:: /_images/awk/awk_cover.jpg
   :scale: 75%
   :align: center

The basic function of AWK is to search **files** for **lines** (or other units of text) that contain certain **patterns** that match regular expressions. When a line matches one of the patterns, AWK performs specified **actions** on that line. The program continues to process input lines in this way until it reaches the end of the input files.


.. figure:: /_images/awk/awk_command.png
   :align: center

AWK, as many modern languages, is an **interpreted language**. This means that the ``awk`` interpreter reads our **AWK program** and then processes the input files according to the instructions in our program. For this reason we should make an important distinction between:

* The **AWK programming language**.
* The ``awk`` command, which is the name of the **interpreter** we use to run AWK programs.
* An **AWK program**, (it may be just a one-liner) which is a program written in the AWK programming language.

.. note:: When written in all lowercase letters, ``awk`` refers to the program that runs scripts written in the **AWK programming language.** 

There are several **implementations** of AWK, but the four main ones are:

* The original ``awk`` (1977).
* **New Awk** or ``nawk`` (1985), which is the version of Awk described in the well known book `The AWK Programming Language`_, written by the authors of the language themselves. This is the version pre-installed on many BSD-based systems, including macOS. The project is now maintained in Github under the name `The One True Awk`_.
* **GNU awk** also known as `Gawk`_.
* `mawk`_ (a variant of new awk)which is the default awk on Ubuntu and Debian Linux, and is still the fastest version of AWK.

Some interesting sites:

* `The GNU Awk User’s Guide`_, which is quite good.
* `TCP/IP Internetworking With gawk`_.
* `Bruce Barnett's awk tutorial`_.
* The `freeshell wiki`_
* `AWK in Wikipedia`_.
* `The state of the AWK`_, an article in https://lwn.net.

.. _`Turing-complete`: https://en.wikipedia.org/wiki/Turing_completeness
.. _`The One True Awk`: https://github.com/onetrueawk/awk
.. _`The AWK Programming Language`: https://en.wikipedia.org/wiki/The_AWK_Programming_Language
.. _`Gawk`: https://www.gnu.org/software/gawk/
.. _`mawk`: https://invisible-island.net/mawk/
.. _`The GNU Awk User’s Guide`: https://www.gnu.org/software/gawk/manual/
.. _`TCP/IP Internetworking With gawk`: https://www.gnu.org/software/gawk/manual/
.. _`Bruce Barnett's awk tutorial`: https://www.grymoire.com/Unix/Awk.html
.. _`freeshell wiki`: http://awk.freeshell.org/HomePage
.. _`AWK in Wikipedia`: https://en.wikipedia.org/wiki/AWK
.. _`The state of the AWK`: https://lwn.net/Articles/820829/
