*************
Input records
*************
``awk`` reads input in units called `input records`_, which by default are **lines**. Lines are strings of text separated by a **newline character**, which means that by default, the **newline character** is the **record separator**. 

Built-in variables
==================
There are several **built-in variables** related to **input records**:

* ``RS`` stores the **input record separator**, which by default is the **newline character**, that's why, by default, an input record consists of a single line of text.
* ``FNR`` stores the **current record number** in the **current file**. ``awk`` resets this variable to **zero** each time it starts a new input file. 
* ``NR`` keeps the **current record number** counting from the beginning of the program’s execution, without taking into consideration the input file.

We go over how to :ref:`modifying the input record separator <modifying_separators>` in the section about the :ref:`BEGIN and END blocks` because that's usually the place to do it. So here we'll take care of the ``NR`` and ``FNR`` variables.

Current Record Number
---------------------
Let's practice with ``NR``::

    $ awk '{ print NR }' sample.txt 
    1
    2

The command above used ``NR`` inside the **action**. The output show just the number of the records. We can also use ``NR`` to create expressions that can be used in the **pattern**::

    $ awk 'NR <= 3' mail-list 
    Amelia       555-5553     amelia.zodiacusque@gmail.com    F
    Anthony      555-3412     anthony.asserturo@hotmail.com   A
    Becky        555-7685     becky.algebrarum@gmail.com      A

The command above filters out records that are greater than three.

Current Record Number in a File
-------------------------------
Let's play with ``FNR`` also::

    $ awk '
    > BEGIN { print "File name\tFNR\tNR" 
    >         print "---------\t---\t--" } 
    > { print FILENAME "\t" FNR "\t" NR }
    > ' sample.txt mail-list
    File name       FNR     NR
    ---------       ---     --
    sample.txt      1       1
    sample.txt      2       2
    mail-list       1       3
    mail-list       2       4
    mail-list       3       5
    mail-list       4       6
    [...]
    
We're feeding two input files to our program. We can see how the value of ``FNR`` (second column) gets reset to zero every time a new file is started. 

The ``NR == FNR`` idiom
-----------------------
The ``NR == FNR`` expression is often used as a **pattern** to match records of the **first file** only. For example::

    $ awk 'NR == FNR { print FILENAME, NR }' sample.txt mail-list 
    sample.txt 1
    sample.txt 2

The command above only runs the **action** for the records of the first file.  That's because ``NR == FNR`` evaluates to **true** as long as the values of ``NR`` and ``FNR`` are equal, which happens as long as we're reading records from the first file. When we move to another file, ``NR`` is reset to zero, but ``FNR`` continues where it left. 

What if we wanted to run it only for the records of the second file and further files? Simple::

    $ awk '!(NR == FNR) { print FILENAME }' sample.txt mail-list 
    mail-list
    mail-list
    mail-list
    [...]

We just have to use the ``!`` negation operator.

.. _`input records`: https://www.gnu.org/software/gawk/manual/gawk.html#Records
.. _`CSV`: https://en.wikipedia.org/wiki/Comma-separated_values
