************
Input fields
************
When ``awk`` reads an `input record`_, the record is automatically parsed or separated into chunks called `input fields`_. By default, fields are separated by **whitespace**, like words in a line. In AWK, whitespace means:

* Any string of one or more **spaces**.
* One or more **TABs**.
* One or more **newlines**.

The purpose of fields is to make it more convenient for you to refer to these pieces of the record. We use a dollar sign (``$``) to refer to a field in an AWK program, followed by the number of the field you want:

* ``$0`` refers to the **whole input record**.
* ``$1`` refers to the first field, ``$2`` refers to the second field, etc.
* ``$NF`` refers to the **last field**.

For example, imagine we want to extract only the **email addresses** from the :download:`mail-list </_code/awk/mail-list>` file::

    $ awk '{ print $3 }' mail-list 
    amelia.zodiacusque@gmail.com
    anthony.asserturo@hotmail.com
    becky.algebrarum@gmail.com
    [...]

We used ``$3`` to refer to the **third field** of each record.

Changing the contents of an input field
=======================================
Once ``awk`` has read a field, it's possible to change its value. This changes the value of the field **in memory**, the actual **input file** is left untouched. For example, let's suppose we want to add all the numeric values (fields 2 to 5) of the :download:`inventory-shipped </_code/awk/inventory-shipped>` file::

    $ awk '{ $2 = $2 + $3 + $4 + $5; print $1, $2 }' inventory-shipped
    Jan 168
    Feb 297
    Mar 301
    [...]

We've assigned the sum of fields 2 to 5 to field 2, then print fields 1 and 2.

Built-in variables
==================
There are several **built-in variables** related to **input fields**:

* ``FS`` stores the field separator, by default **whitespace**. 
* ``NF`` stores the number of input fields in the current input record.

``FS``: Field Separator
-----------------------
Regarding the ``FS`` check the section about the :ref:`BEGIN and END blocks` where we change its value. Apart from that, we can also use the ``-F`` **command-line option** to do so::

    $ awk -F, 'program' input-files

The command above sets ``FS`` to the ``,`` character. Notice that the option uses an **uppercase F** instead of a **lowercase f**. The latter option (``-f``) is for specifying a file containing an AWK program.

.. note:: We found the explanation of the use of the ``-F`` option in the `The GNU Awk User’s Guide`_ it's a bit confusing, specially the part about setting the ``/`` character as separator.

``NF``: Number of input fields
------------------------------
We already mentioned that ``NF`` stores the number of input fields in the current input record. It's possible to assign values to this variable in order to **create** fields in or **remove** fields from the current record. There are some caveats to be aware of, for example, if we wanted to remove all fields after the third one, we could try something like this::

    $ echo 'one,two,three,four,five' | awk -v FS=, 'BEGIN{ NF = 3 }{ print }'
    one,two,three,four,five

This does not work because ``NF`` is recalculated every time ``awk`` reads a new record, and we were setting the ``NF`` value in a ``BEGIN`` block, which runs **before** any record is read.
We have to set the variable every time a record is read, like this::

    $ echo 'one,two,three,four,five' | awk -v FS=, '{NF=3; $1=$1; print}'
    one two three

.. warning:: The solution lies in the idiom ``$1==$1``, which is used to force the rebuilding of the full record (``$0``).

.. _`input record`: https://www.gnu.org/software/gawk/manual/gawk.html#Records
.. _`input fields`: https://www.gnu.org/software/gawk/manual/gawk.html#Fields
.. _`The GNU Awk User’s Guide`: https://www.gnu.org/software/gawk/manual/gawk.html#Command-Line-Field-Separator
