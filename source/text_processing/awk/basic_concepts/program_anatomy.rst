*************************
Anatomy of an AWK program
*************************
In our first example you probably noticed the cryptic expression sandwiched between the call to the ``awk`` interpreter and the name of the **input file**::

  $ awk '// { print }' sample.txt
  The quick brown fox
  jumps over the lazy dog.

The expression ``'// { print }'`` is the AWK program itself. Let's take a closer look at its different parts.

Single quotes
=============
The first thing you may have noticed are the **single quotes** wrapping the whole thing. They're not part of the AWK program; they're there so that the shell won’t interpret the symbols used in the AWK program as special shell characters.

.. warning:: AWK **one-liners** must always be surrounded by **single quotes** to protect them from the shell. Instructions almost always contain **curly braces** and/or **dollar signs**, which are interpreted as **special characters** by the shell.

The **single-quotes** are only necessary when we're using:

* AWK programs written on the command-line (**One-liners**).
* AWK programs embedded in shell-scripts (Check our :ref:`section about shell wrappers <awk embedded>`).

To prove you that, let's copy our program to a **separate file** which we'll name :download:`look_ma_no_quotes </_code/awk/look_ma_no_quotes.awk>`, and strip the single quotes away so it looks like this:

.. literalinclude:: /_code/awk/look_ma_no_quotes.awk

Now you can run it using the ``-f`` option::

    $ awk -f look_ma_no_quotes sample.txt 
    The quick brown fox
    jumps over the lazy dog.

The **single-quotes** are **not** necessary when we're using:

* AWK programs in separate files, like the one above.
* Stand-alone AWK programs (we'll explain these a bit later).

.. _rules, patterns and actions:

Rules
=====
Our first program had only one **rule**::

    // { print }

This rule contains:

* **Pattern**: In this case the pattern is a `regular expression`_. Regular expressions must be wrapped in slashes ``//``; in this case there's nothing in between the slashes, which is known as an **empty regular expression**. An empty regular expression will match all the lines, so the pattern will evaluate to **true** for each and every line.
* The **action**, in this case, is the ``print`` action.

So if the pattern evaluates to **true** or **non-zero** for a given line, the action will run. The **action** is enclosed in braces to separate it from the **pattern**.

.. note:: For readability, patterns and actions are usually separated by a **space**.

An AWK program consists of a series of **rules**. A rule must specify:

* A **pattern** which is an expression whose value determines if the following statement will run for a given line.
* An **action** which is a statement to perform on the line matched by the pattern.

The cycle pattern-action runs as many times as lines (records) has the input. The general form of an AWK program is something like this::

    pattern {action}
    pattern {action}
    ...

.. note:: A middle to big size AWK program may also contain `user defined functions`_, which is an advanced feature we'll see later.


.. _`regular expression`: https://en.wikipedia.org/wiki/Regular_expression
.. _`user defined functions`: https://www.gnu.org/software/gawk/manual/gawk.html#User_002ddefined
