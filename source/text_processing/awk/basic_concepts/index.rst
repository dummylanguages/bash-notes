##############
Basic concepts
##############

.. toctree::
    :maxdepth: 3
    :hidden:

    program_anatomy
    omitting_part_of_rule
    input_records
    input_fields

In the :ref:`basic use section <basic use>`, we mentioned that AWK programs are made up of **rules**, and that each rule is made up of a **pattern** and an **action**.::

    /pattern/ {action}

In this section we'll go a bit deeper into these concepts, and we'll also cover two other basic elements used in an AWK program:

* Input records, aka **records**.
* Input fields, aka **fields**.

.. note:: **Input records** are commonly referred to as simply **records**, and **input fields** as **fields**.

During program execution, ``awk`` divides the **input** of our program into **records** and **fields**:

* By default, a **record** is a line, because by default AWK uses the **newline** character as the **record separator**; later we'll see how to change that.
* When ``awk`` reads a record, it splits the record into pieces separated by whitespace (one or more consecutive spaces or tabs count as a single delimiter). Such pieces are called **fields**.

.. note:: There are also **output records** and **output fields**, but these ones are covered in the :ref:`generating output section <printing output>`.
