***********************
Omitting part of a rule
***********************
Quite often, you'll come accross some **AWK rules** that seem to be missing a part, either the pattern of the action. That's because, in some cases, it is possible to omit either the **pattern** or the **action**.

.. warning:: You can **omit** either the **pattern** or the **action** but not both!

Omitting the pattern: the empty pattern
=======================================
In our first program, we didn't write anything in between the slashes, which is known as an **empty regular expression** (``//``)::

    $ awk '// { print }' sample.txt
    The quick brown fox
    jumps over the lazy dog.

An empty regular expression will match all the lines in the **input file** (``sample.txt``).

.. warning:: An **empty regular expression** is not an **empty pattern**.

As we'll see later, AWK also allows us to use various types of **expressions** as patterns. We can use **numeric expressions**::

    $ awk '1 { print }' sample.txt
    The quick brown fox
    jumps over the lazy dog.

Numeric expressions that evaluate to **non-zero** result in a match, that's why the literal value ``1`` used above, will match all lines in the file.

.. note:: You may be wondering what's the point in ommitting the pattern. Well, in practice, you'll see how there are a lot of ocassions when we want to apply some action to **all** the input lines.

Since is quite often that we want to match **all** the lines in a file, AWK allows us to omit the **pattern** altogether, and write only the **action**::

    $ awk '{ print }' sample.txt
    The quick brown fox
    jumps over the lazy dog.

Omitting the pattern is known as the **empty pattern**, and it has the effect of applying the action to **all** the lines of the file.

Omitting the action: the ``print`` statement
============================================
We can omit the **action** instead, which has the effect of running the ``print`` action on all the lines that match the pattern::

    $ awk '/dog/' sample.txt 
    jumps over the lazy dog.

The ``/dog/`` pattern is matching the second line of the file, and **no action** is equivalent to ``print`` action, so the second line is printed. 

.. note:: Omitting the action is equivalent to the ``print`` action.

And if you want to match **all** the lines you could write::

    $ awk '1' sample.txt 
    The quick brown fox
    jumps over the lazy dog.

The program above uses the numeric expression ``1`` as the **pattern** and omits the **action**, hence all the lines are printed. That's probably the shortest AWK program you could write ;-)

The empty action
================
We just saw how **omitting the pattern** is known as the **empty pattern**. A bit counterintuitively, **omitting the action** is not equivalent to an **empty action**.

If you run::

    $ awk '//' sample.txt
    The quick brown fox
    jumps over the lazy dog.

You're omitting the action which results in printing the input. But if we use an empty set of curly braces (``{}``), we're not omitting the action, it's there, just **empty**::

    $ awk '{}' sample.txt 

The **empty action** does nothing, that's why we get **no output** at all. The program does nothing!
