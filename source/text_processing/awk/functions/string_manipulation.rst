*****************************
String manipulation functions
*****************************
The functions in this section allow us to look at or change the text of one or more strings.

.. note:: In AWK a character is a character regardless of the amount of bytes it takes. 

All the string processing is done in terms of **characters**, not bytes. This distinction is particularly important to understand for locales where one character may be represented by multiple bytes. Thus, for example, ``length()`` returns the number of characters in a string, and not the number of bytes used to represent those characters.

.. warning:: For functions that work with character indices, the **first character** of a string is at **index one**. This is different from C and the languages descended from it, where the first character is at position zero.

Beginning of a substring: ``index()`` and ``match()``
=====================================================
To find the beginning of a string, meaning the position of the first character of the substring in the string, we can use the ``index()`` function. For example::

    $ awk 'BEGIN { print index("peanut", "an") }'
    3

As you can see, ``index()`` returns the index of the letter ``a`` (the first character of the substring) in the string ``peanut``. If the substring is **not found**, ``index()`` returns **zero**. 

.. note:: Remember, the **first character** of a string is at **index one**.

The ``match()`` function works the same, but allows not only a **string**, but also a **regular expression** to be supplied as second argument. For example::

    $ awk 'BEGIN { print match("know-it-all", /[[:punct:]]/) }'
    5

In this case we're using the **regex** ``[[:punct:]]`` to look for the leftmost substring that starts with a punctuation character, in this case a dash.

Numbers of characters of a string: ``length()``
===============================================
The ``length()`` function returns the number of characters of a string, for example::

    $ awk 'BEGIN { print length("peanut") }'
    6

If no argument is supplied, ``length()`` returns the length of ``$0``. For example::

    $ echo "one two three" | awk '{ print length() }'
    13

.. warning:: In older versions of ``awk``, the ``length()`` function could be called **without any parentheses**. Doing so is considered **poor practice**, although the 2008 POSIX standard explicitly allows it, to support historical practice. For programs to be maximally portable, always supply the parentheses.

If string is a **number**, the length of the digit string representing that number is returned. For example::

    $ awk 'BEGIN { print length(10 * 10) }'
    3

In this example, **10 * 10 = 100**, and **100** is then converted to the string ``"100"``, which has three characters. 

Replacing text: ``sub()`` and ``gsub()``
========================================
Both of these functions allow us to replaces parts of strings. They work the same, the only difference is that ``gsub()`` makes substitutions **globally** whereas ``sub()`` stops when a substitution is made.

.. note:: The ``g`` in ``gsub()`` stands for **global**, which means replace everywhere.

For example::

    $ echo "water, water, everywhere" | awk '{ sub(/water/, "gold"); print }'
    gold, water, everywhere

Using ``sub()`` we replace the first occurence of the word ``water``. Let's use ``gsub()`` instead::

    $ echo "water, water, everywhere" | awk '{ gsub(/water/, "gold"); print }'
    gold, gold, everywhere

General form
------------
The general form of this function is::

    sub(regexp, replacement [, target])

Where:

* ``regexp`` may be a **regular expression**, but also a simple **string**.
* ``replacement`` is the **string** that will be use as a replacement for the match.
*  If no ``target`` is supplied (as in our example), use ``$0`` (the whole record). When it's not omitted, it must be a **variable**, **field**, or **array element**. 

Both of these functions return the number of substitutions made; in the case of ``sub()`` that is ``1`` or ``0``.

The ``&`` special character
---------------------------
When the special character ``&`` appears in ``replacement``, it stands for the precise substring that was matched by ``regexp``. For example::

    $ awk '{ sub(/lazy/, "&, fat"); print }' sample.txt 
    The quick brown kangaroo
    jumps over the lazy, fat dog.

The ``lazy`` dog has become the ``lazy, fat`` dog.

Specifying a ``target``
-----------------------
When a ``target`` is not specified, the substitution takes over the whole record, which in some cases is not what we want. For example, imagine we have the :download:`following file </_code/awk/selected.txt>`:

.. literalinclude:: /_code/awk/selected.txt

And we want to replace the ``Y`` by the full word ``Yes``, and we run::

    $ awk '{ gsub(/Y/, "Yes"); print }' selected.txt 
    Name    Selected
    ----    --------
    Yesassin      Yes
    Norm        N
    Yesulia       Yes
    Nick        N

The result is a mess. What we clearly want to do is to target the **second field**::

    $ awk 'BEGIN{ OFS = "\t\t" } 
    > { second = $2; gsub("Y", "&es", second); $2 = second; print }
    > ' selected.txt 
    Name            Selected
    ----            --------
    Yassin          Yes
    Norm            N
    Yulia           Yes
    Nick            N

We had to store the **second field** in a variable named ``second``, and then reassign ``$2`` after the substitution was done. Note also how we had to use a ``BEGIN`` block, since our version of ``awk`` messed the indentation. Another possibility would have been to use ``printf $1 "\t\t" second)``.

Return formatted string: ``sprintf()``
======================================
This function returns (without printing) the string that the ``printf`` statement would have printed out with the same argument. The returned value could be assigned to a variable, field, or array argument. For example::

    $ awk 'BEGIN { pi = sprintf("Pi = %.2f (approx.)\n", atan2(0, -1)); printf "%s", pi }'
    Pi = 3.14 (approx.)

Changing the case: ``toupper()`` and ``tolower()``
==================================================
Both of these function return a copy of the argument string, in uppercase and lowercase, respectively. For example:

    $ awk 'BEGIN { lower = tolower("BOB"); printf "%s\n", lower }'

We can also pass a variable, field, or array element as arguments::

    $ awk 'BEGIN { name = "bob"; upper = toupper(name); printf "%s\n", upper }'
    BOB

.. _`input fields`: https://www.gnu.org/software/gawk/manual/gawk.html#Fields

