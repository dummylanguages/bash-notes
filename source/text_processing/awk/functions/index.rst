################
Functions in AWK
################

.. toctree::
    :maxdepth: 3
    :hidden:

    string_manipulation
    user_defined_functions

AWK includes a set of `built-in functions`_, which fall into three categories: 

* `String functions`_
* `Numeric functions`_
* `I/O functions`_

Apart from the builtins, AWK allows `user-defined functions`_. Let's take the chance to mention here a couple of basic points of interest:

1. To call one of the **built-in functions**, write the name of the function followed by arguments in parentheses. For example::

    sqrt(4)

 Whitespace is ignored between the built-in function name and the opening parenthesis, so we may also write::

    sqrt (4) # Don't do that

 But it is **good practice** to *avoid using whitespace* there. Why, you ask? Because **user-defined functions** do not allow whitespace in this way, and it is easier to avoid mistakes by following a simple convention that always works—no whitespace after a function name.  

 .. note:: Don't use **whitespace** between the function's name and the opening parentheses!

2. Each built-in function accepts a certain **number of arguments**. In some cases, arguments can be omitted. The defaults for omitted arguments vary from function to function and are described under the individual functions. In some ``awk`` implementations, extra arguments given to built-in functions are ignored, but in others (``gawk``), it is a fatal error to give extra arguments to a built-in function.

 .. note:: Respect the function signature in your call.

3. When a function is called, expressions that create the function’s actual parameters are evaluated completely before the call is performed. For example, in the following code fragment::

    i = 3
    j = sqrt(i++)

 The variable ``i`` is incremented to the value ``4`` before ``sqrt()`` is called with a value of four for its actual parameter.

4. If a function takes several arguments, the order of evaluation of the expressions used for calculating them is undefined. Thus, avoid writing programs that assume that parameters are evaluated from left to right or from right to left. For example::

    i = 5
    j = atan2(++i, i *= 2)

 If the order of evaluation is **left to right**, then ``i`` first becomes ``6``, and then ``12``, and ``atan2()`` is called with the two arguments ``6`` and ``12``. But if the order of evaluation is **right to left**, ``i`` first becomes ``10``, then ``11``, and ``atan2()`` is called with the two arguments ``11`` and ``10``. 

.. _`built-in functions`: https://www.gnu.org/software/gawk/manual/html_node/Built_002din.html
.. _`user-defined functions`: https://www.gnu.org/software/gawk/manual/html_node/User_002ddefined.html
.. _`Numeric functions`: https://www.gnu.org/software/gawk/manual/html_node/Numeric-Functions.html
.. _`String functions`: https://www.gnu.org/software/gawk/manual/html_node/String-Functions.html
.. _`I/O functions`: https://www.gnu.org/software/gawk/manual/html_node/I_002fO-Functions.html
