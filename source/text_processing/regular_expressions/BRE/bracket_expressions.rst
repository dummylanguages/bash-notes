.. _BRE bracket expressions:

*******************
Bracket expressions
*******************
A **bracket expression** is just an expression enclosed in square brackets, ``[]``. If we want to match a specific set of characters, we can write a expression wrapped in **square brackets** to identify the exact set of characters we are searching for. For example, to match any vowel::

    $ echo "The quick fox jumped over the lazy dog" | sed 's/[aeiou]/*/'
    Th* quick fox jumped over the lazy dog

As you can see, only the first vowel was matched, which means that **bracket expressions** are **lazy** (not greedy). If you want the above command to match in a greedy way use the ``g`` flag::

    $ echo "The quick fox jumped over the lazy dog" | sed 's/[aeiou]/*/g'
    Th* q**ck f*x j*mp*d *v*r th* l*zy d*g

Specifying a range
==================
In some cases it's more convenient to specify a **range of characters** writing the initial and the final character in the set, separated by an hyphen (``-``). For example, imagine we want to match only digits::

    $ echo -e "The answer\nis 42" | grep '[0-9]'
    is 42

Specifying the set of characters as the range ``[0-9]`` is more convenient than the verbose ``[0123456789]``. This is specially useful if we wanted to match only lowercase letters; instead of writing the whole alphabet we would have to write ``[a-z]``. For example::

    $ echo -e "The answer\nis 42" | sed 's/[a-z]/*/g'
    T** ******
    ** 42

Combining character sets
------------------------
Character sets can be combined by placing them next to each other. If you wanted to search for a word that

1. Started with a capital letter "T".
2. Was the first word on a line
3. The second letter was a **lowercase** letter
4. Was exactly three letters long,
5. And the third letter was a vowel 

We would use the regular expression ``'^T[a-z][aeiou] '``. For example::

    $ echo -e "The answer\nis 42" | grep '^T[a-z][aeiou]'
    The answer

Exceptions in a character set
-----------------------------
To search for all characters except those in square brackets, you have to use a ``^`` as the first character after the first bracket (``[``). For example, to match all characters except vowels::

    $ echo -e "The answer\nis 42" | sed 's/[^aeiou]/*/g'
    **e*a***e*
    i****

As you can see, even the **space character** was matched; all characters except vowels.

The position matters
--------------------
The characters ``-`` and ``]`` do not have a special meaning if they directly follow ``[``. For example:

+====================+======================================================+
| Regular Expression | Matches                                              |
+====================+======================================================+
| ``[]``             | The literal characters ``[]``                        |
+--------------------+------------------------------------------------------+
| ``[0]``            | The character ``0``                                  |
+--------------------+------------------------------------------------------+
| ``[0-9]``          | Any number                                           |
+--------------------+------------------------------------------------------+
| ``[^0-9]``         | Any character other than a number                    |
+--------------------+------------------------------------------------------+
| ``[-0-9]``         | Any number or a ``-``                                |
+--------------------+------------------------------------------------------+
| ``[0-9-]``         | Any number or a ``-``                                |
+--------------------+------------------------------------------------------+
| ``[^-0-9]``        | Any character except a number or a ``-``             |
+--------------------+------------------------------------------------------+
| ``[]0-9]``         | Any number or a ``]``                                |
+--------------------+------------------------------------------------------+
| ``[0-9]]``         | Any number followed by a ``]``                       |
+--------------------+------------------------------------------------------+
| ``[0-9-z]``        | Any number,	or any character between ``9`` and ``z``|
+--------------------+------------------------------------------------------+
| ``[0-9\-a\]]``     | Any number, or a literal ``-``, an ``a``, or a ``]`` |
+--------------------+------------------------------------------------------+

Character classes
=================
POSIX added newer and **more portable** ways to search for character sets. Instead of using ``[a-zA-Z]`` we can use ``[[:alpha:]]``; basically we're replacing ``a-zA-Z`` with ``[:alpha:]``. For example::

    $ echo -e "The answer\nis 42" | sed 's/[[:alpha:]]/*/g'
    *** ******
    ** 42

Above we're replace all **alphabetic** characters, with stars.

.. note:: Character classes are **more portable** because they work according to the settings of **system locale** (``LC_CTYPE``), allowing us to match **international character sets**.

We can negate a character class:: To replace the **digits** we could use::

    $ echo -e "The answer\nis 42" | sed 's/[^[:alpha:]]/*/g'
    The*answer
    is***

The regex above, ``[^[:alpha:]]``, is matching everything except alphabetic characters, even the space. If you wanted to avoid matching the space, just add it after the ``[:alpha:]`` character class::

    $ echo -e "The answer\nis 42" | sed 's/[^[:alpha:] ]/*/g'
    The answer
    is **

Now we're matching only the **digits**, which could be more properly done using the ``[:digit:]`` character class::

    $ echo -e "The answer\nis 42" | sed 's/[[:digit:]]/*/g'
    The answer
    is **

.. warning:: Character classes are **bracket expressions**, so don't forget the external brackets that surround the character class, for example, ``[[:alnum:]]``.

The following table contains the **character class expressions** that are supported in all locales:

+================+==========================================================+
| Expression     | Matches                                                  |
+================+==========================================================+
| ``[:alnum:]``  | Alphabetic and numeric characters (letters and numbers). |
+----------------+----------------------------------------------------------+
| ``[:alpha:]``  | Alphabetic characters, i.e., just letters.               |
+----------------+----------------------------------------------------------+
| ``[:blank:]``  | White space: spaces and tabs.                            |
+----------------+----------------------------------------------------------+
| ``[:cntrl:]``  | Control characters: spaces and tabs.                     |
+----------------+----------------------------------------------------------+
| ``[:digit:]``  | Numeric characters, ``0`` to ``9``                       |
+----------------+----------------------------------------------------------+
| ``[:graph:]``  | Visible characters (Excluding any whitespace character). |
+----------------+----------------------------------------------------------+
| ``[:lower:]``  | Lowercase letters.                                       |
+----------------+----------------------------------------------------------+
| ``[:print:]``  | Visible characters and space.                            |
+----------------+----------------------------------------------------------+
| ``[:punct:]``  | Punctuation characters:.                                 |
+----------------+----------------------------------------------------------+
| ``[:space:]``  | Whitespace characters:.                                  |
+----------------+----------------------------------------------------------+
| ``[:upper:]``  | Uppercase letters.                                       |
+----------------+----------------------------------------------------------+
| ``[:xdigit:]`` | Hexadecimal numbers.                                     |
+----------------+----------------------------------------------------------+

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Regular_expression
