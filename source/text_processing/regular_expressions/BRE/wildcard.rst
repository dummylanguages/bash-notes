********
Wildcard
********
The **single dot** matches a single instance of **any character**, for example::

    $ echo -e "One\nTwo\nThree" | sed 's/T.o/*/'
    One
    *
    Three

In the command above we're matching a ``T`` followed by a single instance of any character and then an ``o``. That expression would not match the word ``Tour`` for example, since there's nothing in between the ``T`` and the ``o``.

.. note:: ``.`` is a **metacharacter** that matches **one instance** of any character except a newline. 

Escaping the Wildcard
---------------------
What if you need to match a **literal dot** (``.`)? Then you have to precede it with a backslash (``\``) to strip it from its special meaning. For example::

    $ echo "foo." | awk '{ sub(/\./, "*"); print }'
    foo*

Note how we are passing the regex ``/\./`` as the first argument to the ``sub()`` AWK function. To do the same in ``sed``::

    $ echo "foo." | sed 's/\./*/'
    foo*

Note that the enclosing ``/`` characters are not part of the regex, they are just required in ``awk`` and ``sed`` to wrap the regex, which is simply ``\.``. To make it clearer let's see an example with ``grep``::

    $ echo -e "Text files not always\nhave the .txt\nextension." | grep '\.'
    have the .txt
    extension.

We're matching the lines that has a literal dot, no matter where it is located.

Bracket expressions
===================
If you want to match a set of characters, you can use the **square brackets** to identify the exact characters you are searching for. For example, to match any vowel::

    $ echo "The quick fox jumped over the lazy dog" | sed s/[aeiou]/*/
    Th* quick fox jumped over the lazy dog

As you can see, only the first vowel was matched, which means that **bracket expressions** are **lazy** (not greedy). If you want the above command to match in a greedy way use the ``g`` flag::

    $ echo "The quick fox jumped over the lazy dog" | sed s/[aeiou]/*/g
    Th* q**ck f*x j*mp*d *v*r th* l*zy d*g

Specifying a range
------------------
In some cases it's more convenient to specify a **range of characters** writing the initial and the final character in the set, separated by an hyphen (``-``). For example, imagine we want to match only digits::

    $ echo -e "The answer\nis 42" | grep '[0-9]'
    is 42

Specifying the set of characters as the range ``[0-9]`` is more convenient than the verbose ``[0123456789]``. This is specially useful if we wanted to match only lowercase letters; instead of writing the whole alphabet we would have to write ``[a-z]``. For example::

    $ echo -e "The answer\nis 42" | sed 's/[a-z]/*/g'
    T** ******
    ** 42

Combining character sets
------------------------
Character sets can be combined by placing them next to each other. If you wanted to search for a word that

1. Started with a capital letter "T".
2. Was the first word on a line
3. The second letter was a **lowercase** letter
4. Was exactly three letters long,
5. And the third letter was a vowel 

We would use::

    $ echo -e "The answer\nis 42" | grep '^T[a-z][aeiou]'
    The answer

As you can see, we're matching the first line because the word ``The``, matches our regular expression.

Exceptions in a character set
-----------------------------
To search for all characters except those in square brackets, you have to use a ``^`` as the first character after the first bracket (``[``). For example, to match all characters except vowels::

    $ echo -e "The answer\nis 42" | sed 's/[^aeiou]/*/g'
    **e*a***e*
    i****

As you can see, even the **space character** was matched; all characters except vowels.

The position matters
--------------------
The characters ``-`` and ``]`` do not have a special meaning if they directly follow ``[``. For example:

+====================+======================================================+
| Regular Expression | Matches                                              |
+====================+======================================================+
| ``[]``             | The literal characters ``[]``                        |
+--------------------+------------------------------------------------------+
| ``[0]``            | The character ``0``                                  |
+--------------------+------------------------------------------------------+
| ``[0-9]``          | Any number                                           |
+--------------------+------------------------------------------------------+
| ``[^0-9]``         | Any character other than a number                    |
+--------------------+------------------------------------------------------+
| ``[-0-9]``         | Any number or a ``-``                                |
+--------------------+------------------------------------------------------+
| ``[0-9-]``         | Any number or a ``-``                                |
+--------------------+------------------------------------------------------+
| ``[^-0-9]``        | Any character except a number or a ``-``             |
+--------------------+------------------------------------------------------+
| ``[]0-9]``         | Any number or a ``]``                                |
+--------------------+------------------------------------------------------+
| ``[0-9]]``         | Any number followed by a ``]``                       |
+--------------------+------------------------------------------------------+
| ``[0-9-z]``        | Any number,	or any character between ``9`` and ``z``|
+--------------------+------------------------------------------------------+
| ``[0-9\-a\]]``     | Any number, or a literal ``-``, an ``a``, or a ``]`` |
+--------------------+------------------------------------------------------+

Quantifiers
===========
A quantifier specifies how many times a preceding element is allowed to occur. Even though they seem simple at first glance, the behavior of regex quantifiers is a common source of surprises for beginners.

.. warning:: By default, quantifiers are **greedy**, meaning a quantifier tells the engine to match as many instances of its quantified token or subpattern as possible.


Common quantifiers
------------------
The most **common quantifiers** are:

* The **asterisk** (``*``), indicates **zero or more** occurrences of the preceding element.
* The **question mark** (``?``), indicates **zero or one** occurrences of the preceding element.
* The **plus sign** (``+``), indicates **one or more** occurrences of the preceding element.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Regular_expression
