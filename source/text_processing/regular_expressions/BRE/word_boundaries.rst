***************
Word boundaries
***************
The POSIX standard defines ``[[:<:]]`` as a **start-of-word boundary**, and ``[[:>:]]`` as an **end-of-word boundary**. 

.. warning:: Though the syntax is borrowed from **POSIX bracket expressions**, these tokens cannot be used inside character classes.

For example, let's say we want to match the word ``cat``, but only as a whole word, not when is part of another word. If we simply do::

    $ echo -e "The bobcat and the cat looked\nat the caterpillar." |
    > sed 's/cat/***/g'
    The bob*** and the *** looked
    at the ***erpillar.

As you can see we're matching strings we didn't intend to. Let's try to fix it using a **start-of-word** boundary::

    $ echo -e "The bobcat and the cat looked\nat the caterpillar." |
    > sed 's/[[:<:]]cat/***/g'
    The bobcat and the *** looked
    at the ***erpillar.

Great, we skipped ``bobcat``, but we're still matching the string ``cat`` at the beginning of the word ``caterpillar``. Let's also add an **end-of-word** boundary to our regex::

    $ echo -e "The bobcat and the cat looked\nat the caterpillar." | 
    > sed 's/[[:<:]]cat[[:>:]]/***/g'
    The bobcat and the *** looked
    at the caterpillar.

Awesome, now we're only matching ``cat`` when it's a separate word.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Regular_expression
