****************
Capturing groups
****************
A **group** is a section of a regular expression enclosed in **parentheses**. Groups are also commonly known as **subexpressions**.

.. warning:: The **BRE syntax** requires ``\(`` and ``\)``.

Using the **BRE syntax**, we can do two things with subexpressions:

* Apply the ``*`` operator to a sequence of characters; for example, the subexpression ``\(abcd\)*`` will search for zero or more whole sequences of ``abcd``, while ``abcd*`` would search for ``abc`` followed by zero or more occurrences of ``d``.
* Use **back references**, which we'll explain next.

Back references
===============
Once a subexpression gets a match, the resulting string will be stored by the engine in one of the **nine** available positions. We can then recall the substring using ``\n`` where ``n`` is a number from ``1`` to ``9``. For example, imagine we want to match the word ``choo-choo``::

    $ echo "Here comes the choo-choo!" | sed 's/\(choo\)-\1/***/'
    Here comes the ***!

What if we want to match hyphenated words, which have four characters to each side of the hyphen::

    $ echo "Here comes the choo-choo!" | sed 's/\(.\{4\}\)-\1/***/'
    Here comes the ***!
    
    $ echo "I like high-tech cars!" |  sed 's/\(.\{4\}\)-\1/***/'
    
.. _`Wikipedia`: https://en.wikipedia.org/wiki/Regular_expression
