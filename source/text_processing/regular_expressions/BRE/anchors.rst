*********************
The anchor characters
*********************
Most UNIX text utilities are line oriented and they search for text in between newline characters. **Newline characters** act like separators, and regular expressions examine the text between these separators. If you want to search for a pattern that is at the beginning or at the end of a line, you use any of the anchor special characters:

* The **caret** (``^``), for matching a string at the **beginning of a line**.
* The **dollar sign** (``$``), for matching a string at the **end of a line**.

.. warning:: Later we'll explain another use of the ``^`` character also as a regex **metacharacter**, but with a differente meaning. (See the :ref:`bracket expressions section <BRE bracket expressions>`.)

Matching characters at the beginning of a line
==============================================
For example, let's match a ``T`` at the beginning of a line::

    $ echo -e "One\nTwo\nThree" | grep '^T'
    Two
    Three

.. note:: The ``^`` is only an anchor if it is the **first character** in a regular expression. Otherwise we would be matching a literal ``^`` character.

As you can see, we matched the lines that start with a ``T``. Let's use the same regexp with ``sed``::

    $ echo -e "One\nTwo\nThree" | sed 's/^T/*/'
    One
    *wo
    *hree

.. note:: In these examples, the regular expressions are matching match as many lines as possible. That is known as **greedy matching**, which is the default behaviour of almost all regex engines. (If the matching process stopped as soon as one match is found that would be described as **lazy**.)

What if you wanted to match a literal ``^`` which is also at the beginning of a line? Then, you would have to use ``^^``.

Matching characters at the end of a line
========================================
Now let's match an ``e`` at the end of the line::

    $ echo -e "One\nTwo\nThree" | sed 's/e$/*/'
    On*
    Two
    Thre*

.. note:: The ``$`` is only an anchor if it is the **last character** in a regular expression. Otherwise we would be matching a literal ``$``.

What if you wanted to match a literal ``$`` which is also at the end of a line? Easy, use ``$$``.

Using both anchors
==================
In case we knew the characters at the beginning and at the end of our string, we could use both anchors simultaneously. For example::

    $ echo -e "One goal\nOne" | sed 's/^One$/*/'
    One goal
    *

In the example above we've matched the ``One`` of the second line, but not the one at the first line.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Regular_expression
