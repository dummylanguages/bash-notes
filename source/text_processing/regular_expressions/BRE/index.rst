#########################
Basic Regular Expressions
#########################

.. toctree::
    :maxdepth: 3
    :hidden:

    anchors
    wildcard
    bracket_expressions
    word_boundaries
    quantifiers
    capturing_groups

Here is the list of metacharacters, also known as special characters, that are used in building regular expressions:

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Regular_expression
