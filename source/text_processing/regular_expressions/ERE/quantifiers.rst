***********
Quantifiers
***********
A quantifier specifies how many times a preceding element is allowed to occur. Even though they seem simple at first glance, the behavior of regex quantifiers is a common source of surprises for beginners.

.. warning:: By default, quantifiers are **greedy**, meaning a quantifier tells the engine to match as many instances of its quantified token or subpattern as possible.

Common quantifiers
==================
The most **common quantifiers** are:

* The **asterisk** (``*``), indicates **zero or more** occurrences of the preceding element.
* The **question mark** (``?``), indicates **zero or one** occurrences of the preceding element.
* The **plus sign** (``+``), indicates **one or more** occurrences of the preceding element.

.. warning:: the ``?`` and ``+`` belong to the **ERE** syntax, but they're usually available (with a preceding slash, i.e., ``\?`` and ``\+`` as a **GNU extensions** in **GNU tools**.

The **asterisk** or **star** works with the character to the inmediate left; for example, ``ab*c`` would match:

* No ``b`` in between the ``a`` and the ``c``. That's ``ac``
* One ``b`` , ``abc``
* Any number of ``b`` characters, like ``abbc``, ``abbbc``, etc.

In the example above, it's important to realize that the **star** refers to the ``b``. But what if we wanted to match any number of ``ab`` characters? Then we'd need to use a **capturing group**::

    $ echo "ab1 abab2 ababab3" | sed 's/\(ab\)*/!!/'
    !!1 abab2 ababab3

In the example above we've used ``\(ab\)*`` to match the first ``ab`` occurrence, which we've replaced with ``!!`` for illustrative purposes. Check what happens when we use the ``g`` flag to match greedily::

    $ echo "ab1 abab2 ababab3" | sed 's/\(ab\)*/!!/g'
    !!1!! !!2!! !!3!!

Sure enough we've matched before the digits the characters ``ab``, ``abab``, and ``ababab``. But why we're getting matches after the digits too? Remember, the ``*`` matches **zero or more** occurrences, so we're matching also where the ``ab`` characters appear **zero** times, i.e., where they don't appear at all.

Matching a specific number of sets
==================================
There is a special pattern you can use to specify the **minimum and maximum** number of repeats. This is done by putting those two numbers between ``\{`` and ``\}``.

.. note:: The **ERE syntax** uses ``{`` and ``}``, no need for the preceding backslashes.

Matching at least ``m`` and not more than ``n`` times
-----------------------------------------------------
For example, to match one or two occurrences of the ``o`` character we would use::

    $ echo "foo is not food" | sed 's/o\{1,2\}/*/g'
    f* is n*t f*d

We are **globally** matching one or two consecutive ``o`` characters. 

Matches the preceding element exactly ``m`` times
-------------------------------------------------
If we wanted to match **two and only two** occurrences of the ``o``, we could use the expression ``o\{2,2\}``::

    $ echo "foo is not food" | sed 's/o\{2,2\}/*/g'
    f* is not f*d
    
.. note:: If you want to match, not a range, but an exact number of some character occurrences, you could use the same number before and after the comma.

But that's silly when we can use just::

    $ echo "foo is not food" | sed 's/o\{2\}/*/g'
    f* is not f*d

Matches the preceding element at least ``m`` times
--------------------------------------------------
Let's say we want to match the ``o`` only when appears a **minimum** of two times::

    $ echo "foo is not food" | sed 's/o\{2,\}/*/g'
    f* is not f*d

Matching not more than ``n`` times
----------------------------------
And if we want to match the ``o`` only when appears a **maximum** of two times::

    $ echo "fooo is not food" | sed 's/o\{,2\}/*/g'
    sed: 1: "s/o\{,2\}/*/g": RE error: invalid repetition count(s)

My version of ``sed`` throws an error when I try to use ``o\{,2\}`` to match a maximum of two ``o`` characters.

Matching words
==============

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Regular_expression
