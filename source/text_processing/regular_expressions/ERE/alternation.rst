***********
Alternation
***********
The **ERE syntax** includes a `vertical bar`_ (commonly known in the Unix world as a **pipe**) that works as a **metacharacter** to separate two alternatives strings that we want to match. For example, imagine we wanted to match any of the words ``fox`` or ``dog``, and replace them with three stars::

    $ echo -e "The quick fox jumps\nover the lazy dog." | sed -E 's/(dog|fox)/***/'
    The quick *** jumps
    over the lazy ***.

As soon as the regex engine finds the word ``dog`` **or** the word ``fox`` it matches it. It works lik a **logical OR**, finding any of the supplied strings, will evaluate to true.

.. note:: Note how we have to use the ``-E`` flag to make ``sed`` to interpret the regex as extended regular expressions.

Use parentheses
===============
It's a good practice to use **parentheses** to define the start and end position for the list of alternatives. That wasn't necessary in the last example, but when regexes become a bit more complex is imperative to do so::

    $ echo -e "Buy-list:\nDog food\nCat food\nBird food." |
    > sed -E 's/Dog|Cat|Bird food/***/'
    Buy-list:
    *** food
    *** food
    ***.

As you can see, without parentheses we are matching the strings:

* ``Dog`` and not ``Dog food`` as we intended.
* ``Cat`` and not ``Cat food`` as we intended.
* ``Bird food``, that's the only one right.

Let's add the parentheses and see what happens::

    $ echo -e "Buy-list:\nDog food.\nCat food.\nBird food." |  sed -E 's/(Dog|Cat|Bird) food/***/'
    Buy-list:
    ***.
    ***.
    ***.

Now we're matching ``Dog food``, ``Cat food``, and ``Bird food``.

.. _`vertical bar`: https://en.wikipedia.org/wiki/Vertical_bar
