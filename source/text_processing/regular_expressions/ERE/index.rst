############################
Extended Regular Expressions
############################

.. toctree::
    :maxdepth: 3
    :hidden:

    quantifiers
    alternation

For the most part, in this section we're gonna be building up, on the already explained BRE syntax; after all, the **E** in **ERE** stands for **extended**. There are several programs that use the extended syntax:

* ``egrep``, or should we say ``grep`` when using the ``-E`` option.
* ``sed``, when using the ``-E`` option.
* Most versions of ``awk``. (Actually AWK uses a **superset** of **ERE**)

The **extended syntax** adds a few features that don't exist in the basic syntax:

* The ``?`` and ``+`` **quantifiers** (although they exist as **GNU extensions** for **BRE** as ``\?`` and ``\+``).
* The **choice operator** (``|``), which also exists as **GNU extensions** for **BRE** as ``\|``.

Apart from the added features, there are some **differences** between the two syntaxes:

* No need to precede ``{`` or ``}`` with backslashes (quantifiers).
* No need to precede ``(`` or ``)`` with backslashes (subexpressions) or the number when **back referencing** text previously matched.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Regular_expression
