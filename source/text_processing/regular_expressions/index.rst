.. _regular expressions:

###################
Regular Expressions
###################

.. toctree::
    :maxdepth: 3
    :hidden:

    BRE/index
    ERE/index
    
Many **programming languages** as well as different Unix/Linux **command line utilities** use some form of **regular expressions**.

.. note:: The `Wikipedia`_ entry for **regular expressions** is a good source to learn some historical background and generalities about the topic.

A regular expression is a **pattern** of characters used for matching text. Each character in a regular expression (that is, each character in the string describing its pattern) is either a **metacharacter**, having a special meaning, or a **regular character** that has a literal meaning. For example,  in the regex ``b.``, the ``b`` is a literal character that matches just the letter ``b``, while the ``.`` is a metacharacter that matches every character (except a newline). Therefore, this regex matches strings such as, ``bx``, ``b%``, or ``b5``. 

.. note:: A **regular expression** is often called a **pattern**.

The `POSIX standard`_ describes two different syntaxes:

1. `Basic Regular Expressions`_, or **BRE** for short.
2. `Extended Regular Expressions`_, known as **ERE**.

Different tools use one or another(or both), for example:

* The **BRE syntax** is used by the ``vi`` editor, ``sed``, and ``grep``.
* The **ERE syntax** used by ``awk``, ``nawk``, ``gawk``, and ``egrep``.

.. note:: Both ``sed`` and ``grep`` have available the ``-E`` option that allows them to interpret regular expressions using the **ERE** syntax.

We should also mention the `Perl Compatible Regular Expressions (PCRE)`_, which is a library writen in **C**, that implements a regular expression engine that uses the same syntax and semantics as `Perl 5`_.


.. _`Wikipedia`: https://en.wikipedia.org/wiki/Regular_expression
.. _`POSIX standard`: https://pubs.opengroup.org/onlinepubs/9699919799/
.. _`Basic Regular Expressions`: https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_03
.. _`Extended Regular Expressions`: https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_04
.. _`Perl Compatible Regular Expressions (PCRE)`: https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions
.. _`Perl 5`: https://www.perl.org
.. _`sed.sf.net`: http://sed.sourceforge.net/
