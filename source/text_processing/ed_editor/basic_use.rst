*********************
Basic interactive use
*********************
We can **invoke** ``ed`` in two different ways::

    ed [-] [-sx] [-p string] [file]
    red [-] [-sx] [-p string] [file]

The ``red`` command is a **restricted version** of the ``ed`` command. Using ``red``, you can edit only the files in the **current directory** or in the ``/tmp`` directory; you cannot use the ``!`` **subcommand**.

Options
=======
In the **BSD** version, the following options are available:

* ``-s``: Suppress diagnostics. This should be used if we're using a script to put our ``ed`` commands.
* ``-x``: Prompt for an encryption key to be used in subsequent reads and writes (Unsupported on **macOS**).
* ``-p string``: The ``-p`` option allows us to set a **prompt** for the **command mode**. The value of ``string`` can be whatever we want to: ``%>``, ``>``, etc.  The prompt may be toggled on and off from inside ``ed`` with the ``P`` command.
* ``file``: The name of the **file** to read.

Creating an empty buffer
========================
If we run::

    $ ed

``ed`` creates an **empty buffer** so we can add content.

Changing modes
==============
``ed`` is a **modal editor**, meaning that it works in two modes: **command mode** and **input mode**. When you launch ``ed``, it starts in **command mode**. To change to **input mode** we have can use two subcommands:

* ``i`` or ``a`` will switch the editor to **input mode**. Since ``ed`` is **line-oriented** there's a difference between these two subcommands:

    * With ``i`` we can start **inserting** before the current line.
    * Whereas ``a`` allows us to keep **appending text** right after the current line.

* Use the ``.`` subcommand on a line by itself to switch back to **command mode**.

For example::

    $ ed
    a
    This is some text,
    to test the ed editor.
    .

In the lines above we've use ``a`` to enable **input mode**, and started adding some text to the buffer. Then we used the ``.`` to get back to **command mode**.

.. warning:: Don't forget to press ``Enter`` after each subcommand; or to add a **newline character** when you're editing in input mode.

Writing the buffer to a file
============================
Once we've satisfied with our edits, we can write the buffer to a file using the ``w`` command. When using this command for the first time, it's necessary to use a **file name**, otherwise we will be prompted with the cryptic ``?``.

.. note:: The message that ``ed`` will produce in case of **error**, or when it wants to make sure the user wishes to quit without saving, is ``?``. By default, **error explanations** are not printed, but if you need them, toggle them with ``H``.

In the example above, if we wanted to save our changes to a file named ``tested.txt``, we would run::

    w tested.txt
    33

As a return value we're getting the number of characters in the file, ``33`` in this case.

Quitting
========
To quit we can use:

* The ``q`` subcommand, which will issue a ``?`` prompting us to save changes. If we don't to ``w`` the changes, we can hit ``q`` to **quit**.
* The ``Q`` subcommand will **quit** regardless of unsaved changes.

Prompt
======
As you can see, ``ed`` subcommands are mostly **single letters**, after a while it may get confusing to discern which **mode** we're in. The ``P`` command allows us to **toggle** the **prompt** on the fly. By default, the prompt is represented by the ``*`` character, but using the ``-p`` option when invoking ``ed``, we can set the prompt to any string we liked::

    $ ed -p '%>' test.txt 
    33
    %>

.. note:: Using a **prompt** is quite useful to indicate what mode we're using. Use ``P`` to toggle it **on** and **off**.

Editing an existing file
========================
To edit a file we just have to pass its **filename** as argument::

    $ ed test.txt
    33

Using the ``p`` command, we **print** the **current address**, which in this case is the last line in the file::

    33
    p
    to test the ed editor.

We can use just **line numbers** to move between lines. For example, to set the first line as the **current address** (``ed`` jargon for the number of the current line):

.. code-block:: text

    1
    This is some text,

.. note:: Instead of using **absolute addresses**, we can also move **up** or **down** with respect to the current address by using ``-`` or ``+`` respectively. For example, ``-1`` will move us one line up, whereas ``+2`` would take us two lines down.

This command print the line as feedback; we could have also typed ``1p``, but when using a single number is not needed, only that the ``p`` command is implicit. That's not the case if we wanted to print a **range** of lines; then we'd have to explicitely use the ``p``:

.. code-block:: text

    1,2p
    This is some text,
    to test the ed editor.

.. note:: Using a **number** sets the **current line** to that number. Any command would affect that line, try the ``n`` command for example.

Using a range can be useful to examine the whole file:

.. code-block:: text

    1,$p
    This is some text,
    to test the ed editor.

The ``$`` represents the address of the **last line**. Actually to see the whole file you can use ``,p``.

Adding some lines
=================
Imagine we want to add some lines to an existing file. Let's start by opening the file using a descriptive prompt::

    $ ed -p '%>' test.txt 
    33
    %>,n
    1       This is some text,
    2       to test the ed editor.
    %>

Note how we're using ``,n`` to print all the lines prefixed with their addresses. Then, to **add a line at the end**, we use ``$a``::

    %>$a
    Last line
    .
    %>,n
    1       This is some text,
    2       to test the ed editor.
    3       Last line

But what if you want to add a line at the very beginning? Use ``0i``::
    
    %>0i
    First line.
    .

Let's check what we have so far:

.. code-block:: text

    %>,n
    1       First line.
    2       This is some text,
    3       to test the ed editor.
    4       Last line
    %>

As you can see, we've **inserted** a line at address ``0``, or in other words, before address ``1``.

Copying and pasting lines
=========================
To copy a line and paste it at some other location, use the ``t`` command. You need to **prefix** ``t`` with the number of the line you want to copy, and use the destination line number as a **postfix**. For example, to copy the last line to the beginning use ``$t0``::

    %>$t0
    %>,n
    1       Last line
    2       First line.
    3       This is some text,
    4       to test the ed editor.
    5       Last line
    %>

We are always pasting the line **after** the number specified in the **postfix**. In this example we are pasting after the ``0`` address, hence the line appears at address ``1``.

Deleting lines
==============
Let's say we want to get rid of the ``Last line``, just use ``$d``::

    %>$d
    %>,n
    1       Last line
    2       First line.
    3       This is some text,
    4       to test the ed editor.

We can also delete a **range of lines**, let's say the first and the second ones:

.. code-block:: text

    %>1,2d
    %>,n
    1       This is some text,
    2       to test the ed editor.

Moving lines
============
To move a line, we use the ``m`` command. This command needs the line we want to move as a **prefix** and the address where we want to move it as a **postfix**. Let's start by adding an extra line::

    %>$a
    ed sucks!
    .
    %>,n
    1       This is some text,
    2       to test the ed editor.
    3       ed sucks!

To move line ``3`` to address ``1``, we have to use ``0`` as a **postfix**. That's because the line is placed **after** the address specified in the postfix; so if we specify ``0``, it will be placed at ``1``, and so on::

    %>3m0
    %>,n
    1       ed sucks!
    2       This is some text,
    3       to test the ed editor.
    %>

.. _`foo`: https://www.foo.org
