#################
The ``ed`` editor
#################

.. toctree::
    :maxdepth: 3
    :hidden:

    basic_use

According to `Wikipedia`_, ``ed`` is a `line editor`_ for Unix and Unix-like operating systems. It was developed in **1969**, and it was included in `Version 1 Unix`_. It remains part of the `POSIX`_ and Open Group standards for Unix-based operating systems, alongside the more sophisticated full-screen editor ``vi``.

.. figure:: /_images/teletype_model_33.jpg
    :scale: 50%

**Line editors** originated in an era when a computer operator typically interacted with a teleprinter (essentially a printer with a keyboard), with no video display, and no ability to move a cursor interactively within a document.

There are several ``ed`` implementations:

* The **BSD version**, POSIX compliant. (That's the one preinstalled in **macOS**)
* `GNU ed`_, which includes several extra, **non-standard**, nice features.

Using the ``ed`` editor may feel a bit awkward, specially today when they're available **full-screen editors** such as **GNU Emacs**, or **Vim**. We're gonna cover it anyways, because being familiar with it may help understand the logic behind other command-line tools that were somehow inspired by it. For example:

* The name of the ``grep`` command comes from the ``ed`` subcommand ``g/re/p``, which allows us to **globally** search for a regular expression (``re``) and print (``p``) the matching lines.
* The **Perl** programming language was influenced by ``sed``, which in turn was based on the scripting features of ``ed``.
* You can trace the lineage of **AWK** to ``sed`` and ``grep``, and through those two programs to ``ed``, the original UNIX line editor.
* If we think about it, the ``ed`` editor has also influenced, at least indirectly the ``vi`` editor. In 1975, the ``em`` editor was written (as in improved version of ``ed``) in order to take advantage of **video terminals**. Right after, Bill Joy modified it to be less demanding on the processor, and ``ex`` was born. ``ex`` was eventually given a full-screen visual interface and became the ``vi`` text editor. If you've used ``vi``, you'll recognize certain ``ed`` patterns.


.. _`Wikipedia`: https://en.wikipedia.org/wiki/Ed_(text_editor)
.. _`line editor`: https://en.wikipedia.org/wiki/Line_editor
.. _`Version 1 Unix`: https://en.wikipedia.org/wiki/Research_Unix
.. _`POSIX`: https://pubs.opengroup.org/onlinepubs/9699919799/
.. _`GNU ed`: https://www.gnu.org/software/ed/manual/ed_manual.html
